/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.imaging.platform.jclient;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import javax.imageio.ImageIO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.ngt.jopenmetaverse.shared.sim.imaging.LoadTGAClass;
import com.ngt.jopenmetaverse.shared.sim.imaging.ManagedImage;
import com.ngt.jopenmetaverse.shared.sim.imaging.platform.jclient.tga.JclientTGADecoder;
import com.ngt.jopenmetaverse.shared.util.FileUtils;
import com.ngt.jopenmetaverse.shared.util.JLogger;
import com.ngt.jopenmetaverse.shared.util.Utils;

public class JclientTGADecoderTests {

	String resourceLocation;
	String outputLocation;
	@Before
	public void setup() throws Exception
	{
		URL propLoc  =  getClass().getClassLoader().getResource("jclient_test_resources_dir.properties");

		Assert.assertNotNull(propLoc);

		String basePath = org.apache.commons.io.FileUtils.readFileToString(new File(propLoc.getPath())).trim();
		outputLocation = basePath + "output";
		File f = new File(outputLocation);
		if(!f.exists())
			f.mkdir();

		resourceLocation = FileUtils.combineFilePath(new File(basePath).getAbsolutePath()
				, "files/images/tga/");
		System.out.println(resourceLocation);
	}
	
	
//	@Test
	public void ExportTGA()
	{
		try
		{
			File[] files = FileUtils.getFileList(resourceLocation,".*\\.tga$" ,true);

			for(File f: files)
			{
				JLogger.debug("Reading from File: " + f.getAbsolutePath());
				BitmapBufferedImageImpl bitmap = new BitmapBufferedImageImpl(JclientTGADecoder.loadImage(new FileInputStream(f)));

				File f1 = new File(resourceLocation + "/" + f.getName() + ".jpg");
				f1.createNewFile();
				FileOutputStream bos = new FileOutputStream(f1);
				ImageIO.write( bitmap.getImage(), "jpg",  bos );
				bos.close();
				Assert.assertTrue(bitmap.getHeight() > 0 && bitmap.getWidth() > 0);
			}
		}
		catch(Exception e)
		{
			Assert.fail(Utils.getExceptionStackTraceAsString(e));			
		}
	}

	@Test
	public void ExportTGAPrecomiledFiles()
	{
		try
		{
			
			File[] files = FileUtils.getFileList(resourceLocation, ".*b6ecaae4-81ea-444a-990f-956d29f6fb32\\.tga$", true);
//			File[] files = FileUtils.getFileList(resourceLocation, ".*\\.tga$", true);

			//			File[] files = FileUtils.getFileList(fileLocation.getPath(), "head_color.tga$", true);

			for(File f: files)
			{
				JLogger.debug("Reading from File: " + f.getAbsolutePath());
				InputStream is = new FileInputStream(f);
				BitmapBufferedImageImpl bitmap = new BitmapBufferedImageImpl(JclientTGADecoder.loadImage(is));
				is.close();
				int width = bitmap.getWidth();
				int height = bitmap.getHeight();
				
				ManagedImage inputImage = new ManagedImage(bitmap);
				byte[] inputImagePixels = inputImage.ExportRaw();

				File[] compiledfiles = FileUtils.getFileList(resourceLocation + "/compiled", f.getName() + ".bin", true);
				Assert.assertTrue("No File or Multiple files exists with name: " + f.getName()  + " on path: " + resourceLocation + "/compiled", 
						compiledfiles.length == 1 );

				byte[] compiledImagePixels = FileUtils.readBytes(compiledfiles[0]);
				//				byte[] compiledImagePixels =  new ManagedImage(new BitmapBufferedImageImpl(TGADecoder2.loadImage(new FileInputStream(f)))).ExportRaw(); 

//				JLogger.debug("inputImagePixels");
//				printPixels(inputImagePixels, width, height);
//				JLogger.debug("compiledImagePixels");
//				printPixels(compiledImagePixels, width, height);

				Assert.assertEquals("No of image pixels differ" , inputImagePixels.length, compiledImagePixels.length);

				//				JLogger.debug(String.format("Compiled Image \n%s"
				//						, Utils.bytesToHexDebugString(compiledImagePixels, "")));

				
//				 // RGBA
//	            int Height = bitmap.getHeight();
//	            int Width = bitmap.getWidth();
//				for (int h = 0; h < Height; h++)
//				{
//					for (int w = 0; w < Width; w++)
//					{
//						int pos = (Height - 1 - h) * Width + w;
//						int srcPos = h * Width + w;
//
//						int origColor = bitmap.getRGB(w, h); 
//						
////						int origColor2 = Utils.ubyteToInt(inputImage.Red[srcPos]) << 16 | 
////								Utils.ubyteToInt(inputImage.Green[srcPos]) << 8 |
////								Utils.ubyteToInt(inputImage.Blue[srcPos]) |
////								Utils.ubyteToInt(inputImage.Alpha[srcPos]) << 24;
//						
//						int newColor = Utils.ubyteToInt(compiledImagePixels[pos * 4 + 0]) << 16 | 
//						Utils.ubyteToInt(compiledImagePixels[pos * 4 + 1]) << 8 |
//						Utils.ubyteToInt(compiledImagePixels[pos * 4 + 2]) |
//						Utils.ubyteToInt(compiledImagePixels[pos * 4 + 3]) << 24;
//												
////						Assert.assertEquals(origColor, origColor2);
//
//						Assert.assertEquals(origColor, newColor);
//						
//						if(origColor != newColor)
//							System.out.println(String.format("X %d Y %d orig color %d New color %d", w, h, origColor, bitmap.getRGB(w, h)));
//					}
//				}
				
				
				int length = 32;
				for(int i = 0; i < compiledImagePixels.length; i+= length)
				{
					int actuallength = Math.min(length, compiledImagePixels.length - i);
					byte[] inputPixelsSubArray = Arrays.copyOfRange(inputImagePixels, i, i + actuallength);
					byte[] compiledPixelsSubArray = Arrays.copyOfRange(compiledImagePixels, i, i + actuallength);

//					JLogger.debug(String.format("Comparing Pixel Index: %d <X= %d Y= %d> original: \n\t%s\n Compiled: \n\t%s\n"
//							,i, (int)(i/(4*width)),  (int)((i/4)%width) 
//							, Utils.bytesToHexDebugString(compiledPixelsSubArray, "")
//							, Utils.bytesToHexDebugString(inputPixelsSubArray, "")
//							));

//					System.out.println(String.format("Comparing Pixel Index: %d <X= %d Y= %d> original: \n\t%s\n Compiled: \n\t%s\n"
//					,i, (int)(i/(4*width)),  (int)((i/4)%width) 
//					, Utils.bytesToHexDebugString(compiledPixelsSubArray, "")
//					, Utils.bytesToHexDebugString(inputPixelsSubArray, "")
//					));
					
					Assert.assertArrayEquals(String.format("Error at Pixel Index: %d <X= %d Y= %d> ",i, (int)(i/(4*width)),  (int)((i/4)%width) ), compiledPixelsSubArray, inputPixelsSubArray);

				}
			}
		}
		catch(Exception e)
		{
			Assert.fail(Utils.getExceptionStackTraceAsString(e));			
		}
	}


	void printPixels(byte[] bytes, int w, int h)
	{
		StringBuilder sb2 = new StringBuilder();
		StringBuilder sb = new StringBuilder();
		int index = 0;
		for(int y=0; y< h; y++)
		{
			for(int x =0; x< w; x++)
			{
				index = x*4*w + y*4;
				byte[] next4bytes = Arrays.copyOfRange(bytes, index, index+4);
				if(Utils.bytesToUInt(next4bytes) > 0)
				{
					sb.append(String.format("<X = %d,  Y = %d, %s>\n", x, y, Utils.bytesToHexDebugString(next4bytes, ""))) ;
				}
				if(Utils.bytesToUInt(next4bytes) > 0)
				{
					sb2.append("1");
				}
				else
				{
					sb2.append("0");
				}
			}
			sb2.append("\n");
		}
		JLogger.debug(sb.toString());
		JLogger.debug(sb2.toString());
	}
}
