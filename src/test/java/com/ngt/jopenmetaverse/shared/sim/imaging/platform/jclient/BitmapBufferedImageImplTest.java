/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.imaging.platform.jclient;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

import com.ngt.jopenmetaverse.shared.sim.imaging.platform.jclient.BitmapBufferedImageImpl;
import com.ngt.jopenmetaverse.shared.util.FileUtils;

public class BitmapBufferedImageImplTest {
	String resourceLocation;
	String outputLocation;
	@Before
	public void setup() throws Exception
	{
		URL propLoc  =  getClass().getClassLoader().getResource("jclient_test_resources_dir.properties");

		Assert.assertNotNull(propLoc);

		String basePath = org.apache.commons.io.FileUtils.readFileToString(new File(propLoc.getPath())).trim();
		outputLocation = basePath + "output";
		File f = new File(outputLocation);
		if(!f.exists())
			f.mkdir();

		resourceLocation = FileUtils.combineFilePath(new File(basePath).getAbsolutePath()
				, "files/images/jpg/");
		System.out.println(resourceLocation);
	}

	@Test
	public void cloneRotateAndFlipTests() throws Exception
	{
		try{
			File[] files = FileUtils.getFileList(resourceLocation, true);
			BitmapBufferedImageImpl abbi;
			for(File f: files)
			{

				BufferedImage bitmap = ImageIO.read(new File(f.getAbsolutePath()));
				abbi = new BitmapBufferedImageImpl(bitmap);
				cloneResizeAndSave(abbi, FileUtils.combineFilePath(outputLocation, f.getName()));
				cloneTileAndSave(abbi, 5, FileUtils.combineFilePath(outputLocation, f.getName()));
				cloneTileAndSave(abbi, 5, FileUtils.combineFilePath(outputLocation, f.getName()));
				cloneRotateAndFlip(abbi, Math.toRadians(0), true, false, 
						FileUtils.combineFilePath(outputLocation, f.getName()));
				
				createImageWithSolidColor(abbi, 256, 256, 255, 164, 136, 117, 
						FileUtils.combineFilePath(outputLocation, f.getName()));
				
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	private void cloneResizeAndSave(BitmapBufferedImageImpl abbi, String outputFile) throws IOException
	{
		BufferedImage resizedbitmap = ((BitmapBufferedImageImpl)abbi.cloneAndResize(abbi.getWidth()*5, abbi.getHeight()*5)).getImage();

		FileOutputStream os = new FileOutputStream(new File(outputFile + ".jpeg"));
		ImageIO.write(resizedbitmap, "jpeg", os);
		os.flush();
		os.close();
	}

	private void cloneTileAndSave(BitmapBufferedImageImpl abbi, int tile, String outputFile) throws IOException
	{
		BufferedImage resizedbitmap = ((BitmapBufferedImageImpl)abbi.cloneAndTile(tile)).getImage();

		FileOutputStream os = new FileOutputStream(new File(outputFile + "tile" + tile + ".jpeg"));
		ImageIO.write(resizedbitmap, "jpeg", os);
		os.flush();
		os.close();
	}
	
	private void cloneRotateAndFlip(BitmapBufferedImageImpl abbi, double radians, boolean flipx, boolean flipy, String outputFile) throws IOException
	{
		BufferedImage resizedbitmap = ((BitmapBufferedImageImpl)abbi.cloneRotateAndFlip(radians, flipx, flipy)).getImage();

		FileOutputStream os = new FileOutputStream(new File(outputFile + radians + flipx+ flipy + ".jpeg"));
		ImageIO.write(resizedbitmap, "jpeg", os);
		os.flush();
		os.close();
	}
	
	private void createImageWithSolidColor(BitmapBufferedImageImpl abbi, int width, int height, int r, int g, int b, int a, String outputFile) throws IOException
	{
		BufferedImage resizedbitmap = ((BitmapBufferedImageImpl)abbi.createImageWithSolidColor(width, height, r,  g,  b, a)).getImage();

		FileOutputStream os = new FileOutputStream(new File(outputFile + width + height+ r + g + b + a + "solid.jpeg"));
		ImageIO.write(resizedbitmap, "jpeg", os);
		os.flush();
		os.close();
	}
}
