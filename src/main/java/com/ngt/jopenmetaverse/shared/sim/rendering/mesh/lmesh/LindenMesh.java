/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering.mesh.lmesh;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.BitPack;
import com.ngt.jopenmetaverse.shared.types.Vector2;
import com.ngt.jopenmetaverse.shared.types.Vector3;
import com.ngt.jopenmetaverse.shared.util.FileUtils;
import com.ngt.jopenmetaverse.shared.util.Utils;

public class LindenMesh
{
    final static String MESH_HEADER = "Linden Binary Mesh 1.0";
    final static String MORPH_FOOTER = "End Morphs";

    public float MinPixelWidth;

    public String getName() {return _name;}
    public String getHeader() {return _header;}
    public boolean getHasWeights() {return _hasWeights;}
    public boolean getHasDetailTexCoords() {return _hasDetailTexCoords;}
    public Vector3 getPosition() {return _position;}
    public Vector3 getRotationAngles() {return _rotationAngles;}
    //public byte RotationOrder
    public Vector3 getScale() {return _scale;}
    //ushort
    public int getNumVertices() {return _numVertices;}
    public Vertex[] getVertices() {return _vertices;}
    //ushort
    public int getNumFaces() {return _numFaces;}
    public Face[] getFaces() {return _faces;}
    //ushort
    public int getNumSkinJoints() {return _numSkinJoints;}
    public String[] getSkinJoints() {return _skinJoints;}
    public Morph[] getMorphs() {return _morphs;}
    public int getNumRemaps() {return _numRemaps;}
    public VertexRemap[] getVertexRemaps() {return _vertexRemaps;}
    public SortedMap<Integer, LODMesh> getLODMeshes() {return _lodMeshes;}

    protected String _name;
    protected String _header;
    protected boolean _hasWeights;
    protected boolean _hasDetailTexCoords;
    protected Vector3 _position;
    protected Vector3 _rotationAngles;
    protected byte _rotationOrder;
    protected Vector3 _scale;
    //ushort
    protected int _numVertices;
    protected Vertex[] _vertices;
    //ushort
    protected int _numFaces;
    protected Face[] _faces;
    //ushort
    protected int _numSkinJoints;
    protected String[] _skinJoints;
    protected Morph[] _morphs;
    protected int _numRemaps;
    protected VertexRemap[] _vertexRemaps;
    protected SortedMap<Integer, LODMesh> _lodMeshes;

    public LindenMesh(String name)
    {
        _name = name;
        _lodMeshes = new TreeMap<Integer, LODMesh>();
    }

    public void LoadMesh(String filename) throws IOException
    {
        byte[] buffer = FileUtils.readBytes(new File(filename));
        BitPack input = new BitPack(buffer, 0);

        _header = Utils.TrimAt0(input.UnpackString(24));
        if (!_header.equalsIgnoreCase(MESH_HEADER))
            throw new IOException("Unrecognized mesh format");

        // Populate base mesh variables
        _hasWeights = (input.UnpackByte() != 0);
        _hasDetailTexCoords = (input.UnpackByte() != 0);
        _position = new Vector3(input.UnpackFloat(), input.UnpackFloat(), input.UnpackFloat());
        _rotationAngles = new Vector3(input.UnpackFloat(), input.UnpackFloat(), input.UnpackFloat());
        _rotationOrder = input.UnpackByte();
        _scale = new Vector3(input.UnpackFloat(), input.UnpackFloat(), input.UnpackFloat());
        _numVertices = input.UnpackUShort();

        // Populate the vertex array
        _vertices = new Vertex[_numVertices];

        for (int i = 0; i < _numVertices; i++)
            _vertices[i].Coord = new Vector3(input.UnpackFloat(), input.UnpackFloat(), input.UnpackFloat());

        for (int i = 0; i < _numVertices; i++)
            _vertices[i].Normal = new Vector3(input.UnpackFloat(), input.UnpackFloat(), input.UnpackFloat());

        for (int i = 0; i < _numVertices; i++)
            _vertices[i].BiNormal = new Vector3(input.UnpackFloat(), input.UnpackFloat(), input.UnpackFloat());

        for (int i = 0; i < _numVertices; i++)
            _vertices[i].TexCoord = new Vector2(input.UnpackFloat(), input.UnpackFloat());

        if (_hasDetailTexCoords)
        {
            for (int i = 0; i < _numVertices; i++)
                _vertices[i].DetailTexCoord = new Vector2(input.UnpackFloat(), input.UnpackFloat());
        }

        if (_hasWeights)
        {
            for (int i = 0; i < _numVertices; i++)
                _vertices[i].Weight = input.UnpackFloat();
        }

        _numFaces = input.UnpackUShort();

        _faces = new Face[_numFaces];

        for (int i = 0; i < _numFaces; i++)
            _faces[i].Indices = new short[] { input.UnpackShort(), input.UnpackShort(), input.UnpackShort() };

        if (_hasWeights)
        {
            _numSkinJoints = input.UnpackUShort();
            _skinJoints = new String[_numSkinJoints];

            for (int i = 0; i < _numSkinJoints; i++)
            {
                _skinJoints[i] = Utils.TrimAt0(input.UnpackString(64));
            }
        }
        else
        {
            _numSkinJoints = 0;
            _skinJoints = new String[0];
        }

        // Grab morphs
        List<Morph> morphs = new ArrayList<Morph>();
        String morphName = Utils.TrimAt0(input.UnpackString(64));

        while (!morphName.equalsIgnoreCase(MORPH_FOOTER))
        {
            if (input.getBytePos() + 48 >= input.Data.length) throw new IOException("Encountered end of file while parsing morphs");

            Morph morph = new Morph();
            morph.Name = morphName;
            morph.NumVertices = input.UnpackInt();
            morph.Vertices = new MorphVertex[morph.NumVertices];

            for (int i = 0; i < morph.NumVertices; i++)
            {
                morph.Vertices[i].VertexIndex = input.UnpackUInt();
                morph.Vertices[i].Coord = new Vector3(input.UnpackFloat(), input.UnpackFloat(), input.UnpackFloat());
                morph.Vertices[i].Normal = new Vector3(input.UnpackFloat(), input.UnpackFloat(), input.UnpackFloat());
                morph.Vertices[i].BiNormal = new Vector3(input.UnpackFloat(), input.UnpackFloat(), input.UnpackFloat());
                morph.Vertices[i].TexCoord = new Vector2(input.UnpackFloat(), input.UnpackFloat());
            }

            morphs.add(morph);

            // Grab the next name
            morphName = Utils.TrimAt0(input.UnpackString(64));
        }

        _morphs = morphs.toArray(new Morph[0]);

        // Check if there are remaps or if we're at the end of the file
        if (input.getBytePos() < input.Data.length - 1)
        {
            _numRemaps = input.UnpackInt();
            _vertexRemaps = new VertexRemap[_numRemaps];

            for (int i = 0; i < _numRemaps; i++)
            {
                _vertexRemaps[i].RemapSource = input.UnpackInt();
                _vertexRemaps[i].RemapDestination = input.UnpackInt();
            }
        }
        else
        {
            _numRemaps = 0;
            _vertexRemaps = new VertexRemap[0];
        }
    }

    public void LoadLODMesh(int level, String filename) throws IOException
    {
        LODMesh lod = new LODMesh();
        lod.LoadMesh(filename);
        _lodMeshes.put(level, lod);
    }
}
