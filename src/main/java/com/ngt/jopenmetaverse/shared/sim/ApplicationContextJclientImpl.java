/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim;

import com.ngt.jopenmetaverse.shared.sim.imaging.BitmapFactory;
import com.ngt.jopenmetaverse.shared.sim.imaging.IBitmapFactory;
import com.ngt.jopenmetaverse.shared.sim.imaging.IOpenJPEG;
import com.ngt.jopenmetaverse.shared.sim.imaging.ITGAImageReader;
import com.ngt.jopenmetaverse.shared.sim.imaging.OpenJPEGFactory;
import com.ngt.jopenmetaverse.shared.sim.imaging.TGAImageReaderFactory;
import com.ngt.jopenmetaverse.shared.sim.imaging.BitmapFactory.BitmapFactoryMachine;
import com.ngt.jopenmetaverse.shared.sim.imaging.platform.jclient.BitmapFactoryImpl;
import com.ngt.jopenmetaverse.shared.sim.imaging.platform.jclient.TGAImageReaderImpl;
import com.ngt.jopenmetaverse.shared.sim.imaging.platform.jclient.OpenJPEGFactoryImpl;

public class ApplicationContextJclientImpl
{
//	static {
//	IIORegistry registry = IIORegistry.getDefaultInstance();
//	registry.registerServiceProvider(new  com.ngt.jopenmetaverse.shared.sim.imaging.platform.jclient.tga.TGAImageReaderSpi());
//	
//}
	protected IBitmapFactory bitmapFactory;
	
	public IBitmapFactory getBitmapFactory() {
		return bitmapFactory;
	}

	public void setBitmapFactory(IBitmapFactory bitmapFactory) {
		this.bitmapFactory = bitmapFactory;
	}
	
	public void init()
	{	
		if(bitmapFactory==null) 
			bitmapFactory = new BitmapFactoryImpl();
		setupBitmapFactory();
		setupTGAImageReaderFactory();
		setupOpenJPEGFactory();
	}
	
	protected void setupBitmapFactory()
	{
		BitmapFactoryMachine bitmapFactoryMachine = new BitmapFactoryMachine()
		{
			@Override
			public IBitmapFactory getInstance() {
				return bitmapFactory;
			}
		};
		
		BitmapFactory.setBitmapFactoryMachine(bitmapFactoryMachine);
	}
	
	private static void setupTGAImageReaderFactory()
	{
		TGAImageReaderFactory.TGAImageReaderMachine machine = new TGAImageReaderFactory.TGAImageReaderMachine()
		{
			@Override
			public ITGAImageReader createInstance() {
				return new TGAImageReaderImpl();
			}
		};
		TGAImageReaderFactory.setTGAImageReaderMachine(machine);
	}
	
	private static void setupOpenJPEGFactory()
	{
		OpenJPEGFactory.OpenJPEGMachine machine = new OpenJPEGFactory.OpenJPEGMachine()
		{
			@Override
			public IOpenJPEG getInstance() {
				return OpenJPEGFactoryImpl.getIntance();
			}
		};
		OpenJPEGFactory.setOpenJPEGMachine(machine);
	}
}
