/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering.platform.jclient;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import com.ngt.jopenmetaverse.shared.sim.GridClient;
import com.ngt.jopenmetaverse.shared.sim.Settings;
import com.ngt.jopenmetaverse.shared.sim.Simulator.SimStats;
import com.ngt.jopenmetaverse.shared.sim.rendering.Camera;
import com.ngt.jopenmetaverse.shared.sim.rendering.Rendering;
import com.ngt.jopenmetaverse.shared.sim.rendering.RenderingContext;
import com.ngt.jopenmetaverse.shared.sim.rendering.ShaderProgram;
import com.ngt.jopenmetaverse.shared.sim.rendering.TextureManagerR;
import com.ngt.jopenmetaverse.shared.sim.rendering.math.FloatMatrix;
import com.ngt.jopenmetaverse.shared.sim.rendering.math.FloatMatrix4x4;
import com.ngt.jopenmetaverse.shared.sim.rendering.math.MatrixUtil;
import com.ngt.jopenmetaverse.shared.sim.rendering.math.Vector3f;
import com.ngt.jopenmetaverse.shared.sim.rendering.math.Vectorf;
import com.ngt.jopenmetaverse.shared.types.Quaternion;
import com.ngt.jopenmetaverse.shared.types.Vector3;


public class DefaultRenderingTerrainAndPrimitiveWithCameraWindow extends GLApp{
	//private Sphere cube ;

	private RenderingContext renderingContext;
	private GridClient client;
	SimStats simStats;

	private ShaderProgram shaderProgram;
	private int vertexPositionAttribute;
	private int textureCoordAttribute;
	private int vertexNormalAttribute;

	private FloatBuffer resultingMatrixBuffer;
	private FloatBuffer normalMatrixBuffer;


	private int projectionMatrixUniform;
	private int textureUniform;	
	private int uAmbientColorUniform;
	private int uLightingDirectionUniform;
	private int uDirectionalColorUniform;
	private int uUseLightingUniform;
	private int normalMatrixUniform;

	private int angleX = 0;
	private int angleY = 0;
	private int angleZ = 0;
	
	private Camera camera;
//	private int currentTexture = 0;
		
	//Input Values for Lighting Effect
	int uUseLighting = 1;
	private Vectorf lightingDirection = new Vector3f(-1, -1, -1);
	private float directionalColorRed = 0.8f;
	private float directionalColorGreen = 0.8f;
	private float directionalColorBlue = 0.8f;
	private float ambientColorRed = 0.5f;
	private float ambientColorGreen = 0.5f;
	private float ambientColorBlue = 0.5f;

	private FloatMatrix perspectiveMatrix;
	private FloatMatrix translationMatrix;
	private FloatMatrix rotationMatrix;
	private FloatMatrix resultingMatrix;

	Rendering rendering;
	TextureManagerR textureManager;

	/*
	 * Temporary and Misc 
	 */
	IntBuffer tmpIntBuffer;

	public DefaultRenderingTerrainAndPrimitiveWithCameraWindow(GridClient client)
	{
		this.client = client;
		this.simStats = client.network.getCurrentSim().Stats;
	}


	/**
	 * Initialize the scene.  Called by GLApp.run().  For now the default
	 * settings will be fine, so no code here.
	 * @throws Exception 
	 */
	public void setup() throws Exception
	{
		try{
			System.out.println("Setup Initialized..");

		GL11.glEnableClientState(GL11.GL_VERTEX_ARRAY);
		GL11.glEnableClientState(GL11.GL_NORMAL_ARRAY);
		GL11.glEnableClientState(GL11.GL_COLOR_ARRAY);
		GL11.glEnableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
		//    	initParams(); //done by initGL
		initCamera();
		initShaders();
		initBuffers();
		initTextures();
		textureManager = new TextureManagerR(client);
		textureManager.start();
		renderingContext =  new  RenderingContext(textureManager,
				shaderProgram, projectionMatrixUniform,
				textureUniform, uAmbientColorUniform,
				uLightingDirectionUniform, uDirectionalColorUniform,
				uUseLightingUniform, normalMatrixUniform,
				vertexPositionAttribute, textureCoordAttribute,
				vertexNormalAttribute);
		
		rendering = new Rendering(client, camera, renderingContext);
		
		System.out.println("Setup Finished...");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw(e);
		}
		
	}
	
	/**
	 * Render one frame.  Called by GLApp.run().
	 */
	public void draw() {
		System.out.println("Drawing ...");

		// Clear screen and depth buffer
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
		
		float[] resultingMatrixData = updateView();
		resultingMatrixBuffer.position(0);
		resultingMatrixBuffer.put(resultingMatrixData);
		resultingMatrixBuffer.position(0);


		// Set the lighting related uniforms
		GL20.glUniform1i(uUseLightingUniform, uUseLighting);
		Vectorf adjustedLightDirection = lightingDirection.toUnitVector()
				.multiply(-1);
		float[] flatLightDirection = adjustedLightDirection.toArray();
		GL20.glUniform3f(uLightingDirectionUniform, flatLightDirection[0], flatLightDirection[1],
				flatLightDirection[2]);		
		GL20.glUniform3f(uAmbientColorUniform, ambientColorRed, ambientColorGreen, ambientColorBlue);		
		GL20.glUniform3f(uDirectionalColorUniform, directionalColorRed, directionalColorGreen, directionalColorBlue);	

		//Normal matrix to adjust the directional light depending upon rotation
		//TODO need to handle the rotation
		FloatMatrix4x4 normalMatrix = ((FloatMatrix4x4)rotationMatrix).inverse();
		normalMatrix = normalMatrix.transpose();
		float[] normalMatrixData = normalMatrix.getColumnWiseFlatData();
		normalMatrixBuffer.position(0);
		normalMatrixBuffer.put(normalMatrixData);
		normalMatrixBuffer.position(0);

		GL20.glUniformMatrix4(normalMatrixUniform, false, normalMatrixBuffer);
		checkErrors("Drawing.Initializing NormalMatrixUniform");

		renderingContext.setResultingMatrixBuffer(resultingMatrixBuffer);
		renderingContext.setResultingMatrix(resultingMatrix);
		renderingContext.setRotationMatrix(rotationMatrix);
		renderingContext.setTranslationMatrix(translationMatrix);
		renderingContext.setPerspectiveMatrix(perspectiveMatrix);
		
			try {
				rendering.render(false);
			} catch (Exception e) {
				e.printStackTrace();
			}
		
//		PlatformUtils.sleep(10000);
	}

	public float[]  updateView()
	{
		UpdateCamera();
		camera.Step(1);
		perspectiveMatrix = MatrixUtil.createPerspectiveMatrix((int)(45.0f * camera.Zoom), getWidth()/getHeight(), 0.1f, 1000f);
//		translationMatrix = MatrixUtil.createTranslationMatrix(translateX, translateY, translateZ);
		translationMatrix = MatrixUtil.createTranslationMatrix(-1*camera.RenderFocalPoint.X, -1*camera.RenderFocalPoint.Y, 
				-1*camera.RenderFocalPoint.Z);
		rotationMatrix = MatrixUtil.createRotationMatrix(angleX, angleY, angleZ);
		resultingMatrix = perspectiveMatrix.multiply(camera.createLookAtMatrix());
		
//		float[][] f = camera.createLookAtMatrix().transpose().getData();
//		Matrix4 m = new Matrix4(f[0][0], f[0][1],f[0][2],f[0][3],
//				f[1][0], f[1][1],f[1][2],f[1][3],
//				f[2][0], f[2][1],f[2][2],f[2][3],
//				f[3][0], f[3][1],f[3][2],f[3][3]);
				
		float[] resultingMatrixData = resultingMatrix.getColumnWiseFlatData(); 
		return resultingMatrixData;
	}

	public float getDrawDistance()
	{
		return 1000;
	}
	
    public void UpdateCamera()
    {
        if (client != null)
        {
            client.self.Movement.Camera.LookAt(camera.getPosition(), camera.getFocalPoint());
            client.self.Movement.Camera.Far = getDrawDistance(); 
            camera.Far = getDrawDistance();
        }
    }
	
	@Override
	public void keyUp(int keycode) {

		client.self.Movement.setAtPos(false);
		client.self.Movement.setAtNeg(false);
		client.self.Movement.setTurnLeft(false);
		client.self.Movement.setTurnRight(false);
	}
    
    
	@Override
	public void keyDown(int keycode) {
		System.out.println("KeyDown");
		int time = 1;
		
			client.self.Movement.setAtPos(keycode == Keyboard.KEY_UP);
			client.self.Movement.setAtNeg(keycode == Keyboard.KEY_DOWN);
			client.self.Movement.setTurnLeft(keycode == Keyboard.KEY_RIGHT);
			client.self.Movement.setTurnRight(keycode == Keyboard.KEY_LEFT);
		
        //Don't send the nudge pos flags, we don't need them
        client.self.Movement.setNudgeUpPos(false);
        client.self.Movement.setNudgeUpNeg(false);
        client.self.Movement.setUpPos(false);
        client.self.Movement.setUpNeg(false);
		
		
//		if(keycode == Keyboard.KEY_UP)
//			camera.MoveToTarget(1);
//		else if(keycode == Keyboard.KEY_DOWN)
//			camera.MoveToTarget(-1);
//
//		
//		else if(keycode == Keyboard.KEY_RIGHT)
//			camera.Rotate(1, true);
//
//		else if(keycode == Keyboard.KEY_LEFT)
//			camera.Rotate(-1, true);
			
		
        if (client.self.Movement.getTurnLeft())
        {
            client.self.Movement.BodyRotation = Quaternion.multiply(client.self.Movement.BodyRotation, Quaternion.createFromAxisAngle(Vector3.UnitZ, time));
        }
        else if (client.self.Movement.getTurnRight())
        {
            client.self.Movement.BodyRotation = Quaternion.multiply(client.self.Movement.BodyRotation, Quaternion.createFromAxisAngle(Vector3.UnitZ, -time));
        }
		
		
//        Vector3 camPos = myself.RenderPosition + new Vector3(-4, 0, 1) * client.self.Movement.BodyRotation;
//        Camera.Position = camPos;
//        Camera.FocalPoint = myself.RenderPosition + new Vector3(5, 0, 0) * client.self.Movement.BodyRotation;
		
        

        Vector3 camPos = Vector3.add(client.self.getSimPosition(), Vector3.multiply(new Vector3(-4, 0, 1), client.self.Movement.BodyRotation));
        camera.setPosition(camPos);
        camera.setFocalPoint(Vector3.add(client.self.getSimPosition(), Vector3.multiply(new Vector3(5, 0, 0), client.self.Movement.BodyRotation)));
        
        client.self.Movement.SendUpdate(false);
//		
//		else if(keycode == Keyboard.KEY_W)
//			translateY += 1;
//		else if(keycode == Keyboard.KEY_S)
//			translateY -= 1;
//
//		else if(keycode == Keyboard.KEY_E)
//			angleX += 5;
//		else if(keycode == Keyboard.KEY_D)
//			angleX -= 5;
//
//		else if(keycode == Keyboard.KEY_Q)
//			angleZ += 5;
//		else if(keycode == Keyboard.KEY_A)
//			angleZ -= 5;
//
//		else if(keycode == Keyboard.KEY_R)
//			translateX += 1;
//		else if(keycode == Keyboard.KEY_F)
//			translateX -= 1; 
	}


	/**
	 * Creates the ShaderProgram used by the example to render.
	 * @throws Exception 
	 */
	 private void initShaders() throws Exception {

		 shaderProgram = new ShaderProgram();
		 boolean shaderLoadingStatus = shaderProgram.Load(new String[] {Settings.RESOURCE_DIR + "/shaders/vertex-shader-3.vert"
				 , Settings.RESOURCE_DIR + "/shaders/fragment-shader-3.frag"}, 
				 new String[] {"vertexPosition", "texPosition", "aVertexNormal"});

		 if(!shaderLoadingStatus)
			 throw new Exception("Shader could not be loaded");

		 shaderProgram.Start();

		 vertexPositionAttribute = shaderProgram.getAttribLocation("vertexPosition");
		 GL20.glEnableVertexAttribArray(vertexPositionAttribute);

		 textureCoordAttribute = shaderProgram.getAttribLocation( "texPosition");
		 GL20.glEnableVertexAttribArray(textureCoordAttribute);

		 vertexNormalAttribute = shaderProgram.getAttribLocation( "aVertexNormal");
		 GL20.glEnableVertexAttribArray(vertexNormalAttribute);


		 // get the position of the projectionMatrix uniform.
		 projectionMatrixUniform = shaderProgram.getUniformLocation("projectionMatrix");

		 // get the position of the tex uniform.
		 textureUniform = shaderProgram.getUniformLocation( "tex");

		 uUseLightingUniform = shaderProgram.getUniformLocation( "uUseLighting");
		 uAmbientColorUniform = shaderProgram.getUniformLocation( "uAmbientColor");
		 uLightingDirectionUniform = shaderProgram.getUniformLocation( "uLightingDirection");
		 uDirectionalColorUniform = shaderProgram.getUniformLocation( "uDirectionalColor");

		 normalMatrixUniform = shaderProgram.getUniformLocation( "normalMatrix");
		 checkErrors("Getting Shaders UniformLocation ");


	 }

	 /**
	  * Initializes the texture of this example.
	  * @throws Exception 
	  */
	 private void initTextures() throws Exception {
	 }

	 /**
	  * Initializes the buffers for vertex coordinates, normals and texture
	  * coordinates.
	  */
	 private void initBuffers() {
		 tmpIntBuffer = ByteBuffer
				 .allocateDirect(1*4).order(ByteOrder.nativeOrder()).asIntBuffer();

		 float[] resultingMatrixData = updateView();
		 resultingMatrixBuffer = ByteBuffer.allocateDirect(resultingMatrixData.length*4).order(ByteOrder.nativeOrder()).asFloatBuffer();
		 resultingMatrixBuffer.put(resultingMatrixData);
		 resultingMatrixBuffer.position(0);

		 FloatMatrix4x4 normalMatrix = ((FloatMatrix4x4)rotationMatrix).inverse();
		 normalMatrix = normalMatrix.transpose();
		 float[] normalMatrixData = normalMatrix.getColumnWiseFlatData();
		 normalMatrixBuffer = ByteBuffer.allocateDirect(normalMatrixData.length*4).order(ByteOrder.nativeOrder()).asFloatBuffer();
		 normalMatrixBuffer.put(normalMatrixData);
		 normalMatrixBuffer.position(0);

		 checkErrors("After Initializing BUffers");
	 }

	public void initCamera()
	{
			this.camera = new Camera();
	        Vector3 camPos = Vector3.add(client.self.getSimPosition(), 
	        		Vector3.multiply(new Vector3(-4, 0, 1), client.self.Movement.BodyRotation));
	        
	        camera.setPosition(camPos);
			System.out.println(String.format("Initial Camera Position %s Sim Pos %s ", camPos.toString(), client.self.getSimPosition().toString()));
	        camera.setFocalPoint(Vector3.add(client.self.getSimPosition(), Vector3.multiply(new Vector3(5, 0, 0), client.self.Movement.BodyRotation)));
	        camera.Zoom = 1.0f;
	        //TODO need to set it following properly. Actually it means upto how far objects should be rendered
	        camera.Far = 100;
	}
	 
	 
	 private void checkErrors(String locaton) {
		 int error = GL11.glGetError();
		 if (error != GL11.GL_NO_ERROR) {
			 String message = "@ " + locaton + " OpenGL Error: " + error;
			 switch(error)
			 {
			 case GL11.GL_INVALID_OPERATION:
			 {
				 message += " INVALID_OPERATION";
				 break;
			 }
			 case GL11.GL_INVALID_ENUM:
			 {
				 message += " INVALID_ENUM";
				 break;
			 }
			 case GL11.GL_INVALID_VALUE:
			 {
				 message += " INVALID_VALUE";
				 break;
			 }
			 case GL11.GL_OUT_OF_MEMORY:
			 {
				 message += " OUT_OF_MEMORY";
				 break;
			 }
			 }			
			 throw new RuntimeException(message);
		 }
	 }
}