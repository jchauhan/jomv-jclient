/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering;

import com.ngt.jopenmetaverse.shared.sim.rendering.math.FloatMatrix4x4;
import com.ngt.jopenmetaverse.shared.types.Quaternion;
import com.ngt.jopenmetaverse.shared.types.Vector3;

public  class Math3D
    {
        // Column-major:
        // |  0  4  8 12 |
        // |  1  5  9 13 |
        // |  2  6 10 14 |
        // |  3  7 11 15 |

        public static float[] CreateTranslationMatrix(Vector3 v)
        {
            float[] mat = new float[16];

            mat[12] = v.X;
            mat[13] = v.Y;
            mat[14] = v.Z;
            mat[0] = mat[5] = mat[10] = mat[15] = 1;

            return mat;
        }

        /*
         * @return 
         * 	Return matrix in Column major format
         */
        public static float[] CreateRotationMatrix(Quaternion q)
        {
            float[] mat = new float[16];

            // Transpose the quaternion (don't ask me why)
//            q = new Quaternion(q.W, q.X, q.Y, q.Z);
            q = new Quaternion(q);
            q.X = q.X * -1f;
            q.Y = q.Y * -1f;
            q.Z = q.Z * -1f;

            float x2 = q.X + q.X;
            float y2 = q.Y + q.Y;
            float z2 = q.Z + q.Z;
            float xx = q.X * x2;
            float xy = q.X * y2;
            float xz = q.X * z2;
            float yy = q.Y * y2;
            float yz = q.Y * z2;
            float zz = q.Z * z2;
            float wx = q.W * x2;
            float wy = q.W * y2;
            float wz = q.W * z2;

            mat[0] = 1.0f - (yy + zz);
            mat[1] = xy - wz;
            mat[2] = xz + wy;
            mat[3] = 0.0f;

            mat[4] = xy + wz;
            mat[5] = 1.0f - (xx + zz);
            mat[6] = yz - wx;
            mat[7] = 0.0f;

            mat[8] = xz - wy;
            mat[9] = yz + wx;
            mat[10] = 1.0f - (xx + yy);
            mat[11] = 0.0f;

            mat[12] = 0.0f;
            mat[13] = 0.0f;
            mat[14] = 0.0f;
            mat[15] = 1.0f;
            
            
            return mat;
        }

        /*
         * @return 
         * 	Return matrix in Column major format
         */
        public static float[] CreateSRTMatrix(Vector3 scale, Quaternion q, Vector3 pos)
        {
            float[] mat = new float[16];

            // Transpose the quaternion (don't ask me why)
//            q = new Quaternion(q.W, q.X, q.Y, q.Z);
            q = new Quaternion(q);
            q.X = q.X * -1f;
            q.Y = q.Y * -1f;
            q.Z = q.Z * -1f;

            float x2 = q.X + q.X;
            float y2 = q.Y + q.Y;
            float z2 = q.Z + q.Z;
            float xx = q.X * x2;
            float xy = q.X * y2;
            float xz = q.X * z2;
            float yy = q.Y * y2;
            float yz = q.Y * z2;
            float zz = q.Z * z2;
            float wx = q.W * x2;
            float wy = q.W * y2;
            float wz = q.W * z2;

            mat[0] = (1.0f - (yy + zz)) * scale.X;
            mat[1] = (xy - wz) * scale.X;
            mat[2] = (xz + wy) * scale.X;
            mat[3] = 0.0f;

            mat[4] = (xy + wz) * scale.Y;
            mat[5] = (1.0f - (xx + zz)) * scale.Y;
            mat[6] = (yz - wx) * scale.Y;
            mat[7] = 0.0f;

            mat[8] = (xz - wy) * scale.Z;
            mat[9] = (yz + wx) * scale.Z;
            mat[10] = (1.0f - (xx + yy)) * scale.Z;
            mat[11] = 0.0f;

            //Positional parts
            mat[12] = pos.X;
            mat[13] = pos.Y;
            mat[14] = pos.Z;
            mat[15] = 1.0f;

            return mat;
        }


        public static float[] CreateScaleMatrix(Vector3 v)
        {
            float[] mat = new float[16];

            mat[0] = v.X;
            mat[5] = v.Y;
            mat[10] = v.Z;
            mat[15] = 1;

            return mat;
        }

        public static float[] Lerp(float[] matrix1, float[] matrix2, float amount)
        {

            float[] lerp = new float[16];
            //Probably not doing this as a loop is cheaper(unrolling)
            //also for performance we probably should not create new objects
            // but meh.
            for (int x = 0; x < 16; x++)
            {
                lerp[x] = matrix1[x] + ((matrix2[x] - matrix1[x]) * amount);
            }

            return lerp;
        }


        public static boolean GluProject(org.lwjgl.util.vector.Vector3f objPos, org.lwjgl.util.vector.Matrix4f modelMatrix, 
        		org.lwjgl.util.vector.Matrix4f projMatrix, int[] viewport, org.lwjgl.util.vector.Vector3f[] screenPos)
        {
        	org.lwjgl.util.vector.Vector4f _in = new org.lwjgl.util.vector.Vector4f();
        	org.lwjgl.util.vector.Vector4f _out = new org.lwjgl.util.vector.Vector4f();

            _in.x = objPos.x;
            _in.y = objPos.y;
            _in.z = objPos.z;
            _in.w = 1.0f;

             org.lwjgl.util.vector.Matrix4f.transform(modelMatrix, _in, _out);
             org.lwjgl.util.vector.Matrix4f.transform(projMatrix, _out, _in);

            if (_in.w <= 0.0)
            {
                screenPos[0] = new org.lwjgl.util.vector.Vector3f(0, 0, 0);
                return false;
            }

            _in.x /= _in.w;
            _in.y /= _in.w;
            _in.z /= _in.w;
            /* Map x, y and z to range 0-1 */
            _in.x = _in.x * 0.5f + 0.5f;
            _in.y = _in.y * 0.5f + 0.5f;
            _in.z = _in.z * 0.5f + 0.5f;

            /* Map x,y to viewport */
            _in.x = _in.x * viewport[2] + viewport[0];
            _in.y = _in.y * viewport[3] + viewport[1];

            screenPos[0].x = _in.x;
            screenPos[0].y = _in.y;
            screenPos[0].z = _in.z;

            return true;
        }

        public static boolean GluUnProject(float winx, float winy, float winz, org.lwjgl.util.vector.Matrix4f modelMatrix
        		, org.lwjgl.util.vector.Matrix4f projMatrix, int[] viewport, org.lwjgl.util.vector.Vector3f[] pos)
        {
        	org.lwjgl.util.vector.Matrix4f finalMatrix = new org.lwjgl.util.vector.Matrix4f();
        	org.lwjgl.util.vector.Vector4f _in = new org.lwjgl.util.vector.Vector4f();
        	org.lwjgl.util.vector.Vector4f _out = new org.lwjgl.util.vector.Vector4f();

            org.lwjgl.util.vector.Matrix4f.mul(modelMatrix, projMatrix, finalMatrix);

            finalMatrix.invert();

            _in.x = winx;
            _in.y = winy;
            _in.z = winz;
            _in.w = 1.0f;

            /* Map x and y from window coordinates */
            _in.x = (_in.x - viewport[0]) / viewport[2];
            _in.y = (_in.y - viewport[1]) / viewport[3];

            pos[0] = new org.lwjgl.util.vector.Vector3f(0,0,0);

            /* Map to range -1 to 1 */
            _in.x = _in.x * 2 - 1;
            _in.y = _in.y * 2 - 1;
            _in.z = _in.z * 2 - 1;

            //__gluMultMatrixVecd(finalMatrix, _in, _out);
            // check if this works:
            
            org.lwjgl.util.vector.Matrix4f.transform(finalMatrix, _in, _out);

            if (_out.w == 0.0f)
                return false;
            _out.x /= _out.w;
            _out.y /= _out.w;
            _out.z /= _out.w;
            pos[0].x = _out.x;
            pos[0].y = _out.y;
            pos[0].z = _out.z;
            return true;
        }

        public static double[] AbovePlane(double height)
        {
            return new double[] { 0, 0, 1, -height };
        }

        public static double[] BelowPlane(double height)
        {
            return new double[] { 0, 0, -1, height };
        }
        
        /*
         * @param column major matrix
         * @return FloatMatrix4x4
         */
        public static FloatMatrix4x4 convertToFloatMatrix4x4(float[] srtMatrix)
        {
    		return new FloatMatrix4x4(new float[][]{
    				{srtMatrix[0], srtMatrix[4], srtMatrix[8], srtMatrix[12]},
    				{srtMatrix[1], srtMatrix[5], srtMatrix[9], srtMatrix[13]},
    				{srtMatrix[2], srtMatrix[6], srtMatrix[10], srtMatrix[14]},
    				{srtMatrix[3], srtMatrix[7], srtMatrix[11], srtMatrix[15]}				
    				});	
        }
        
        public static void printMatrix(float[] mat)
        {
        System.out.println(String.format("%f %f %f %f", mat[0], mat[1], mat[2], mat[3]));
        System.out.println(String.format("%f %f %f %f", mat[4], mat[5], mat[6], mat[7]));
        System.out.println(String.format("%f %f %f %f", mat[8], mat[9], mat[10], mat[11]));
        System.out.println(String.format("%f %f %f %f", mat[12], mat[13], mat[14], mat[15]));
        }
    }
