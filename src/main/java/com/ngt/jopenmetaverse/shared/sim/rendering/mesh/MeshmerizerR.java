/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering.mesh;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ngt.jopenmetaverse.shared.protocol.primitives.ConstructionData;
import com.ngt.jopenmetaverse.shared.protocol.primitives.Enums.MappingType;
import com.ngt.jopenmetaverse.shared.protocol.primitives.Primitive;
import com.ngt.jopenmetaverse.shared.protocol.primitives.TextureEntryFace;
import com.ngt.jopenmetaverse.shared.sim.imaging.IBitmap;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.primmesher.*;
import com.ngt.jopenmetaverse.shared.types.EnumsPrimitive.HoleType;
import com.ngt.jopenmetaverse.shared.types.EnumsPrimitive.PathCurve;
import com.ngt.jopenmetaverse.shared.types.EnumsPrimitive.ProfileCurve;
import com.ngt.jopenmetaverse.shared.types.Vector2;
import com.ngt.jopenmetaverse.shared.types.Vector3;
import com.ngt.jopenmetaverse.shared.util.FileUtils;

public class MeshmerizerR 
{
    /// <summary>
    /// Generates a basic mesh structure from a primitive
    /// </summary>
    /// <param name="prim">Primitive to generate the mesh from</param>
    /// <param name="lod">Level of detail to generate the mesh at</param>
    /// <returns>The generated mesh or null on failure</returns>
    public SimpleMesh GenerateSimpleMesh(Primitive prim, DetailLevel lod)
    {
        PrimMesh newPrim = GeneratePrimMesh(prim, lod, false);
        if (newPrim == null)
            return null;

        SimpleMesh mesh = new SimpleMesh();
        mesh.Path = new Path();
        mesh.Prim = prim;
        mesh.Profile = new Profile();
        mesh.Vertices = new ArrayList<Vertex>(newPrim.coords.size());
        for (int i = 0; i < newPrim.coords.size(); i++)
        {
            Coord c = newPrim.coords.get(i);
            Vertex vertex = new Vertex();
            vertex.Position = new Vector3(c.X, c.Y, c.Z);
            
            mesh.Vertices.add(vertex);
        }

        mesh.Indices = new ArrayList<Integer>(newPrim.faces.size() * 3);
        for (int i = 0; i < newPrim.faces.size(); i++)
        {
        	com.ngt.jopenmetaverse.shared.sim.rendering.mesh.primmesher.Face face = newPrim.faces.get(i);
            mesh.Indices.add(face.v1);
            mesh.Indices.add(face.v2);
            mesh.Indices.add(face.v3);
        }

        return mesh;
    }

    /// <summary>
    /// Generates a basic mesh structure from a sculpted primitive
    /// </summary>
    /// <param name="prim">Sculpted primitive to generate the mesh from</param>
    /// <param name="sculptTexture">Sculpt texture</param>
    /// <param name="lod">Level of detail to generate the mesh at</param>
    /// <returns>The generated mesh or null on failure</returns>
    public SimpleMesh GenerateSimpleSculptMesh(Primitive prim, IBitmap sculptTexture, DetailLevel lod) throws Exception
    {
        FacetedMesh faceted = GenerateFacetedSculptMesh(prim, sculptTexture, lod);

        if (faceted != null && faceted.Faces.size() == 1)
        {
            Face face = faceted.Faces.get(0);

            SimpleMesh mesh = new SimpleMesh();
            mesh.Indices = face.Indices;
            mesh.Vertices = face.Vertices;
            mesh.Path = faceted.Path;
            mesh.Prim = prim;
            mesh.Profile = faceted.Profile;
            mesh.Vertices = face.Vertices;

            return mesh;
        }

        return null;
    }

    /// <summary>
    /// Generates a a series of faces, each face containing a mesh and
    /// metadata
    /// </summary>
    /// <param name="prim">Primitive to generate the mesh from</param>
    /// <param name="lod">Level of detail to generate the mesh at</param>
    /// <returns>The generated mesh</returns >
    public FacetedMesh GenerateFacetedMesh(Primitive prim, DetailLevel lod) throws Exception
    {
        boolean isSphere = ((ProfileCurve.get((byte)(prim.PrimData.profileCurve & 0x07))) == ProfileCurve.HalfCircle);
        PrimMesh newPrim = GeneratePrimMesh(prim, lod, true);
        
        //Following statement is useful for debugging, sometimes
        //newPrim.DumpRaw("/media/development/workspace/dev/vw/testclientjomv/openmetaverse_data/logs/"
        //		, Long.toString(prim.LocalID), "PrimMesh.raw");
        
        if (newPrim == null)
            return null;

        int numViewerFaces = newPrim.viewerFaces.size();
        int numPrimFaces = newPrim.numPrimFaces;

        for (int i = 0; i < numViewerFaces; i++)
        {
            ViewerFace vf = newPrim.viewerFaces.get(i);

            if (isSphere)
            {
                vf.uv1.U = (vf.uv1.U - 0.5f) * 2.0f;
                vf.uv2.U = (vf.uv2.U - 0.5f) * 2.0f;
                vf.uv3.U = (vf.uv3.U - 0.5f) * 2.0f;
            }
        }

        // copy the vertex information into IRendering structures
        FacetedMesh omvrmesh = new FacetedMesh();
        omvrmesh.Faces = new ArrayList<Face>();
        omvrmesh.Prim = prim;
        omvrmesh.Profile = new Profile();
        omvrmesh.Profile.Faces = new ArrayList<ProfileFace>();
        omvrmesh.Profile.Positions = new ArrayList<Vector3>();
        omvrmesh.Path = new Path();
        omvrmesh.Path.Points = new ArrayList<PathPoint>();

        Map<Vector3, Integer> vertexAccount = new HashMap<Vector3, Integer>();
        for (int ii = 0; ii < numPrimFaces; ii++)
        {
            Face oface = new Face();
            oface.Vertices = new ArrayList<Vertex>();
            oface.Indices = new ArrayList<Integer>();
            oface.TextureFace = prim.Textures.GetFace(ii);
            int faceVertices = 0;
            vertexAccount.clear();
            Vector3 pos;
            int indx;
            Vertex vert;
            for (ViewerFace vface : newPrim.viewerFaces)
            {
                if (vface.primFaceNumber == ii)
                {
//                	System.out.println(String.format("Face Number %d v1 %s v2 %s v3 %s uv1 %s uv2 %s uv3 %s", 
//                			ii, vface.v1, vface.v2, vface.v3, vface.uv1, vface.uv2, vface.uv3));
                	
                    faceVertices++;
                    pos = new Vector3(vface.v1.X, vface.v1.Y, vface.v1.Z);
                    if (vertexAccount.containsKey(pos))
                    {
                        // we aleady have this vertex in the list. Just point the index at it
                        oface.Indices.add(vertexAccount.get(pos));
                    }
                    else
                    {
                        // the vertex is not in the list. Add it and the new index.
                        vert = new Vertex();
                        vert.Position = pos;
                        vert.TexCoord = new Vector2(vface.uv1.U, 1.0f - vface.uv1.V);
                        vert.Normal = new Vector3(vface.n1.X, vface.n1.Y, vface.n1.Z);
                        oface.Vertices.add(vert);
                        indx = oface.Vertices.size() - 1;
                        vertexAccount.put(pos, indx);
                        oface.Indices.add(indx);
                    }

                    pos = new Vector3(vface.v2.X, vface.v2.Y, vface.v2.Z);
                    if (vertexAccount.containsKey(pos))
                    {
                        oface.Indices.add(vertexAccount.get(pos));
                    }
                    else
                    {
                        vert = new Vertex();
                        vert.Position = pos;
                        vert.TexCoord = new Vector2(vface.uv2.U, 1.0f - vface.uv2.V);
                        vert.Normal = new Vector3(vface.n2.X, vface.n2.Y, vface.n2.Z);
                        oface.Vertices.add(vert);
                        indx = oface.Vertices.size() - 1;
                        vertexAccount.put(pos, indx);
                        oface.Indices.add(indx);
                    }

                    pos = new Vector3(vface.v3.X, vface.v3.Y, vface.v3.Z);
                    if (vertexAccount.containsKey(pos))
                    {
                        oface.Indices.add(vertexAccount.get(pos));
                    }
                    else
                    {
                        vert = new Vertex();
                        vert.Position = pos;
                        vert.TexCoord = new Vector2(vface.uv3.U, 1.0f - vface.uv3.V);
                        vert.Normal = new Vector3(vface.n3.X, vface.n3.Y, vface.n3.Z);
                        oface.Vertices.add(vert);
                        indx = oface.Vertices.size() - 1;
                        vertexAccount.put(pos, indx);
                        oface.Indices.add(indx);
                    }
                }
            }
            if (faceVertices > 0)
            {
                oface.TextureFace = prim.Textures.FaceTextures[ii];
                if (oface.TextureFace == null)
                {
                    oface.TextureFace = prim.Textures.DefaultTexture;
                }
                oface.ID = ii;
                omvrmesh.Faces.add(oface);
            }
        }

//        DumpRaw("/media/development/workspace/dev/vw/testclientjomv/openmetaverse_data/logs/"
//        		,prim.LocalID, omvrmesh);
        
        return omvrmesh;
    }

    /// <summary>
    /// Create a sculpty faceted mesh. The actual scuplt texture is fetched and passed to this
    /// routine since all the context for finding teh texture is elsewhere.
    /// </summary>
    /// <returns>The faceted mesh or null if can't do it</returns>
    public FacetedMesh GenerateFacetedSculptMesh(Primitive prim, IBitmap scupltTexture, DetailLevel lod) throws Exception
    {
        SculptMesh.SculptType smSculptType;
        switch (prim.Sculpt.getType())
        {
            case Cylinder:
                smSculptType = SculptMesh.SculptType.cylinder;
                break;
            case Plane:
                smSculptType = SculptMesh.SculptType.plane;
                break;
            case Sphere:
                smSculptType = SculptMesh.SculptType.sphere;
                break;
            case Torus:
                smSculptType = SculptMesh.SculptType.torus;
                break;
            default:
                smSculptType = SculptMesh.SculptType.plane;
                break;
        }
        // The lod for sculpties is the resolution of the texture passed.
        // The first guess is 1:1 then lower resolutions after that
        // int mesherLod = (int)Math.Sqrt(scupltTexture.Width * scupltTexture.Height);
        int mesherLod = 32; // number used in Idealist viewer
        switch (lod)
        {
            case Highest:
                break;
            case High:
                break;
            case Medium:
                mesherLod /= 2;
                break;
            case Low:
                mesherLod /= 4;
                break;
        }
        SculptMesh newMesh =
            new SculptMesh(scupltTexture, smSculptType, mesherLod, true, prim.Sculpt.getMirror(), 
            		prim.Sculpt.getInvert());

        int numPrimFaces = 1;       // a scuplty has only one face

        // copy the vertex information into IRendering structures
        FacetedMesh omvrmesh = new FacetedMesh();
        omvrmesh.Faces = new ArrayList<Face>();
        omvrmesh.Prim = prim;
        omvrmesh.Profile = new Profile();
        omvrmesh.Profile.Faces = new ArrayList<ProfileFace>();
        omvrmesh.Profile.Positions = new ArrayList<Vector3>();
        omvrmesh.Path = new Path();
        omvrmesh.Path.Points = new ArrayList<PathPoint>();

        Map<Vertex, Integer> vertexAccount = new HashMap<Vertex, Integer>();


        for (int ii = 0; ii < numPrimFaces; ii++)
        {
            vertexAccount.clear();
            Face oface = new Face();
            oface.Vertices = new ArrayList<Vertex>();
            oface.Indices = new ArrayList<Integer>();
            oface.TextureFace = prim.Textures.GetFace(ii);
            int faceVertices = newMesh.coords.size();
            Vertex vert;

            for (int j = 0; j < faceVertices; j++)
            {
                vert = new Vertex();
                vert.Position = new Vector3(newMesh.coords.get(j).X, newMesh.coords.get(j).Y, newMesh.coords.get(j).Z);
                vert.Normal = new Vector3(newMesh.normals.get(j).X, newMesh.normals.get(j).Y, newMesh.normals.get(j).Z);
                vert.TexCoord = new Vector2(newMesh.uvs.get(j).U, newMesh.uvs.get(j).V);
                oface.Vertices.add(vert);
            }

            for (int j = 0; j < newMesh.faces.size(); j++)
            {
                oface.Indices.add(newMesh.faces.get(j).v1);
                oface.Indices.add(newMesh.faces.get(j).v2);
                oface.Indices.add(newMesh.faces.get(j).v3);
            }

            if (faceVertices > 0)
            {
                oface.TextureFace = prim.Textures.FaceTextures[ii];
                if (oface.TextureFace == null)
                {
                    oface.TextureFace = prim.Textures.DefaultTexture;
                }
                oface.ID = ii;
                omvrmesh.Faces.add(oface);
            }
        }


//        DumpRaw("/media/development/workspace/dev/vw/testclientjomv/openmetaverse_data/logs/"
//        		,prim.LocalID, omvrmesh);
        
        return omvrmesh;
    }

    /// <summary>
    /// Apply texture coordinate modifications from a
    /// <seealso cref="TextureEntryFace"/> to a list of vertices
    /// </summary>
    /// <param name="vertices">Vertex list to modify texture coordinates for</param>
    /// <param name="center">Center-point of the face</param>
    /// <param name="teFace">Face texture parameters</param>
    public void TransformTexCoords(List<Vertex> vertices, Vector3 center, 
    		TextureEntryFace teFace, Vector3 primScale)
    {
        // compute trig stuff up front
        float cosineAngle = (float)Math.cos(teFace.getRotation());
        float sinAngle = (float)Math.sin(teFace.getRotation());

//        System.out.println(String.format("TransformTexCoords Center %s scale %s cos %f sin %f Rot %f Repeat U%f V%f Offset U%f V%f"
//        		, center, primScale, cosineAngle, sinAngle, teFace.getRotation()
//        		, teFace.getRepeatU(), teFace.getRepeatV(), teFace.getOffsetU(), teFace.getOffsetV()));
        
        for (int ii = 0; ii < vertices.size(); ii++)
        {
            // tex coord comes to us as a number between zero and one
            // transform about the center of the texture
            Vertex vert = vertices.get(ii);

            //System.out.println("\nTexture " + vert.TexCoord);
            
            // aply planar tranforms to the UV first if applicable
            if (teFace.getTexMapType() == MappingType.Planar)
            {
                Vector3 binormal;
                float d = Vector3.dot(vert.Normal, Vector3.UnitX);
                if (d >= 0.5f || d <= -0.5f)
                {
                    binormal = Vector3.UnitY;
                    if (vert.Normal.X < 0f) 
                    	binormal = Vector3.multiply(binormal, -1);
                }
                else
                {
                    binormal = Vector3.UnitX;
                    if (vert.Normal.Y > 0f) 
                    	binormal = Vector3.multiply(binormal, -1);
                }
                Vector3 tangent = Vector3.modulus(vert.Normal, binormal);
                Vector3 scaledPos = Vector3.multiply(vert.Position,  primScale);
                vert.TexCoord.X = 1f + (Vector3.dot(binormal, scaledPos) * 2f - 0.5f);
                vert.TexCoord.Y = -(Vector3.dot(tangent, scaledPos) * 2f - 0.5f);
            }
            
            float repeatU = teFace.getRepeatU();
            float repeatV = teFace.getRepeatV();
            float tX = vert.TexCoord.X - 0.5f;
            float tY = vert.TexCoord.Y - 0.5f;

            vert.TexCoord.X = (tX * cosineAngle + tY * sinAngle) * repeatU + teFace.getOffsetU() + 0.5f;
            vert.TexCoord.Y = (-tX * sinAngle + tY * cosineAngle) * repeatV + teFace.getOffsetV() + 0.5f;
            
            //System.out.println("\nTexture " + vert.TexCoord);
            vertices.set(ii, vert);
        }
        return;
    }

    private PrimMesh GeneratePrimMesh(Primitive prim, DetailLevel lod, boolean viewerMode)
    {
        ConstructionData primData = prim.PrimData;
        int sides = 4;
        int hollowsides = 4;

        float profileBegin = primData.ProfileBegin;
        float profileEnd = primData.ProfileEnd;

        if (ProfileCurve.get((byte)(primData.profileCurve & 0x07)) == ProfileCurve.Circle)
        {
            switch (lod)
            {
                case Low:
                    sides = 6;
                    break;
                case Medium:
                    sides = 12;
                    break;
                default:
                    sides = 24;
                    break;
            }
        }
        else if (ProfileCurve.get((byte)(primData.profileCurve & 0x07)) == ProfileCurve.EqualTriangle)
            sides = 3;
        else if (ProfileCurve.get((byte)(primData.profileCurve & 0x07)) == ProfileCurve.HalfCircle)
        {
            // half circle, prim is a sphere
            switch (lod)
            {
                case Low:
                    sides = 6;
                    break;
                case Medium:
                    sides = 12;
                    break;
                default:
                    sides = 24;
                    break;
            }
            profileBegin = 0.5f * profileBegin + 0.5f;
            profileEnd = 0.5f * profileEnd + 0.5f;
        }

        if ((HoleType)primData.getProfileHole() == HoleType.Same)
            hollowsides = sides;
        else if ((HoleType)primData.getProfileHole() == HoleType.Circle)
        {
            switch (lod)
            {
                case Low:
                    hollowsides = 6;
                    break;
                case Medium:
                    hollowsides = 12;
                    break;
                default:
                    hollowsides = 24;
                    break;
            }
        }
        else if ((HoleType)primData.getProfileHole() == HoleType.Triangle)
            hollowsides = 3;

        PrimMesh newPrim = new PrimMesh(sides, profileBegin, profileEnd, (float)primData.ProfileHollow, hollowsides);
        newPrim.viewerMode = viewerMode;
        newPrim.holeSizeX = primData.PathScaleX;
        newPrim.holeSizeY = primData.PathScaleY;
        newPrim.pathCutBegin = primData.PathBegin;
        newPrim.pathCutEnd = primData.PathEnd;
        newPrim.topShearX = primData.PathShearX;
        newPrim.topShearY = primData.PathShearY;
        newPrim.radius = primData.PathRadiusOffset;
        newPrim.revolutions = primData.PathRevolutions;
        newPrim.skew = primData.PathSkew;
        switch (lod)
        {
            case Low:
                newPrim.stepsPerRevolution = 6;
                break;
            case Medium:
                newPrim.stepsPerRevolution = 12;
                break;
            default:
                newPrim.stepsPerRevolution = 24;
                break;
        }

        if ((primData.PathCurve == PathCurve.Line) || (primData.PathCurve == PathCurve.Flexible))
        {
            newPrim.taperX = 1.0f - primData.PathScaleX;
            newPrim.taperY = 1.0f - primData.PathScaleY;
            newPrim.twistBegin = (int)(180 * primData.PathTwistBegin);
            newPrim.twistEnd = (int)(180 * primData.PathTwist);
            newPrim.ExtrudeLinear();
        }
        else
        {
            newPrim.taperX = primData.PathTaperX;
            newPrim.taperY = primData.PathTaperY;
            newPrim.twistBegin = (int)(360 * primData.PathTwistBegin);
            newPrim.twistEnd = (int)(360 * primData.PathTwist);
            newPrim.ExtrudeCircular();
        }

//        System.out.println(String.format("Prim Data Path Curve %s ScaleXY %f %f  PathTaperXY %f %f New Prim taperX %f", primData.PathCurve, 
//        		primData.PathScaleX, primData.PathScaleY
//        		, primData.PathTaperX, primData.PathTaperY, newPrim.taperX));
//        
        
        return newPrim;
    }

    /// <summary>
    /// Method for generating mesh Face from a heightmap
    /// </summary>
    /// <param name="zMap">Two dimension array of floats containing height information</param>
    /// <param name="xBegin">Starting value for X</param>
    /// <param name="xEnd">Max value for X</param>
    /// <param name="yBegin">Starting value for Y</param>
    /// <param name="yEnd">Max value of Y</param>
    /// <returns></returns>
    public Face TerrainMesh(float[][] zMap, float xBegin, float xEnd, float yBegin, float yEnd)
    {
        SculptMesh newMesh = new SculptMesh(zMap, xBegin, xEnd, yBegin, yEnd, true);
        Face terrain = new Face();
        int faceVertices = newMesh.coords.size();
        terrain.Vertices = new ArrayList<Vertex>(faceVertices);
        terrain.Indices = new ArrayList<Integer>(newMesh.faces.size() * 3);

        for (int j = 0; j < faceVertices; j++)
        {
        	Vertex vert = new Vertex();
            vert.Position = new Vector3(newMesh.coords.get(j).X, newMesh.coords.get(j).Y, newMesh.coords.get(j).Z);
            vert.Normal = new Vector3(newMesh.normals.get(j).X, newMesh.normals.get(j).Y, newMesh.normals.get(j).Z);
            vert.TexCoord = new Vector2(newMesh.uvs.get(j).U, newMesh.uvs.get(j).V);
            terrain.Vertices.add(vert);
        }

        for (int j = 0; j < newMesh.faces.size(); j++)
        {
            terrain.Indices.add(newMesh.faces.get(j).v1);
            terrain.Indices.add(newMesh.faces.get(j).v2);
            terrain.Indices.add(newMesh.faces.get(j).v3);
        }

        return terrain;
    }
    
    public void DumpRaw(String path, long id, FacetedMesh facetedMesh) throws IOException
    {
        String fileName = id + "_"+ ".prim";
        String completePath = FileUtils.combineFilePath(path, fileName);
        BufferedWriter sw = new BufferedWriter(new FileWriter(completePath));

		for (Face face : facetedMesh.Faces)
		{
			for(Vertex vertex : face.Vertices)
			{
				String s = "";
				s+= vertex.toString();
	            sw.write(s);
	            sw.newLine();
			}
			
			for(Integer index: face.Indices)
			{
	            sw.write(index + " ");
			}
		}
	
        sw.flush();
        sw.close();
    }
    
}
