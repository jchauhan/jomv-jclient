/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.lwjgl.opengl.GL15;

import com.ngt.jopenmetaverse.shared.sim.GridClient;
import com.ngt.jopenmetaverse.shared.sim.asset.AssetTexture;
import com.ngt.jopenmetaverse.shared.sim.asset.pipeline.TexturePipeline.TextureRequestState;
import com.ngt.jopenmetaverse.shared.sim.cache.LoadCachedImageResult;
import com.ngt.jopenmetaverse.shared.sim.events.EventObserver;
import com.ngt.jopenmetaverse.shared.sim.events.MethodDelegate;
import com.ngt.jopenmetaverse.shared.sim.events.ThreadPool;
import com.ngt.jopenmetaverse.shared.sim.events.ThreadPoolFactory;
import com.ngt.jopenmetaverse.shared.sim.events.asm.TextureDownloadCallbackArgs;
import com.ngt.jopenmetaverse.shared.sim.events.terrain.LandPatchReceivedEventArgs;
import com.ngt.jopenmetaverse.shared.sim.imaging.IBitmap;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.Face;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.MeshmerizerR;
import com.ngt.jopenmetaverse.shared.types.UUID;

import com.ngt.jopenmetaverse.shared.types.Vector3;
import com.ngt.jopenmetaverse.shared.util.JLogger;
import com.ngt.jopenmetaverse.shared.util.Utils;


public  class SceneWindow 
{
    /// <summary>
    /// List of prims in the scene
    /// </summary>
    Map<Long, RenderPrimitive> Prims = new HashMap<Long, RenderPrimitive>();
    List<SceneObject> SortedObjects;
    List<SceneObject> OccludedObjects;
    List<RenderAvatar> VisibleAvatars;
    Map<Long, RenderAvatar> Avatars = new HashMap<Long, RenderAvatar>();

    /// <summary>
    /// Cache images after jpeg2000 decode. Uses a lot of disk space and can cause disk trashing
    /// </summary>
    public boolean CacheDecodedTextures = false;

    /// <summary>
    /// Size of OpenGL window we're drawing on
    /// </summary>
    public int[] Viewport = new int[4];
    //endregion Public fields
	
	
	GridClient Client;
    Map<UUID, TextureInfo> TexturesPtrMap = new HashMap<UUID, TextureInfo>();
    MeshmerizerR renderer;
    ThreadPool terrainThreadPool;
	
    BlockingQueue<TextureLoadItem> PendingTextures = new LinkedBlockingQueue<TextureLoadItem>();
    Map<UUID, Integer> AssetFetchFailCount = new HashMap<UUID, Integer>();
    
    
	public SceneWindow(GridClient Client)
	{
		this.Client = Client;
		terrainThreadPool = ThreadPoolFactory.getNewInstance(2, 20, 1, TimeUnit.SECONDS, 100);
        renderer = new MeshmerizerR();
        EventObserver<LandPatchReceivedEventArgs> landPatchReceivedHandler  = new EventObserver<LandPatchReceivedEventArgs>()
        		{
					@Override
					public void handleEvent(Observable sender,
							LandPatchReceivedEventArgs arg) {
						Terrain_LandPatchReceived(sender, arg);
					}
        		};
        
        Client.terrain.registerOnLandPatchReceived(landPatchReceivedHandler);

        
	}
	
    ShaderProgram shinyProgram = new ShaderProgram();
    void InitShaders()
    {
        if (RenderSettings.HasShaders)
        {
        	//TODO enable following
//            shinyProgram.Load(new String[]{"shiny.vert", "shiny.frag"});
//            shinyProgram.SetUniform1("colorMap", 0);
        }
    }
	
    /// <summary>
    /// Select shiny shader as the current shader
    /// </summary>
    public void StartShiny()
    {
        if (RenderSettings.EnableShiny)
        {
            shinyProgram.Start();
        }
    }
    
    //region Terrain
    boolean terrainModified = true;
    float[][] heightTable = new float[256][256];
    Face terrainFace;
    Integer[] terrainIndices;
    ColorVertex[] terrainVertices;
    int terrainTexture = -1;
    boolean fetchingTerrainTexture = false;
    IBitmap terrainImage = null;
    int terrainVBO = -1;
    int terrainIndexVBO = -1;
    boolean terrainVBOFailed = false;
    boolean terrainInProgress = false;
    boolean terrainTextureNeedsUpdate = false;
    float terrainTimeSinceUpdate = RenderSettings.MinimumTimeBetweenTerrainUpdated + 1f; // Update terrain om first run

    private void ResetTerrain()
    {
        ResetTerrain(true);
    }

    private void ResetTerrain(boolean removeImage)
    {
        if (terrainImage != null)
        {
            terrainImage.dispose();
            terrainImage = null;
        }

        //TODO 
        if (terrainVBO != -1)
        {
            GL15.glDeleteBuffers(terrainVBO);
            terrainVBO = -1;
        }

        if (terrainIndexVBO != -1)
        {
            GL15.glDeleteBuffers(terrainIndexVBO);
            terrainIndexVBO = -1;
        }

        if (removeImage)
        {
            if (terrainTexture != -1)
            {
                GL15.glDeleteBuffers(terrainTexture);                
                terrainTexture = -1;
            }
        }

        fetchingTerrainTexture = false;
        terrainModified = true;
    }

//    private void UpdateTerrain()
//    {
//        if (Client.network.getCurrentSim() == null || Client.network.getCurrentSim().Terrain == null) return;
//
//        Runnable task = new Runnable()
//        {
//			public void run() {
//				 int step = 1;
//
//		            for (int x = 0; x < 256; x += step)
//		            {
//		                for (int y = 0; y < 256; y += step)
//		                {
//		                    float z = 0;
//		                    int patchNr = ((int)x / 16) * 16 + (int)y / 16;
//		                    if (Client.network.getCurrentSim().Terrain[patchNr] != null
//		                        && Client.network.getCurrentSim().Terrain[patchNr].Data != null)
//		                    {
//		                        float[] data = Client.network.getCurrentSim().Terrain[patchNr].Data;
//		                        z = data[(int)x % 16 * 16 + (int)y % 16];
//		                    }
//		                    heightTable[x][y] = z;
//		                }
//		            }
//
//		            terrainFace = renderer.TerrainMesh(heightTable, 0f, 255f, 0f, 255f);
//		            terrainVertices = new ColorVertex[terrainFace.Vertices.size()];
//		            for (int i = 0; i < terrainFace.Vertices.size(); i++)
//		            {
//		                byte[] part = Utils.intToBytes(i);
//		                terrainVertices[i] = new ColorVertex
//		                (
//			                    terrainFace.Vertices.get(i),
//		                    new Color4b(part[0], part[1], part[2], (byte)253)
//		                );
//		            }
//		            
//		            terrainIndices = terrainFace.Indices.toArray(new Integer[0]);
//		            terrainInProgress = false;
//		            terrainModified = false;
//		            terrainTextureNeedsUpdate = true;
//		            terrainTimeSinceUpdate = 0f;
//			}
//        };
//        
//        terrainThreadPool.execute(task);
//    }
//
//    void UpdateTerrainTexture()
//    {
//        if (!fetchingTerrainTexture)
//        {
//            fetchingTerrainTexture = true;
//            Runnable task = new Runnable()
//            {
//    			public void run() {
//                Simulator sim = Client.network.getCurrentSim();
//                terrainImage = TerrainSplat.Splat(Client, heightTable,
//                    new UUID[] { sim.TerrainDetail0, sim.TerrainDetail1, sim.TerrainDetail2, sim.TerrainDetail3 },
//                    new float[] { sim.TerrainStartHeight00, sim.TerrainStartHeight01, sim.TerrainStartHeight10, sim.TerrainStartHeight11 },
//                    new float[] { sim.TerrainHeightRange00, sim.TerrainHeightRange01, sim.TerrainHeightRange10, sim.TerrainHeightRange11 });
//
//                fetchingTerrainTexture = false;
//                terrainTextureNeedsUpdate = false;
//            }
//            };
//            terrainThreadPool.execute(task);
//        }        
//    }
//
//    private void RenderTerrain(RenderPass pass)
//    {
//        terrainTimeSinceUpdate += lastFrameTime;
//
//        if (terrainModified && terrainTimeSinceUpdate > RenderSettings.MinimumTimeBetweenTerrainUpdated)
//        {
//            if (!terrainInProgress)
//            {
//                terrainInProgress = true;
//                ResetTerrain(false);
//                UpdateTerrain();
//            }
//        }
//
//        if (terrainTextureNeedsUpdate)
//        {
//            UpdateTerrainTexture();
//        }
//
//        if (terrainIndices == null || terrainVertices == null) return;
//
//        GL11.glColor3f(1f, 1f, 1f);
//        GL11.glEnableClientState(GL11.GL_VERTEX_ARRAY);
//        GL11.glEnableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
//        GL11.glEnableClientState(GL11.GL_NORMAL_ARRAY);
//        if (pass == RenderPass.Picking)
//        {
//            GL11.glEnableClientState(GL11.GL_COLOR_ARRAY);
//            GL11.glShadeModel(GL11.GL_FLAT); 
//        }
//
//        if (terrainImage != null)
//        {
//            if (terrainTexture != -1)
//            {
//                GL11.glDeleteTextures(terrainTexture);
//            }
//
//            terrainTexture = RHelp.GLLoadImage(terrainImage, false);
//            terrainImage.dispose();
//            terrainImage = null;
//        }
//
//        if (pass != RenderPass.Picking && terrainTexture != -1)
//        {
//            GL.Enable(EnableCap.Texture2D);
//            GL.BindTexture(TextureTarget.Texture2D, terrainTexture);
//        }
//
//        if (!RenderSettings.UseVBO || terrainVBOFailed)
//        {
//            unsafe
//            {
//                fixed (float* normalPtr = &terrainVertices[0].Vertex.Normal.X)
//                fixed (float* texPtr = &terrainVertices[0].Vertex.TexCoord.X)
//                fixed (byte* colorPtr = &terrainVertices[0].Color.R)
//                {
//                    GL.NormalPointer(NormalPointerType.Float, ColorVertex.Size, (IntPtr)normalPtr);
//                    GL.TexCoordPointer(2, TexCoordPointerType.Float, ColorVertex.Size, (IntPtr)texPtr);
//                    GL.VertexPointer(3, VertexPointerType.Float, ColorVertex.Size, terrainVertices);
//                    if (pass == RenderPass.Picking)
//                    {
//                        GL.ColorPointer(4, ColorPointerType.UnsignedByte, ColorVertex.Size, (IntPtr)colorPtr);
//                    }
//                    GL.DrawElements(BeginMode.Triangles, terrainIndices.Length, DrawElementsType.UnsignedShort, terrainIndices);
//                }
//            }
//        }
//        else
//        {
//            if (terrainVBO == -1)
//            {
//                Compat.GenBuffers(out terrainVBO);
//                Compat.BindBuffer(BufferTarget.ArrayBuffer, terrainVBO);
//                Compat.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(terrainVertices.Length * ColorVertex.Size), terrainVertices, BufferUsageHint.StaticDraw);
//                if (Compat.BufferSize(BufferTarget.ArrayBuffer) != terrainVertices.Length * ColorVertex.Size)
//                {
//                    terrainVBOFailed = true;
//                    Compat.BindBuffer(BufferTarget.ArrayBuffer, 0);
//                    terrainVBO = -1;
//                }
//            }
//            else
//            {
//                Compat.BindBuffer(BufferTarget.ArrayBuffer, terrainVBO);
//            }
//
//            if (terrainIndexVBO == -1)
//            {
//                Compat.GenBuffers(out terrainIndexVBO);
//                Compat.BindBuffer(BufferTarget.ElementArrayBuffer, terrainIndexVBO);
//                Compat.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(terrainIndices.Length * sizeof(ushort)), terrainIndices, BufferUsageHint.StaticDraw);
//                if (Compat.BufferSize(BufferTarget.ElementArrayBuffer) != terrainIndices.Length * sizeof(ushort))
//                {
//                    terrainVBOFailed = true;
//                    Compat.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
//                    terrainIndexVBO = -1;
//                }
//            }
//            else
//            {
//                Compat.BindBuffer(BufferTarget.ElementArrayBuffer, terrainIndexVBO);
//            }
//
//            if (!terrainVBOFailed)
//            {
//                GL.NormalPointer(NormalPointerType.Float, ColorVertex.Size, (IntPtr)12);
//                GL.TexCoordPointer(2, TexCoordPointerType.Float, ColorVertex.Size, (IntPtr)(24));
//                if (pass == RenderPass.Picking)
//                {
//                    GL.ColorPointer(4, ColorPointerType.UnsignedByte, ColorVertex.Size, (IntPtr)32);
//                }
//                GL.VertexPointer(3, VertexPointerType.Float, ColorVertex.Size, (IntPtr)(0));
//
//                GL.DrawElements(BeginMode.Triangles, terrainIndices.Length, DrawElementsType.UnsignedShort, IntPtr.Zero);
//            }
//
//            Compat.BindBuffer(BufferTarget.ArrayBuffer, 0);
//            Compat.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
//        }
//
//        if (pass == RenderPass.Picking)
//        {
//            GL.DisableClientState(ArrayCap.ColorArray);
//            GL.ShadeModel(ShadingModel.Smooth);
//        }
//        else
//        {
//            GL.BindTexture(TextureTarget.Texture2D, 0);
//        }
//        GL.DisableClientState(ArrayCap.VertexArray);
//        GL.DisableClientState(ArrayCap.TextureCoordArray);
//        GL.DisableClientState(ArrayCap.NormalArray);
//    }
//    //endregion Terrain
    
    
    //Handle Network Events
    void Terrain_LandPatchReceived(Object sender, LandPatchReceivedEventArgs e)
    {
        if (e.getSimulator().Handle.equals(Client.network.getCurrentSim().Handle))
        {
            terrainModified = true;
        }
    }
    
    
    public TextureInfo TryGetTextureInfo(UUID textureID)
    {
    	TextureInfo info = null;

        if (TexturesPtrMap.containsKey(textureID))
        {
            info = TexturesPtrMap.get(textureID);
            return info;
        }

        return null;
    }


    public final static  UUID invisi1 = new UUID("38b86f85-2575-52a9-a531-23108d8da837");
    public final static  UUID invisi2 = new UUID("e97cf410-8e61-7005-ec06-629eba4cd1fb");

    int texturesRequestedThisFrame = 0;
    int meshingsRequestedThisFrame;
    int meshingsRequestedLastFrame;
    int framesSinceReflection = 0;
    float timeSinceReflection = 0f;
    
    public void DownloadTexture(final TextureLoadItem item, boolean force) throws Exception
    {
        if (force || texturesRequestedThisFrame < RenderSettings.TexturesToDownloadPerFrame)
        {
            texturesRequestedThisFrame++;

            synchronized (TexturesPtrMap)
            {
                if (TexturesPtrMap.containsKey(item.TeFace.getTextureID()))
                {
                    item.Data.TextureInfo = TexturesPtrMap.get(item.TeFace.getTextureID());
                }
                else if (item.TeFace.getTextureID().equals(invisi1) || item.TeFace.getTextureID().equals(invisi2))
                {
                    TexturesPtrMap.put(item.TeFace.getTextureID(), item.Data.TextureInfo);
                    TexturesPtrMap.get(item.TeFace.getTextureID()).HasAlpha = false;
                    TexturesPtrMap.get(item.TeFace.getTextureID()).IsInvisible = true;
                }
                else
                {
                    TexturesPtrMap.put(item.TeFace.getTextureID(), item.Data.TextureInfo);

                    if (item.TextureData == null && item.TGAData == null)
                    {
                        if (CacheDecodedTextures)
                        {
                        	LoadCachedImageResult lcir = Client.assets.Cache.loadCompressedImageFromCache(item.TeFace.getTextureID());
                        	if(lcir!=null)
                        		PendingTextures.put(item);
                        }
                        else if (Client.assets.Cache.hasAsset(item.Data.TextureInfo.TextureID))
                        {
                            item.LoadAssetFromCache = true;
                            PendingTextures.put(item);
                        }
                        else if (!item.Data.TextureInfo.FetchFailed)
                        {
                        	MethodDelegate<Void, TextureDownloadCallbackArgs> callback 
                        		= new MethodDelegate<Void, TextureDownloadCallbackArgs>()
                        		{
									public Void execute(
											TextureDownloadCallbackArgs e) {
										TextureRequestState state = e.getState();
										AssetTexture asset = e.getAssetTexture();
										 switch (state)
			                                {
			                                    case Finished:
			                                        item.TextureData = asset.AssetData;
											try {
												PendingTextures.put(item);
											} catch (InterruptedException e1) {
												e1.printStackTrace();
												JLogger.debug(Utils.getExceptionStackTraceAsString(e1));
		                                        item.Data.TextureInfo.FetchFailed = true;
											}
			                                        break;

			                                    case Aborted:
			                                    case NotFound:
			                                    case Timeout:
			                                        item.Data.TextureInfo.FetchFailed = true;
			                                        break;
			                                }
										return null;
									}
                        		};
                        	
                        		 Client.assets.RequestImage(item.TeFace.getTextureID(), callback);
                        	
                        }
                    }
                    else
                    {
                        PendingTextures.put(item);
                    }
                }
            }
        }
    }

//    private void CalculateBoundingBox(RenderPrimitive rprim)
//    {
//        Primitive prim = rprim.BasePrim;
//
//        // Calculate bounding volumes for each prim and adjust textures
//        rprim.BoundingVolume = new BoundingVolume();
//        for (int j = 0; j < rprim.Faces.Count; j++)
//        {
//            Primitive.TextureEntryFace teFace = prim.Textures.GetFace((uint)j);
//            if (teFace == null) continue;
//
//            Face face = rprim.Faces[j];
//            FaceData data = new FaceData();
//
//            data.BoundingVolume.CreateBoundingVolume(face, prim.Scale);
//            rprim.BoundingVolume.AddVolume(data.BoundingVolume, prim.Scale);
//
//            // With linear texture animation in effect, texture repeats and offset are ignored
//            if ((prim.TextureAnim.Flags & Primitive.TextureAnimMode.ANIM_ON) != 0
//                && (prim.TextureAnim.Flags & Primitive.TextureAnimMode.ROTATE) == 0
//                && (prim.TextureAnim.Face == 255 || prim.TextureAnim.Face == j))
//            {
//                teFace.RepeatU = 1;
//                teFace.RepeatV = 1;
//                teFace.OffsetU = 0;
//                teFace.OffsetV = 0;
//            }
//
//            // Need to adjust UV for spheres as they are sort of half-prim
//            if (prim.PrimData.ProfileCurve == ProfileCurve.HalfCircle)
//            {
//                teFace = (Primitive.TextureEntryFace)teFace.Clone();
//                teFace.RepeatV *= 2;
//                teFace.OffsetV += 0.5f;
//            }
//
//            // Sculpt UV vertically flipped compared to prims. Flip back
//            if (prim.Sculpt != null && prim.Sculpt.SculptTexture != UUID.Zero && prim.Sculpt.Type != SculptType.Mesh)
//            {
//                teFace = (Primitive.TextureEntryFace)teFace.Clone();
//                teFace.RepeatV *= -1;
//            }
//
//            // Texture transform for this face
//            renderer.TransformTexCoords(face.Vertices, face.Center, teFace, prim.Scale);
//
//            // Set the UserData for this face to our FaceData struct
//            face.UserData = data;
//            rprim.Faces[j] = face;
//        }
//    }
//
//    private void MeshPrim(RenderPrimitive rprim)
//    {
//        if (rprim.Meshing) return;
//
//        rprim.Meshing = true;
//        Primitive prim = rprim.BasePrim;
//
//        // Regular prim
//        if (prim.Sculpt == null || prim.Sculpt.SculptTexture == UUID.Zero)
//        {
//            DetailLevel detailLevel = RenderSettings.PrimRenderDetail;
//            if (RenderSettings.AllowQuickAndDirtyMeshing)
//            {
//                if (prim.Flexible == null && prim.Type == PrimType.Box &&
//                    prim.PrimData.ProfileHollow == 0 &&
//                    prim.PrimData.PathTwist == 0 &&
//                    prim.PrimData.PathTaperX == 0 &&
//                    prim.PrimData.PathTaperY == 0 &&
//                    prim.PrimData.PathSkew == 0 &&
//                    prim.PrimData.PathShearX == 0 &&
//                    prim.PrimData.PathShearY == 0 &&
//                    prim.PrimData.PathRevolutions == 1 &&
//                    prim.PrimData.PathRadiusOffset == 0)
//                    detailLevel = DetailLevel.Low;//Its a box or something else that can use lower meshing
//            }
//            FacetedMesh mesh = renderer.GenerateFacetedMesh(prim, detailLevel);
//            rprim.Faces = mesh.Faces;
//            CalculateBoundingBox(rprim);
//            rprim.Meshing = false;
//            rprim.Meshed = true;
//        }
//        else
//        {
//            PendingTasks.Enqueue(GenerateSculptOrMeshPrim(rprim, prim));
//            return;
//        }
//    }
//
//    private GenericTask GenerateSculptOrMeshPrim(RenderPrimitive rprim, Primitive prim)
//    {
//        return new GenericTask(() =>
//        {
//            FacetedMesh mesh = null;
//
//            try
//            {
//                if (prim.Sculpt.Type != SculptType.Mesh)
//                { // Regular sculptie
//                    Image img = null;
//
//                    lock (sculptCache)
//                    {
//                        if (sculptCache.ContainsKey(prim.Sculpt.SculptTexture))
//                        {
//                            img = sculptCache[prim.Sculpt.SculptTexture];
//                        }
//                    }
//
//                    if (img == null)
//                    {
//                        if (LoadTexture(prim.Sculpt.SculptTexture, ref img, true))
//                        {
//                            sculptCache[prim.Sculpt.SculptTexture] = (Bitmap)img;
//                        }
//                        else
//                        {
//                            return;
//                        }
//                    }
//
//                    mesh = renderer.GenerateFacetedSculptMesh(prim, (Bitmap)img, RenderSettings.SculptRenderDetail);
//                }
//                else
//                { // Mesh
//                    AutoResetEvent gotMesh = new AutoResetEvent(false);
//
//                    Client.Assets.RequestMesh(prim.Sculpt.SculptTexture, (success, meshAsset) =>
//                    {
//                        if (!success || !FacetedMesh.TryDecodeFromAsset(prim, meshAsset, RenderSettings.MeshRenderDetail, out mesh))
//                        {
//                            Logger.Log("Failed to fetch or decode the mesh asset", Helpers.LogLevel.Warning, Client);
//                        }
//                        gotMesh.Set();
//                    });
//
//                    gotMesh.WaitOne(20 * 1000, false);
//                }
//            }
//            catch
//            { }
//
//            if (mesh != null)
//            {
//                rprim.Faces = mesh.Faces;
//                CalculateBoundingBox(rprim);
//                rprim.Meshing = false;
//                rprim.Meshed = true;
//            }
//            else
//            {
//                lock (Prims)
//                {
//                    Prims.Remove(rprim.BasePrim.LocalID);
//                }
//            }
//        });
//    }
    
}
