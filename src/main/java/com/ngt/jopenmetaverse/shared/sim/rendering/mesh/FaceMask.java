/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering.mesh;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public enum FaceMask
	    {
	        Single(0x0001),
	        Cap(0x0002),
	        End(0x0004),
	        Side(0x0008),
	        Inner(0x0010),
	        Outer(0x0020),
	        Hollow(0x0040),
	        Open(0x0080),
	        Flat(0x0100),
	        Top(0x0200),
	        Bottom(0x0400);
	        
	        private int index;
	        FaceMask(int index)
			{
				this.index = index;
			}     

			public int getIndex()
			{
				return index;
			}
			
			private static final Map<Integer,FaceMask> lookup  = new HashMap<Integer,FaceMask>();

			static {
				for(FaceMask s : EnumSet.allOf(FaceMask.class))
					lookup.put(s.getIndex(), s);
			}

	        public static EnumSet<FaceMask> get(Integer index)
	        {
	                EnumSet<FaceMask> enumsSet = EnumSet.allOf(FaceMask.class);
	                for(Entry<Integer,FaceMask> entry: lookup.entrySet())
	                {
	                        if((entry.getKey().intValue() | index) != index)
	                        {
	                                enumsSet.remove(entry.getValue());
	                        }
	                }
	                return enumsSet;
	        }

	        public static int getIndex(EnumSet<FaceMask> enumSet)
	        {
	                int ret = 0;
	                for(FaceMask s: enumSet)
	                {
	                        ret |= s.getIndex();
	                }
	                return ret;
	        }	        
	    }

	    