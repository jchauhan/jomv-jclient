/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering;

import com.ngt.jopenmetaverse.shared.util.Utils;

// [StructLayout(LayoutKind.Sequential)]
public  class Color4b
{
	public byte R;
	public byte G;
	public byte B;
	public byte A;

	public Color4b(byte r, byte g, byte b, byte a) {
		super();
		R = r;
		G = g;
		B = b;
		A = a;
	}

	public byte[] toBytes()
	{
		return new byte[]{R, G, B, A};
	}
	
	@Override
	public String toString()
	{
		return String.format("R: %d G %d B %d A %d", Utils.ubyteToInt(R), Utils.ubyteToInt(G), 
				Utils.ubyteToInt(B), Utils.ubyteToInt(A));
	}
	
}

//			    [StructLayout(LayoutKind.Explicit)]
