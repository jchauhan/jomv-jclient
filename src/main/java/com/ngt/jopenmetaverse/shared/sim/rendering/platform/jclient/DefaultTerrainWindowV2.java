/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering.platform.jclient;

import java.io.File;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import javax.imageio.ImageIO;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;

import com.ngt.jopenmetaverse.shared.sim.GridClient;
import com.ngt.jopenmetaverse.shared.sim.Settings;
import com.ngt.jopenmetaverse.shared.sim.imaging.IBitmap;
import com.ngt.jopenmetaverse.shared.sim.imaging.platform.jclient.BitmapBufferedImageImpl;
import com.ngt.jopenmetaverse.shared.sim.rendering.RHelp;
import com.ngt.jopenmetaverse.shared.sim.rendering.RenderTerrain;
import com.ngt.jopenmetaverse.shared.sim.rendering.RenderingContext;
import com.ngt.jopenmetaverse.shared.sim.rendering.ShaderProgram;
import com.ngt.jopenmetaverse.shared.sim.rendering.math.ConversionUtils;
import com.ngt.jopenmetaverse.shared.sim.rendering.math.FloatMatrix;
import com.ngt.jopenmetaverse.shared.sim.rendering.math.FloatMatrix4x4;
import com.ngt.jopenmetaverse.shared.sim.rendering.math.MatrixUtil;
import com.ngt.jopenmetaverse.shared.sim.rendering.math.Sphere;
import com.ngt.jopenmetaverse.shared.sim.rendering.math.Vector3f;
import com.ngt.jopenmetaverse.shared.sim.rendering.math.Vectorf;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.InterleavedVBO;


public class DefaultTerrainWindowV2 extends GLApp{

	private RenderingContext renderingContext;
	private ShaderProgram shaderProgram;
	private int vertexPositionAttribute;
	private int textureCoordAttribute;
	private int vertexNormalAttribute;
	
	private FloatBuffer resultingMatrixBuffer;
	private FloatBuffer normalMatrixBuffer;

	private int projectionMatrixUniform;
	private int textureUniform;	
	private int uAmbientColorUniform;
	private int uLightingDirectionUniform;
	private int uDirectionalColorUniform;
	private int uUseLightingUniform;
	private int normalMatrixUniform;


	private int speedX = 0;
	private int speedY = 0;
	private int angleX = 0;
	private int angleY = 0;
	private int angleZ = 0;
	private float translateX = 0;
	private float translateY = 0;
	private float translateZ = -6;
	private int currentTexture = 0;

	//Input Values for Lighting Effect
	int uUseLighting = 1;
	private Vectorf lightingDirection = new Vector3f(-1, -1, -1);
	private float directionalColorRed = 0.8f;
	private float directionalColorGreen = 0.8f;
	private float directionalColorBlue = 0.8f;
	private float ambientColorRed = 0.5f;
	private float ambientColorGreen = 0.5f;
	private float ambientColorBlue = 0.5f;

	private FloatMatrix perspectiveMatrix;
	private FloatMatrix translationMatrix;
	private FloatMatrix rotationMatrix;
	private FloatMatrix resultingMatrix;

	
	RenderTerrain renderTerrain;

	/*
	 * Temporary and Misc 
	 */
	IntBuffer tmpIntBuffer;


	public DefaultTerrainWindowV2(GridClient client, IBitmap terrainImage, InterleavedVBO vertexVBO, short[] vertexIndices)
	{
		renderTerrain = new RenderTerrain(client, vertexVBO, vertexIndices, terrainImage);
	}


	/**
	 * Initialize the scene.  Called by GLApp.run().  For now the default
	 * settings will be fine, so no code here.
	 * @throws Exception 
	 */
	public void setup() throws Exception
	{
		GL11.glEnableClientState(GL11.GL_VERTEX_ARRAY);
		GL11.glEnableClientState(GL11.GL_NORMAL_ARRAY);
		GL11.glEnableClientState(GL11.GL_COLOR_ARRAY);
		GL11.glEnableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
		//    	initParams(); //done by initGL
		initShaders();
		renderTerrain.Initialize();
		
		renderingContext =  new  RenderingContext(null,
				shaderProgram, projectionMatrixUniform,
				textureUniform, uAmbientColorUniform,
				uLightingDirectionUniform, uDirectionalColorUniform,
				uUseLightingUniform, normalMatrixUniform,
				vertexPositionAttribute, textureCoordAttribute,
				vertexNormalAttribute);
		
		initBuffers();
		initTextures();
	}

	/**
	 * Render one frame.  Called by GLApp.run().
	 */
	public void draw() {
		System.out.println("Drawing ...");
		angleX = (angleX + speedX) % 360;
		angleY = (angleY + speedY) % 360;
		// angleZ=(angleZ+2)%360;

		// Clear screen and depth buffer
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);

//		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vertexInterleavedId);
//
//		GL20.glVertexAttribPointer(vertexNormalAttribute, vertexVBO.getVertexNormalSize(), GL11.GL_FLOAT, false, 
//				vertexVBO.getStride(), vertexVBO.getVertexNormalOffet());
//		GL20.glVertexAttribPointer(vertexPositionAttribute, vertexVBO.getVertexSize(), GL11.GL_FLOAT, false, 
//				vertexVBO.getStride(), vertexVBO.getVertexOffset());		
//		GL20.glVertexAttribPointer(textureCoordAttribute, vertexVBO.getTexCoordSize(), GL11.GL_FLOAT, false, 
//				vertexVBO.getStride(), vertexVBO.getTexCoordOffset());

		float[] resultingMatrixData = updateView();
		resultingMatrixBuffer.position(0);
		resultingMatrixBuffer.put(resultingMatrixData);
		resultingMatrixBuffer.position(0);

		GL20.glUniformMatrix4(projectionMatrixUniform, false, resultingMatrixBuffer);


		// Set the lighting related uniforms
		GL20.glUniform1i(uUseLightingUniform, uUseLighting);
		Vectorf adjustedLightDirection = lightingDirection.toUnitVector()
				.multiply(-1);
		float[] flatLightDirection = adjustedLightDirection.toArray();
		GL20.glUniform3f(uLightingDirectionUniform, flatLightDirection[0], flatLightDirection[1],
				flatLightDirection[2]);		
		GL20.glUniform3f(uAmbientColorUniform, ambientColorRed, ambientColorGreen, ambientColorBlue);		
		GL20.glUniform3f(uDirectionalColorUniform, directionalColorRed, directionalColorGreen, directionalColorBlue);	

		//Normal matrix to adjust the directional light depending upon rotation
		FloatMatrix4x4 normalMatrix = ((FloatMatrix4x4)rotationMatrix).inverse();
		normalMatrix = normalMatrix.transpose();
		float[] normalMatrixData = normalMatrix.getColumnWiseFlatData();
		normalMatrixBuffer.position(0);
		normalMatrixBuffer.put(normalMatrixData);
		normalMatrixBuffer.position(0);

		GL20.glUniformMatrix4(normalMatrixUniform, false, normalMatrixBuffer);
		checkErrors("Drawing.Initializing NormalMatrixUniform");


//		// Bind the texture to texture unit 0
//		GL13.glActiveTexture(GL13.GL_TEXTURE0);
//		GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureId[0]);
//		shaderProgram.setUniform1(textureUniform, 0);
//
//		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vertexIndicesBufferId);
//		checkErrors("After Binding Element Array Buffer");
//		GL11.glDrawElements(GL11.GL_TRIANGLES, vertexIndices.length, GL11.GL_UNSIGNED_SHORT, 0);
//		GL11.glFlush();
		
		renderTerrain.Render(renderingContext);
		
	}

	public float[]  updateView()
	{
		perspectiveMatrix = MatrixUtil.createPerspectiveMatrix(45, getWidth()/getHeight(), 0.1f, 100);
		translationMatrix = MatrixUtil.createTranslationMatrix(translateX, translateY, translateZ);
		rotationMatrix = MatrixUtil.createRotationMatrix(angleX, angleY, angleZ);
		resultingMatrix = perspectiveMatrix.multiply(translationMatrix).multiply(rotationMatrix);
		float[] resultingMatrixData = resultingMatrix.getColumnWiseFlatData(); 
		return resultingMatrixData;
	}

	@Override
	public void keyDown(int keycode) {
		if(keycode == Keyboard.KEY_UP)
			translateY += 1;
		else if(keycode == Keyboard.KEY_DOWN)
			translateY -= 1;
		else if(keycode == Keyboard.KEY_RIGHT)
			translateX += 1;
		else if(keycode == Keyboard.KEY_LEFT)
			translateX -= 1;
		else if(keycode == Keyboard.KEY_W)
			translateZ += 1;
		else if(keycode == Keyboard.KEY_S)
			translateZ -= 1;

		else if(keycode == Keyboard.KEY_E)
			angleX += 5;
		else if(keycode == Keyboard.KEY_D)
			angleX -= 5;

		else if(keycode == Keyboard.KEY_Q)
			angleZ += 5;
		else if(keycode == Keyboard.KEY_A)
			angleZ -= 5;

		else if(keycode == Keyboard.KEY_R)
			angleY += 5;
		else if(keycode == Keyboard.KEY_F)
			angleY -= 5; 
	}


	/**
	 * Creates the ShaderProgram used by the example to render.
	 * @throws Exception 
	 */
	 private void initShaders() throws Exception {

		 shaderProgram = new ShaderProgram();
		 boolean shaderLoadingStatus = shaderProgram.Load(new String[] {Settings.RESOURCE_DIR + "/shaders/vertex-shader-2.vert"
				 , Settings.RESOURCE_DIR + "/shaders/fragment-shader-2.frag"}, 
				 new String[] {"vertexPosition", "texPosition", "aVertexNormal"});

		 if(!shaderLoadingStatus)
			 throw new Exception("Shader could not be loaded");

		 shaderProgram.Start();

		 vertexPositionAttribute = shaderProgram.getAttribLocation("vertexPosition");
		 GL20.glEnableVertexAttribArray(vertexPositionAttribute);

		 textureCoordAttribute = shaderProgram.getAttribLocation( "texPosition");
		 GL20.glEnableVertexAttribArray(textureCoordAttribute);

		 vertexNormalAttribute = shaderProgram.getAttribLocation( "aVertexNormal");
		 GL20.glEnableVertexAttribArray(vertexNormalAttribute);


		 // get the position of the projectionMatrix uniform.
		 projectionMatrixUniform = shaderProgram.getUniformLocation("projectionMatrix");

		 // get the position of the tex uniform.
		 textureUniform = shaderProgram.getUniformLocation( "tex");

		 uUseLightingUniform = shaderProgram.getUniformLocation( "uUseLighting");
		 uAmbientColorUniform = shaderProgram.getUniformLocation( "uAmbientColor");
		 uLightingDirectionUniform = shaderProgram.getUniformLocation( "uLightingDirection");
		 uDirectionalColorUniform = shaderProgram.getUniformLocation( "uDirectionalColor");

		 normalMatrixUniform = shaderProgram.getUniformLocation( "normalMatrix");
		 checkErrors("Getting Shaders UniformLocation ");


	 }

	 /**
	  * Initializes the texture of this example.
	  * @throws Exception 
	  */
	 private void initTextures() throws Exception {
//		 if(terrainImage == null)
//			 terrainImage = new BitmapBufferedImageImpl(ImageIO.read(new File(Settings.RESOURCE_DIR + "/textures/moon.jpeg")));
//
//		 textureId[0] = RHelp.GLLoadImage(terrainImage, true);
//
//		 GL11.glEnable(GL11.GL_TEXTURE_2D);
//		 GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureId[0]);
	 }

	 /**
	  * Initializes the buffers for vertex coordinates, normals and texture
	  * coordinates.
	  */
	 private void initBuffers() {
		 tmpIntBuffer = ByteBuffer
				 .allocateDirect(1*4).order(ByteOrder.nativeOrder()).asIntBuffer();

		 float[] resultingMatrixData = updateView();
		 resultingMatrixBuffer = ByteBuffer.allocateDirect(resultingMatrixData.length*4).order(ByteOrder.nativeOrder()).asFloatBuffer();
		 resultingMatrixBuffer.put(resultingMatrixData);
		 resultingMatrixBuffer.position(0);

		 FloatMatrix4x4 normalMatrix = ((FloatMatrix4x4)rotationMatrix).inverse();
		 normalMatrix = normalMatrix.transpose();
		 float[] normalMatrixData = normalMatrix.getColumnWiseFlatData();
		 normalMatrixBuffer = ByteBuffer.allocateDirect(normalMatrixData.length*4).order(ByteOrder.nativeOrder()).asFloatBuffer();
		 normalMatrixBuffer.put(normalMatrixData);
		 normalMatrixBuffer.position(0);

		 //        initVertexBuffer(tmpIntBuffer);
		 //        initVertexTextureCoordBuffer(tmpIntBuffer);
		 //        initVertexNormalBufferBuffer(tmpIntBuffer);

		 checkErrors("After Initializing BUffers");
	 }

	 //	private void initVertexBuffer(IntBuffer tmpIntBuffer)
	 //	{
	 //        tmpIntBuffer.position(0);
	 //        GL15.glGenBuffers(tmpIntBuffer);
	 //        vertexBufferId = tmpIntBuffer.get(0);
	 //        
	 //
	 //        vertexBuffer = ByteBuffer.allocateDirect(cube.getVertices().length*4).order(ByteOrder.nativeOrder()).asFloatBuffer();
	 //        vertexBuffer.put(cube.getVertices());
	 //        vertexBuffer.position(0);
	 //        
	 //        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vertexBufferId);
	 //        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, vertexBuffer, GL15.GL_STATIC_DRAW);
	 //	}
	 //	
	 //	private void initVertexTextureCoordBuffer(IntBuffer tmpIntBuffer)
	 //	{
	 //        tmpIntBuffer.position(0);
	 //        GL15.glGenBuffers(tmpIntBuffer);
	 //        vertexTextureCoordBufferId = tmpIntBuffer.get(0);
	 //		
	 //        vertexTextureCoordBuffer = ByteBuffer.allocateDirect(cube.getTexCoords().length*4).order(ByteOrder.nativeOrder()).asFloatBuffer();
	 //        vertexTextureCoordBuffer.put(cube.getTexCoords());
	 //        vertexTextureCoordBuffer.position(0);
	 //        
	 //        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vertexTextureCoordBufferId);
	 //        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, vertexTextureCoordBuffer, GL15.GL_STATIC_DRAW);
	 //	}
	 //
	 //	
	 //	private void initVertexNormalBufferBuffer(IntBuffer tmpIntBuffer)
	 //	{
	 //        tmpIntBuffer.position(0);
	 //        GL15.glGenBuffers(tmpIntBuffer);
	 //        vertexNormalBufferId = tmpIntBuffer.get(0);
	 //		
	 //        vertexNormalBuffer = ByteBuffer.allocateDirect(cube.getVertexNormals().length*4).order(ByteOrder.nativeOrder()).asFloatBuffer();
	 //        vertexNormalBuffer.put(cube.getVertexNormals());
	 //        vertexNormalBuffer.position(0);
	 //        
	 //        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vertexNormalBufferId);
	 //        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, vertexNormalBuffer, GL15.GL_STATIC_DRAW);
	 //	}

	 private void checkErrors(String locaton) {
		 int error = GL11.glGetError();
		 if (error != GL11.GL_NO_ERROR) {
			 String message = "@ " + locaton + " OpenGL Error: " + error;
			 switch(error)
			 {
			 case GL11.GL_INVALID_OPERATION:
			 {
				 message += " INVALID_OPERATION";
				 break;
			 }
			 case GL11.GL_INVALID_ENUM:
			 {
				 message += " INVALID_ENUM";
				 break;
			 }
			 case GL11.GL_INVALID_VALUE:
			 {
				 message += " INVALID_VALUE";
				 break;
			 }
			 case GL11.GL_OUT_OF_MEMORY:
			 {
				 message += " OUT_OF_MEMORY";
				 break;
			 }
			 }			
			 throw new RuntimeException(message);
		 }
	 }
}