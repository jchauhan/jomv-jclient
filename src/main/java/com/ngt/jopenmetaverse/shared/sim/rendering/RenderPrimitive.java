/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.List;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;

import com.ngt.jopenmetaverse.shared.protocol.primitives.Enums.TextureAnimMode;
import com.ngt.jopenmetaverse.shared.protocol.primitives.Primitive;
import com.ngt.jopenmetaverse.shared.protocol.primitives.TextureEntryFace;
import com.ngt.jopenmetaverse.shared.sim.rendering.math.FloatMatrix;
import com.ngt.jopenmetaverse.shared.sim.rendering.math.FloatMatrix4x4;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.Face;
import com.ngt.jopenmetaverse.shared.types.Color4;
import com.ngt.jopenmetaverse.shared.types.EnumsPrimitive.PrimType;
import com.ngt.jopenmetaverse.shared.types.Quaternion;
import com.ngt.jopenmetaverse.shared.types.Vector3;
import com.ngt.jopenmetaverse.shared.util.Utils;

public class RenderPrimitive extends SceneObject
{
	//region Public fields
	/// <summary>Base simulator object</summary>
	public Primitive Prim;
	public List<Face> Faces;
	/// <summary>Is this object attached to an avatar</summary>
	public boolean Attached;
	/// <summary>Do we know if object is attached</summary>
	public boolean AttachedStateKnown;
	/// <summary>Are meshes constructed and ready for this prim</summary>
	public boolean Meshed;
	/// <summary>Process of creating a mesh is underway</summary>
	public boolean Meshing;
	//endregion Public fields

	public SceneObject parentSceneObject;

	//NIO buffers
	FloatBuffer floatBuffer;
	FloatBuffer materialDataBuffer;

	//Default Data
	private final float[] materialData = new float[] { 0.5f, 0.5f, 0.5f, 1f };


	//region Private fields
	int prevTEHash;
	int prevSculptHash;
	int prevShapeHash;
	//endregion Private fields

	/// <summary>
	/// Default constructor
	/// </summary>
	public RenderPrimitive()
	{
		Type = SceneObjectType.Primitive;
		//Allocate a buffer to store Scale Translation and Rotation matrix
		floatBuffer = ByteBuffer.allocateDirect(16 * 4)
				.order(ByteOrder.nativeOrder()).asFloatBuffer();
		materialDataBuffer = ByteBuffer
				.allocateDirect(materialData.length*4).order(ByteOrder.nativeOrder()).asFloatBuffer();
		materialDataBuffer.put(materialData);
		materialDataBuffer.position(0);
	}

	public SceneObject getParentSceneObject() {
		return parentSceneObject;
	}

	public void setParentSceneObject(SceneObject parentSceneObject) {
		this.parentSceneObject = parentSceneObject;
	}

	/// <summary>
	/// Remove any GL resource we may still have in use
	/// </summary>
	@Override
	public void Dispose()
	{
		if (Faces != null)
		{
			for (Face f : Faces)
			{
				if (f.UserData != null && f.UserData instanceof FaceData)
				{
					FaceData data = (FaceData)f.UserData;
					data.Dispose();
					data = null;
				}
			}
			Faces = null;
		}

		if(floatBuffer != null)
			floatBuffer.clear();

		if(materialDataBuffer != null)
			materialDataBuffer.clear();
		super.Dispose();
	}

	/// <summary>
	/// Simulator object that is basis for this SceneObject
	/// </summary>
	@Override
	public Primitive getBasePrim()
	{
		return Prim; 
	}

	public void setBasePrim(Primitive value)
	{
		Prim = value;

		int TEHash = Prim.Textures == null ? 0 : Prim.Textures.hashCode();
		int sculptHash, shapeHash;

		if (Meshed)
		{
			if (Prim.getType() == PrimType.Sculpt || Prim.getType() == PrimType.Mesh)
			{
				sculptHash = Prim.Sculpt.hashCode();
				if (Prim.Sculpt.hashCode() != prevSculptHash || TEHash != prevTEHash)
				{
					Meshed = false;
				}
				prevSculptHash = sculptHash;
			}
			else
			{
				shapeHash = Prim.PrimData.hashCode();
				if (shapeHash != prevShapeHash)
				{
					Meshed = false;
				}
				else if (TEHash != prevTEHash)
				{
					Meshed = false;
				}
				prevShapeHash = shapeHash;
			}
		}
		prevTEHash = TEHash;
	}

	/// <summary>
	/// Set initial state of the object
	/// </summary>
	@Override
	public void Initialize()
	{
		AttachedStateKnown = false;
		super.Initialize();
	}

	/// <summary>
	/// Render Primitive
	/// </summary>
	/// <param name="pass">Which pass are we currently in</param>
	/// <param name="pickingID">ID used to identify which object was picked</param>
	/// <param name="scene">Main scene renderer</param>
	/// <param name="time">Time it took to render the last frame</param>
	@Override
	public void Render(RenderPass pass, int pickingID, RenderingContext scene, float time) throws Exception
	{
		if (!RenderSettings.AvatarRenderingEnabled && Attached) return;

		// Prim roation and position and scale
		FloatMatrix4x4 srt4x4Matrix= Math3D.convertToFloatMatrix4x4(Math3D.CreateSRTMatrix(Prim.Scale, RenderRotation, RenderPosition));

		//		FloatMatrix totalMatrix = scene.getPerspectiveMatrix().multiply(scene.getTranslationMatrix()).multiply(scene.getRotationMatrix().multiply(srt2Matrix));
		FloatMatrix totalMatrix = scene.getResultingMatrix().multiply(srt4x4Matrix);
		floatBuffer.position(0);
		floatBuffer.put(totalMatrix.getColumnWiseFlatData()).position(0);

		GL20.glUniformMatrix4(scene.getProjectionMatrixUniform(), false, floatBuffer);


		// Do we have animated texture on this face
		boolean animatedTexture = false;

		// Initialise flags tracking what type of faces this prim has
		if (pass == RenderPass.Simple)
		{
			HasSimpleFaces = false;
		}
		else if (pass == RenderPass.Alpha)
		{
			HasAlphaFaces = false;
		}
		else if (pass == RenderPass.Invisible)
		{
			HasInvisibleFaces = false;
		}

		// Draw the prim faces
		for (int j = 0; j < Faces.size(); j++)
		{
			TextureEntryFace teFace = Prim.Textures.GetFace(j);
			Face face = Faces.get(j);
			FaceData data = (FaceData)face.UserData;

			if (data == null)
				continue;

			if (teFace == null)
				continue;

			// Don't render transparent faces
			Color4 RGBA = teFace.getRGBA();

			if (data.TextureInfo.FullAlpha || RGBA.getA() <= 0.01f) continue;

			boolean switchedLightsOff = false;

			if (pass == RenderPass.Picking)
			{
				data.PickingID = pickingID;
				byte[] primNrBytes = new byte[2];
				Utils.uint16ToBytes(pickingID, primNrBytes, 0);
				//                    byte[] faceColor = new byte[] { primNrBytes[0], primNrBytes[1], (byte)j, (byte)255 };
				GL11.glColor4f( primNrBytes[0], primNrBytes[1], (byte)j, (byte)255);
			}
			else if (pass == RenderPass.Invisible)
			{
				if (!data.TextureInfo.IsInvisible) continue;
				HasInvisibleFaces = true;
			}
			else
			{
				//				System.out.println("Pass is Simple..");
				if (data.TextureInfo.IsInvisible)
				{
					System.out.println("Primitive is invisible.. so skipping texturing...");
					continue;
				}
				boolean belongToAlphaPass = (RGBA.getA() < 0.99f) || (data.TextureInfo.HasAlpha && !data.TextureInfo.IsMask);
				//TODO only for debugging
				if (belongToAlphaPass && pass != RenderPass.Alpha) continue;
				if (!belongToAlphaPass && pass == RenderPass.Alpha) continue;

				//				System.out.println("Going to Activate Texture");

				if (pass == RenderPass.Simple)
				{
					HasSimpleFaces = true;
				}
				else if (pass == RenderPass.Alpha)
				{
					HasAlphaFaces = true;
				}

				if (teFace.getFullbright())
				{
					GL11.glDisable(GL11.GL_LIGHTING );
					switchedLightsOff = true;
				}

				float shiny = 0f;
				switch (teFace.getShiny())
				{
				case High:
					shiny = 0.96f;
					break;

				case Medium:
					shiny = 0.64f;
					break;

				case Low:
					shiny = 0.24f;
					break;
				}

				if (shiny > 0f)
				{
					//TODO need to handle
					//                        scene.StartShiny();
				}
				GL11.glMaterialf(GL11.GL_FRONT, GL11.GL_SHININESS, shiny);
				GL11.glColor4f(RGBA.getR(), RGBA.getG(), RGBA.getB(), RGBA.getA());

				//Optmized by keeping a instance buffer, as materialData is constant
				//				float[] materialData = new float[] { 0.5f, 0.5f, 0.5f, 1f };
				//				final FloatBuffer materialDataBuffer = ByteBuffer
				//						.allocateDirect(materialData.length*4).order(ByteOrder.nativeOrder()).asFloatBuffer();
				//				materialDataBuffer.put(materialData);
				materialDataBuffer.position(0);
				GL11.glMaterial(GL11.GL_FRONT, GL11.GL_SPECULAR, materialDataBuffer);

				//				System.out.println("Trying to Activate the Texture " + data.TextureInfo.TextureID 
				//						+ " --> "+ data.TextureInfo.TexturePointer);

				if (data.TextureInfo.TexturePointer == 0)
				{
					TextureInfo teInfo;
					if ((teInfo = scene.textureManager.getTextureInfo(teFace.getTextureID()))!=null)
					{
						data.TextureInfo = teInfo;
					}
				}

				if (data.TextureInfo.TexturePointer == 0)
				{
					System.out.println("Texture pointer is 0 .. requesting to download.." + teFace.getTextureID());
					GL11.glDisable(GL11.GL_TEXTURE_2D);
					if (!data.TextureInfo.FetchFailed)
					{                        	
						scene.textureManager.requestDownloadTexture(new TextureLoadItem(data, this.Prim, teFace));
					}
					else
					{
						System.out.println("Texture Download failed for " + teFace.getTextureID());
					}
				}
				else
				{
					//					System.out.println("Texture pointer is not 0 ..");

					// Is this face using texture animation

					if ((TextureAnimMode.and(Prim.TextureAnim.Flags, TextureAnimMode.ANIM_ON)) != 0
							&& (Prim.TextureAnim.Face == j || Prim.TextureAnim.Face == 255))
					{
						if (data.AnimInfo == null)
						{
							data.AnimInfo = new TextureAnimationInfo();
						}
						data.AnimInfo.PrimAnimInfo = Prim.TextureAnim;
						data.AnimInfo.Step(time);
						animatedTexture = true;
					}
					else if (data.AnimInfo != null) // Face texture not animated. Do we have previous anim setting?
					{
						data.AnimInfo = null;
					}

					//					System.out.println("Activating the Texture");

					GL11.glEnable(GL11.GL_TEXTURE_2D);
					GL13.glActiveTexture(GL13.GL_TEXTURE0);
					GL11.glBindTexture(GL11.GL_TEXTURE_2D, data.TextureInfo.TexturePointer);
					scene.getShaderProgram().setUniform1(scene.getTextureUniform(), 0);

				}
			}

			if (data.CheckVBO(face))
			{
				GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, data.VertexVBO);
				GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER,  data.IndexVBO);

				GL20.glVertexAttribPointer(scene.getVertexNormalAttribute(), 3, GL11.GL_FLOAT, false, FaceData.VertexSize, 12);
				GL20.glVertexAttribPointer(scene.getVertexPositionAttribute(), 3, GL11.GL_FLOAT, false, FaceData.VertexSize, 0);
				GL20.glVertexAttribPointer(scene.getTextureCoordAttribute(), 2, GL11.GL_FLOAT, false, FaceData.VertexSize, 24);

				GL11.glDrawElements(GL11.GL_TRIANGLES, face.Indices.size(), GL11.GL_UNSIGNED_SHORT, 0);
			}
			GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
			GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER,  0);


			if (switchedLightsOff)
			{
				GL11.glEnable(GL11.GL_LIGHTING);
				switchedLightsOff = false;
			}

		}

		GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
		RHelp.ResetMaterial();

		// Reset texture coordinates if we modified them in texture animation
		if (animatedTexture)
		{
			GL11.glMatrixMode(GL11.GL_TEXTURE);
			GL11.glLoadIdentity();
			GL11.glMatrixMode(GL11.GL_MODELVIEW);
		}

		// Pop the prim matrix
		//GL11.glPopMatrix();

		super.Render(pass, pickingID, scene, time);
	}

	/// <summary>
	/// String representation of the object
	/// </summary>
	/// <returns>String containing local ID of the object and it's distance from the camera</returns>
	@Override
	public String toString()
	{

		long id = Prim == null ? 0 : Prim.LocalID;
		float distance = (float)Math.sqrt(DistanceSquared);
		return String.format("LocalID: %d, distance %f", id, distance);
	}


	/// <summary>
	/// Calculates finar rendering position for objects on the scene
	/// </summary>
	/// <param name="obj">SceneObject whose position is calculated</param>
	/// <param name="pos">Rendering position</param>
	/// <param name="rot">Rendering rotation</param>
	@Override
	public void calPrimPosAndRot(Vector3 cameraRenderPosition)
	{
		// Sanity check
		//        if (obj == null)
		//        {
		//            pos = RHelp.InvalidPosition;
		//            rot = Quaternion.Identity;
		//            return;
		//        }

		if (getBasePrim().ParentID == 0)
		{
			// We are the root prim, return our interpolated position
			RenderPosition = new Vector3(InterpolatedPosition);
			RenderRotation = new Quaternion(InterpolatedRotation);
			return;
		}
		else
		{
			RenderPosition = new Vector3(RHelp.InvalidPosition);
			RenderRotation = new Quaternion(Quaternion.Identity);

			// Not root, find our parent
			SceneObject p = getParentSceneObject();
			if (p == null) 
			{
				System.out.println("Parent Primitive is null...");
				return;
			}

			// If we don't know parent position, recursively find out
			if (!p.PositionCalculated)
			{
				p.calPrimPosAndRot(cameraRenderPosition);
				p.DistanceSquared = Vector3.distanceSquared(cameraRenderPosition, p.RenderPosition);
				p.PositionCalculated = true;
			}

			Vector3 parentPos = new Vector3(p.RenderPosition);
			Quaternion parentRot = new Quaternion(p.RenderRotation);

			if (p instanceof RenderPrimitive)
			{
				// Child prim (our parent is another prim here)
				RenderPosition = Vector3.add(parentPos, Vector3.multiply(InterpolatedPosition, parentRot));
				RenderRotation = Quaternion.multiply(parentRot, InterpolatedRotation);
			}
			else if (p instanceof RenderAvatar)
			{
				//TODO only for debug
				RenderPosition = new Vector3(p.RenderPosition);
				RenderRotation = new Quaternion(p.RenderRotation);
				//TODO implement
				//                // Calculating position and rotation of the root prim of an attachment here
				//                // (our parent is an avatar here)
				//                RenderAvatar parentav = (RenderAvatar)p;
				//
				//                // Check for invalid attachment point
				//                int attachment_index = (int)getBasePrim().PrimData.AttachmentPoint;
				//                if (attachment_index >= GLAvatar.attachment_points.Count()) return;
				//                attachment_point apoint = GLAvatar.attachment_points[attachment_index];
				//                skeleton skel = parentav.glavatar.skel;
				//                if (!skel.mBones.ContainsKey(apoint.joint)) return;
				//
				//                // Bone position and rotation
				//                Bone bone = skel.mBones[apoint.joint];
				//                Vector3 bpos = bone.getTotalOffset();
				//                Quaternion brot = bone.getTotalRotation();
				//
				//                // Start with avatar positon
				//                pos = parentPos;
				//                rot = parentRot;
				//
				//                // Move by pelvis offset
				//                pos -= parentav.glavatar.skel.getOffset("mPelvis") * rot;
				//                //rot = parentav.glavatar.skel.getRotation("mPelvis") * rot;
				//
				//                // Translate and rotate to the joint calculated position
				//                pos += bpos * rot;
				//                rot *= brot;
				//
				//                // Translate and rotate built in joint offset
				//                pos += apoint.position * rot;
				//                rot *= apoint.rotation;
				//
				//                // Translate and rotate from the offset from the attachment point
				//                // set in teh appearance editor
				//                pos += getBasePrim().Position * rot;
				//                rot *= getBasePrim().Rotation;

			}
			return;
		}
	}

}