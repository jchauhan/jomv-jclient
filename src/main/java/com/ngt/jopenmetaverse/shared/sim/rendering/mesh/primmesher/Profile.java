/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering.mesh.primmesher;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.ngt.jopenmetaverse.shared.util.FileUtils;
import com.ngt.jopenmetaverse.shared.util.Utils;

public class Profile
	    {
	        private final float twoPi = 2.0f * (float)Math.PI;

	        public String errorMessage = null;

	        public List<Coord> coords;
	        public List<Face> faces;
	        public List<Coord> vertexNormals;
	        public List<Float> us;
	        public List<UVCoord> faceUVs;
	        public List<Integer> faceNumbers;

	        // use these for making individual meshes for each prim face
	        public List<Integer> outerCoordIndices = null;
	        public List<Integer> hollowCoordIndices = null;
	        public List<Integer> cut1CoordIndices = null;
	        public List<Integer> cut2CoordIndices = null;

	        public Coord faceNormal = new Coord(0.0f, 0.0f, 1.0f);
	        public Coord cutNormal1 = new Coord();
	        public Coord cutNormal2 = new Coord();

	        public int numOuterVerts = 0;
	        public int numHollowVerts = 0;

	        public int outerFaceNumber = -1;
	        public int hollowFaceNumber = -1;

	        public boolean calcVertexNormals = false;
	        public int bottomFaceNumber = 0;
	        public int numPrimFaces = 0;

	        public Profile()
	        {
	            this.coords = new ArrayList<Coord>();
	            this.faces = new ArrayList<Face>();
	            this.vertexNormals = new ArrayList<Coord>();
	            this.us = new ArrayList<Float>();
	            this.faceUVs = new ArrayList<UVCoord>();
	            this.faceNumbers = new ArrayList<Integer>();
	        }

	        public Profile(int sides, float profileStart, float profileEnd, float hollow, 
	        		int hollowSides, boolean createFaces, boolean calcVertexNormals)
	        {
	            this.calcVertexNormals = calcVertexNormals;
	            this.coords = new ArrayList<Coord>();
	            this.faces = new ArrayList<Face>();
	            this.vertexNormals = new ArrayList<Coord>();
	            this.us = new ArrayList<Float>();
	            this.faceUVs = new ArrayList<UVCoord>();
	            this.faceNumbers = new ArrayList<Integer>();

	            Coord center = new Coord(0.0f, 0.0f, 0.0f);

	            List<Coord> hollowCoords = new ArrayList<Coord>();
	            List<Coord> hollowNormals = new ArrayList<Coord>();
	            List<Float> hollowUs = new ArrayList<Float>();

	            if (calcVertexNormals)
	            {
	                this.outerCoordIndices = new ArrayList<Integer>();
	                this.hollowCoordIndices = new ArrayList<Integer>();
	                this.cut1CoordIndices = new ArrayList<Integer>();
	                this.cut2CoordIndices = new ArrayList<Integer>();
	            }

	            boolean hasHollow = (hollow > 0.0f);

	            boolean hasProfileCut = (profileStart > 0.0f || profileEnd < 1.0f);

	            AngleList angles = new AngleList();
	            AngleList hollowAngles = new AngleList();

	            float xScale = 0.5f;
	            float yScale = 0.5f;
	            if (sides == 4)  // corners of a square are sqrt(2) from center
	            {
	                xScale = 0.707107f;
	                yScale = 0.707107f;
	            }

	            float startAngle = profileStart * twoPi;
	            float stopAngle = profileEnd * twoPi;
	            
	            try { angles.makeAngles(sides, startAngle, stopAngle); }
	            catch (Exception ex)
	            {

	                errorMessage = "makeAngles failed: Exception: " + ex
	                + "\nsides: " + sides + " startAngle: " + startAngle + " stopAngle: " + stopAngle;

	                return;
	            }

	            this.numOuterVerts = angles.angles.size();

	            // flag to create as few triangles as possible for 3 or 4 side profile
	            boolean simpleFace = (sides < 5 && !hasHollow && !hasProfileCut);

	            if (hasHollow)
	            {
	                if (sides == hollowSides)
	                    hollowAngles = new AngleList(angles);
	                else
	                {
	                    try { hollowAngles.makeAngles(hollowSides, startAngle, stopAngle); }
	                    catch (Exception ex)
	                    {
	                        errorMessage = "makeAngles failed: Exception: " + ex
	                        + "\nsides: " + sides + " startAngle: " + startAngle + " stopAngle: " + stopAngle;

	                        return;
	                    }
	                }
	                this.numHollowVerts = hollowAngles.angles.size();
	            }
	            else if (!simpleFace)
	            {
	                this.coords.add(new Coord(center));
	                if (this.calcVertexNormals)
	                    this.vertexNormals.add(new Coord(0.0f, 0.0f, 1.0f));
	                this.us.add(0.0f);
	            }

	            float z = 0.0f;

	            Angle angle;
	            Coord newVert = new Coord();
	            if (hasHollow && hollowSides != sides)
	            {
	                int numHollowAngles = hollowAngles.angles.size();
	                for (int i = 0; i < numHollowAngles; i++)
	                {
	                    angle = new Angle(hollowAngles.angles.get(i));
	                    newVert.X = hollow * xScale * angle.X;
	                    newVert.Y = hollow * yScale * angle.Y;
	                    newVert.Z = z;

	                    hollowCoords.add(new Coord(newVert));
	                    if (this.calcVertexNormals)
	                    {
	                        if (hollowSides < 5)
	                            hollowNormals.add(new Coord(hollowAngles.normals.get(i)).Invert());
	                        else
	                            hollowNormals.add(new Coord(-angle.X, -angle.Y, 0.0f));

	                        if (hollowSides == 4)
	                            hollowUs.add(angle.angle * hollow * 0.707107f);
	                        else
	                            hollowUs.add(angle.angle * hollow);
	                    }
	                }
	            }

	            int index = 0;
	            int numAngles = angles.angles.size();

	            for (int i = 0; i < numAngles; i++)
	            {
	                angle = new Angle(angles.angles.get(i));
	                newVert.X = angle.X * xScale;
	                newVert.Y = angle.Y * yScale;
	                newVert.Z = z;
	                this.coords.add(new Coord(newVert));
	                if (this.calcVertexNormals)
	                {
	                    this.outerCoordIndices.add(this.coords.size() - 1);

	                    if (sides < 5)
	                    {
	                        this.vertexNormals.add(new Coord(angles.normals.get(i)));
	                        float u = angle.angle;
	                        this.us.add(u);
	                    }
	                    else
	                    {
	                        this.vertexNormals.add(new Coord(angle.X, angle.Y, 0.0f));
	                        this.us.add(angle.angle);
	                    }
	                }

	                if (hasHollow)
	                {
	                    if (hollowSides == sides)
	                    {
	                        newVert.X *= hollow;
	                        newVert.Y *= hollow;
	                        newVert.Z = z;
	                        hollowCoords.add(new Coord(newVert));
	                        if (this.calcVertexNormals)
	                        {
	                            if (sides < 5)
	                            {
	                                hollowNormals.add(new Coord(angles.normals.get(i)).Invert());
	                            }

	                            else
	                                hollowNormals.add(new Coord(-angle.X, -angle.Y, 0.0f));

	                            hollowUs.add(angle.angle * hollow);
	                        }
	                    }
	                }
	                else if (!simpleFace && createFaces && angle.angle > 0.0001f)
	                {
	                    Face newFace = new Face();
	                    newFace.v1 = 0;
	                    newFace.v2 = index;
	                    newFace.v3 = index + 1;
	                    this.faces.add(newFace);
	                }
	                index += 1;
	            }

	            if (hasHollow)
	            {
	            	Collections.reverse(hollowCoords);
	                if (this.calcVertexNormals)
	                {
	                	Collections.reverse(hollowNormals);
	                	Collections.reverse(hollowUs);
	                }

	                if (createFaces)
	                {
	                    int numTotalVerts = this.numOuterVerts + this.numHollowVerts;
	                    if (this.numOuterVerts == this.numHollowVerts)
	                    {
	                        Face newFace = new Face();

	                        for (int coordIndex = 0; coordIndex < this.numOuterVerts - 1; coordIndex++)
	                        {
	                        	newFace = new Face();
	                            newFace.v1 = coordIndex;
	                            newFace.v2 = coordIndex + 1;
	                            newFace.v3 = numTotalVerts - coordIndex - 1;
	                            this.faces.add(newFace);

	                            newFace = new Face();
	                            newFace.v1 = coordIndex + 1;
	                            newFace.v2 = numTotalVerts - coordIndex - 2;
	                            newFace.v3 = numTotalVerts - coordIndex - 1;
	                            this.faces.add(newFace);
	                        }
	                    }
	                    else
	                    {
	                        if (this.numOuterVerts < this.numHollowVerts)
	                        {
	                            Face newFace;
	                            int j = 0; // j is the index for outer vertices
	                            int maxJ = this.numOuterVerts - 1;
	                            for (int i = 0; i < this.numHollowVerts; i++) // i is the index for inner vertices
	                            {
	                                if (j < maxJ)
	                                    if (angles.angles.get(j + 1).angle - hollowAngles.angles.get(i).angle < hollowAngles.angles.get(i).angle - angles.angles.get(j).angle + 0.000001f)
	                                    {
	                                    	newFace = new Face();
	                                        newFace.v1 = numTotalVerts - i - 1;
	                                        newFace.v2 = j;
	                                        newFace.v3 = j + 1;

	                                        this.faces.add(newFace);
	                                        j += 1;
	                                    }
	                                newFace = new Face();
	                                newFace.v1 = j;
	                                newFace.v2 = numTotalVerts - i - 2;
	                                newFace.v3 = numTotalVerts - i - 1;

	                                this.faces.add(newFace);
	                            }
	                        }
	                        else // numHollowVerts < numOuterVerts
	                        {
	                            Face newFace;
	                            int j = 0; // j is the index for inner vertices
	                            int maxJ = this.numHollowVerts - 1;
	                            for (int i = 0; i < this.numOuterVerts; i++)
	                            {
	                                if (j < maxJ)
	                                    if (hollowAngles.angles.get(j + 1).angle - angles.angles.get(i).angle < angles.angles.get(i).angle - hollowAngles.angles.get(j).angle + 0.000001f)
	                                    {
	                                    	newFace = new Face();
	                                        newFace.v1 = i;
	                                        newFace.v2 = numTotalVerts - j - 2;
	                                        newFace.v3 = numTotalVerts - j - 1;

	                                        this.faces.add(newFace);
	                                        j += 1;
	                                    }
	                                
	                                newFace = new Face();
	                                newFace.v1 = numTotalVerts - j - 1;
	                                newFace.v2 = i;
	                                newFace.v3 = i + 1;

	                                this.faces.add(newFace);
	                            }
	                        }
	                    }
	                }

	                if (calcVertexNormals)
	                {
	                    for (Coord hc : hollowCoords)
	                    {
	                        this.coords.add(new Coord(hc));
	                        hollowCoordIndices.add(this.coords.size() - 1);
	                    }
	                }
	                else
	                    this.coords.addAll(hollowCoords);

	                if (this.calcVertexNormals)
	                {
	                    this.vertexNormals.addAll(hollowNormals);
	                    this.us.addAll(hollowUs);

	                }
	            }

	            if (simpleFace && createFaces)
	            {
	                if (sides == 3)
	                    this.faces.add(new Face(0, 1, 2));
	                else if (sides == 4)
	                {
	                    this.faces.add(new Face(0, 1, 2));
	                    this.faces.add(new Face(0, 2, 3));
	                }
	            }

	            if (calcVertexNormals && hasProfileCut)
	            {
	                int lastOuterVertIndex = this.numOuterVerts - 1;

	                if (hasHollow)
	                {
	                    this.cut1CoordIndices.add(0);
	                    this.cut1CoordIndices.add(this.coords.size() - 1);

	                    this.cut2CoordIndices.add(lastOuterVertIndex + 1);
	                    this.cut2CoordIndices.add(lastOuterVertIndex);

	                    this.cutNormal1.X = this.coords.get(0).Y - this.coords.get(this.coords.size() - 1).Y;
	                    this.cutNormal1.Y = -(this.coords.get(0).X - this.coords.get(this.coords.size() - 1).X);

	                    this.cutNormal2.X = this.coords.get(lastOuterVertIndex + 1).Y - this.coords.get(lastOuterVertIndex).Y;
	                    this.cutNormal2.Y = -(this.coords.get(lastOuterVertIndex + 1).X - this.coords.get(lastOuterVertIndex).X);
	                }

	                else
	                {
	                    this.cut1CoordIndices.add(0);
	                    this.cut1CoordIndices.add(1);

	                    this.cut2CoordIndices.add(lastOuterVertIndex);
	                    this.cut2CoordIndices.add(0);

	                    this.cutNormal1.X = this.vertexNormals.get(1).Y;
	                    this.cutNormal1.Y = -this.vertexNormals.get(1).X;

	                    this.cutNormal2.X = -this.vertexNormals.get(this.vertexNormals.size() - 2).Y;
	                    this.cutNormal2.Y = this.vertexNormals.get(this.vertexNormals.size() - 2).X;

	                }
	                this.cutNormal1.Normalize();
	                this.cutNormal2.Normalize();
	            }

	            this.MakeFaceUVs();

	            hollowCoords = null;
	            hollowNormals = null;
	            hollowUs = null;

	            if (calcVertexNormals)
	            { // calculate prim face numbers

	                // face number order is top, outer, hollow, bottom, start cut, end cut
	                // I know it's ugly but so is the whole concept of prim face numbers

	                int faceNum = 1; // start with outer faces
	                this.outerFaceNumber = faceNum;

	                int startVert = hasProfileCut && !hasHollow ? 1 : 0;
	                if (startVert > 0)
	                    this.faceNumbers.add(-1);
	                for (int i = 0; i < this.numOuterVerts - 1; i++)
	                    this.faceNumbers.add(sides < 5 && i <= sides ? faceNum++ : faceNum);

	                this.faceNumbers.add(hasProfileCut ? -1 : faceNum++);

	                if (sides > 4 && (hasHollow || hasProfileCut))
	                    faceNum++;

	                if (sides < 5 && (hasHollow || hasProfileCut) && this.numOuterVerts < sides)
	                    faceNum++;

	                if (hasHollow)
	                {
	                    for (int i = 0; i < this.numHollowVerts; i++)
	                        this.faceNumbers.add(faceNum);

	                    this.hollowFaceNumber = faceNum++;
	                }

	                this.bottomFaceNumber = faceNum++;

	                if (hasHollow && hasProfileCut)
	                    this.faceNumbers.add(faceNum++);

	                for (int i = 0; i < this.faceNumbers.size(); i++)
	                    if (this.faceNumbers.get(i) == -1)
	                        this.faceNumbers.set(i, faceNum++);

	                this.numPrimFaces = faceNum;
	            }

	        }

	        public void MakeFaceUVs()
	        {
	            this.faceUVs = new ArrayList<UVCoord>();
	            for (Coord c : this.coords)
	                this.faceUVs.add(new UVCoord(1.0f - (0.5f + c.X), 1.0f - (0.5f - c.Y)));
	        }

	        public Profile Copy()
	        {
	            return this.Copy(true);
	        }

	        public Profile Copy(boolean needFaces)
	        {
	            Profile copy = new Profile();

	            Coord c2;
//	            copy.coords.addAll(this.coords);
	            for(Coord c: this.coords)
	            {
	            	c2 = new Coord(c.X, c.Y, c.Z);
	            	copy.coords.add(c2);
	            }
	            
	            for(UVCoord u: this.faceUVs)
	            {
	            	copy.faceUVs.add(new UVCoord(u.U, u.V));
	            }
//	            copy.faceUVs.addAll(this.faceUVs);

	            if (needFaces)
	            {
//	                copy.faces.addAll(this.faces);
	            	for(Face f: this.faces)
	            	{
	            		Face f2 = new Face(f);
	            		copy.faces.add(f2);
	            	}
	            }
	            if ((copy.calcVertexNormals = this.calcVertexNormals) == true)
	            {
//	                copy.vertexNormals.addAll(this.vertexNormals);
	            	for(Coord c: this.vertexNormals)
	            	{
	            		copy.vertexNormals.add(new Coord(c));
	            	}
	                copy.faceNormal = this.faceNormal;
	                copy.cutNormal1 = this.cutNormal1;
	                copy.cutNormal2 = this.cutNormal2;
//	                copy.us.addAll(this.us);
	                for(Float f: this.us)
	                {
	                	copy.us.add(new Float(f.floatValue()));
	                }
//	                copy.faceNumbers.addAll(this.faceNumbers);
	                for(Integer i: this.faceNumbers)
	                {
	                	copy.faceNumbers.add(new Integer(i.intValue()));
	                }
	                
	                copy.cut1CoordIndices = Utils.deepCopy(this.cut1CoordIndices);
	                copy.cut2CoordIndices = Utils.deepCopy(this.cut2CoordIndices);
	                copy.hollowCoordIndices = Utils.deepCopy(this.hollowCoordIndices);
	                copy.outerCoordIndices = Utils.deepCopy(this.outerCoordIndices);
	            }
	            copy.numOuterVerts = this.numOuterVerts;
	            copy.numHollowVerts = this.numHollowVerts;

	            return copy;
	        }

	        public void addPos(Coord v)
	        {
	            this.addPos(v.X, v.Y, v.Z);
	        }

	        public void addPos(float x, float y, float z)
	        {
	            int i;
	            int numVerts = this.coords.size();
	            Coord vert;

	            for (i = 0; i < numVerts; i++)
	            {
	                vert = new Coord(this.coords.get(i));
	                vert.X += x;
	                vert.Y += y;
	                vert.Z += z;
	                this.coords.set(i, vert);
	            }
	        }

	        public void addRot(Quat q)
	        {
	            int i;
	            int numVerts = this.coords.size();

	            for (i = 0; i < numVerts; i++)
//	                this.coords[i] *= q;
	            	this.coords.set(i, Coord.multiply(this.coords.get(i), q));

	            if (this.calcVertexNormals)
	            {
	                int numNormals = this.vertexNormals.size();
	                for (i = 0; i < numNormals; i++)
//	                    this.vertexNormals[i] *= q;
	                	this.vertexNormals.set(i, Coord.multiply(this.vertexNormals.get(i), q));

	                
	            	this.faceNormal = Coord.multiply(this.faceNormal, q);
	            	this.cutNormal1 = Coord.multiply(this.cutNormal1, q);
	            	this.cutNormal2 = Coord.multiply(this.cutNormal2, q);

//	                this.faceNormal *= q;
//	                this.cutNormal1 *= q;
//	                this.cutNormal2 *= q;

	            }
	        }

	        public void Scale(float x, float y)
	        {
	            int i;
	            int numVerts = this.coords.size();
	            Coord vert;

	            for (i = 0; i < numVerts; i++)
	            {
	                vert = new Coord(this.coords.get(i));
	                vert.X *= x;
	                vert.Y *= y;
	                this.coords.set(i, vert);
	            }
	        }

	        /// <summary>
	        /// Changes order of the vertex indices and negates the center vertex normal. Does not alter vertex normals of radial vertices
	        /// </summary>
	        public void FlipNormals()
	        {
	            int i;
	            int numFaces = this.faces.size();
	            Face tmpFace;
	            int tmp;

	            for (i = 0; i < numFaces; i++)
	            {
	                tmpFace = new Face(this.faces.get(i));
	                tmp = tmpFace.v3;
	                tmpFace.v3 = tmpFace.v1;
	                tmpFace.v1 = tmp;
	                this.faces.set(i, tmpFace);
	            }

	            if (this.calcVertexNormals)
	            {
	                int normalCount = this.vertexNormals.size();
	                if (normalCount > 0)
	                {
	                    Coord n = new Coord(this.vertexNormals.get(normalCount - 1));
	                    n.Z = -n.Z;
	                    this.vertexNormals.set(normalCount - 1,  n);
	                }
	            }

	            this.faceNormal.X = -this.faceNormal.X;
	            this.faceNormal.Y = -this.faceNormal.Y;
	            this.faceNormal.Z = -this.faceNormal.Z;

	            int numfaceUVs = this.faceUVs.size();
	            for (i = 0; i < numfaceUVs; i++)
	            {
	                UVCoord uv = new UVCoord(this.faceUVs.get(i));
	                uv.V = 1.0f - uv.V;
	                this.faceUVs.set(i,  uv);
	            }
	        }

	        public void addValue2FaceVertexIndices(int num)
	        {
	            int numFaces = this.faces.size();
	            Face tmpFace;
	            for (int i = 0; i < numFaces; i++)
	            {
	                tmpFace = new Face(this.faces.get(i));
	                tmpFace.v1 += num;
	                tmpFace.v2 += num;
	                tmpFace.v3 += num;

	                this.faces.set(i, tmpFace);
	            }
	        }

	        public void addValue2FaceNormalIndices(int num)
	        {
	            if (this.calcVertexNormals)
	            {
	                int numFaces = this.faces.size();
	                Face tmpFace;
	                for (int i = 0; i < numFaces; i++)
	                {
	                    tmpFace = new Face(this.faces.get(i));
	                    tmpFace.n1 += num;
	                    tmpFace.n2 += num;
	                    tmpFace.n3 += num;

		                this.faces.set(i, tmpFace);
	                }
	            }
	        }

	        public void DumpRaw(String path, String name, String title) throws IOException
	        {
	            if (path == null)
	                return;
	            
	            String fileName = name + "_" + title + ".raw";
	            String completePath = FileUtils.combineFilePath(path, fileName);
	            BufferedWriter sw = new BufferedWriter(new FileWriter(completePath));

	            for (int i = 0; i < this.faces.size(); i++)
	            {
	                String s = this.coords.get(this.faces.get(i).v1).toString();
	                s += " " + this.coords.get(this.faces.get(i).v2).toString();
	                s += " " + this.coords.get(this.faces.get(i).v3).toString();
	                s += " " + this.faceUVs.get(this.faces.get(i).v1).toString();
	                s += " " + this.faceUVs.get(this.faces.get(i).v2).toString();
	                s += " " + this.faceUVs.get(this.faces.get(i).v3).toString();
	                
	                sw.write(s);
	                sw.newLine();
	            }
	            sw.flush();
	            sw.close();
	            
//	            String fileName = name + "_" + title + ".raw";
//	            String completePath = System.IO.Path.Combine(path, fileName);
//	            StreamWriter sw = new StreamWriter(completePath);
//
//	            for (int i = 0; i < this.faces.size(); i++)
//	            {
//	                String s = this.coords[this.faces[i].v1];
//	                s += " " + this.coords[this.faces[i].v2];
//	                s += " " + this.coords[this.faces[i].v3];
//
//	                sw.WriteLine(s);
//	            }
//
//	            sw.Close();
	        }
	    }

	    