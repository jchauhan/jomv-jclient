/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering.mesh;
 public enum FaceType 
	    {
	    	//ushort
	        PathBegin(0x1 << 0),
	        PathEnd (0x1 << 1),
	        InnerSide (0x1 << 2),
	        ProfileBegin (0x1 << 3),
	        ProfileEnd (0x1 << 4),
	        OuterSide0 (0x1 << 5),
	        OuterSide1 (0x1 << 6),
	        OuterSide2 (0x1 << 7),
	        OuterSide3 (0x1 << 8);
	        private int index;
	        FaceType(int index)
			{
				this.index = index;
			}     

			public int getIndex()
			{
				return index;
			}
	    }
 