/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.imaging.platform.jclient;

import java.io.InputStream;
import com.ngt.jopenmetaverse.shared.sim.imaging.IBitmap;
import com.ngt.jopenmetaverse.shared.sim.imaging.ITGAImageReader;
import com.ngt.jopenmetaverse.shared.sim.imaging.platform.jclient.tga.JclientTGADecoder;
import com.ngt.jopenmetaverse.shared.util.JLogger;
import com.ngt.jopenmetaverse.shared.util.Utils;

public class TGAImageReaderImpl implements ITGAImageReader{
	public IBitmap read(InputStream stream) {
		try {			
			BitmapBufferedImageImpl img2 = new BitmapBufferedImageImpl(JclientTGADecoder.loadImage(stream));
			return img2;
			} catch (Exception e) {
				JLogger.warn(Utils.getExceptionStackTraceAsString(e));
				return null;
			}
	}	
}
