/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering.mesh.primmesher;
public class Coord
	    {
	        public float X;
	        public float Y;
	        public float Z;
	        
	        public Coord() {
				super();
			}

			public Coord(float x, float y, float z)
	        {
	            this.X = x;
	            this.Y = y;
	            this.Z = z;
	        }

			public Coord(Coord c)
	        {
	            this(c.X, c.Y, c.Z);
	        }			
			
	        public float Length()
	        {
	            return (float)Math.sqrt(this.X * this.X + this.Y * this.Y + this.Z * this.Z);
	        }

	        public Coord Invert()
	        {
	            this.X = -this.X;
	            this.Y = -this.Y;
	            this.Z = -this.Z;

	            return this;
	        }

	        public Coord Normalize()
	        {
	            final float MAG_THRESHOLD = 0.0000001f;
	            float mag = Length();

	            // Catch very small rounding errors when normalizing
	            if (mag > MAG_THRESHOLD)
	            {
	                float oomag = 1.0f / mag;
	                this.X *= oomag;
	                this.Y *= oomag;
	                this.Z *= oomag;
	            }
	            else
	            {
	                this.X = 0.0f;
	                this.Y = 0.0f;
	                this.Z = 0.0f;
	            }

	            return this;
	        }

	        @Override
	        public String toString()
	        {
	            return this.X + " " + this.Y + " " + this.Z;
	        }

	        public static Coord Cross(Coord c1, Coord c2)
	        {
	            return new Coord(
	                c1.Y * c2.Z - c2.Y * c1.Z,
	                c1.Z * c2.X - c2.Z * c1.X,
	                c1.X * c2.Y - c2.X * c1.Y
	                );
	        }

	        public static Coord add(Coord v, Coord a)
	        {
	            return new Coord(v.X + a.X, v.Y + a.Y, v.Z + a.Z);
	        }

	        public Coord add(Coord v)
	        {
	        	this.X = this.X + v.X;
	        	this.Y = this.Y + v.Y;
	        	this.Z = this.Z + v.Z;
	        	return this;
	        }
	        
	        public static Coord multiply(Coord v, Coord m)
	        {
	            return new Coord(v.X * m.X, v.Y * m.Y, v.Z * m.Z);
	        }

	        public static Coord multiply(Coord v, Quat q, Coord c2)
	        {
	        	   c2.X = q.W * q.W * v.X +
	   	                2f * q.Y * q.W * v.Z -
	   	                2f * q.Z * q.W * v.Y +
	   	                     q.X * q.X * v.X +
	   	                2f * q.Y * q.X * v.Y +
	   	                2f * q.Z * q.X * v.Z -
	   	                     q.Z * q.Z * v.X -
	   	                     q.Y * q.Y * v.X;

	   	            c2.Y =
	   	                2f * q.X * q.Y * v.X +
	   	                     q.Y * q.Y * v.Y +
	   	                2f * q.Z * q.Y * v.Z +
	   	                2f * q.W * q.Z * v.X -
	   	                     q.Z * q.Z * v.Y +
	   	                     q.W * q.W * v.Y -
	   	                2f * q.X * q.W * v.Z -
	   	                     q.X * q.X * v.Y;

	   	            c2.Z =
	   	                2f * q.X * q.Z * v.X +
	   	                2f * q.Y * q.Z * v.Y +
	   	                     q.Z * q.Z * v.Z -
	   	                2f * q.W * q.Y * v.X -
	   	                     q.Y * q.Y * v.Z +
	   	                2f * q.W * q.X * v.Y -
	   	                     q.X * q.X * v.Z +
	   	                     q.W * q.W * v.Z;

	   	            return c2;
	        }
	        
	        public static Coord multiply(Coord v, Quat q)
	        {
	            // From http://www.euclideanspace.com/maths/algebra/realNormedAlgebra/quaternions/transforms/
	            Coord c2 = new Coord(0.0f, 0.0f, 0.0f);
	            multiply(v, q, c2);
	            return c2;
	        }
	        
	    }

	    