/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering.terrain;

import java.awt.Color;
import java.awt.Graphics;

import com.ngt.jopenmetaverse.shared.sim.GridClient;
import com.ngt.jopenmetaverse.shared.sim.asset.AssetTexture;
import com.ngt.jopenmetaverse.shared.sim.asset.pipeline.TexturePipeline.TextureRequestState;
import com.ngt.jopenmetaverse.shared.sim.events.AutoResetEvent;
import com.ngt.jopenmetaverse.shared.sim.events.MethodDelegate;
import com.ngt.jopenmetaverse.shared.sim.events.asm.TextureDownloadCallbackArgs;
import com.ngt.jopenmetaverse.shared.sim.imaging.BitmapFactory;
import com.ngt.jopenmetaverse.shared.sim.imaging.IBitmap;
import com.ngt.jopenmetaverse.shared.sim.imaging.IOpenJPEG;
import com.ngt.jopenmetaverse.shared.sim.imaging.ImageUtils;
import com.ngt.jopenmetaverse.shared.sim.imaging.ManagedImage;
import com.ngt.jopenmetaverse.shared.sim.imaging.OpenJPEGFactory;
import com.ngt.jopenmetaverse.shared.sim.imaging.PixelFormat;
import com.ngt.jopenmetaverse.shared.sim.rendering.Perlin;
import com.ngt.jopenmetaverse.shared.types.Color4;
import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.shared.types.Vector3;
import com.ngt.jopenmetaverse.shared.util.Utils;

public  class TerrainSplat
{
	//region Constants

	private static final UUID DIRT_DETAIL = new UUID("0bc58228-74a0-7e83-89bc-5c23464bcec5");
	private static final UUID GRASS_DETAIL = new UUID("63338ede-0037-c4fd-855b-015d77112fc8");
	private static final UUID MOUNTAIN_DETAIL = new UUID("303cd381-8560-7579-23f1-f0a880799740");
	private static final UUID ROCK_DETAIL = new UUID("53a2f406-4895-1d13-d541-d2e3b86bc19c");
	private static final int RegionSize = 256;

	private static final UUID[] DEFAULT_TERRAIN_DETAIL = new UUID[]
			{
		DIRT_DETAIL,
		GRASS_DETAIL,
		MOUNTAIN_DETAIL,
		ROCK_DETAIL
			};

	private static final int[][] DEFAULT_TERRAIN_COLOR = new int[][]
			{
		{255, 164, 136, 117},
		{255, 65, 87, 47},
		{255, 157, 145, 131},
		{255, 125, 128, 130}
			};

	private static final UUID TERRAIN_CACHE_MAGIC = new UUID("2c0c7ef2-56be-4eb8-aacb-76712c535b4b");

//	private static void printfloatarray(float[] array)
//	{
//		int count = 0;
//		while(count < array.length)
//		{
//			System.out.println("");
//			for(int x =0 ; x < 50 && count <  array.length	 ; x++)
//			{
//				System.out.print((int)array[count++] + " ");
//			}
//		}
//	}
//
//	private static void printfloatarray(float[][] array)
//	{
//		int count = 0;
//		for(int y =0 ; y < array.length	 ; y++)
//		{
//			System.out.println("");
//			for(int x =0 ; x < array[y].length; x++)
//			{
//				System.out.print((int)array[y][x] + " ");
//			}
//		}
//	}
	
	
	//endregion Constants

	/// <summary>
	/// Builds a composited terrain texture given the region texture
	/// and heightmap settings
	/// </summary>
	/// <param name="heightmap">Terrain heightmap</param>
	/// <param name="regionInfo">Region information including terrain texture parameters</param>
	/// <returns>A composited 256x256 RGB texture ready for rendering</returns>
	/// <remarks>Based on the algorithm described at http://opensimulator.org/wiki/Terrain_Splatting
	/// </remarks>
	public static IBitmap Splat(GridClient Client, float[][] heightmap, UUID[] textureIDs, 
			float[] startHeights, float[] heightRanges) throws Exception
	{
		if(textureIDs.length != 4 && startHeights.length != 4 
				&& heightRanges.length != 4)
			throw new Exception("textureIDs, startHeights & heightRanges should be length 4");
			
		int outputSize = 2048;

		// Swap empty terrain textureIDs with default IDs
		for (int i = 0; i < textureIDs.length; i++)
		{
			if (textureIDs[i].equals(UUID.Zero))
				textureIDs[i] = DEFAULT_TERRAIN_DETAIL[i];
		}

		//region Texture Fetching
		IBitmap[] detailTexture = TerrainSplat.downloadAndDecodeTextures(Client, textureIDs);           	            
		//endregion Texture Fetching

		// Fill in any missing textures with a solid color
		for(int i =0; i < detailTexture.length; i++)
		{
			if (detailTexture[i] == null)
			{
				detailTexture[i] = ImageUtils.createImageWithSolidColor(outputSize, outputSize, 
						DEFAULT_TERRAIN_COLOR[i][0],
						DEFAULT_TERRAIN_COLOR[i][1], 
						DEFAULT_TERRAIN_COLOR[i][2],
						DEFAULT_TERRAIN_COLOR[i][3]);
			}
			else if (detailTexture[i].getWidth() != outputSize || detailTexture[i].getHeight() != outputSize)
			{
				//FIXME should we resize to  RegionSize or outputSize
				//	            		detailTexture[i] = ResizeBitmap(detailTexture[i], 256, 256);
				detailTexture[i].resize(RegionSize, RegionSize);
				//	            		detailTexture[i].resize(outputSize, outputSize);
			}
		}

		//region Layer Map
		int diff = heightmap.length / RegionSize;
		float[] layermap = new float[RegionSize * RegionSize];
		layermap = createLayermap(heightmap, startHeights, heightRanges, layermap);
		
//		//TODO just for debugging
//		System.out.println("startHeights:");
//		printfloatarray(startHeights);
//		
//		System.out.println("heightRanges:");
//		printfloatarray(heightRanges);
//
//		System.out.println("heightmap:");
//		printfloatarray(heightmap);
//		
//		System.out.println("LayerMap:");
//		printfloatarray(layermap);
//		//endregion Layer Map

		//region Texture Compositing
		IBitmap output = BitmapFactory.getIntance().getNewIntance(outputSize, outputSize, PixelFormat.Format32bppArgb);


		//	    		int[] comps = new int[]
		//	    			{
		//	    				(detailTexture[0].getPixelFormat() == PixelFormat.Format32bppArgb) ? 4 : 3,
		//	    				(detailTexture[1].getPixelFormat() == PixelFormat.Format32bppArgb) ? 4 : 3,
		//	    				(detailTexture[2].getPixelFormat() == PixelFormat.Format32bppArgb) ? 4 : 3,
		//	    				(detailTexture[3].getPixelFormat() == PixelFormat.Format32bppArgb) ? 4 : 3
		//	    			};
		//	    		
		//	    		int[] strides = new int[]
		//	    				{
		//	    				detailTexture[0].getWidth(),
		//	    				detailTexture[1].getWidth(),
		//	    				detailTexture[2].getWidth(),
		//	    				detailTexture[3].getWidth()
		//	    				};
		//
		//	    		int[] scans = new int[]
		//	    				{
		//	    					0,
		//	    					0,
		//	    					0,
		//	    					0
		//	    				};

		int ratio = outputSize / RegionSize;
		for (int y = 0; y < outputSize; y++)
		{
			for (int x = 0; x < outputSize; x++)
			{
				float layer = layermap[(y / ratio) * RegionSize + x / ratio];
				float layerx = layermap[(y / ratio) * RegionSize + Math.min(outputSize - 1, (x + 1)) / ratio];
				float layerxx = layermap[(y / ratio) * RegionSize + Math.max(0, (x - 1)) / ratio];
				float layery = layermap[Math.min(outputSize - 1, (y + 1)) / ratio * RegionSize + x / ratio];
				float layeryy = layermap[(Math.max(0, (y - 1)) / ratio) * RegionSize + x / ratio];

				// Select two textures
				int l0 = (int)Math.floor(layer);
				int l1 = Math.min(l0 + 1, 3);

				int pixelA =  detailTexture[l0].getRGB((x % 256), (y % 256));
				int pixelB = detailTexture[l1].getRGB((x % 256), (y % 256));
				//	                        int pixel0 = output.getRGB(x,  y);

				float aB = pixelA & 0xff;
				float aG = (pixelA >> 8) & 0xff;
				float aR = (pixelA >> 16) & 0xff;

				//	                        byte* ptrA = (byte*)scans[l0] + (y % 256) * strides[l0] + (x % 256) * comps[l0];
				//	                        byte* ptrB = (byte*)scans[l1] + (y % 256) * strides[l1] + (x % 256) * comps[l1];
				//	                        byte* ptrO = (byte*)outputData.Scan0 + y * outputData.Stride + x * 3;

				//	                        float aB = *(ptrA + 0);
				//	                        float aG = *(ptrA + 1);
				//	                        float aR = *(ptrA + 2);

				int lX = (int)Math.floor(layerx);
				int pixelX =  detailTexture[lX].getRGB((x % 256), (y % 256));

				int lXX = (int)Math.floor(layerxx);
				int pixelXX =  detailTexture[lXX].getRGB((x % 256), (y % 256));

				int lY = (int)Math.floor(layery);
				int pixelY =  detailTexture[lY].getRGB((x % 256), (y % 256));

				int lYY = (int)Math.floor(layeryy);
				int pixelYY =  detailTexture[lYY].getRGB((x % 256), (y % 256));

				float bB = pixelB & 0xff;
				float bG = (pixelB >> 8) & 0xff;
				float bR = (pixelB >> 16) & 0xff;

				float XB = pixelX & 0xff;
				float XG = (pixelX >> 8) & 0xff;
				float XR = (pixelX >> 16) & 0xff;

				float XXB = pixelXX & 0xff;
				float XXG = (pixelXX >> 8) & 0xff;
				float XXR = (pixelXX >> 16) & 0xff;

				float YB = pixelY & 0xff;
				float YG = (pixelY >> 8) & 0xff;
				float YR = (pixelY >> 16) & 0xff;

				float YYB = pixelYY & 0xff;
				float YYG = (pixelYY >> 8) & 0xff;
				float YYR = (pixelYY >> 16) & 0xff;


				//	                        int lX = (int)Math.floor(layerx);
				//	                        byte* ptrX = (byte*)scans[lX] + (y % 256) * strides[lX] + (x % 256) * comps[lX];
				//	                        int lXX = (int)Math.floor(layerxx);
				//	                        byte* ptrXX = (byte*)scans[lXX] + (y % 256) * strides[lXX] + (x % 256) * comps[lXX];
				//	                        int lY = (int)Math.floor(layery);
				//	                        byte* ptrY = (byte*)scans[lY] + (y % 256) * strides[lY] + (x % 256) * comps[lY];
				//	                        int lYY = (int)Math.floor(layeryy);
				//	                        byte* ptrYY = (byte*)scans[lYY] + (y % 256) * strides[lYY] + (x % 256) * comps[lYY];
				//	
				//	                        float bB = *(ptrB + 0);
				//	                        float bG = *(ptrB + 1);
				//	                        float bR = *(ptrB + 2);

				float layerDiff = layer - l0;
				float xlayerDiff = layerx - layer;
				float xxlayerDiff = layerxx - layer;
				float ylayerDiff = layery - layer;
				float yylayerDiff = layeryy - layer;

				// Interpolate between the two selected textures


				int oB = ((int)Math.floor(aB + layerDiff * (bB - aB) + 
						xlayerDiff * (XB - aB) + 
						xxlayerDiff * (XXB - aB) + 
						ylayerDiff * (YB - aB) + 
						yylayerDiff * (YYB - aB)) ) & 0xff;

				int oG = ((int)Math.floor(aG + layerDiff * (bG - aG) + 
						xlayerDiff * (XG - aG) +
						xxlayerDiff * (XXG - aG) + 
						ylayerDiff * (YG - aG) +
						yylayerDiff * (YYG - aG)) ) & 0xff;	                        

				int oR = ((int)Math.floor(aR + layerDiff * (bR - aR) +
						xlayerDiff * (XR - aR) + 
						xxlayerDiff * (XXR - aR) +
						ylayerDiff * (YR - aR) + 
						yylayerDiff * (YYR - aR)) ) & 0xff;


				output.setRGB(x,  y, (0xff << 24) | (oR  << 16) | (oG << 8) | oB);


				//	                        *(ptrO + 0) = (byte)Math.Floor(aB + layerDiff * (bB - aB) + 
				//	                            xlayerDiff * (*ptrX - aB) + 
				//	                            xxlayerDiff * (*(ptrXX) - aB) + 
				//	                            ylayerDiff * (*ptrY - aB) + 
				//	                            yylayerDiff * (*(ptrYY) - aB));
				//	                        *(ptrO + 1) = (byte)Math.Floor(aG + layerDiff * (bG - aG) + 
				//	                            xlayerDiff * (*(ptrX + 1) - aG) +
				//	                            xxlayerDiff * (*(ptrXX + 1) - aG) + 
				//	                            ylayerDiff * (*(ptrY + 1) - aG) +
				//	                            yylayerDiff * (*(ptrYY + 1) - aG));
				//	                        *(ptrO + 2) = (byte)Math.Floor(aR + layerDiff * (bR - aR) +
				//	                            xlayerDiff * (*(ptrX + 2) - aR) + 
				//	                            xxlayerDiff * (*(ptrXX + 2) - aR) +
				//	                            ylayerDiff * (*(ptrY + 2) - aR) + 
				//	                            yylayerDiff * (*(ptrYY + 2) - aR));
			}
		}

		for (int i = 0; i < detailTexture.length; i++)
		{
			//	                    detailTexture[i].UnlockBits(datas[i]);
			detailTexture[i].dispose();
		}


		layermap = null;
		//	            output.UnlockBits(outputData);

		//	            output.RotateFlip(RotateFlipType.Rotate270FlipNone);
		output.rotateAndFlip(Math.toRadians(270), false, false);

		//endregion Texture Compositing
		return output;
	}


	public static IBitmap[] downloadAndDecodeTextures(GridClient Client, UUID[] textureIds) throws Exception
	{
		IBitmap[] bitmaps = new IBitmap[textureIds.length];
		final AssetTexture[] result = new AssetTexture[1]; 
		for(int i=0; i < textureIds.length; i++)
		{

			AutoResetEvent textureDone = new AutoResetEvent(false);
			//Download the texture image
			Client.assets.RequestImage(textureIds[i], TextureDownloadCallback(result, textureDone));
			//wait for 1 min
			textureDone.waitOne(60 * 1000);
			if(result[0] != null)
			{
				//Decode the jpeg2000 image
				IOpenJPEG openJpeg = OpenJPEGFactory.getIntance();
				bitmaps[i] = openJpeg.DecodeToIBitMap(result[0].AssetData);
			}
			else
			{
				bitmaps[i] = null;
			}
		}
		return bitmaps;
	}

	private static MethodDelegate<Void, TextureDownloadCallbackArgs> TextureDownloadCallback(final AssetTexture[] detailTexture, 
			final AutoResetEvent textureDone)
			{	
		return new MethodDelegate<Void, TextureDownloadCallbackArgs>()
				{
			public Void execute(TextureDownloadCallbackArgs e) {
				if (e.getState() == TextureRequestState.Finished && e.getAssetTexture() != null && e.getAssetTexture().AssetData != null)
				{
					detailTexture[0] =  e.getAssetTexture();
				}
				else
					detailTexture[0] = null;

				textureDone.set();
				return null;
			}
				};
			}


	/*
	 * @param dstLayermap destination array to store layermap, if null a new array is created and returned
	 */
	private static float[] createLayermap(float[][] heightmap, float[] startHeights, float[] heightRanges, float[] dstLayermap)
	{
		int diff = heightmap.length / RegionSize;
		float[] layermap = null;

		if(dstLayermap == null)
			layermap = new float[RegionSize * RegionSize];
		else
			layermap = dstLayermap;

		for (int y = 0; y < heightmap.length; y += diff)
		{
			for (int x = 0; x < heightmap[0].length; x += diff)
			{
				int newX = x / diff;
				int newY = y / diff;
				float height = heightmap[newX][newY];

				float pctX = (float)newX / 255f;
				float pctY = (float)newY / 255f;

				// Use bilinear interpolation between the four corners of start height and
				// height range to select the current values at this position
				float startHeight = ImageUtils.Bilinear(
						startHeights[0],
						startHeights[2],
						startHeights[1],
						startHeights[3],
						pctX, pctY);
				startHeight = Utils.clamp(startHeight, 0f, 255f);

				float heightRange = ImageUtils.Bilinear(
						heightRanges[0],
						heightRanges[2],
						heightRanges[1],
						heightRanges[3],
						pctX, pctY);
				heightRange = Utils.clamp(heightRange, 0f, 255f);

				// Generate two frequencies of perlin noise based on our global position
				// The magic values were taken from http://opensimulator.org/wiki/Terrain_Splatting
				Vector3 vec = new Vector3
						(
								newX * 0.20319f,
								newY * 0.20319f,
								height * 0.25f
								);

				float lowFreq = Perlin.noise2(vec.X * 0.222222f, vec.Y * 0.222222f) * 6.5f;
				float highFreq = Perlin.turbulence2(vec.X, vec.Y, 2f) * 2.25f;
				float noise = (lowFreq + highFreq) * 2f;

				// Combine the current height, generated noise, start height, and height range parameters, then scale all of it
				float layer = 0;
				if(heightRange > 0.0)
					layer = ((height + noise - startHeight) / heightRange) * 4f;
				else
					layer = Float.NaN;

				if (Float.isNaN(layer))
					layer = 0f;

//				System.out.println(String.format("X %d Y %d heightRange %f lowFreq %f highFreq %f " +
//						"noise %f layer %f startHeight %f newX %d newY %d height %f",
//						x, y, heightRange, lowFreq, highFreq, noise, layer, startHeight, newX, newY, height));
				
				layermap[newY * RegionSize + newX] = Utils.clamp(layer, 0f, 3f);
			}
		}

		return layermap;
	}


	public static IBitmap SplatSimple(float[][] heightmap)
	{
		final float BASE_HSV_H = 93f / 360f;
		final float BASE_HSV_S = 44f / 100f;
		final float BASE_HSV_V = 34f / 100f;

		IBitmap bitmap = BitmapFactory.getIntance().getNewIntance(256, 256, PixelFormat.Format32bppArgb);

		//            IBitmap img = new Bitmap(256, 256);
		//            BitmapData bitmapData = img.LockBits(new Rectangle(0, 0, 256, 256), ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
		for (int y = 255; y >= 0; y--)
		{
			//			System.out.println("");
			for (int x = 0; x < 256; x++)
			{
				float normHeight = heightmap[x][y] / 255f;
				normHeight = Utils.clamp(normHeight, BASE_HSV_V, 1.0f);
				//				System.out.print(normHeight + " ");
				Color4 color = Color4.fromHSV(BASE_HSV_H, BASE_HSV_S, normHeight);

				//				int k = y*256 + x;
				int pixel = (int)(color.getB() * 255f) | ( ((int)(color.getG() * 255f)) << 8) | ( ((int)(color.getR() * 255f)) <<  16) | (0xff << 24);   
				bitmap.setRGB(x, y, pixel);

				//                        byte* ptr = (byte*)bitmapData.Scan0 + y * bitmapData.Stride + x * 3;
				//                        *(ptr + 0) = (byte)(color.B * 255f);
				//                        *(ptr + 1) = (byte)(color.G * 255f);
				//                        *(ptr + 2) = (byte)(color.R * 255f);
			}
		}


		//            img.UnlockBits(bitmapData);
		return bitmap;
	}
}