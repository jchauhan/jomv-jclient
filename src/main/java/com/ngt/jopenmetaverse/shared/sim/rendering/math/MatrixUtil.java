/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering.math;


import static java.lang.Math.cos;
import static java.lang.Math.sin;

import com.ngt.jopenmetaverse.shared.types.Vector3;

/**
 * Contains several helper methods to construct matrices like projection (etc).
 * 
 * @author Steffen Schäfer
 * @author Sönke Sothmann
 * 
 */
public final class MatrixUtil {
	private MatrixUtil() {
	}

	/**
	 * 
	 * @param fieldOfViewVertical
	 *            Vertikaler Öffnungswinkel in Grad
	 * @param aspectRatio
	 *            Verhältnis von Höhe zu Breite
	 * @param minimumClearance
	 *            Mindestabstand sichtbarer Punkte
	 * @param maximumClearance
	 *            Höchstabstand sichtbarer Punkte
	 * @return the created perspective matrix
	 */
	public static FloatMatrix4x4 createPerspectiveMatrix(
			int fieldOfViewVertical, float aspectRatio, float minimumClearance,
			float maximumClearance) {
		double fieldOfViewInRad = fieldOfViewVertical * Math.PI / 180.0;
		return new FloatMatrix4x4(new float[][] {
				new float[] {
						(float) (Math.tan(fieldOfViewInRad) / aspectRatio), 0,
						0, 0 },
				new float[] {
						0,
						(float) (1 / Math.tan(fieldOfViewVertical * Math.PI
								/ 180.0)), 0, 0 },
				new float[] {
						0,
						0,
						(minimumClearance + maximumClearance)
								/ (minimumClearance - maximumClearance),
						2 * minimumClearance * maximumClearance
								/ (minimumClearance - maximumClearance) },
				new float[] { 0, 0, -1, 0 } });
	};

	/**
	 * Creates a rotation matrix.
	 * 
	 * @param angleX
	 *            the angle in degrees for the rotation around the x axis
	 * @param angleY
	 *            the angle in degrees for the rotation around the y axis
	 * @param angleZ
	 *            the angle in degrees for the rotation around the z axis
	 * @return the created matrix
	 */
	public static FloatMatrix4x4 createRotationMatrix(int angleX, int angleY,
			int angleZ) {
		return createRotationMatrixX(angleX).multiply(
				createRotationMatrixY(angleY)).multiply(
				createRotationMatrixZ(angleZ));
	}

	private static FloatMatrix4x4 createRotationMatrixX(int angle) {
		double angleInRad = angle * (Math.PI / 180.0);

		// 1 0 0 0
		//
		// 0 cos(q) sin(q) 0
		//
		// 0 -sin(q) cos(q) 0
		//
		// 0 0 0 1

		return new FloatMatrix4x4(new float[][] {
				new float[] { 1, 0, 0, 0 },
				new float[] { 0, (float) cos(angleInRad),
						(float) sin(angleInRad), 0 },
				new float[] { 0, (float) -sin(angleInRad),
						(float) cos(angleInRad), 0 },
				new float[] { 0, 0, 0, 1 } });
	}

	private static FloatMatrix4x4 createRotationMatrixY(int angle) {
		double angleInRad = angle * (Math.PI / 180.0);

		// cos(a) 0 -sin(a) 0
		//
		// 0 1 0 0
		//
		// sin(a) 0 cos(a) 0
		//
		// 0 0 0 1

		return new FloatMatrix4x4(new float[][] {
				new float[] { (float) cos(angleInRad), 0,
						(float) -sin(angleInRad), 0 },
				new float[] { 0, 1, 0, 0 },
				new float[] { (float) sin(angleInRad), 0.0f,
						(float) cos(angleInRad), 0.0f },
				new float[] { 0, 0, 0, 1 } });
	}

	private static FloatMatrix4x4 createRotationMatrixZ(int angle) {
		double angleInRad = angle * (Math.PI / 180.0);

		// cos(a) sin(a) 0 0
		//
		// -sin(a) cos(a) 0 0
		//
		// 0 0 1 0
		//
		// 0 0 0 1

		return new FloatMatrix4x4(new float[][] {
				new float[] { (float) cos(angleInRad), (float) sin(angleInRad),
						0, 0 },
				new float[] { (float) -sin(angleInRad),
						(float) cos(angleInRad), 0, 0 },
				new float[] { 0, 0, 1, 0 }, new float[] { 0, 0, 0, 1 } });
	}

	/**
	 * Creates a translation matrix.
	 * 
	 * @param translateX
	 *            the amount to translate parallel to the x axis
	 * @param translateY
	 *            the amount to translate parallel to the y axis
	 * @param translateZ
	 *            the amount to translate parallel to the z axis
	 * @return the created matrix
	 */
	public static FloatMatrix4x4 createTranslationMatrix(float translateX,
			float translateY, float translateZ) {
		return new FloatMatrix4x4(
				new float[][] { new float[] { 1, 0, 0, translateX },
						new float[] { 0, 1, 0, translateY },
						new float[] { 0, 0, 1, translateZ },
						new float[] { 0, 0, 0, 1 } });
	}
	
	/*
	 * Create Lookat matrix that can used in shaders. Alternatively use glLookAt
	 */
	
	public static FloatMatrix4x4 createLookAtMatrix(Vector3 eye, Vector3 center, Vector3 up )
	{
		return createLookAtMatrix(eye.X, eye.Y, eye.Z, 
				center.X, center.Y, center.Z, 
				up.X, up.Y, up.Z);
	}
	
	
	/*
	 * Create Lookat matrix that can used in shaders. Alternatively use glLookAt
	 * 
	 * @see Implemented using apprach described in http://www.opengl.org/sdk/docs/man/xhtml/gluLookAt.xml
	 */
	public static FloatMatrix4x4 createLookAtMatrix(float  	eyeX,
		 	float  	eyeY,
		 	float  	eyeZ,
		 	float  	centerX,
		 	float  	centerY,
		 	float  	centerZ,
		 	float  	upX,
		 	float  	upY,
		 	float  	upZ)
	{
		Vector3 eye = new Vector3(eyeX, eyeY, eyeZ);
		Vector3 center = new Vector3(centerX, centerY, centerZ);
		Vector3 up = new Vector3(upX, upY, upZ);
		
		Vector3 f = Vector3.subtract(center, eye);
		f.normalize();
//		Vector3 up2 = new Vector3(up);
//		up2.normalize();
		
		Vector3 s = Vector3.cross(f, up);
		s.normalize();
		Vector3 u = Vector3.cross(s, f);
//		
//		float[][] m =		new float[][] { 
//								new float[] { s.X, u.X, -1 * f.X, 0},
//								new float[] { s.Y, u.Y, -1 * f.Y, 0},
//								new float[] { s.Z, u.Z, -1 * f.Z, 0},
//								new float[] { 0,   0,   0, 1 } 
//							};
		
		
		float[][] m =		new float[][] { 
								new float[] { s.X, s.Y, s.Z, 0},
								new float[] { u.X, u.Y, u.Z, 0},
								new float[] { -1 * f.X, -1 * f.Y, -1 * f.Z, 0},
								new float[] { 0,   0,   0, 1 } 
							};
		
		for (int i=0 ; i<4 ; i++) 
		{
			m[i][3] += m[i][0] * (-1*eyeX) + m[i][1] * (-1 * eyeY) + m[i][2] * (-1 * eyeZ);
		}
		
		FloatMatrix4x4 result = new FloatMatrix4x4(m);
		return result;
	}

	
//	public static void translatef(FloatMatrix4x4 src, float eyeX, float eyeY, float eyeZ)
//	{
//		FloatMatrix4x4 result = new FloatMatrix4x4(src.data);
//		float[][] m = src.data;		
//		for (int i=0 ; i<4 ; i++) 
//		{
//			m[3][i] += m[0][i] * (-1*eyeX) + m[1][i] * (-1 * eyeY) + m[2][i] * (-1 * eyeZ);
//		}
//		
//		FloatMatrix4x4 result = new FloatMatrix4x4(src.data);
//
//	}
	
}
