/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.lwjgl.opengl.GL20;

import com.ngt.jopenmetaverse.shared.util.JLogger;
import com.ngt.jopenmetaverse.shared.util.Utils;


public class Shader
{
    public int ID = -1;

    public boolean Load(String fileName)
    {
        try
        {
            int type;

            String code = FileUtils.readFileToString(new File(fileName));
            if (fileName.endsWith(".vert"))
            {
                type = GL20.GL_VERTEX_SHADER;
            }
            else if (fileName.endsWith(".frag"))
            {
                type = GL20.GL_FRAGMENT_SHADER;
            }
            else
                return false;

            ID = GL20.glCreateShader(type);
            GL20.glShaderSource(ID, code);
            GL20.glCompileShader(ID);
            String info = GL20.glGetShaderInfoLog(ID, 1000);
            int res;
            res = GL20.glGetShader(ID, GL20.GL_COMPILE_STATUS);
            if (res != 1)
            {
                JLogger.debug("Compilation failed: " + info);
                ID = -1;
                return false;
            }
            JLogger.debug(String.format("%d %s compiled successfully", type, fileName));
            return true;
        }
        catch (Exception e)
        {
        	JLogger.debug(Utils.getExceptionStackTraceAsString(e));
            return false;
        }
    }

    public void Dispose()
    {
        if (ID == -1) return;
        GL20.glDeleteShader(ID);
    }
}

