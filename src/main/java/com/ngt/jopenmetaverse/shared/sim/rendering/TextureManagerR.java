/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering;

import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import org.lwjgl.opengl.GL11;
import com.ngt.jopenmetaverse.shared.sim.GridClient;
import com.ngt.jopenmetaverse.shared.sim.cache.LoadCachedImageResult;
import com.ngt.jopenmetaverse.shared.sim.events.MethodDelegate;
import com.ngt.jopenmetaverse.shared.sim.events.ThreadPoolFactory;
import com.ngt.jopenmetaverse.shared.sim.events.asm.TextureDownloadCallbackArgs;
import com.ngt.jopenmetaverse.shared.sim.imaging.DecodedTgaImage;
import com.ngt.jopenmetaverse.shared.sim.imaging.IBitmap;
import com.ngt.jopenmetaverse.shared.sim.imaging.LoadTGAClass;
import com.ngt.jopenmetaverse.shared.sim.imaging.OpenJPEGFactory;
import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.shared.util.JLogger;
import com.ngt.jopenmetaverse.shared.util.Utils;

public class TextureManagerR 
{
	public final static  UUID invisi1 = new UUID("38b86f85-2575-52a9-a531-23108d8da837");
	public final static  UUID invisi2 = new UUID("e97cf410-8e61-7005-ec06-629eba4cd1fb");
	final int  MAX_WAITING_TIME = 10;
	private GridClient Client;
	private Map<UUID, TextureInfo> texturesPtrMap = new HashMap<UUID, TextureInfo>();
	private BlockingQueue<TextureLoadItem> PendingTexturesToDownload  = new  LinkedBlockingDeque<TextureLoadItem>();
	private BlockingQueue<TextureLoadItem> PendingTexturesToGLLoad  = new  LinkedBlockingDeque<TextureLoadItem>();
	final private AtomicBoolean threadRunning = new AtomicBoolean(false);


	public TextureManagerR(GridClient Client) {
		super();
		this.Client = Client;
	}

	public TextureInfo getTextureInfo(UUID textureId)
	{
		return texturesPtrMap.get(textureId);
	}

	public boolean hasTextureInfo(UUID textureId)
	{
		return texturesPtrMap.containsKey(textureId);
	}	

	public void stop()
	{
		threadRunning.set(false);
	}

	public void dispose()
	{
		stop();
		PendingTexturesToDownload.clear();
		//TODO should we release OpenGL textures also
		texturesPtrMap.clear();
	}

	public void start()
	{		
		//If not already running
		if(!threadRunning.get())
		{
		threadRunning.set(true);
		ThreadPoolFactory.getThreadPool().execute(new Runnable()
		{
			public void run() {
				while(threadRunning.get())
				{
					JLogger.debug("Texture Thread Started ...");
					try {
						TextureLoadItem item = PendingTexturesToDownload.poll(MAX_WAITING_TIME, TimeUnit.SECONDS);
						JLogger.debug("Waking up Texture Thread  ...Queue Size: " + PendingTexturesToDownload.size());
						if(item != null)
						{
							JLogger.debug("Loading Texture OpenGL " + item.Data.TextureInfo.TextureID);
							decodeTexture(item);
						}
						else
						{
							JLogger.debug("Not Texture to load  ...");
						}

					} catch (InterruptedException e) {
						JLogger.warn("Texture Thread Interrupted" + Utils.getExceptionStackTraceAsString(e));
						threadRunning.set(false);
					} 
					catch(Exception e)
					{
						JLogger.warn("Exception in Texture Thread" + Utils.getExceptionStackTraceAsString(e));
					}
				}
				JLogger.debug("Texture Thread Stopped ...");
			}	
		});
		}
	}


	public void decodeTextures() throws InterruptedException
	{
		JLogger.debug("Texture Thread Started ...");
			TextureLoadItem item = null;
			while((item = PendingTexturesToDownload.poll(1, TimeUnit.SECONDS)) != null)
			{
				try {
				JLogger.debug("Loading Texture OpenGL " + item.Data.TextureInfo.TextureID);
				decodeTexture(item);
				}
				catch(Exception e)
				{
					System.out.println("Exception " + e.getMessage());
					JLogger.warn("Exception in Texture Thread" + Utils.getExceptionStackTraceAsString(e));
				}
			}
	}
	
	public void decodeTexture(TextureLoadItem item) throws Exception
	{
		// Already have this one loaded
		if (item.Data.TextureInfo.TexturePointer != 0) 
			return;

		byte[] imageBytes = null;
		if (item.TGAData != null)
		{
			imageBytes = item.TGAData;
		}
		else if (item.TextureData != null || item.LoadAssetFromCache)
		{
			if (item.LoadAssetFromCache)
			{
				item.TextureData = Client.assets.Cache.getCachedAssetBytes(item.Data.TextureInfo.TextureID);
			}

			DecodedTgaImage cachedImage  = decodeJpeg200Image(item.TextureData);
			if(cachedImage == null)
				throw new Exception("Error in decoding the image: " + item.Data.TextureInfo.TextureID);

			item.Data.TextureInfo.HasAlpha = cachedImage.hasAlpha;
			item.Data.TextureInfo.FullAlpha = cachedImage.fullAlpha;
			item.Data.TextureInfo.IsMask = cachedImage.isMask;
			imageBytes = cachedImage.data;

			Client.assets.Cache.compressAndSaveImageToCache(item.TeFace.getTextureID(), cachedImage.data
					,cachedImage.hasAlpha, cachedImage.fullAlpha, cachedImage.isMask);

		}

		if (imageBytes != null)
		{
			IBitmap img = LoadTGAClass.LoadTGA(new ByteArrayInputStream(imageBytes));
			//FIXME should we flip along x axis or Y axis
//			img.rotateAndFlip(0, false, false);
			item.bitmap = img;
			PendingTexturesToGLLoad.offer(item);
		}

		item.TextureData = null;
		item.TGAData = null;
		imageBytes = null;
	}

	
	public void glLoadTextures() throws Exception
	{
		TextureLoadItem item = null;
		while((item = PendingTexturesToGLLoad.poll()) != null)
		{
			item.Data.TextureInfo.TexturePointer = RHelp.GLLoadImage(item.bitmap, item.Data.TextureInfo.HasAlpha);
			System.out.println("Texture Pointer: " + item.TeFace.getTextureID().toString() + " --> " + item.Data.TextureInfo.TexturePointer);
			GL11.glFlush();
			item.bitmap.dispose();
		}
	}
	

	public void requestDownloadTexture(TextureLoadItem item) throws Exception
	{
		synchronized (texturesPtrMap)
		{
//			System.out.println("Going to download Texture " + item.TeFace.getTextureID());
			if (texturesPtrMap.containsKey(item.TeFace.getTextureID()))
			{
				item.Data.TextureInfo = texturesPtrMap.get(item.TeFace.getTextureID());
			}
			else if (item.TeFace.getTextureID().equals(invisi1) || item.TeFace.getTextureID().equals(invisi2))
			{
				texturesPtrMap.put(item.TeFace.getTextureID(), item.Data.TextureInfo);
				texturesPtrMap.get(item.TeFace.getTextureID()).HasAlpha = false;
				texturesPtrMap.get(item.TeFace.getTextureID()).IsInvisible = true;
			}
			else
			{
				texturesPtrMap.put(item.TeFace.getTextureID(), item.Data.TextureInfo);
				if (item.TextureData == null && item.TGAData == null)
				{
					LoadCachedImageResult cachedTgaImage = Client.assets.Cache.loadCompressedImageFromCache(item.TeFace.getTextureID());
					if (cachedTgaImage !=null)
					{
						item.TGAData = cachedTgaImage.data;
						item.Data.TextureInfo.HasAlpha = cachedTgaImage.hasAlpha ;
						item.Data.TextureInfo.FullAlpha  = cachedTgaImage.fullAlpha;
						item.Data.TextureInfo.IsMask = cachedTgaImage.isMask;
						PendingTexturesToDownload.offer(item);
					}
					else if (Client.assets.Cache.hasAsset(item.TeFace.getTextureID()))
					{
						item.LoadAssetFromCache = true;
						PendingTexturesToDownload.offer(item);
					}
					else if (!item.Data.TextureInfo.FetchFailed)
					{
						Client.assets.RequestImage(item.TeFace.getTextureID(), 
								getTextureDownloadCallback(item));
					}
				}
				else
				{
					PendingTexturesToDownload.offer(item);
				}
			}
		}
		
//		System.out.println("Downloading Texture Completed" + item.TeFace.getTextureID());

	}

	private DecodedTgaImage decodeJpeg200Image(byte[] imageData) throws Exception
	{
		return OpenJPEGFactory.getIntance().DecodeToTgaImage(imageData);
	}


	public MethodDelegate<Void, TextureDownloadCallbackArgs>  getTextureDownloadCallback(final TextureLoadItem item)
	{
		return new MethodDelegate<Void, TextureDownloadCallbackArgs>()
				{
			public Void execute(TextureDownloadCallbackArgs e) 
			{
				switch (e.getState())
				{
				case Finished:
					item.TextureData = e.getAssetTexture().AssetData;
					PendingTexturesToDownload.offer(item);
					Client.assets.Cache.saveAssetToCache(item.Data.TextureInfo.TextureID, e.getAssetTexture().AssetData);
					break;

				case Aborted:
				case NotFound:
				case Timeout:
					item.Data.TextureInfo.FetchFailed = true;
					break;
				}
				return null;
			}

				};
	}

}
