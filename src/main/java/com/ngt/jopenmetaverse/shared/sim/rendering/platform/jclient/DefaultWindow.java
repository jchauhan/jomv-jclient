/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering.platform.jclient;


import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;


public class DefaultWindow extends GLApp{
    /**
     * Initialize the scene.  Called by GLApp.run().  For now the default
     * settings will be fine, so no code here.
     */
    public void setup()
    {
    }

    /**
     * Render one frame.  Called by GLApp.run().
     */
    public void draw() {
        // Clear screen and depth buffer
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
        // Select The Modelview Matrix (controls model orientation)
        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        // Reset the coordinate system to center of screen
        GL11.glLoadIdentity();
        
       
        // Place the viewpoint
        GLU.gluLookAt(
            0f, 0f, 10f,   // eye position (10 units in front of the origin)
            0f, 0f, 0f,    // target to look at (the origin)
            0f, 1f, 0f);   // which way is up (Y axis)
        // draw a triangle centered around 0,0,0
        GL11.glBegin(GL11.GL_TRIANGLES);           // draw triangles
            GL11.glVertex3f( 0.0f, 1.0f, 0.0f);         // Top
            GL11.glVertex3f(-1.0f,-1.0f, 0.0f);         // Bottom Left
            GL11.glVertex3f( 1.0f,-1.0f, 0.0f);         // Bottom Right
        GL11.glEnd();                              // done
    }	
}