/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering.mesh.primmesher;

import java.util.ArrayList;
import java.util.List;
import com.ngt.jopenmetaverse.shared.sim.imaging.IBitmap;

public class SculptMap
    {
        public int width;
        public int height;
        public byte[] redBytes;
        public byte[] greenBytes;
        public byte[] blueBytes;

        public SculptMap()
        {
        }

        public SculptMap(IBitmap bm, int lod) throws Exception
        {
            int bmW = bm.getWidth();
            int bmH = bm.getHeight();
            //Used to dispose the cloned image
            boolean imgCloned = false;

            if (bmW == 0 || bmH == 0)
                throw new Exception("SculptMap: bitmap has no data");

            int numLodPixels = lod * 2 * lod * 2;  // (32 * 2)^2  = 64^2 pixels for default sculpt map image

            boolean needsScaling = false;

            boolean smallMap = bmW * bmH <= lod * lod;

            width = bmW;
            height = bmH;
            while (width * height > numLodPixels)
            {
                width >>= 1;
                height >>= 1;
                needsScaling = true;
            }

            try
            {
                if (needsScaling)
                {
//                    bm = ScaleImage(bm, width, height,
//                        System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor);
                	bm = bm.cloneAndResize(width, height);
                	imgCloned = true;
                }
            }

            catch (Exception e)
            {
                throw new Exception("Exception in ScaleImage(): e: " + e.toString());
            }
            finally
            {if(imgCloned) bm.dispose();}

            if (width * height > lod * lod)
            {
                width >>= 1;
                height >>= 1;
            }

            int numBytes = smallMap ? width * height : (width + 1) * (height + 1);
            redBytes = new byte[numBytes];
            greenBytes = new byte[numBytes];
            blueBytes = new byte[numBytes];

            int byteNdx = 0;

            try
            {
                if (smallMap)
                    for (int y = 0; y < height; y++)
                    {
                        for (int x = 0; x < width; x++)
                        {
                            int c = bm.getRGB(x, y);

                            redBytes[byteNdx] = (byte)((c >> 16) & 0xff); 
                            greenBytes[byteNdx] = (byte)((c >> 8) & 0xff); 
                            blueBytes[byteNdx] = (byte)(c & 0xff); 

                            ++byteNdx;
                        }
                    }
                else
                    for (int y = 0; y <= height; y++)
                    {
                        for (int x = 0; x <= width; x++)
                        {
                            int c = bm.getRGB(x < width ? x * 2 : x * 2 - 1,
                                                y < height ? y * 2 : y * 2 - 1);
                            
                            //System.out.print(String.format("x %d y %d %s", x, y, Utils.bytesToHexDebugString(Utils.intToBytes(c), "")));
                            
                            redBytes[byteNdx] = (byte)((c >> 16) & 0xff); 
                            greenBytes[byteNdx] = (byte)((c >> 8) & 0xff); 
                            blueBytes[byteNdx] = (byte)(c & 0xff); 

                            ++byteNdx;
                        }
                    }
            }
            catch (Exception e)
            {
                throw new Exception("Caught exception processing byte arrays in SculptMap(): e: " + e.toString());
            }
            finally
            {if(imgCloned) bm.dispose();}

            if (!smallMap)
            {
                width++;
                height++;
            }
        }

        public List<List<Coord>> ToRows(boolean mirror)
        {
            int numRows = height;
            int numCols = width;

            List<List<Coord>> rows = new ArrayList<List<Coord>>(numRows);

            float pixScale = 1.0f / 255;

            int rowNdx, colNdx;
            int smNdx = 0;

            for (rowNdx = 0; rowNdx < numRows; rowNdx++)
            {
                List<Coord> row = new ArrayList<Coord>(numCols);
                for (colNdx = 0; colNdx < numCols; colNdx++)
                {
                    if (mirror)
                        row.add(new Coord(-( (redBytes[smNdx] & 0x00ff) * pixScale - 0.5f), ((greenBytes[smNdx] & 0x00ff) * pixScale - 0.5f), (blueBytes[smNdx] & 0x00ff) * pixScale - 0.5f));
                    else
                        row.add(new Coord((redBytes[smNdx] & 0x00ff) * pixScale - 0.5f, (greenBytes[smNdx] & 0x00ff) * pixScale - 0.5f, (blueBytes[smNdx] & 0x00ff) * pixScale - 0.5f));

                   // System.out.println("Added Coord " + row.get(row.size()-1));	
                    
                    ++smNdx;
                }
                rows.add(row);
            }
            return rows;
        }
    }