/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering.mesh.lmesh;

import java.io.File;
import java.io.IOException;

import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.BitPack;
import com.ngt.jopenmetaverse.shared.types.Vector3;
import com.ngt.jopenmetaverse.shared.util.FileUtils;
import com.ngt.jopenmetaverse.shared.util.Utils;
  /// <summary>
    /// Level of Detail mesh
    /// </summary>
    public class LODMesh
    {
        static final String MESH_HEADER = "Linden Binary Mesh 1.0";
        static final String MORPH_FOOTER = "End Morphs";
        
        public float MinPixelWidth;

        protected String _header;
        protected boolean _hasWeights;
        protected boolean _hasDetailTexCoords;
        protected Vector3 _position;
        protected Vector3 _rotationAngles;
        protected byte _rotationOrder;
        protected Vector3 _scale;
        //ushort
        protected int _numFaces;
        protected Face[] _faces;

        public void LoadMesh(String filename) throws IOException
        {
            byte[] buffer = FileUtils.readBytes(new File(filename));
            BitPack input = new BitPack(buffer, 0);

            _header = Utils.TrimAt0(input.UnpackString(24));
            if (!_header.equalsIgnoreCase(MESH_HEADER))
                throw new IOException("Unrecognized mesh format");

            // Populate base mesh variables
            _hasWeights = (input.UnpackByte() != 0);
            _hasDetailTexCoords = (input.UnpackByte() != 0);
            _position = new Vector3(input.UnpackFloat(), input.UnpackFloat(), input.UnpackFloat());
            _rotationAngles = new Vector3(input.UnpackFloat(), input.UnpackFloat(), input.UnpackFloat());
            _rotationOrder = input.UnpackByte();
            _scale = new Vector3(input.UnpackFloat(), input.UnpackFloat(), input.UnpackFloat());
            _numFaces = input.UnpackUShort();

            _faces = new Face[_numFaces];

            for (int i = 0; i < _numFaces; i++)
                _faces[i].Indices = new short[] { input.UnpackShort(), input.UnpackShort(), input.UnpackShort() };
        }
    }