/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering.mesh.primmesher;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ngt.jopenmetaverse.shared.util.Utils;

public class ObjMesh
{
	List<Coord> coords = new ArrayList<Coord>();
	List<Coord> normals = new ArrayList<Coord>();
	List<UVCoord> uvs = new ArrayList<UVCoord>();

	public String meshName = "";
	public List<List<ViewerVertex>> viewerVertices = new ArrayList<List<ViewerVertex>>();
	public List<List<ViewerPolygon>> viewerPolygons = new ArrayList<List<ViewerPolygon>>();

	List<ViewerVertex> faceVertices = new ArrayList<ViewerVertex>();
	List<ViewerPolygon> facePolygons = new ArrayList<ViewerPolygon>();
	public int numPrimFaces;

	Map<Integer, Integer> viewerVertexLookup = new HashMap<Integer, Integer>();

	public ObjMesh(String path) throws IOException
	{
		BufferedReader in
		= new BufferedReader(new FileReader(path));
		ProcessStream(in);
	}


	public ObjMesh(BufferedReader sr) throws IOException
	{
		ProcessStream(sr);
	}


	private void ProcessStream(BufferedReader s) throws IOException
	{
		numPrimFaces = 0;

		String line = null;
		while ( (line = s.readLine()) != null)
		{
			line = line.trim();
			String[] tokens = line.split("\\s+");

			// Skip blank lines and comments
			if (tokens.length > 0 && tokens[0] != "" && !tokens[0].endsWith("#"))
				ProcessTokens(tokens);
		}
		MakePrimFace();
	}

	public VertexIndexer GetVertexIndexer()
	{
		VertexIndexer vi = new VertexIndexer();
		vi.numPrimFaces = this.numPrimFaces;
		vi.viewerPolygons = this.viewerPolygons;
		vi.viewerVertices = this.viewerVertices;

		return vi;
	}


	private void ProcessTokens(String[] tokens)
	{
		String token = tokens[0].toLowerCase();

		if(token.equals("o"))
		{
			meshName = tokens[1];
		}
		else if(token.equals("v"))
		{
			coords.add(ParseCoord(tokens));

		}
		else if(token.equals("vt"))
		{
			uvs.add(ParseUVCoord(tokens));
		}
		else if(token.equals("vn"))
		{
			normals.add(ParseCoord(tokens));
		}
		else if(token.equals("g"))
		{

			MakePrimFace();
		}
		else if(token.equals("s"))
		{
		}
		else if(token.equals("f"))
		{
			int[] vertIndices = new int[3];
			int[] temp = new int[1];

			for (int vertexIndex = 1; vertexIndex <= 3; vertexIndex++)
			{
				String[] indices = tokens[vertexIndex].split("/");

				int positionIndex = Integer.parseInt(indices[0]) - 1;

				int texCoordIndex = -1;
				int normalIndex = -1;

				if (indices.length > 1)
				{
					if (Utils.tryParseInt(indices[1], temp))
					{
						texCoordIndex = temp[0];
						texCoordIndex--;
					}
					else texCoordIndex = -1;

				}

				if (indices.length > 2)
				{
					if (Utils.tryParseInt(indices[1], temp))
					{
						normalIndex = temp[0];
						normalIndex--;
					}
					else normalIndex = -1;
				}

				int hash = hashInts(positionIndex, texCoordIndex, normalIndex);

				if (viewerVertexLookup.containsKey(hash))
					vertIndices[vertexIndex - 1] = viewerVertexLookup.get(hash);
				else
				{
					ViewerVertex vv = new ViewerVertex();
					vv.v = coords.get(positionIndex);
					if (normalIndex > -1)
						vv.n = normals.get(normalIndex);
					if (texCoordIndex > -1)
						vv.uv = uvs.get(texCoordIndex);
					faceVertices.add(vv);
					viewerVertexLookup.put(hash, faceVertices.size() - 1);
					vertIndices[vertexIndex - 1] = faceVertices.size() - 1;
				}
			}

			facePolygons.add(new ViewerPolygon(vertIndices[0], vertIndices[1], vertIndices[2]));
		}
		else if(token.equals("mtllib"))
		{
		}
		else if(token.equals("usemtl"))
		{
		}
	}


	private void MakePrimFace()
	{
		if (faceVertices.size() > 0 && facePolygons.size() > 0)
		{
			viewerVertices.add(faceVertices);
			faceVertices = new ArrayList<ViewerVertex>();
			viewerPolygons.add(facePolygons);

			facePolygons = new ArrayList<ViewerPolygon>();

			viewerVertexLookup = new HashMap<Integer, Integer>();

			numPrimFaces++;
		}
	}

	private UVCoord ParseUVCoord(String[] tokens)
	{
		return new UVCoord(
				Float.parseFloat(tokens[1]),
				Float.parseFloat(tokens[1]));
	}

	private Coord ParseCoord(String[] tokens)
	{
		return new Coord(
				Float.parseFloat(tokens[1]),
				Float.parseFloat(tokens[2]),
				Float.parseFloat(tokens[3]));
	}

	private int hashInts(int i1, int i2, int i3)
	{
		return (i1 + " " + i2 + " " + i3).hashCode();
	}
}