/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;

import com.ngt.jopenmetaverse.shared.sim.GridClient;
import com.ngt.jopenmetaverse.shared.sim.imaging.IBitmap;
import com.ngt.jopenmetaverse.shared.sim.rendering.math.FloatMatrix;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.InterleavedVBO;
import com.ngt.jopenmetaverse.shared.types.Quaternion;
import com.ngt.jopenmetaverse.shared.types.Vector3;

public class RenderTerrain 
{
	GridClient client;

//	float[][] heightMap = new float[256][256];
//	short[] newIndices;
//	InterleavedVBO newVertexVBO;

	//FIXME if the number of vertices increases more than GL12.GL_MAX_ELEMENTS_VERTICES slow down happens
	// Same case with GL12.GL_MAX_ELEMENTS_INDICES
	InterleavedVBO vertexVBO;
	IBitmap terrainTexture;
	short[] indices;
	/// <summary>Rendered position of the object in the region</summary>
	public Vector3 RenderPosition;
	/// <summary>Rendered rotationm of the object in the region</summary>
	public Quaternion RenderRotation;

	private FloatBuffer vertexDataBuffer;
	private ShortBuffer indexDataBuffer;
	private IntBuffer tmpIntBuffer;
	private FloatBuffer floatBuffer;

	public boolean Initialized;
	
	private int vertexVBOId = -1;
	private int indexVBOId = -1;
	private int textureId = -1;

//	private AtomicBoolean updatingTerrainHeightmap = new AtomicBoolean(false); 
//	private AtomicBoolean updatedTerrainHeightmap = new AtomicBoolean(false);
//	private AtomicBoolean updatingTexture = new AtomicBoolean(false); 
//	private AtomicBoolean updatingTerrain = new AtomicBoolean(false);
//	private AtomicBoolean terrainUpdated = new AtomicBoolean(false); 



	public RenderTerrain(GridClient client, InterleavedVBO vertexVBO, short[] indices, IBitmap bitmap) {
		super();
		this.client = client;
		this.vertexVBO = vertexVBO;
		this.indices = indices;
		this.terrainTexture = bitmap;
		Initialized = false;
	}

	public short[] getIndices() {
		return indices;
	}
	public void setIndices(short[] indices) {
		this.indices = indices;
	}

	public void Render(RenderingContext renderingContext)
	{
		FloatMatrix totalMatrix = renderingContext.getResultingMatrix();
		floatBuffer.position(0);
		floatBuffer.put(totalMatrix.getColumnWiseFlatData()).position(0);
		
		GL20.glUniformMatrix4(renderingContext.getProjectionMatrixUniform(), false, floatBuffer);
		
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vertexVBOId);

		GL20.glVertexAttribPointer(renderingContext.getVertexNormalAttribute(), vertexVBO.getVertexNormalSize(), GL11.GL_FLOAT, false, 
				vertexVBO.getStride(), vertexVBO.getVertexNormalOffet());
		GL20.glVertexAttribPointer(renderingContext.getVertexPositionAttribute(), vertexVBO.getVertexSize(), GL11.GL_FLOAT, false, 
				vertexVBO.getStride(), vertexVBO.getVertexOffset());		
		GL20.glVertexAttribPointer(renderingContext.getTextureCoordAttribute(), vertexVBO.getTexCoordSize(), GL11.GL_FLOAT, false, 
				vertexVBO.getStride(), vertexVBO.getTexCoordOffset());

		// Bind the texture to texture unit 0
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureId);
		renderingContext.getShaderProgram().setUniform1(renderingContext.getTextureUniform(), 0);

		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, indexVBOId);
		//		checkErrors("After Binding Element Array Buffer");
		GL11.glDrawElements(GL11.GL_TRIANGLES, indices.length, GL11.GL_UNSIGNED_SHORT, 0);
		GL11.glFlush();		
	}

//	public void updateTerrain()
//	{
//		Simulator sim = client.network.getCurrentSim();
//		heightMap = TerrainHelper.createHeightTable(sim.Terrain, heightMap);
//		MeshmerizerR renderer = new MeshmerizerR();
//		Face terrainFace = renderer.TerrainMesh(heightMap, 0f, 255f, 0f, 255f);
//		ColorVertex[] terrainVertices = TerrainHelper.genColorVertices(terrainFace);
//		newIndices = terrainFace.toIndicesArray();
//		byte[] data = ColorVertex.toBytes(terrainVertices);
//		newVertexVBO = new InterleavedVBO(data, ColorVertex.Size, 3, 3, 
//				2, 0, 12, 24);
//	}
//
//	public void updateTexture() throws Exception
//	{
//		Simulator sim = client.network.getCurrentSim();
//		UUID[] textureIds = new UUID[] { sim.TerrainDetail0, sim.TerrainDetail1, sim.TerrainDetail2, sim.TerrainDetail3 };
//		float[] startHeights = new float[] { sim.TerrainStartHeight00, sim.TerrainStartHeight01, sim.TerrainStartHeight10, sim.TerrainStartHeight11 };
//		float[] heightRanges = new float[] { sim.TerrainHeightRange00, sim.TerrainHeightRange01, sim.TerrainHeightRange10, sim.TerrainHeightRange11 };
//		IBitmap bitmap = TerrainSplat.Splat(client, heightMap, textureIds, startHeights, heightRanges);
//
//		System.out.println(String.format("Image splated Width %d, Height %d", bitmap.getWidth(), bitmap.getHeight()));
//		//TODO need to implement
//	}

	public void Initialize() throws Exception
	{
		if(tmpIntBuffer == null)
			tmpIntBuffer = ByteBuffer.allocateDirect(1*4).order(ByteOrder.nativeOrder()).asIntBuffer();
		if(floatBuffer == null)
			floatBuffer = ByteBuffer.allocateDirect(16 * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
		
		initvertexDataBuffer(tmpIntBuffer);
		initindexDataBuffer(tmpIntBuffer);
		initTexture(tmpIntBuffer);
		Initialized = true;
	}

	
	
	public void Dispose()
	{
		if (vertexVBOId != -1) GL15.glDeleteBuffers(vertexVBOId);
		if (indexVBOId != -1) GL15.glDeleteBuffers(indexVBOId);
		if(textureId != -1) GL15.glDeleteBuffers(textureId);
		vertexVBOId = -1;
		indexVBOId = -1;
		if(vertexDataBuffer != null) vertexDataBuffer.clear();
		if(indexDataBuffer != null) indexDataBuffer.clear();
		if(tmpIntBuffer != null) tmpIntBuffer.clear();
	}
	
	private void initTexture(IntBuffer tmpIntBuffer) throws Exception
	{
		 textureId = RHelp.GLLoadImage(terrainTexture, true);
		 GL11.glEnable(GL11.GL_TEXTURE_2D);
		 GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureId);
	}

	
	private void initvertexDataBuffer(IntBuffer tmpIntBuffer)
	{
		tmpIntBuffer.position(0);
		GL15.glGenBuffers(tmpIntBuffer);
		vertexVBOId = tmpIntBuffer.get(0);

		float[] fdata = new float[vertexVBO.getVboData().length/4];
		ByteBuffer.wrap(vertexVBO.getVboData()).asFloatBuffer().get(fdata);

		vertexDataBuffer = ByteBuffer.allocateDirect(vertexVBO.getVboData().length).order(ByteOrder.nativeOrder()).asFloatBuffer()
				.put(fdata);
		vertexDataBuffer.position(0);

		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vertexVBOId);
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, vertexDataBuffer, GL15.GL_STATIC_DRAW);
	}


	private void initindexDataBuffer(IntBuffer tmpIntBuffer)
	{
		tmpIntBuffer.position(0);
		GL15.glGenBuffers(tmpIntBuffer);
		indexVBOId = tmpIntBuffer.get(0);

		indexDataBuffer = ByteBuffer.allocateDirect(indices.length*2).order(ByteOrder.nativeOrder()).asShortBuffer();
		indexDataBuffer.put(indices);
		indexDataBuffer.position(0);

		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, indexVBOId);
		GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, indexDataBuffer, GL15.GL_STATIC_DRAW);
	}

}
