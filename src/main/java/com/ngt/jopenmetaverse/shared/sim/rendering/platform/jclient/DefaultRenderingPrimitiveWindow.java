/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering.platform.jclient;

import java.io.File;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.List;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import com.ngt.jopenmetaverse.shared.sim.Settings;
import com.ngt.jopenmetaverse.shared.sim.Simulator.SimStats;
import com.ngt.jopenmetaverse.shared.sim.imaging.IBitmap;
import com.ngt.jopenmetaverse.shared.sim.imaging.platform.jclient.BitmapBufferedImageImpl;
import com.ngt.jopenmetaverse.shared.sim.rendering.RHelp;
import com.ngt.jopenmetaverse.shared.sim.rendering.RenderPass;
import com.ngt.jopenmetaverse.shared.sim.rendering.RenderPrimitive;
import com.ngt.jopenmetaverse.shared.sim.rendering.RenderingContext;
import com.ngt.jopenmetaverse.shared.sim.rendering.ShaderProgram;
import com.ngt.jopenmetaverse.shared.sim.rendering.TextureManagerR;
import com.ngt.jopenmetaverse.shared.sim.rendering.math.ConversionUtils;
import com.ngt.jopenmetaverse.shared.sim.rendering.math.FloatMatrix;
import com.ngt.jopenmetaverse.shared.sim.rendering.math.FloatMatrix4x4;
import com.ngt.jopenmetaverse.shared.sim.rendering.math.MatrixUtil;
import com.ngt.jopenmetaverse.shared.sim.rendering.math.Sphere;
import com.ngt.jopenmetaverse.shared.sim.rendering.math.Vector3f;
import com.ngt.jopenmetaverse.shared.sim.rendering.math.Vectorf;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.InterleavedVBO;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.primmesher.Coord;
import com.ngt.jopenmetaverse.shared.types.Quaternion;
import com.ngt.jopenmetaverse.shared.types.Vector3;
import com.ngt.jopenmetaverse.shared.util.PlatformUtils;


public class DefaultRenderingPrimitiveWindow extends GLApp{
	private Sphere cube ;

	private RenderingContext renderingContext;
	SimStats simStats;
	TextureManagerR textureManager;
	List<RenderPrimitive> renderPrimitives;


	private ShaderProgram shaderProgram;
	private int vertexPositionAttribute;
	private int textureCoordAttribute;
	private int vertexNormalAttribute;

	private FloatBuffer resultingMatrixBuffer;
	private FloatBuffer normalMatrixBuffer;


	private int projectionMatrixUniform;
	private int textureUniform;	
	private int uAmbientColorUniform;
	private int uLightingDirectionUniform;
	private int uDirectionalColorUniform;
	private int uUseLightingUniform;
	private int normalMatrixUniform;

	private int speedX = 0;
	private int speedY = 0;
	private int angleX = 0;
	private int angleY = 0;
	private int angleZ = 0;
	private float translateX = -207;
	private float translateY = -56;
	private float translateZ = -23;
//	private int currentTexture = 0;
		
	//Input Values for Lighting Effect
	int uUseLighting = 1;
	private Vectorf lightingDirection = new Vector3f(-1, -1, -1);
	private float directionalColorRed = 0.8f;
	private float directionalColorGreen = 0.8f;
	private float directionalColorBlue = 0.8f;
	private float ambientColorRed = 0.5f;
	private float ambientColorGreen = 0.5f;
	private float ambientColorBlue = 0.5f;

	private FloatMatrix perspectiveMatrix;
	private FloatMatrix translationMatrix;
	private FloatMatrix rotationMatrix;
	private FloatMatrix resultingMatrix;


	/*
	 * Temporary and Misc 
	 */
	IntBuffer tmpIntBuffer;

	public DefaultRenderingPrimitiveWindow(SimStats simStats, TextureManagerR textureManager, List<RenderPrimitive> renderPrimitives)
	{
		this.simStats = simStats;
		this.textureManager = textureManager;
		textureManager.start();
		this.renderPrimitives = renderPrimitives;
	}


	/**
	 * Initialize the scene.  Called by GLApp.run().  For now the default
	 * settings will be fine, so no code here.
	 * @throws Exception 
	 */
	public void setup() throws Exception
	{
		try{
			System.out.println("Setup Initialized..");

		GL11.glEnableClientState(GL11.GL_VERTEX_ARRAY);
		GL11.glEnableClientState(GL11.GL_NORMAL_ARRAY);
		GL11.glEnableClientState(GL11.GL_COLOR_ARRAY);
		GL11.glEnableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
		//    	initParams(); //done by initGL
		initShaders();
		initBuffers();
		initTextures();
				
		renderingContext =  new  RenderingContext(textureManager,
				shaderProgram, projectionMatrixUniform,
				textureUniform, uAmbientColorUniform,
				uLightingDirectionUniform, uDirectionalColorUniform,
				uUseLightingUniform, normalMatrixUniform,
				vertexPositionAttribute, textureCoordAttribute,
				vertexNormalAttribute);
		
		//Update initial coordinates same as the parent primitive
		int count =0;
		for(RenderPrimitive r :renderPrimitives)
		{
			if(r.getBasePrim().ParentID == 0)
			{
				translateX = -1* r.getBasePrim().Position.X;
				translateY = -1* r.getBasePrim().Position.Y; 
				translateZ = -1* r.getBasePrim().Position.Z; 

			}
			count ++;
		}
		
		if(count ==1)
		{
			RenderPrimitive r = renderPrimitives.get(0);
			translateX = -1* r.getBasePrim().Position.X;
			translateY = -1* r.getBasePrim().Position.Y; 
			translateZ = -1* r.getBasePrim().Position.Z;
			r.RenderPosition = new Vector3(r.getBasePrim().Position);
			r.RenderRotation = new Quaternion(0, 0, 0, 1);
			System.out.println(String.format("Pos %s Rot %s", r.RenderPosition.toString(), 
					r.RenderRotation.toString()));
		}
		
		System.out.println("Setup Finished...");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw(e);
		}
		
	}

	/**
	 * Render one frame.  Called by GLApp.run().
	 */
	public void draw() {
		System.out.println("Drawing ...");
//		angleX = (angleX + speedX) % 360;
//		angleY = (angleY + speedY) % 360;
		// angleZ=(angleZ+2)%360;

		// Clear screen and depth buffer
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
		
		float[] resultingMatrixData = updateView();
		resultingMatrixBuffer.position(0);
		resultingMatrixBuffer.put(resultingMatrixData);
		resultingMatrixBuffer.position(0);


		// Set the lighting related uniforms
		GL20.glUniform1i(uUseLightingUniform, uUseLighting);
		Vectorf adjustedLightDirection = lightingDirection.toUnitVector()
				.multiply(-1);
		float[] flatLightDirection = adjustedLightDirection.toArray();
		GL20.glUniform3f(uLightingDirectionUniform, flatLightDirection[0], flatLightDirection[1],
				flatLightDirection[2]);		
		GL20.glUniform3f(uAmbientColorUniform, ambientColorRed, ambientColorGreen, ambientColorBlue);		
		GL20.glUniform3f(uDirectionalColorUniform, directionalColorRed, directionalColorGreen, directionalColorBlue);	

		//Normal matrix to adjust the directional light depending upon rotation
		FloatMatrix4x4 normalMatrix = ((FloatMatrix4x4)rotationMatrix).inverse();
		normalMatrix = normalMatrix.transpose();
		float[] normalMatrixData = normalMatrix.getColumnWiseFlatData();
		normalMatrixBuffer.position(0);
		normalMatrixBuffer.put(normalMatrixData);
		normalMatrixBuffer.position(0);

		GL20.glUniformMatrix4(normalMatrixUniform, false, normalMatrixBuffer);
		checkErrors("Drawing.Initializing NormalMatrixUniform");

		renderingContext.setResultingMatrixBuffer(resultingMatrixBuffer);
		renderingContext.setResultingMatrix(resultingMatrix);
		renderingContext.setRotationMatrix(rotationMatrix);
		renderingContext.setTranslationMatrix(translationMatrix);
		renderingContext.setPerspectiveMatrix(perspectiveMatrix);
		
		try {
			textureManager.glLoadTextures();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		for(RenderPrimitive renderPrimitive: renderPrimitives)
		{
			try {
				if(!renderPrimitive.Initialized)
					renderPrimitive.Initialize();
				
//				System.out.println("After Initializaion Prim ID " + renderPrimitive.getBasePrim().LocalID + " Resultant Rotation: " + renderPrimitive.getBasePrim().Rotation.toString());
				
				
				renderPrimitive.Step(0, simStats.Dilation);
				
//				System.out.println("After Step Prim ID " + renderPrimitive.getBasePrim().LocalID + " Resultant Rotation: " + renderPrimitive.getBasePrim().Rotation.toString());
//				System.out.println("Calculated Pos: " + renderPrimitive.InterpolatedPosition.toString() 
//						+ " Rot: " + renderPrimitive.InterpolatedRotation.toString());
				
				
				renderPrimitive.calPrimPosAndRot(new Vector3(0, 0, 0));
				
//				System.out.println("After Calculation " + renderPrimitive.getBasePrim().LocalID + " Resultant Rotation: " + renderPrimitive.getBasePrim().Rotation.toString());
//				System.out.println("Calculated Pos: " + renderPrimitive.RenderPosition.toString() 
//						+ " Rot: " + renderPrimitive.RenderRotation.toString());
				
//				if(renderPrimitive.parentSceneObject != null)
//				System.out.println(String.format("Prim: %d \n\tPosition %s Rotation %s " +
//						"\n\t InterpolatedPosition %s InterpolatedRotation %s " +
//						"\n\tRenderPosition %s RenderRotation %s" +
//						"\n\t Parent Pos %s Rot %s"
//						, renderPrimitive.getBasePrim().LocalID
//						, renderPrimitive.getBasePrim().Position,  renderPrimitive.getBasePrim().Rotation
//						, renderPrimitive.InterpolatedPosition.toString() , renderPrimitive.InterpolatedRotation.toString() 
//						, renderPrimitive.RenderPosition.toString(), renderPrimitive.RenderRotation.toString()
//						, renderPrimitive.parentSceneObject.getBasePrim().Position, 
//						 renderPrimitive.parentSceneObject.getBasePrim().Rotation));
//				else
//					System.out.println(String.format("Prim: %d \n\tPosition %s Rotation %s " +
//							"\n\t InterpolatedPosition %s InterpolatedRotation %s " +
//							"\n\tRenderPosition %s RenderRotation %s"
//							, renderPrimitive.getBasePrim().LocalID
//							, renderPrimitive.getBasePrim().Position,  renderPrimitive.getBasePrim().Rotation
//							, renderPrimitive.InterpolatedPosition.toString() , renderPrimitive.InterpolatedRotation.toString() 
//							, renderPrimitive.RenderPosition.toString(), renderPrimitive.RenderRotation.toString()
//							));
				
				renderPrimitive.Render(RenderPass.Simple, 0, renderingContext, 0);
				renderPrimitive.Render(RenderPass.Alpha, 0, renderingContext, 0);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
//		PlatformUtils.sleep(10000);
	}

	public float[]  updateView()
	{
		perspectiveMatrix = MatrixUtil.createPerspectiveMatrix(45, getWidth()/getHeight(), 0.1f, 100);
		translationMatrix = MatrixUtil.createTranslationMatrix(translateX, translateY, translateZ);
		rotationMatrix = MatrixUtil.createRotationMatrix(angleX, angleY, angleZ);
		resultingMatrix = perspectiveMatrix.multiply(rotationMatrix).multiply(translationMatrix);
		System.out.println(String.format("Camera Pos at <%f, %f, %f>", translateX, translateY, translateZ));
		float[] resultingMatrixData = resultingMatrix.getColumnWiseFlatData(); 
		return resultingMatrixData;
	}

	@Override
	public void keyDown(int keycode) {
		if(keycode == Keyboard.KEY_UP)
			translateZ += 0.25;
		else if(keycode == Keyboard.KEY_DOWN)
			translateZ -= 0.25;
		else if(keycode == Keyboard.KEY_RIGHT)
			angleY += 0.25;
		else if(keycode == Keyboard.KEY_LEFT)
			angleY -= 0.25;
		
		else if(keycode == Keyboard.KEY_W)
			translateY += 1;
		else if(keycode == Keyboard.KEY_S)
			translateY -= 1;

		else if(keycode == Keyboard.KEY_E)
			angleX += 5;
		else if(keycode == Keyboard.KEY_D)
			angleX -= 5;

		else if(keycode == Keyboard.KEY_Q)
			angleZ += 5;
		else if(keycode == Keyboard.KEY_A)
			angleZ -= 5;

		else if(keycode == Keyboard.KEY_R)
			translateX += 1;
		else if(keycode == Keyboard.KEY_F)
			translateX -= 1; 
	}


	/**
	 * Creates the ShaderProgram used by the example to render.
	 * @throws Exception 
	 */
	 private void initShaders() throws Exception {

		 shaderProgram = new ShaderProgram();
		 boolean shaderLoadingStatus = shaderProgram.Load(new String[] {Settings.RESOURCE_DIR + "/shaders/vertex-shader-3.vert"
				 , Settings.RESOURCE_DIR + "/shaders/fragment-shader-3.frag"}, 
				 new String[] {"vertexPosition", "texPosition", "aVertexNormal"});

		 if(!shaderLoadingStatus)
			 throw new Exception("Shader could not be loaded");

		 shaderProgram.Start();

		 vertexPositionAttribute = shaderProgram.getAttribLocation("vertexPosition");
		 GL20.glEnableVertexAttribArray(vertexPositionAttribute);

		 textureCoordAttribute = shaderProgram.getAttribLocation( "texPosition");
		 GL20.glEnableVertexAttribArray(textureCoordAttribute);

		 vertexNormalAttribute = shaderProgram.getAttribLocation( "aVertexNormal");
		 GL20.glEnableVertexAttribArray(vertexNormalAttribute);


		 // get the position of the projectionMatrix uniform.
		 projectionMatrixUniform = shaderProgram.getUniformLocation("projectionMatrix");

		 // get the position of the tex uniform.
		 textureUniform = shaderProgram.getUniformLocation( "tex");

		 uUseLightingUniform = shaderProgram.getUniformLocation( "uUseLighting");
		 uAmbientColorUniform = shaderProgram.getUniformLocation( "uAmbientColor");
		 uLightingDirectionUniform = shaderProgram.getUniformLocation( "uLightingDirection");
		 uDirectionalColorUniform = shaderProgram.getUniformLocation( "uDirectionalColor");

		 normalMatrixUniform = shaderProgram.getUniformLocation( "normalMatrix");
		 checkErrors("Getting Shaders UniformLocation ");


	 }

	 /**
	  * Initializes the texture of this example.
	  * @throws Exception 
	  */
	 private void initTextures() throws Exception {
		 
		 //Dispose the textureManager by calling textureManager.dispose()
//		 textureManager.start();
			textureManager.decodeTextures();
		 
//		 if(terrainImage == null)
//			 terrainImage = new BitmapBufferedImageImpl(ImageIO.read(new File(Settings.RESOURCE_DIR + "/textures/moon.jpeg")));
//
//		 textureId[0] = RHelp.GLLoadImage(terrainImage, true);
//
//		 GL11.glEnable(GL11.GL_TEXTURE_2D);
//		 GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureId[0]);
	 }

	 /**
	  * Initializes the buffers for vertex coordinates, normals and texture
	  * coordinates.
	  */
	 private void initBuffers() {
		 tmpIntBuffer = ByteBuffer
				 .allocateDirect(1*4).order(ByteOrder.nativeOrder()).asIntBuffer();

		 float[] resultingMatrixData = updateView();
		 resultingMatrixBuffer = ByteBuffer.allocateDirect(resultingMatrixData.length*4).order(ByteOrder.nativeOrder()).asFloatBuffer();
		 resultingMatrixBuffer.put(resultingMatrixData);
		 resultingMatrixBuffer.position(0);

		 FloatMatrix4x4 normalMatrix = ((FloatMatrix4x4)rotationMatrix).inverse();
		 normalMatrix = normalMatrix.transpose();
		 float[] normalMatrixData = normalMatrix.getColumnWiseFlatData();
		 normalMatrixBuffer = ByteBuffer.allocateDirect(normalMatrixData.length*4).order(ByteOrder.nativeOrder()).asFloatBuffer();
		 normalMatrixBuffer.put(normalMatrixData);
		 normalMatrixBuffer.position(0);

		 //        initVertexBuffer(tmpIntBuffer);
		 //        initVertexTextureCoordBuffer(tmpIntBuffer);
		 //        initVertexNormalBufferBuffer(tmpIntBuffer);
//		 initVertexInterleavedBuffer(tmpIntBuffer);
//		 initVertexIndicesBuffer(tmpIntBuffer);

		 checkErrors("After Initializing BUffers");
	 }


	 private void checkErrors(String locaton) {
		 int error = GL11.glGetError();
		 if (error != GL11.GL_NO_ERROR) {
			 String message = "@ " + locaton + " OpenGL Error: " + error;
			 switch(error)
			 {
			 case GL11.GL_INVALID_OPERATION:
			 {
				 message += " INVALID_OPERATION";
				 break;
			 }
			 case GL11.GL_INVALID_ENUM:
			 {
				 message += " INVALID_ENUM";
				 break;
			 }
			 case GL11.GL_INVALID_VALUE:
			 {
				 message += " INVALID_VALUE";
				 break;
			 }
			 case GL11.GL_OUT_OF_MEMORY:
			 {
				 message += " OUT_OF_MEMORY";
				 break;
			 }
			 }			
			 throw new RuntimeException(message);
		 }
	 }
}