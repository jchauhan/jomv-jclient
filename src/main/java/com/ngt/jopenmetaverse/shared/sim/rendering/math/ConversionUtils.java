/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering.math;


import java.nio.FloatBuffer;
import java.util.List;

import com.ngt.jopenmetaverse.shared.util.Utils;

/**
 * Utility class for conversions (e.g. convert collections to arrays)
 * @author ssothman
 *
 */
public class ConversionUtils {
	/**
	 * Coverts Float-List to float array
	 * @param list List to convert
	 * @return float array
	 */
	public static float[] floatListToFloatArray(List<Float> list) {
		float[] array = new float[list.size()];
		for (int i = 0; i < list.size(); i++) {
			array[i] = list.get(i);
		}
		return array;
	}
	
	/**
	 * Converts Integer-List to int array
	 * @param list List to convert
	 * @return int array
	 */
	public static int[] integerListToIntegerArray(List<Integer> list) {
		int[] array = new int[list.size()];
		for (int i = 0; i < list.size(); i++) {
			array[i] = list.get(i);
		}
		return array;
	}
	
	/*
	 * Convert an array of integers to array of shorts
	 * @param array of integers
	 * @return array of short
	 */
	public static short[] integerArrayToInt16Array(int[] array)
	{
		short[] sarray = new short[array.length];
		for(int i =0; i < sarray.length; i++)
		{
			sarray[i] = (short)array[i];
		}
		
		return sarray;
	}
	
	
	public static byte[] floatArrayToByteArray(float[] array)
	{		
		byte[] result = new byte[array.length*4];
		int offset = 0;
		for(float val: array)
		{
			Utils.arraycopy(Utils.floatToBytes(val), 0, result, offset, 4); 
			offset += 4;
		}
		
		return result;
	}
}
