/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering.mesh.primmesher;

import java.util.ArrayList;
import java.util.List;

public class Path
{
	public List<PathNode> pathNodes = new ArrayList<PathNode>();

	public float twistBegin = 0.0f;
	public float twistEnd = 0.0f;
	public float topShearX = 0.0f;
	public float topShearY = 0.0f;
	public float pathCutBegin = 0.0f;
	public float pathCutEnd = 1.0f;
	public float dimpleBegin = 0.0f;
	public float dimpleEnd = 1.0f;
	public float skew = 0.0f;
	public float holeSizeX = 1.0f; // called pathScaleX in pbs
	public float holeSizeY = 0.25f;
	public float taperX = 0.0f;
	public float taperY = 0.0f;
	public float radius = 0.0f;
	public float revolutions = 1.0f;
	public int stepsPerRevolution = 24;

	private final float twoPi = 2.0f * (float)Math.PI;

	public void Create(PathType pathType, int steps)
	{
		if (this.taperX > 0.999f)
			this.taperX = 0.999f;
		if (this.taperX < -0.999f)
			this.taperX = -0.999f;
		if (this.taperY > 0.999f)
			this.taperY = 0.999f;
		if (this.taperY < -0.999f)
			this.taperY = -0.999f;

		if (pathType == PathType.Linear || pathType == PathType.Flexible)
		{
			int step = 0;

			float length = this.pathCutEnd - this.pathCutBegin;
			float twistTotal = twistEnd - twistBegin;
			float twistTotalAbs = Math.abs(twistTotal);
			if (twistTotalAbs > 0.01f)
				steps += (int)(twistTotalAbs * 3.66); //  dahlia's magic number

				float start = -0.5f;
				float stepSize = length / (float)steps;
				float percentOfPathMultiplier = stepSize * 0.999999f;
				float xOffset = this.topShearX * this.pathCutBegin;
				float yOffset = this.topShearY * this.pathCutBegin;
				float zOffset = start;
				float xOffsetStepIncrement = this.topShearX * length / steps;
				float yOffsetStepIncrement = this.topShearY * length / steps;

				float percentOfPath = this.pathCutBegin;
				zOffset += percentOfPath;

				// sanity checks

				boolean done = false;

				while (!done)
				{
					PathNode newNode = new PathNode();

					newNode.xScale = 1.0f;
					if (this.taperX == 0.0f)
						newNode.xScale = 1.0f;
					else if (this.taperX > 0.0f)
						newNode.xScale = 1.0f - percentOfPath * this.taperX;
					else newNode.xScale = 1.0f + (1.0f - percentOfPath) * this.taperX;

					newNode.yScale = 1.0f;
					if (this.taperY == 0.0f)
						newNode.yScale = 1.0f;
					else if (this.taperY > 0.0f)
						newNode.yScale = 1.0f - percentOfPath * this.taperY;
					else newNode.yScale = 1.0f + (1.0f - percentOfPath) * this.taperY;

					float twist = twistBegin + twistTotal * percentOfPath;

					newNode.rotation = new Quat(new Coord(0.0f, 0.0f, 1.0f), twist);
					newNode.position = new Coord(xOffset, yOffset, zOffset);
					newNode.percentOfPath = percentOfPath;

//					System.out.println(String.format("taperx %f tapery %f xscale %f yscale %f percentOfPath %f",
//							this.taperX, this.taperY, newNode.xScale , newNode.yScale, percentOfPath));
					
					pathNodes.add(newNode);

					if (step < steps)
					{
						step += 1;
						percentOfPath += percentOfPathMultiplier;
						xOffset += xOffsetStepIncrement;
						yOffset += yOffsetStepIncrement;
						zOffset += stepSize;
						if (percentOfPath > this.pathCutEnd)
							done = true;
					}
					else done = true;
				}
		} // end of linear path code

		else // pathType == Circular
		{
			float twistTotal = twistEnd - twistBegin;

			// if the profile has a lot of twist, add more layers otherwise the layers may overlap
			// and the resulting mesh may be quite inaccurate. This method is arbitrary and doesn't
			// accurately match the viewer
			float twistTotalAbs = Math.abs(twistTotal);
			if (twistTotalAbs > 0.01f)
			{
				if (twistTotalAbs > Math.PI * 1.5f)
					steps *= 2;
				if (twistTotalAbs > Math.PI * 3.0f)
					steps *= 2;
			}

			float yPathScale = this.holeSizeY * 0.5f;
			float pathLength = this.pathCutEnd - this.pathCutBegin;
			float totalSkew = this.skew * 2.0f * pathLength;
			float skewStart = this.pathCutBegin * 2.0f * this.skew - this.skew;
			float xOffsetTopShearXFactor = this.topShearX * (0.25f + 0.5f * (0.5f - this.holeSizeY));
			float yShearCompensation = 1.0f + Math.abs(this.topShearY) * 0.25f;

			// It's not quite clear what pushY (Y top shear) does, but subtracting it from the start and end
			// angles appears to approximate it's effects on path cut. Likewise, adding it to the angle used
			// to calculate the sine for generating the path radius appears to approximate it's effects there
			// too, but there are some subtle differences in the radius which are noticeable as the prim size
			// increases and it may affect megaprims quite a bit. The effect of the Y top shear parameter on
			// the meshes generated with this technique appear nearly identical in shape to the same prims when
			// displayed by the viewer.

			float startAngle = (twoPi * this.pathCutBegin * this.revolutions) - this.topShearY * 0.9f;
			float endAngle = (twoPi * this.pathCutEnd * this.revolutions) - this.topShearY * 0.9f;
			float stepSize = twoPi / this.stepsPerRevolution;

			int step = (int)(startAngle / stepSize);
			float angle = startAngle;

			boolean done = false;
			while (!done) // loop through the length of the path and add the layers
			{
				PathNode newNode = new PathNode();

				float xProfileScale = (1.0f - Math.abs(this.skew)) * this.holeSizeX;
				float yProfileScale = this.holeSizeY;

				float percentOfPath = angle / (twoPi * this.revolutions);
				float percentOfAngles = (angle - startAngle) / (endAngle - startAngle);

				if (this.taperX > 0.01f)
					xProfileScale *= 1.0f - percentOfPath * this.taperX;
				else if (this.taperX < -0.01f)
					xProfileScale *= 1.0f + (1.0f - percentOfPath) * this.taperX;

				if (this.taperY > 0.01f)
					yProfileScale *= 1.0f - percentOfPath * this.taperY;
				else if (this.taperY < -0.01f)
					yProfileScale *= 1.0f + (1.0f - percentOfPath) * this.taperY;

				newNode.xScale = xProfileScale;
				newNode.yScale = yProfileScale;

				float radiusScale = 1.0f;
				if (this.radius > 0.001f)
					radiusScale = 1.0f - this.radius * percentOfPath;
				else if (this.radius < 0.001f)
					radiusScale = 1.0f + this.radius * (1.0f - percentOfPath);

				float twist = twistBegin + twistTotal * percentOfPath;

				float xOffset = 0.5f * (skewStart + totalSkew * percentOfAngles);
				xOffset += (float)Math.sin(angle) * xOffsetTopShearXFactor;

				float yOffset = yShearCompensation * (float)Math.cos(angle) * (0.5f - yPathScale) * radiusScale;

				float zOffset = (float)Math.sin(angle + this.topShearY) * (0.5f - yPathScale) * radiusScale;

				newNode.position = new Coord(xOffset, yOffset, zOffset);

				// now orient the rotation of the profile layer relative to it's position on the path
				// adding taperY to the angle used to generate the quat appears to approximate the viewer

				newNode.rotation = new Quat(new Coord(1.0f, 0.0f, 0.0f), angle + this.topShearY);

				// next apply twist rotation to the profile layer
				if (twistTotal != 0.0f || twistBegin != 0.0f)
					newNode.rotation = Quat.multiply(newNode.rotation, new Quat(new Coord(0.0f, 0.0f, 1.0f), twist));

				newNode.percentOfPath = percentOfPath;

				pathNodes.add(newNode);

				// calculate terms for next iteration
				// calculate the angle for the next iteration of the loop

				if (angle >= endAngle - 0.01)
					done = true;
				else
				{
					step += 1;
					angle = stepSize * step;
					if (angle > endAngle)
						angle = endAngle;
				}
			}
		}
	}
}

