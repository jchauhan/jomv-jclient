/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering.mesh;


/*
 * The purpose of this class is to provide packed Vertex, Index and Texture coordinates into one big byte array.
 * 
 * Floating view of byte array will look following
 *   [f, f, f] [f, f, f] [f, f]
 *    Vertex     Index    Texture
 *    Part       Part     Coordinates
 *    
 * Packing Vertex, Indices and Textures together is called Interleaving, and it improves OpenGL performance.
 *    
 */
public class InterleavedVBO {

	byte[] vboData;
	int stride;
	int vertexSize;
	int vertexNormalSize;
	int texCoordSize;
	int vertexOffset;
	int vertexNormalOffet;
	int texCoordOffset;
	
	public InterleavedVBO() {
		super();
	}
	
	public InterleavedVBO(byte[] vboData, int stride, int vertexSize,
			int vertexNormalSize, int texCoordSize, int vertexOffset,
			int vertexNormalOffet, int texCoordOffset) {
		super();
		this.vboData = vboData;
		this.stride = stride;
		this.vertexSize = vertexSize;
		this.vertexNormalSize = vertexNormalSize;
		this.texCoordSize = texCoordSize;
		this.vertexOffset = vertexOffset;
		this.vertexNormalOffet = vertexNormalOffet;
		this.texCoordOffset = texCoordOffset;
	}

	public byte[] getVboData() {
		return vboData;
	}
	public void setVboData(byte[] vboData) {
		this.vboData = vboData;
	}
	public int getStride() {
		return stride;
	}
	public void setStride(int stride) {
		this.stride = stride;
	}
	public int getVertexOffset() {
		return vertexOffset;
	}
	public void setVertexOffset(int vertexOffset) {
		this.vertexOffset = vertexOffset;
	}
	public int getTexCoordOffset() {
		return texCoordOffset;
	}
	public void setTexCoordOffset(int texCoordOffset) {
		this.texCoordOffset = texCoordOffset;
	}
	public int getVertexNormalOffet() {
		return vertexNormalOffet;
	}
	public void setVertexNormalOffet(int vertexNormalOffet) {
		this.vertexNormalOffet = vertexNormalOffet;
	}

	public int getVertexSize() {
		return vertexSize;
	}

	public void setVertexSize(int vertexSize) {
		this.vertexSize = vertexSize;
	}

	public int getVertexNormalSize() {
		return vertexNormalSize;
	}

	public void setVertexNormalSize(int vertexNormalSize) {
		this.vertexNormalSize = vertexNormalSize;
	}

	public int getTexCoordSize() {
		return texCoordSize;
	}

	public void setTexCoordSize(int texCoordSize) {
		this.texCoordSize = texCoordSize;
	}
}
