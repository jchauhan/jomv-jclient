/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering;

import org.lwjgl.opengl.GL11;
import com.ngt.jopenmetaverse.shared.protocol.primitives.Enums.TextureAnimMode;
import com.ngt.jopenmetaverse.shared.protocol.primitives.TextureAnimation;
import com.ngt.jopenmetaverse.shared.util.Utils;


public class TextureAnimationInfo
    {
        public TextureAnimation PrimAnimInfo;
        public float CurrentFrame;
        public float CurrentTime;
        public boolean PingPong;
        float LastTime = 0f;
        float TotalTime = 0f;

        /// <summary>
        /// Perform texture manupulation to implement texture animations
        /// </summary>
        /// <param name="lastFrameTime">Time passed since the last run (in seconds)</param>
        public void Step(float lastFrameTime)
        {
            float numFrames = 1f;
            float fullLength = 1f;

            if (PrimAnimInfo.Length > 0)
            {
                numFrames = PrimAnimInfo.Length;
            }
            else
            {
                numFrames = Math.max(1f, (float)(PrimAnimInfo.SizeX * PrimAnimInfo.SizeY));
            }

            if ((TextureAnimMode.and(PrimAnimInfo.Flags, TextureAnimMode.PING_PONG)) != 0)
            {
                if ((TextureAnimMode.and(PrimAnimInfo.Flags, TextureAnimMode.SMOOTH)) != 0)
                {
                    fullLength = 2f * numFrames;
                }
                else if ((TextureAnimMode.and(PrimAnimInfo.Flags, TextureAnimMode.LOOP)) != 0)
                {
                    fullLength = 2f * numFrames - 2f;
                    fullLength = Math.max(1f, fullLength);
                }
                else
                {
                    fullLength = 2f * numFrames - 1f;
                    fullLength = Math.max(1f, fullLength);
                }
            }
            else
            {
                fullLength = numFrames;
            }

            float frameCounter;
            if (TextureAnimMode.and(PrimAnimInfo.Flags, TextureAnimMode.SMOOTH) != 0)
            {
                frameCounter = lastFrameTime * PrimAnimInfo.Rate + LastTime;
            }
            else
            {
                TotalTime += lastFrameTime;
                frameCounter = TotalTime * PrimAnimInfo.Rate;
            }
            LastTime = frameCounter;

            if (TextureAnimMode.and(PrimAnimInfo.Flags, TextureAnimMode.LOOP) != 0)
            {
                frameCounter %= fullLength;
            }
            else
            {
                frameCounter = Math.min(fullLength - 1f, frameCounter);
            }

            if (TextureAnimMode.and(PrimAnimInfo.Flags, TextureAnimMode.SMOOTH) == 0)
            {
                frameCounter = (float)Math.floor(frameCounter + 0.01f);
            }

            if (TextureAnimMode.and(PrimAnimInfo.Flags, TextureAnimMode.PING_PONG) != 0)
            {
                if (frameCounter > numFrames)
                {
                    if (TextureAnimMode.and(PrimAnimInfo.Flags, TextureAnimMode.SMOOTH) != 0)
                    {
                        frameCounter = numFrames - (frameCounter - numFrames);
                    }
                    else
                    {
                        frameCounter = (numFrames - 1.99f) - (frameCounter - numFrames);
                    }
                }
            }

            if (TextureAnimMode.and(PrimAnimInfo.Flags, TextureAnimMode.REVERSE) != 0)
            {
                if (TextureAnimMode.and(PrimAnimInfo.Flags, TextureAnimMode.SMOOTH) != 0)
                {
                    frameCounter = numFrames - frameCounter;
                }
                else
                {
                    frameCounter = (numFrames - 0.99f) - frameCounter;
                }
            }

            frameCounter += PrimAnimInfo.Start;

            if (TextureAnimMode.and(PrimAnimInfo.Flags, TextureAnimMode.SMOOTH) == 0)
            {
                frameCounter = (float)Math.round(frameCounter);
            }


            GL11.glMatrixMode(GL11.GL_TEXTURE);
            GL11.glLoadIdentity();

            if (TextureAnimMode.and(PrimAnimInfo.Flags, TextureAnimMode.ROTATE) != 0)
            {
                GL11.glTranslatef(0.5f, 0.5f, 0f);
                GL11.glRotated(Utils.RAD_TO_DEG * frameCounter, 0, 0, 1);
                GL11.glTranslatef(-0.5f, -0.5f, 0f);
            }
            else if (TextureAnimMode.and(PrimAnimInfo.Flags, TextureAnimMode.SCALE) != 0)
            {
                GL11.glScalef(frameCounter, frameCounter, 0);
            }
            else // Translate
            {
                float sizeX = Math.max(1f, (float)PrimAnimInfo.SizeX);
                float sizeY = Math.max(1f, (float)PrimAnimInfo.SizeY);

                GL11.glScalef(1f / sizeX, 1f / sizeY, 0);
                GL11.glTranslated(frameCounter % sizeX, Math.floor(frameCounter / sizeY), 0);
            }

            GL11.glMatrixMode(GL11.GL_MODELVIEW);
        }
    }

    /// <summary>
    /// Class that handle rendering of objects: simple primitives, scupties, and meshes
    /// </summary>
    