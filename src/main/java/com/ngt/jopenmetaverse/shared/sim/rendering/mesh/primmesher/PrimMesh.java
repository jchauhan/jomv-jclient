/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering.mesh.primmesher;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.shared.util.FileUtils;

public class PrimMesh
	    {
	        public String errorMessage = "";
	        private final float twoPi = 2.0f * (float)Math.PI;

	        public List<Coord> coords;
	        public List<Coord> normals;
	        public List<Face> faces;

	        public List<ViewerFace> viewerFaces;

	        private int sides = 4;
	        private int hollowSides = 4;
	        private float profileStart = 0.0f;
	        private float profileEnd = 1.0f;
	        private float hollow = 0.0f;
	        public int twistBegin = 0;
	        public int twistEnd = 0;
	        public float topShearX = 0.0f;
	        public float topShearY = 0.0f;
	        public float pathCutBegin = 0.0f;
	        public float pathCutEnd = 1.0f;
	        public float dimpleBegin = 0.0f;
	        public float dimpleEnd = 1.0f;
	        public float skew = 0.0f;
	        public float holeSizeX = 1.0f; // called pathScaleX in pbs
	        public float holeSizeY = 0.25f;
	        public float taperX = 0.0f;
	        public float taperY = 0.0f;
	        public float radius = 0.0f;
	        public float revolutions = 1.0f;
	        public int stepsPerRevolution = 24;

	        private int profileOuterFaceNumber = -1;
	        private int profileHollowFaceNumber = -1;

	        private boolean hasProfileCut = false;
	        private boolean hasHollow = false;
	        public boolean calcVertexNormals = false;
	        private boolean normalsProcessed = false;
	        public boolean viewerMode = false;
	        public boolean sphereMode = false;

	        public int numPrimFaces = 0;

	        /// <summary>
	        /// Human readable String representation of the parameters used to create a mesh.
	        /// </summary>
	        /// <returns></returns>
	        public String ParamsToDisplayString()
	        {
	            String s = "";
	            s += "sides..................: " + this.sides;
	            s += "\nhollowSides..........: " + this.hollowSides;
	            s += "\nprofileStart.........: " + this.profileStart;
	            s += "\nprofileEnd...........: " + this.profileEnd;
	            s += "\nhollow...............: " + this.hollow;
	            s += "\ntwistBegin...........: " + this.twistBegin;
	            s += "\ntwistEnd.............: " + this.twistEnd;
	            s += "\ntopShearX............: " + this.topShearX;
	            s += "\ntopShearY............: " + this.topShearY;
	            s += "\npathCutBegin.........: " + this.pathCutBegin;
	            s += "\npathCutEnd...........: " + this.pathCutEnd;
	            s += "\ndimpleBegin..........: " + this.dimpleBegin;
	            s += "\ndimpleEnd............: " + this.dimpleEnd;
	            s += "\nskew.................: " + this.skew;
	            s += "\nholeSizeX............: " + this.holeSizeX;
	            s += "\nholeSizeY............: " + this.holeSizeY;
	            s += "\ntaperX...............: " + this.taperX;
	            s += "\ntaperY...............: " + this.taperY;
	            s += "\nradius...............: " + this.radius;
	            s += "\nrevolutions..........: " + this.revolutions;
	            s += "\nstepsPerRevolution...: " + this.stepsPerRevolution;
	            s += "\nsphereMode...........: " + this.sphereMode;
	            s += "\nhasProfileCut........: " + this.hasProfileCut;
	            s += "\nhasHollow............: " + this.hasHollow;
	            s += "\nviewerMode...........: " + this.viewerMode;

	            return s;
	        }

	        public int getProfileOuterFaceNumber() {return profileOuterFaceNumber;}

	        public int getProfileHollowFaceNumber() {return profileHollowFaceNumber;}

	        public boolean getHasProfileCut() {return hasProfileCut;}

	        public boolean getHasHollow() {return hasHollow;}


	        /// <summary>
	        /// Constructs a PrimMesh object and creates the profile for extrusion.
	        /// </summary>
	        /// <param name="sides"></param>
	        /// <param name="profileStart"></param>
	        /// <param name="profileEnd"></param>
	        /// <param name="hollow"></param>
	        /// <param name="hollowSides"></param>
	        public PrimMesh(int sides, float profileStart, float profileEnd, float hollow, int hollowSides)
	        {	        	
	            this.coords = new ArrayList<Coord>();
	            this.faces = new ArrayList<Face>();

	            this.sides = sides;
	            this.profileStart = profileStart;
	            this.profileEnd = profileEnd;
	            this.hollow = hollow;
	            this.hollowSides = hollowSides;

	            if (sides < 3)
	                this.sides = 3;
	            if (hollowSides < 3)
	                this.hollowSides = 3;
	            if (profileStart < 0.0f)
	                this.profileStart = 0.0f;
	            if (profileEnd > 1.0f)
	                this.profileEnd = 1.0f;
	            if (profileEnd < 0.02f)
	                this.profileEnd = 0.02f;
	            if (profileStart >= profileEnd)
	                this.profileStart = profileEnd - 0.02f;
	            if (hollow > 0.99f)
	                this.hollow = 0.99f;
	            if (hollow < 0.0f)
	                this.hollow = 0.0f;
	        }

	        /// <summary>
	        /// Extrudes a profile along a path.
	        /// </summary>
	        public void Extrude(PathType pathType)
	        {
//	        	System.out.println(ParamsToDisplayString());
//	        	System.out.println("Path Type: " + pathType);
	        	
	            boolean needEndFaces = false;

	            this.coords = new ArrayList<Coord>();
	            this.faces = new ArrayList<Face>();

	            if (this.viewerMode)
	            {
	                this.viewerFaces = new ArrayList<ViewerFace>();
	                this.calcVertexNormals = true;
	            }

	            if (this.calcVertexNormals)
	                this.normals = new ArrayList<Coord>();

	            int steps = 1;

	            float length = this.pathCutEnd - this.pathCutBegin;
	            normalsProcessed = false;

	            if (this.viewerMode && this.sides == 3)
	            {
	                // prisms don't taper well so add some vertical resolution
	                // other prims may benefit from this but just do prisms for now
	                if (Math.abs(this.taperX) > 0.01 || Math.abs(this.taperY) > 0.01)
	                    steps = (int)(steps * 4.5 * length);
	            }

	            if (this.sphereMode)
	                this.hasProfileCut = this.profileEnd - this.profileStart < 0.4999f;
	            else
	                this.hasProfileCut = this.profileEnd - this.profileStart < 0.9999f;
	            this.hasHollow = (this.hollow > 0.001f);

	            float twistBegin = this.twistBegin / 360.0f * twoPi;
	            float twistEnd = this.twistEnd / 360.0f * twoPi;
	            float twistTotal = twistEnd - twistBegin;
	            float twistTotalAbs = Math.abs(twistTotal);
	            if (twistTotalAbs > 0.01f)
	                steps += (int)(twistTotalAbs * 3.66); //  dahlia's magic number

	            float hollow = this.hollow;

	            if (pathType == PathType.Circular)
	            {
	                needEndFaces = false;
	                if (this.pathCutBegin != 0.0f || this.pathCutEnd != 1.0f)
	                    needEndFaces = true;
	                else if (this.taperX != 0.0f || this.taperY != 0.0f)
	                    needEndFaces = true;
	                else if (this.skew != 0.0f)
	                    needEndFaces = true;
	                else if (twistTotal != 0.0f)
	                    needEndFaces = true;
	                else if (this.radius != 0.0f)
	                    needEndFaces = true;
	            }
	            else needEndFaces = true;

	            // sanity checks
	            float initialProfileRot = 0.0f;
	            if (pathType == PathType.Circular)
	            {
	                if (this.sides == 3)
	                {
	                    initialProfileRot = (float)Math.PI;
	                    if (this.hollowSides == 4)
	                    {
	                        if (hollow > 0.7f)
	                            hollow = 0.7f;
	                        hollow *= 0.707f;
	                    }
	                    else hollow *= 0.5f;
	                }
	                else if (this.sides == 4)
	                {
	                    initialProfileRot = 0.25f * (float)Math.PI;
	                    if (this.hollowSides != 4)
	                        hollow *= 0.707f;
	                }
	                else if (this.sides > 4)
	                {
	                    initialProfileRot = (float)Math.PI;
	                    if (this.hollowSides == 4)
	                    {
	                        if (hollow > 0.7f)
	                            hollow = 0.7f;
	                        hollow /= 0.7f;
	                    }
	                }
	            }
	            else
	            {
	                if (this.sides == 3)
	                {
	                    if (this.hollowSides == 4)
	                    {
	                        if (hollow > 0.7f)
	                            hollow = 0.7f;
	                        hollow *= 0.707f;
	                    }
	                    else hollow *= 0.5f;
	                }
	                else if (this.sides == 4)
	                {
	                    initialProfileRot = 1.25f * (float)Math.PI;
	                    if (this.hollowSides != 4)
	                        hollow *= 0.707f;
	                }
	                else if (this.sides == 24 && this.hollowSides == 4)
	                    hollow *= 1.414f;
	            }

	            Profile profile = new Profile(this.sides, this.profileStart, this.profileEnd, hollow, this.hollowSides, true, calcVertexNormals);
	            
//                try {
//	            	String filename = UUID.Random().toString();
//	            	System.out.println("Generated Prim Raw file: " + filename);
//	            	profile.DumpRaw("/media/development/workspace/dev/vw/testclientjomv/openmetaverse_data/logs/", filename, "initial-PrimMesh");
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
	            
	            this.errorMessage = profile.errorMessage;

	            this.numPrimFaces = profile.numPrimFaces;

	            int cut1FaceNumber = profile.bottomFaceNumber + 1;
	            int cut2FaceNumber = cut1FaceNumber + 1;
	            if (!needEndFaces)
	            {
	                cut1FaceNumber -= 2;
	                cut2FaceNumber -= 2;
	            }

	            profileOuterFaceNumber = profile.outerFaceNumber;
	            if (!needEndFaces)
	                profileOuterFaceNumber--;

	            if (hasHollow)
	            {
	                profileHollowFaceNumber = profile.hollowFaceNumber;
	                if (!needEndFaces)
	                    profileHollowFaceNumber--;
	            }

	            int cut1Vert = -1;
	            int cut2Vert = -1;
	            if (hasProfileCut)
	            {
	                cut1Vert = hasHollow ? profile.coords.size() - 1 : 0;
	                cut2Vert = hasHollow ? profile.numOuterVerts - 1 : profile.numOuterVerts;
	            }

	            if (initialProfileRot != 0.0f)
	            {
	                profile.addRot(new Quat(new Coord(0.0f, 0.0f, 1.0f), initialProfileRot));
	                if (viewerMode)
	                    profile.MakeFaceUVs();
	                
//	                System.out.println("Initial Rotation " + initialProfileRot);
	                
//	                try {
//		            	String filename = UUID.Random().toString();
//		            	System.out.println("Generated Prim Raw file: " + filename);
//		            	profile.DumpRaw("/media/development/workspace/dev/vw/testclientjomv/openmetaverse_data/logs/", filename, initialProfileRot + "afterrotation-PrimMesh");
//					} catch (IOException e) {
//						e.printStackTrace();
//					}
	            }

	            Coord lastCutNormal1 = new Coord();
	            Coord lastCutNormal2 = new Coord();
	            float thisV = 0.0f;
	            float lastV = 0.0f;

	            Path path = new Path();
	            path.twistBegin = twistBegin;
	            path.twistEnd = twistEnd;
	            path.topShearX = topShearX;
	            path.topShearY = topShearY;
	            path.pathCutBegin = pathCutBegin;
	            path.pathCutEnd = pathCutEnd;
	            path.dimpleBegin = dimpleBegin;
	            path.dimpleEnd = dimpleEnd;
	            path.skew = skew;
	            path.holeSizeX = holeSizeX;
	            path.holeSizeY = holeSizeY;
	            path.taperX = taperX;
	            path.taperY = taperY;
	            path.radius = radius;
	            path.revolutions = revolutions;
	            path.stepsPerRevolution = stepsPerRevolution;

	            path.Create(pathType, steps);

	            for (int nodeIndex = 0; nodeIndex < path.pathNodes.size(); nodeIndex++)
	            {
	                PathNode node = path.pathNodes.get(nodeIndex);
	                Profile newLayer = profile.Copy();
	                
//	                try {
//		            	String filename = UUID.Random().toString();
//		            	System.out.println("Generated Prim Raw file: " + filename);
//		            	newLayer.DumpRaw("/media/development/workspace/dev/vw/testclientjomv/openmetaverse_data/logs/", filename, nodeIndex + "-PrimMesh");
//					} catch (IOException e) {
//						e.printStackTrace();
//					}
	                
	                newLayer.Scale(node.xScale, node.yScale);
	                
//	                try {
//		            	String filename = UUID.Random().toString();
//		            	System.out.println("Generated Prim Raw file: " + filename);
//		            	newLayer.DumpRaw("/media/development/workspace/dev/vw/testclientjomv/openmetaverse_data/logs/", filename, nodeIndex + "scale-PrimMesh");
//					} catch (IOException e) {
//						e.printStackTrace();
//					}
	                
	                newLayer.addRot(node.rotation);
	                
//	                try {
//		            	String filename = UUID.Random().toString();
//		            	System.out.println("Generated Prim Raw file: " + filename);
//		            	newLayer.DumpRaw("/media/development/workspace/dev/vw/testclientjomv/openmetaverse_data/logs/", filename, nodeIndex + "rot-PrimMesh");
//					} catch (IOException e) {
//						e.printStackTrace();
//					}
	                
	                newLayer.addPos(node.position);
	                
//	                try {
//		            	String filename = UUID.Random().toString();
//		            	System.out.println("Generated Prim Raw file: " + filename);
//		            	newLayer.DumpRaw("/media/development/workspace/dev/vw/testclientjomv/openmetaverse_data/logs/", filename, nodeIndex + "pos-PrimMesh");
//					} catch (IOException e) {
//						e.printStackTrace();
//					}
//	                
//	                System.out.println(String.format("Node Node Pos %s, Rot %s Scale %f %f", node.position.toString(), node.rotation.toString()
//	                		,node.xScale, node.yScale));
	                
	                if (needEndFaces && nodeIndex == 0)
	                {
	                    newLayer.FlipNormals();

	                    // add the bottom faces to the viewerFaces list
	                    if (this.viewerMode)
	                    {
	                        Coord faceNormal = newLayer.faceNormal;
	                        ViewerFace newViewerFace ;
	                        int numFaces = newLayer.faces.size();
	                        List<Face> faces = newLayer.faces;

//	    	                try {
//	    		            	String filename = UUID.Random().toString();
//	    		            	System.out.println("Generated Prim Raw file: " + filename);
//	    		            	newLayer.DumpRaw("/media/development/workspace/dev/vw/testclientjomv/openmetaverse_data/logs/", filename, nodeIndex + "beforefaces-PrimMesh");
//	    					} catch (IOException e) {
//	    						e.printStackTrace();
//	    					}
	                        
	                        for (int i = 0; i < numFaces; i++)
	                        {
		                        newViewerFace = new ViewerFace(profile.bottomFaceNumber);
	                            Face face = faces.get(i);
	                            newViewerFace.v1 = new Coord(newLayer.coords.get(face.v1));
	                            newViewerFace.v2 = new Coord(newLayer.coords.get(face.v2));
	                            newViewerFace.v3 = new Coord(newLayer.coords.get(face.v3));

//	                            System.out.println(String.format("Face Indices %d %d %d", face.v1, face.v2, face.v3));
//	                            System.out.println("Face Coords " + i + " " + newViewerFace.v1.toString()
//	                            		+ newViewerFace.v2.toString() + newViewerFace.v3.toString());
	                            
	                            newViewerFace.coordIndex1 = face.v1;
	                            newViewerFace.coordIndex2 = face.v2;
	                            newViewerFace.coordIndex3 = face.v3;

	                            newViewerFace.n1 = new Coord(faceNormal);
	                            newViewerFace.n2 = new Coord(faceNormal);
	                            newViewerFace.n3 = new Coord(faceNormal);

	                            newViewerFace.uv1 = new UVCoord(newLayer.faceUVs.get(face.v1));
	                            newViewerFace.uv2 = new UVCoord(newLayer.faceUVs.get(face.v2));
	                            newViewerFace.uv3 = new UVCoord(newLayer.faceUVs.get(face.v3));
	                            
	                            
	                            if (pathType == PathType.Linear)
	                            {
	                                newViewerFace.uv1.Flip();
	                                newViewerFace.uv2.Flip();
	                                newViewerFace.uv3.Flip();
	                            }	                            
	                            
	                            this.viewerFaces.add(newViewerFace);
	                            
//	                         	System.out.println(String.format("newViewerFace uv1 %s uv2 %s uv3 %s", 
//	                         			newViewerFace.uv1.toString(), newViewerFace.uv2.toString(), newViewerFace.uv3.toString()));
	                         	
	                            
	                        }
	                    }
	                } // if (nodeIndex == 0)

	                // append this layer

	                int coordsLen = this.coords.size();
	                newLayer.addValue2FaceVertexIndices(coordsLen);

	                for(Coord c: newLayer.coords)
	                {
	                	this.coords.add(new Coord(c));
	                }

	                if (this.calcVertexNormals)
	                {
	                    newLayer.addValue2FaceNormalIndices(this.normals.size());
	                    for(Coord c: newLayer.vertexNormals)
	                    {
	                    	this.normals.add(new Coord(c));
	                    }
	                }

	                if (node.percentOfPath < this.pathCutBegin + 0.01f || node.percentOfPath > this.pathCutEnd - 0.01f)
	                {
	                	for(Face f: newLayer.faces)
	                	{
	                		this.faces.add(new Face(f));
	                	}
	                }

	                // fill faces between layers

	                int numVerts = newLayer.coords.size();
	                Face newFace1 = new Face();
	                Face newFace2 = new Face();

	                thisV = 1.0f - node.percentOfPath;

	                if (nodeIndex > 0)
	                {
	                    int startVert = coordsLen + 1;
	                    int endVert = this.coords.size();

	                    if (sides < 5 || this.hasProfileCut || this.hasHollow)
	                        startVert--;

	                    for (int i = startVert; i < endVert; i++)
	                    {
	    	                newFace1 = new Face();
	    	                newFace2 = new Face();
	    	                
	                        int iNext = i + 1;
	                        if (i == endVert - 1)
	                            iNext = startVert;

	                        int whichVert = i - startVert;

	                        newFace1.v1 = i;
	                        newFace1.v2 = i - numVerts;
	                        newFace1.v3 = iNext;

	                        newFace1.n1 = newFace1.v1;
	                        newFace1.n2 = newFace1.v2;
	                        newFace1.n3 = newFace1.v3;
	                        this.faces.add(new Face(newFace1));

	                        newFace2.v1 = iNext;
	                        newFace2.v2 = i - numVerts;
	                        newFace2.v3 = iNext - numVerts;

	                        newFace2.n1 = newFace2.v1;
	                        newFace2.n2 = newFace2.v2;
	                        newFace2.n3 = newFace2.v3;
	                        this.faces.add(new Face(newFace2));

	                        if (this.viewerMode)
	                        {
	                            // add the side faces to the list of viewerFaces here
	                            int primFaceNum = profile.faceNumbers.get(whichVert);
	                            if (!needEndFaces)
	                                primFaceNum -= 1;

	                            ViewerFace newViewerFace1 = new ViewerFace(primFaceNum);
	                            ViewerFace newViewerFace2 = new ViewerFace(primFaceNum);

	                            int uIndex = whichVert;
	                            if (!hasHollow && sides > 4 && uIndex < newLayer.us.size() - 1)
	                            {
	                                uIndex++;
	                            }

	                            float u1 = newLayer.us.get(uIndex);
	                            float u2 = 1.0f;
	                            if (uIndex < (int)newLayer.us.size() - 1)
	                                u2 = newLayer.us.get(uIndex + 1);

	                            if (whichVert == cut1Vert || whichVert == cut2Vert)
	                            {
	                                u1 = 0.0f;
	                                u2 = 1.0f;
	                            }
	                            else if (sides < 5)
	                            {
	                                if (whichVert < profile.numOuterVerts)
	                                { // boxes and prisms have one texture face per side of the prim, so the U values have to be scaled
	                                    // to reflect the entire texture width
	                                    u1 *= sides;
	                                    u2 *= sides;
	                                    u2 -= (int)u1;
	                                    u1 -= (int)u1;
	                                    if (u2 < 0.1f)
	                                        u2 = 1.0f;
	                                }
	                            }

	                            if (this.sphereMode)
	                            {
	                                if (whichVert != cut1Vert && whichVert != cut2Vert)
	                                {
	                                    u1 = u1 * 2.0f - 1.0f;
	                                    u2 = u2 * 2.0f - 1.0f;

	                                    if (whichVert >= newLayer.numOuterVerts)
	                                    {
	                                        u1 -= hollow;
	                                        u2 -= hollow;
	                                    }

	                                }
	                            }

	                            newViewerFace1.uv1.U = u1;
	                            newViewerFace1.uv2.U = u1;
	                            newViewerFace1.uv3.U = u2;

	                            newViewerFace1.uv1.V = thisV;
	                            newViewerFace1.uv2.V = lastV;
	                            newViewerFace1.uv3.V = thisV;

	                            newViewerFace2.uv1.U = u2;
	                            newViewerFace2.uv2.U = u1;
	                            newViewerFace2.uv3.U = u2;

	                            newViewerFace2.uv1.V = thisV;
	                            newViewerFace2.uv2.V = lastV;
	                            newViewerFace2.uv3.V = lastV;

	                            newViewerFace1.v1 = new Coord(this.coords.get(newFace1.v1));
	                            newViewerFace1.v2 = new Coord(this.coords.get(newFace1.v2));
	                            newViewerFace1.v3 = new Coord(this.coords.get(newFace1.v3));

	                            newViewerFace2.v1 = new Coord(this.coords.get(newFace2.v1));
	                            newViewerFace2.v2 = new Coord(this.coords.get(newFace2.v2));
	                            newViewerFace2.v3 = new Coord(this.coords.get(newFace2.v3));

	                            newViewerFace1.coordIndex1 = newFace1.v1;
	                            newViewerFace1.coordIndex2 = newFace1.v2;
	                            newViewerFace1.coordIndex3 = newFace1.v3;

	                            newViewerFace2.coordIndex1 = newFace2.v1;
	                            newViewerFace2.coordIndex2 = newFace2.v2;
	                            newViewerFace2.coordIndex3 = newFace2.v3;

	                            // profile cut faces
	                            if (whichVert == cut1Vert)
	                            {
	                                newViewerFace1.primFaceNumber = cut1FaceNumber;
	                                newViewerFace2.primFaceNumber = cut1FaceNumber;
	                                newViewerFace1.n1 = new Coord(newLayer.cutNormal1);
	                                newViewerFace1.n2 = new Coord(lastCutNormal1);
	                                newViewerFace1.n3 = new Coord(lastCutNormal1);
	                                newViewerFace2.n1 = new Coord(newLayer.cutNormal1);
	                                newViewerFace2.n3 = new Coord(newLayer.cutNormal1);
	                                newViewerFace2.n2 = new Coord(lastCutNormal1);
	                            }
	                            else if (whichVert == cut2Vert)
	                            {
	                                newViewerFace1.primFaceNumber = cut2FaceNumber;
	                                newViewerFace2.primFaceNumber = cut2FaceNumber;
	                                newViewerFace1.n1 = new Coord(newLayer.cutNormal2);
	                                newViewerFace1.n2 = new Coord(lastCutNormal2);
	                                newViewerFace1.n3 = new Coord(lastCutNormal2);

	                                newViewerFace2.n1 = new Coord(newLayer.cutNormal2);
	                                newViewerFace2.n3 = new Coord(newLayer.cutNormal2);
	                                newViewerFace2.n2 = new Coord(lastCutNormal2);
	                            }

	                            else // outer and hollow faces
	                            {
	                                if ((sides < 5 && whichVert < newLayer.numOuterVerts) || (hollowSides < 5 && whichVert >= newLayer.numOuterVerts))
	                                { // looks terrible when path is twisted... need vertex normals here
	                                    newViewerFace1.CalcSurfaceNormal();
	                                    newViewerFace2.CalcSurfaceNormal();
	                                }
	                                else
	                                {
	                                    newViewerFace1.n1 = new Coord(this.normals.get(newFace1.n1));
	                                    newViewerFace1.n2 = new Coord(this.normals.get(newFace1.n2));
	                                    newViewerFace1.n3 = new Coord(this.normals.get(newFace1.n3));

	                                    newViewerFace2.n1 = new Coord(this.normals.get(newFace2.n1));
	                                    newViewerFace2.n2 = new Coord(this.normals.get(newFace2.n2));
	                                    newViewerFace2.n3 = new Coord(this.normals.get(newFace2.n3));
	                                    
	                                    
	                                }
	                            }

	                            this.viewerFaces.add(new ViewerFace(newViewerFace1));
	                            this.viewerFaces.add(new ViewerFace(newViewerFace2));
	                            
//	                         	System.out.println(String.format("newViewerFace uv1 %s uv2 %s uv3 %s", 
//	                         			newViewerFace1.uv1.toString(), newViewerFace1.uv2.toString(), newViewerFace1.uv3.toString()));
//	                         	
//	                         	System.out.println(String.format("newViewerFace uv1 %s uv2 %s uv3 %s", 
//	                         			newViewerFace2.uv1.toString(), newViewerFace2.uv2.toString(), newViewerFace2.uv3.toString()));
	                            
	                            
	                        } 
	                    }
	                    
	                }

	                lastCutNormal1 = new Coord(newLayer.cutNormal1);
	                lastCutNormal2 = new Coord(newLayer.cutNormal2);
	                lastV = thisV;

	                if (needEndFaces && nodeIndex == path.pathNodes.size() - 1 && viewerMode)
	                {
	                    // add the top faces to the viewerFaces list here
	                    Coord faceNormal = new Coord(newLayer.faceNormal);
	                    ViewerFace newViewerFace;
	                    int numFaces = newLayer.faces.size();
	                    List<Face> faces = newLayer.faces;

	                    for (int i = 0; i < numFaces; i++)
	                    {
	                    	newViewerFace = new ViewerFace(0);
	                        Face face = faces.get(i);
	                       
	                        
	                        newViewerFace.v1 = new Coord(newLayer.coords.get(face.v1 - coordsLen));
	                        newViewerFace.v2 = new Coord(newLayer.coords.get(face.v2 - coordsLen));
	                        newViewerFace.v3 = new Coord(newLayer.coords.get(face.v3 - coordsLen));

//	                    	System.out.println(String.format("newViewerFace v1 %s v2 %s v3 %s", 
//	                    			newViewerFace.v1.toString(), newViewerFace.v2.toString(), newViewerFace.v3.toString()));
	                        
	                        newViewerFace.coordIndex1 = face.v1 - coordsLen;
	                        newViewerFace.coordIndex2 = face.v2 - coordsLen;
	                        newViewerFace.coordIndex3 = face.v3 - coordsLen;

	                        newViewerFace.n1 = new Coord(faceNormal);
	                        newViewerFace.n2 = new Coord(faceNormal);
	                        newViewerFace.n3 = new Coord(faceNormal);

	                        newViewerFace.uv1 = new UVCoord(newLayer.faceUVs.get(face.v1 - coordsLen));
	                        newViewerFace.uv2 = new UVCoord(newLayer.faceUVs.get(face.v2 - coordsLen));
	                        newViewerFace.uv3 = new UVCoord(newLayer.faceUVs.get(face.v3 - coordsLen));

	                        if (pathType == PathType.Linear)
	                        {
	                            newViewerFace.uv1.Flip();
	                            newViewerFace.uv2.Flip();
	                            newViewerFace.uv3.Flip();
	                        }

//	                    	System.out.println(String.format("newViewerFace uv1 %s uv2 %s uv3 %s", 
//	                    			newViewerFace.uv1.toString(), newViewerFace.uv2.toString(), newViewerFace.uv3.toString()));
	                        
	                        this.viewerFaces.add(newViewerFace);
	                    }
	                }
	                
	            } // for (int nodeIndex = 0; nodeIndex < path.pathNodes.size(); nodeIndex++)

	        }


	        /// <summary>
	        /// DEPRICATED - use Extrude(PathType.Linear) instead
	        /// Extrudes a profile along a straight line path. Used for prim types box, cylinder, and prism.
	        /// </summary>
	        /// 
	        public void ExtrudeLinear()
	        {
	            this.Extrude(PathType.Linear);
	        }


	        /// <summary>
	        /// DEPRICATED - use Extrude(PathType.Circular) instead
	        /// Extrude a profile into a circular path prim mesh. Used for prim types torus, tube, and ring.
	        /// </summary>
	        /// 
	        public void ExtrudeCircular()
	        {
	            this.Extrude(PathType.Circular);
	        }


	        private Coord SurfaceNormal(Coord c1, Coord c2, Coord c3)
	        {
	            Coord edge1 = new Coord(c2.X - c1.X, c2.Y - c1.Y, c2.Z - c1.Z);
	            Coord edge2 = new Coord(c3.X - c1.X, c3.Y - c1.Y, c3.Z - c1.Z);

	            Coord normal = Coord.Cross(edge1, edge2);

	            normal.Normalize();

	            return normal;
	        }

	        private Coord SurfaceNormal(Face face)
	        {
	            return SurfaceNormal(new Coord(this.coords.get(face.v1)), new Coord(this.coords.get(face.v2)), new Coord(this.coords.get(face.v3)));
	        }

	        /// <summary>
	        /// Calculate the surface normal for a face in the list of faces
	        /// </summary>
	        /// <param name="faceIndex"></param>
	        /// <returns></returns>
	        public Coord SurfaceNormal(int faceIndex) throws Exception
	        {
	            int numFaces = this.faces.size();
	            if (faceIndex < 0 || faceIndex >= numFaces)
	                throw new Exception("faceIndex out of range");

	            return SurfaceNormal(new Face(this.faces.get(faceIndex)));
	        }

	        /// <summary>
	        /// Duplicates a PrimMesh object. All object properties are copied by value, including lists.
	        /// </summary>
	        /// <returns></returns>
	        public PrimMesh Copy()
	        {
	            PrimMesh copy = new PrimMesh(this.sides, this.profileStart, this.profileEnd, this.hollow, this.hollowSides);
	            copy.twistBegin = this.twistBegin;
	            copy.twistEnd = this.twistEnd;
	            copy.topShearX = this.topShearX;
	            copy.topShearY = this.topShearY;
	            copy.pathCutBegin = this.pathCutBegin;
	            copy.pathCutEnd = this.pathCutEnd;
	            copy.dimpleBegin = this.dimpleBegin;
	            copy.dimpleEnd = this.dimpleEnd;
	            copy.skew = this.skew;
	            copy.holeSizeX = this.holeSizeX;
	            copy.holeSizeY = this.holeSizeY;
	            copy.taperX = this.taperX;
	            copy.taperY = this.taperY;
	            copy.radius = this.radius;
	            copy.revolutions = this.revolutions;
	            copy.stepsPerRevolution = this.stepsPerRevolution;
	            copy.calcVertexNormals = this.calcVertexNormals;
	            copy.normalsProcessed = this.normalsProcessed;
	            copy.viewerMode = this.viewerMode;
	            copy.numPrimFaces = this.numPrimFaces;
	            copy.errorMessage = this.errorMessage;

	            copy.coords = new ArrayList<Coord>();
	            for(Coord c: this.coords)
	            {
	            	copy.coords.add(new Coord(c));
	            }
	            copy.faces = new ArrayList<Face>();
	            for(Face f: copy.faces)
	            {
	            	copy.faces.add(new Face(f));
	            }
	            
	            copy.viewerFaces = new ArrayList<ViewerFace>();
	            for(ViewerFace vf: this.viewerFaces)
	            {
	            	copy.viewerFaces.add(new ViewerFace(vf));
	            }
	            copy.normals = new ArrayList<Coord>();
	            for(Coord c: this.normals)
	            {
	            	copy.normals.add(new Coord(c));
	            }
	            
	            return copy;
	        }

	        /// <summary>
	        /// Calculate surface normals for all of the faces in the list of faces in this mesh
	        /// </summary>
	        public void CalcNormals() throws Exception
	        {
	            if (normalsProcessed)
	                return;

	            normalsProcessed = true;

	            int numFaces = faces.size();

	            if (!this.calcVertexNormals)
	                this.normals = new ArrayList<Coord>();

	            for (int i = 0; i < numFaces; i++)
	            {
	                Face face = new Face(faces.get(i));

	                this.normals.add(SurfaceNormal(i).Normalize());

	                int normIndex = normals.size() - 1;
	                face.n1 = normIndex;
	                face.n2 = normIndex;
	                face.n3 = normIndex;

	                this.faces.set(i, face);
	            }
	        }

	        /// <summary>
	        ///.adds a value to each XYZ vertex coordinate in the mesh
	        /// </summary>
	        /// <param name="x"></param>
	        /// <param name="y"></param>
	        /// <param name="z"></param>
	        public void addPos(float x, float y, float z)
	        {
	            int i;
	            int numVerts = this.coords.size();
	            Coord vert;

	            for (i = 0; i < numVerts; i++)
	            {
	                vert = new Coord(this.coords.get(i));
	                vert.X += x;
	                vert.Y += y;
	                vert.Z += z;
	                this.coords.set(i, vert);
	            }

	            if (this.viewerFaces != null)
	            {
	                int numViewerFaces = this.viewerFaces.size();

	                for (i = 0; i < numViewerFaces; i++)
	                {
	                    ViewerFace v = new ViewerFace(this.viewerFaces.get(i));
	                    v.addPos(x, y, z);
	                    this.viewerFaces.set(i,  v);
	                }
	            }
	        }

	        /// <summary>
	        /// Rotates the mesh
	        /// </summary>
	        /// <param name="q"></param>
	        public void addRot(Quat q)
	        {
	            int i;
	            int numVerts = this.coords.size();

	            for (i = 0; i < numVerts; i++)
	                this.coords.set(i,  Coord.multiply(this.coords.get(i), q));

	            if (this.normals != null)
	            {
	                int numNormals = this.normals.size();
	                for (i = 0; i < numNormals; i++)
	                	this.normals.set(i,  Coord.multiply(this.normals.get(i), q));

	            }

	            if (this.viewerFaces != null)
	            {
	                int numViewerFaces = this.viewerFaces.size();

	                for (i = 0; i < numViewerFaces; i++)
	                {
	                    ViewerFace v = new ViewerFace(this.viewerFaces.get(i));
	                    v.v1 = Coord.multiply(v.v1, q);
	                    v.v2 = Coord.multiply(v.v2, q);
	                    v.v3 = Coord.multiply(v.v3, q);
	                    
	                    v.n1 = Coord.multiply(v.n1, q);
	                    v.n2 = Coord.multiply(v.n2, q);
	                    v.n3 = Coord.multiply(v.n3, q);
	                    
//	                    v.v1 *= q;
//	                    v.v2 *= q;
//	                    v.v3 *= q;
//
//	                    v.n1 *= q;
//	                    v.n2 *= q;
//	                    v.n3 *= q;
	                    this.viewerFaces.set(i, v);
	                }
	            }
	        }

	        public VertexIndexer GetVertexIndexer()
	        {
	            if (this.viewerMode && this.viewerFaces.size() > 0)
	                return new VertexIndexer(this);
	            return null;
	        }

	        /// <summary>
	        /// Scales the mesh
	        /// </summary>
	        /// <param name="x"></param>
	        /// <param name="y"></param>
	        /// <param name="z"></param>
	        public void Scale(float x, float y, float z)
	        {
	            int i;
	            int numVerts = this.coords.size();
	            //Coord vert;

	            Coord m = new Coord(x, y, z);
	            for (i = 0; i < numVerts; i++)
	                this.coords.set(i, Coord.multiply(this.coords.get(i),  m));

	            if (this.viewerFaces != null)
	            {
	                int numViewerFaces = this.viewerFaces.size();
	                for (i = 0; i < numViewerFaces; i++)
	                {
	                    ViewerFace v = new ViewerFace(this.viewerFaces.get(i));
	                    v.v1 = Coord.multiply(v.v1, m);
	                    v.v2 = Coord.multiply(v.v2, m);
	                    v.v3 = Coord.multiply(v.v3, m);
	                    
//	                    v.v1 *= m;
//	                    v.v2 *= m;
//	                    v.v3 *= m;
	                    this.viewerFaces.set(i,  v);
	                }
	            }
	        }

	        /// <summary>
	        /// Dumps the mesh to a Blender compatible "Raw" format file
	        /// </summary>
	        /// <param name="path"></param>
	        /// <param name="name"></param>
	        /// <param name="title"></param>
	        public void DumpRaw(String path, String name, String title) throws IOException
	        {
	            if (path == null)
	                return;
	            String fileName = name + "_" + title + ".raw";
	            String completePath = FileUtils.combineFilePath(path, fileName);
	            BufferedWriter sw = new BufferedWriter(new FileWriter(completePath));

	            for (int i = 0; i < this.faces.size(); i++)
	            {
	                String s = this.coords.get(this.faces.get(i).v1).toString();
	                s += " " + this.coords.get(this.faces.get(i).v2).toString();
	                s += " " + this.coords.get(this.faces.get(i).v3).toString();

	                sw.write(s);
	                sw.newLine();
	            }
	            sw.flush();
	            sw.close();
	        }
	    }