/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering.mesh.primmesher;

import java.util.List;

public class Face
	    {
	        public int primFace;

	        // vertices
	        public int v1;
	        public int v2;
	        public int v3;

	        //normals
	        public int n1;
	        public int n2;
	        public int n3;

	        // uvs
	        public int uv1;
	        public int uv2;
	        public int uv3;

	        public Face(int v1, int v2, int v3)
	        {
	            primFace = 0;

	            this.v1 = v1;
	            this.v2 = v2;
	            this.v3 = v3;

	            this.n1 = 0;
	            this.n2 = 0;
	            this.n3 = 0;

	            this.uv1 = 0;
	            this.uv2 = 0;
	            this.uv3 = 0;

	        }

	        public Face(int v1, int v2, int v3, int n1, int n2, int n3)
	        {
	            primFace = 0;

	            this.v1 = v1;
	            this.v2 = v2;
	            this.v3 = v3;

	            this.n1 = n1;
	            this.n2 = n2;
	            this.n3 = n3;

	            this.uv1 = 0;
	            this.uv2 = 0;
	            this.uv3 = 0;
	        }

	        
	        
	        public Face(int primFace, int v1, int v2, int v3, int n1, int n2,
					int n3, int uv1, int uv2, int uv3) {
				super();
				this.primFace = primFace;
				this.v1 = v1;
				this.v2 = v2;
				this.v3 = v3;
				this.n1 = n1;
				this.n2 = n2;
				this.n3 = n3;
				this.uv1 = uv1;
				this.uv2 = uv2;
				this.uv3 = uv3;
			}

			public Face(Face face)
	        {
	        	this(face.primFace, face.v1, face.v2,face.v3, face.n1, face.n2, face.n3, face.uv1, face.uv2, face.uv3);
	        }
	        
	        public Face() {
			}

			public Coord SurfaceNormal(List<Coord> coordList)
	        {
	            Coord c1 = coordList.get(this.v1);
	            Coord c2 = coordList.get(this.v2);
	            Coord c3 = coordList.get(this.v3);

	            Coord edge1 = new Coord(c2.X - c1.X, c2.Y - c1.Y, c2.Z - c1.Z);
	            Coord edge2 = new Coord(c3.X - c1.X, c3.Y - c1.Y, c3.Z - c1.Z);

	            return Coord.Cross(edge1, edge2).Normalize();
	        }
	    }

	    