/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering;

import org.lwjgl.util.glu.GLU;
import com.ngt.jopenmetaverse.shared.sim.rendering.math.FloatMatrix4x4;
import com.ngt.jopenmetaverse.shared.sim.rendering.math.MatrixUtil;
import com.ngt.jopenmetaverse.shared.types.Quaternion;
import com.ngt.jopenmetaverse.shared.types.Vector3;

/// <summary>
	    /// Represents camera object
	    /// </summary>
	    public class Camera
	    {
	        /// <summary>
	        /// Indicates that there was manual camera movement, stop tracking objects
	        /// </summary>
	        public boolean Manual;
	        
	        Vector3 mPosition;
	        Vector3 mFocalPoint;
	        boolean mModified;

	        public float TimeToTarget = 0f;

	        public Vector3 RenderPosition;
	        public Vector3 RenderFocalPoint;
	        
	        /// <summary>Camera position</summary>
	        public Vector3 getPosition() 
	        { return mPosition; }
	        
	        public void setPosition(Vector3 value)
	        { mPosition = value; Modify(); } 
	        /// <summary>Camera target</summary>
	        
	        public Vector3 getFocalPoint() 
	        {  return mFocalPoint; } 
	        
	        public void setFocalPoint(Vector3 value) 
	        { mFocalPoint = value; Modify(); } 
	        /// <summary>Zoom level</summary>
	        
	        public float Zoom;
	        /// <summary>Draw distance</summary>
	        public float Far;
	       
	        /// <summary>Has camera been modified</summary>
	        public boolean getModified() { return mModified; } 
	        
	        public void setModified(boolean value)
	        { mModified = value; } 

	        public Camera()
	        {
	        	mModified = false;
	        	mPosition = new Vector3();
	        	mFocalPoint = new Vector3();
	        	RenderPosition = new Vector3();
	        	RenderFocalPoint = new Vector3();
	        }
	        
	        void Modify()
	        {
	            mModified = true;
	        }

	        public void Step(float time)
	        {
	            if (!(RenderPosition.equals(getPosition())))
	            {
	                RenderPosition = RHelp.Smoothed1stOrder(RenderPosition, getPosition(), time);
	                setModified(true);
	            }
	            if (! (RenderFocalPoint.equals(getFocalPoint()) ))
	            {
	                RenderFocalPoint = RHelp.Smoothed1stOrder(RenderFocalPoint, getFocalPoint(), time);
	                setModified(true);
	            }
	        }

	        Vector3 Interpolate(Vector3 start, Vector3 end, float fraction)
	        {
//	            float distance = Vector3.distance(start, end);
	            Vector3 direction = Vector3.subtract(end, start);
	            return direction.multiply(fraction).add(start);
	        }

	        public void EndMove()
	        {
	            mModified = true;
	            TimeToTarget = 0;
	            RenderPosition = getPosition();
	            RenderFocalPoint = getFocalPoint();
	        }

	        public void Pan(float deltaX, float deltaY)
	        {
	            Manual = true;
	            Vector3 direction = Vector3.subtract(getPosition(), getFocalPoint());
	            direction.normalize();
	            Vector3 vy = Vector3.modulus(direction, Vector3.UnitZ);
	            Vector3 vx = Vector3.modulus(vy, direction);
	            Vector3 vxy = Vector3.multiply(vx, deltaY).add(Vector3.multiply(vy, deltaX));
	            getPosition().add(vxy);
	            getFocalPoint().add(vxy);
	        }

	        public void Rotate(float delta, boolean horizontal)
	        {
	            Manual = true;
	            Vector3 direction = Vector3.subtract(getPosition(), getFocalPoint());
	            if (horizontal)
	            {
//	                Position = FocalPoint + direction * new Quaternion(0f, 0f, (float)Math.Sin(delta), (float)Math.Cos(delta));
	                setPosition(Vector3.add(getFocalPoint(),Vector3.multiply(direction, new Quaternion(0f, 0f, (float)Math.sin(delta), (float)Math.cos(delta)))));
	            }
	            else
	            {
//	                Position = FocalPoint + direction * Quaternion.CreateFromAxisAngle(direction % Vector3.UnitZ, delta);
	            	setPosition(Vector3.add(getFocalPoint(),Vector3.multiply(direction, Quaternion.createFromAxisAngle(Vector3.modulus(direction, Vector3.UnitZ), delta))));
	            }
	        }

	        public void MoveToTarget(float delta)
	        {
	            Manual = true;
//	            Position += (Position - FocalPoint) * delta;
	            setPosition(Vector3.subtract(getPosition(), getFocalPoint()).multiply(delta).add(getPosition()));
	        }

	        /// <summary>
	        /// Sets the world in perspective of the camera
	        /// </summary>
	        public void LookAt()
	        {
//	        	org.lwjgl.util.vector.Matrix4f lookAt = org.lwjgl.util.vector.Matrix4f.LookAt(
//	                RenderPosition.X, RenderPosition.Y, RenderPosition.Z,
//	                RenderFocalPoint.X, RenderFocalPoint.Y, RenderFocalPoint.Z,
//	                0f, 0f, 1f);
//	            GL.MultMatrix(ref lookAt);
	        	
	            GLU.gluLookAt(RenderPosition.X, RenderPosition.Y, RenderPosition.Z,
		                RenderFocalPoint.X, RenderFocalPoint.Y, RenderFocalPoint.Z,
		                0f, 0f, 1f);
	        }
	        
	        /*
	         * Alternative way of gllookat. Create Lookat matrix and passon to shaders
	         * 
	         * @return lookat matrix. 
	         */
	        
	        public FloatMatrix4x4 createLookAtMatrix()
	        {
	        	FloatMatrix4x4 matrix = MatrixUtil.createLookAtMatrix(RenderPosition.X, RenderPosition.Y, RenderPosition.Z,
		                RenderFocalPoint.X, RenderFocalPoint.Y, RenderFocalPoint.Z,
		                0f, 0f, 1f);
	        	
	        	return matrix;
	        }
	        
	    }