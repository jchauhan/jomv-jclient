/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering.math;


/**
 * Interface of vector with float components
 * @author Sönke Sothmann
 *
 */
public interface Vectorf {

	/**
	 * Creates a new Vector that is this vector multiplied with the given
	 * scalar.
	 * 
	 * @param scalar
	 * @return the multiplied Vector
	 */
	public abstract Vectorf multiply(float scalar);

	/**
	 * Returns the length of the vector.
	 * 
	 * @return the length of the vector
	 */
	public abstract float length();

	/**
	 * Creates a new Vector that is the unit vector of this vector.
	 * 
	 * @return the unit vector
	 */
	public abstract Vectorf toUnitVector();

	/**
	 * Returns the data of this vector as array.
	 * 
	 * @return the data as array
	 */
	public abstract float[] toArray();

}