/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering.mesh;

import com.ngt.jopenmetaverse.shared.types.Vector3;

public class BoundingVolume
    {
        Vector3 Min = new Vector3(99999, 99999, 99999);
        Vector3 Max = new Vector3(-99999, -99999, -99999);
        float R = 0f;

        public Vector3 ScaledMin = new Vector3(99999, 99999, 99999);
        public Vector3 ScaledMax = new Vector3(-99999, -99999, -99999);
        public float ScaledR = 0f;

        public void CalcScaled(Vector3 scale)
        {
            ScaledMin = Vector3.multiply(Min, scale);
            ScaledMax = Vector3.multiply(Max, scale);
            Vector3 dist = Vector3.subtract(ScaledMax, ScaledMin);
            ScaledR = dist.length();
        }

        public void CreateBoundingVolume(Face mesh, Vector3 scale)
        {
            for (int q = 0; q < mesh.Vertices.size(); q++)
            {
                if (mesh.Vertices.get(q).Position.X < Min.X) Min.X = mesh.Vertices.get(q).Position.X;
                if (mesh.Vertices.get(q).Position.Y < Min.Y) Min.Y = mesh.Vertices.get(q).Position.Y;
                if (mesh.Vertices.get(q).Position.Z < Min.Z) Min.Z = mesh.Vertices.get(q).Position.Z;

                if (mesh.Vertices.get(q).Position.X > Max.X) Max.X = mesh.Vertices.get(q).Position.X;
                if (mesh.Vertices.get(q).Position.Y > Max.Y) Max.Y = mesh.Vertices.get(q).Position.Y;
                if (mesh.Vertices.get(q).Position.Z > Max.Z) Max.Z = mesh.Vertices.get(q).Position.Z;
            }

            Vector3 dist = Vector3.subtract(Max, Min);
            R = dist.length();
            mesh.Center = Vector3.add(Min, Vector3.divide(dist, 2));
            CalcScaled(scale);
        }

        public void FromScale(Vector3 scale)
        {
            ScaledMax = Vector3.divide(scale, 2f);
            ScaledMin = Vector3.negate(ScaledMax);
            Vector3 dist = Vector3.subtract(ScaledMax, ScaledMin);
            ScaledR = dist.length();
        }

        public void AddVolume(BoundingVolume vol, Vector3 scale)
        {
            if (vol.Min.X < this.Min.X) this.Min.X = vol.Min.X;
            if (vol.Min.Y < this.Min.Y) this.Min.Y = vol.Min.Y;
            if (vol.Min.Z < this.Min.Z) this.Min.Z = vol.Min.Z;

            if (vol.Max.X > this.Max.X) this.Max.X = vol.Max.X;
            if (vol.Max.Y > this.Max.Y) this.Max.Y = vol.Max.Y;
            if (vol.Max.Z > this.Max.Z) this.Max.Z = vol.Max.Z;
            Vector3 dist = Vector3.subtract(Max, Min);
            R = dist.length();
            CalcScaled(scale);
        }
    }