/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering;


import com.ngt.jopenmetaverse.shared.protocol.primitives.Primitive;
import com.ngt.jopenmetaverse.shared.types.Quaternion;
import com.ngt.jopenmetaverse.shared.types.Vector3;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.BoundingVolume;

/// <summary>
/// Base class for all scene objects
/// </summary>
public abstract  class SceneObject implements Comparable<SceneObject>{
	//region Public fields
	/// <summary>Interpolated local position of the object</summary>
	public Vector3 InterpolatedPosition;
	/// <summary>Interpolated local rotation of the object/summary>
	public Quaternion InterpolatedRotation;
	/// <summary>Rendered position of the object in the region</summary>
	public Vector3 RenderPosition;
	/// <summary>Rendered rotationm of the object in the region</summary>
	public Quaternion RenderRotation;
	/// <summary>Per frame calculated square of the distance from camera</summary>
	public float DistanceSquared;
	/// <summary>Bounding volume of the object</summary>
	public BoundingVolume BoundingVolume;
	/// <summary>Was the sim position and distance from camera calculated during this frame</summary>
	public boolean PositionCalculated;
	/// <summary>Scene object type</summary>
	public SceneObjectType Type = SceneObjectType.None;
	/// <summary>Libomv primitive</summary>
	public abstract Primitive getBasePrim();
	public abstract void setBasePrim(Primitive value);
	/// <summary>Were initial initialization tasks done</summary>
	public boolean Initialized;
	/// <summary>Is this object disposed</summary>
	public boolean IsDisposed = false;
	public int AlphaQueryID = -1;
	public int SimpleQueryID = -1;
	public boolean HasAlphaFaces;
	public boolean HasSimpleFaces;
	public boolean HasInvisibleFaces;

	//endregion Public fields

	//uint
	long previousParent = ((long)Integer.MAX_VALUE) * 2;

	/// <summary>
	/// Cleanup resources used
	/// </summary>
	public void Dispose()
	{
		IsDisposed = true;
	}

	/// <summary>
	/// Task performed the fist time object is set for rendering
	/// </summary>
	public void Initialize()
	{
		InterpolatedPosition = new Vector3(getBasePrim().Position);
		RenderPosition =   new Vector3(getBasePrim().Position);
		InterpolatedRotation = new Quaternion(getBasePrim().Rotation);
		RenderRotation = new Quaternion(getBasePrim().Rotation);
		Initialized = true;
	}

	/// <summary>
	/// Perform per frame tasks
	/// </summary>
	/// <param name="time">Time since the last call (last frame time in seconds)</param>
	public void Step(float time, float dilation)
	{
		if (getBasePrim() == null) return;

		// Don't interpolate when parent changes (sit/stand link/unlink)
		if (previousParent != getBasePrim().ParentID)
		{
			previousParent = getBasePrim().ParentID;
			InterpolatedPosition = new Vector3(getBasePrim().Position);
			InterpolatedRotation = new Quaternion(getBasePrim().Rotation);
			return;
		}

		// Linear velocity and acceleration
		if (!getBasePrim().Velocity.equals(Vector3.Zero))
		{
			getBasePrim().Position =  Vector3.multiply(getBasePrim().Velocity,  time
					* 0.98f * dilation).add( getBasePrim().Position);
			InterpolatedPosition = new Vector3(getBasePrim().Position);
			getBasePrim().Velocity = Vector3.multiply(getBasePrim().Acceleration, time).add(getBasePrim().Velocity);
		}
		else if (!InterpolatedPosition.equals(getBasePrim().Position))
		{
			InterpolatedPosition = RHelp.Smoothed1stOrder(InterpolatedPosition, getBasePrim().Position, time);
		}

		// Angular velocity (target omega)
		if (!(getBasePrim().AngularVelocity.equals(Vector3.Zero)))
		{
			Vector3 angVel = getBasePrim().AngularVelocity;
			float angle = time * angVel.length();
			Quaternion dQ = Quaternion.createFromAxisAngle(angVel, angle);
			InterpolatedRotation = Quaternion.multiply(dQ, InterpolatedRotation);
		}
		else if (!(InterpolatedRotation.equals(getBasePrim().Rotation)) && !(this instanceof RenderAvatar))
		{
			InterpolatedRotation = Quaternion.slerp(InterpolatedRotation, getBasePrim().Rotation, time * 10f);
			if (1f - Math.abs(Quaternion.dot(InterpolatedRotation, getBasePrim().Rotation)) < 0.0001)
				InterpolatedRotation = new Quaternion(getBasePrim().Rotation);
		}
		else
		{
			InterpolatedRotation = new Quaternion(getBasePrim().Rotation);
		}
	}

	/// <summary>
	/// Render scene object
	/// </summary>
	/// <param name="pass">Which pass are we currently in</param>
	/// <param name="pickingID">ID used to identify which object was picked</param>
	/// <param name="context">context of Main scene renderer</param>
	/// <param name="time">Time it took to render the last frame</param>
	public  void Render(RenderPass pass, int pickingID, RenderingContext context, float time) throws Exception
	{
	}

	/// <summary>
	/// Implementation of the IComparable interface
	/// used for sorting by distance
	/// </summary>
	/// <param name="other">Object we are comparing to</param>
	/// <returns>Result of the comparison</returns>
	public  int compareTo(SceneObject other)
	{			        	
		SceneObject o = (SceneObject)other;
		if (this.DistanceSquared < o.DistanceSquared)
			return -1;
		else if (this.DistanceSquared > o.DistanceSquared)
			return 1;
		else
			return 0;
	}

	//region Occlusion queries
	public void StartQuery(RenderPass pass)
	{
		if (!RenderSettings.OcclusionCullingEnabled) return;

		if (pass == RenderPass.Simple)
		{
			StartSimpleQuery();
		}
		else if (pass == RenderPass.Alpha)
		{
			StartAlphaQuery();
		}
	}

	public void EndQuery(RenderPass pass)
	{
		if (!RenderSettings.OcclusionCullingEnabled) return;
		//TODO Need to implement
		//			            if (pass == RenderPass.Simple)
		//			            {
		//			                EndSimpleQuery();
		//			            }
		//			            else if (pass == RenderPass.Alpha)
		//			            {
		//			                EndAlphaQuery();
		//			            }
	}

	public void StartAlphaQuery()
	{
		if (!RenderSettings.OcclusionCullingEnabled) return;

		//TODO Need to implement

		//			            if (AlphaQueryID == -1)
		//			            {
		//			                Compat.GenQueries(out AlphaQueryID);
		//			            }
		//			            if (AlphaQueryID > 0)
		//			            {
		//			                Compat.BeginQuery(QueryTarget.SamplesPassed, AlphaQueryID);
		//			            }
	}

	public void EndAlphaQuery()
	{
		if (!RenderSettings.OcclusionCullingEnabled) return;

		//TODO Need to implement

		//			            if (AlphaQueryID > 0)
		//			            {
		//			                Compat.EndQuery(QueryTarget.SamplesPassed);
		//			            }
	}

	public void StartSimpleQuery()
	{
		if (!RenderSettings.OcclusionCullingEnabled) return;

		//TODO Need to implement

		//			            if (SimpleQueryID == -1)
		//			            {
		//			                Compat.GenQueries(out SimpleQueryID);
		//			            }
		//			            if (SimpleQueryID > 0)
		//			            {
		//			                Compat.BeginQuery(QueryTarget.SamplesPassed, SimpleQueryID);
		//			            }
	}

	public void EndSimpleQuery()
	{
		if (!RenderSettings.OcclusionCullingEnabled) return;

		//TODO Need to implement

		//			            if (SimpleQueryID > 0)
		//			            {
		//			                Compat.EndQuery(QueryTarget.SamplesPassed);
		//			            }
	}

	public boolean Occluded()
	{
		if (!RenderSettings.OcclusionCullingEnabled) return false;

		return false;

		//TODO Need to implement

		//			            if (HasInvisibleFaces) return false;
		//
		//			            if ((SimpleQueryID == -1 && AlphaQueryID == -1))
		//			            {
		//			                return false;
		//			            }
		//
		//			            if ((!HasAlphaFaces && !HasSimpleFaces)) return true;
		//
		//			            int samples = 1;
		//			            if (HasSimpleFaces && SimpleQueryID > 0)
		//			            {
		//			                Compat.GetQueryObject(SimpleQueryID, GetQueryObjectParam.QueryResult, out samples);
		//			            }
		//			            if (HasSimpleFaces && samples > 0)
		//			            {
		//			                return false;
		//			            }
		//
		//			            samples = 1;
		//			            if (HasAlphaFaces && AlphaQueryID > 0)
		//			            {
		//			                Compat.GetQueryObject(AlphaQueryID, GetQueryObjectParam.QueryResult, out samples);
		//			            }
		//			            if (HasAlphaFaces && samples > 0)
		//			            {
		//			                return false;
		//			            }
		//
		//			            return true;
		//			        }
		//endregion Occlusion queries
	}
	
    /// <summary>
    /// Calculates finar rendering position for objects on the scene
    /// </summary>
    /// <param name="obj">SceneObject whose position is calculated</param>
    /// <param name="pos">Rendering position</param>
    /// <param name="rot">Rendering rotation</param>
    public abstract void calPrimPosAndRot(Vector3 cameraRenderPosition);
}
