/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering.mesh.primitive;

import java.io.ByteArrayInputStream;
import com.ngt.jopenmetaverse.shared.protocol.primitives.Enums.TextureAnimMode;
import com.ngt.jopenmetaverse.shared.protocol.primitives.Primitive;
import com.ngt.jopenmetaverse.shared.protocol.primitives.TextureEntryFace;
import com.ngt.jopenmetaverse.shared.sim.GridClient;
import com.ngt.jopenmetaverse.shared.sim.asset.AssetTexture;
import com.ngt.jopenmetaverse.shared.sim.asset.pipeline.TexturePipeline.TextureRequestState;
import com.ngt.jopenmetaverse.shared.sim.cache.LoadCachedImageResult;
import com.ngt.jopenmetaverse.shared.sim.events.AutoResetEvent;
import com.ngt.jopenmetaverse.shared.sim.events.ManualResetEvent;
import com.ngt.jopenmetaverse.shared.sim.events.MethodDelegate;
import com.ngt.jopenmetaverse.shared.sim.events.asm.MeshDownloadCallbackArgs;
import com.ngt.jopenmetaverse.shared.sim.events.asm.TextureDownloadCallbackArgs;
import com.ngt.jopenmetaverse.shared.sim.imaging.IBitmap;
import com.ngt.jopenmetaverse.shared.sim.imaging.LoadTGAClass;
import com.ngt.jopenmetaverse.shared.sim.imaging.ManagedImage;
import com.ngt.jopenmetaverse.shared.sim.imaging.ManagedImage.ImageChannels;
import com.ngt.jopenmetaverse.shared.sim.imaging.OpenJPEGFactory;
import com.ngt.jopenmetaverse.shared.sim.rendering.FaceData;
import com.ngt.jopenmetaverse.shared.sim.rendering.RenderPrimitive;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.BoundingVolume;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.DetailLevel;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.Face;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.FacetedMesh;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.MeshmerizerR;
import com.ngt.jopenmetaverse.shared.types.EnumsPrimitive.ProfileCurve;
import com.ngt.jopenmetaverse.shared.types.EnumsPrimitive.SculptType;
import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.shared.types.EnumsPrimitive.PrimType;
import com.ngt.jopenmetaverse.shared.util.JLogger;
import com.ngt.jopenmetaverse.shared.util.Utils;

public class PrimitiveMesher extends  MeshmerizerR
{
	GridClient client ;	
//	final static int MAX_BITMAP_LRUCACHE_ENTRIES = 20;
	//L1 Inmemory cache to save loading TGA files with same texture ID
//	Map<UUID, IBitmap> bitmapLruCache;
	public PrimitiveMesher(GridClient client)
	{
		this.client = client;
		
//		//Initialize the LRU cache
//		bitmapLruCache = Collections.synchronizedMap(new LinkedHashMap<UUID, IBitmap>(MAX_BITMAP_LRUCACHE_ENTRIES, 0.75f, true)
//		{
//			@Override
//	            protected boolean removeEldestEntry(Map.Entry<UUID, IBitmap> eldest) {
//					boolean shouldDeleteEldest = size() > MAX_BITMAP_LRUCACHE_ENTRIES;
//					//Dispose the image
//					if(shouldDeleteEldest)
//						eldest.getValue().dispose();
//					
//	                // Returns if need to remove eldest entry,
//	                return shouldDeleteEldest; 
//	            }
//		});
		
	}

	public void MeshPrim(RenderPrimitive rprim, boolean allowQuickAndDirtyMeshing, DetailLevel primRenderDetail,
			final DetailLevel SculptRenderDetail, final DetailLevel MeshRenderDetail) throws Exception
	{
		if (rprim.Meshing) return;

		rprim.Meshing = true;
		Primitive prim = rprim.getBasePrim();

		// Regular prim
		if (prim.Sculpt == null || prim.Sculpt.SculptTexture.equals(UUID.Zero))
		{
			System.out.println("Generating Normal Prim");
			if (allowQuickAndDirtyMeshing)
			{
				if (prim.Flexible == null && prim.getType() == PrimType.Box &&
						prim.PrimData.ProfileHollow == 0 &&
						prim.PrimData.PathTwist == 0 &&
						prim.PrimData.PathTaperX == 0 &&
						prim.PrimData.PathTaperY == 0 &&
						prim.PrimData.PathSkew == 0 &&
						prim.PrimData.PathShearX == 0 &&
						prim.PrimData.PathShearY == 0 &&
						prim.PrimData.PathRevolutions == 1 &&
						prim.PrimData.PathRadiusOffset == 0)
					primRenderDetail = DetailLevel.Low;//Its a box or something else that can use lower meshing
			}
			FacetedMesh mesh = GenerateFacetedMesh(prim, primRenderDetail);
			rprim.Faces = mesh.Faces;
			CalculateBoundingBox(rprim);
			rprim.Meshing = false;
			rprim.Meshed = true;
			
//	        DumpRaw("/media/development/workspace/dev/vw/testclientjomv/openmetaverse_data/logs/"
//	        		,prim.LocalID, mesh);
			
		}
		else
		{
			System.out.println("Generating Mesh Sulpt Prim");
			FacetedMesh mesh = GenerateSculptOrMeshPrim(prim, SculptRenderDetail, MeshRenderDetail);
			if (mesh != null)
			{
				rprim.Faces = mesh.Faces;
				CalculateBoundingBox(rprim);
				rprim.Meshed = true;
				
//		        DumpRaw("/media/development/workspace/dev/vw/testclientjomv/openmetaverse_data/logs/"
//		        		,prim.LocalID, mesh);
				
			}
			rprim.Meshing = false;
			return;
		}
	}

	private void CalculateBoundingBox(RenderPrimitive rprim) throws Exception
	{
		Primitive prim = rprim.getBasePrim();

		// Calculate bounding volumes for each prim and adjust textures
		rprim.BoundingVolume = new BoundingVolume();
		for (int j = 0; j < rprim.Faces.size(); j++)
		{
			TextureEntryFace teFace = prim.Textures.GetFace(j);
			if (teFace == null) continue;

			Face face = rprim.Faces.get(j);
			FaceData data = new FaceData();
			data.TextureInfo.TextureID = teFace.getTextureID(); 
			data.BoundingVolume.CreateBoundingVolume(face, prim.Scale);
			rprim.BoundingVolume.AddVolume(data.BoundingVolume, prim.Scale);

			// With linear texture animation in effect, texture repeats and offset are ignored
			if ((TextureAnimMode.and(prim.TextureAnim.Flags, TextureAnimMode.ANIM_ON)) != 0
					&& (TextureAnimMode.and(prim.TextureAnim.Flags, TextureAnimMode.ROTATE)) == 0
					&& (prim.TextureAnim.Face == 255 || prim.TextureAnim.Face == j))
			{
				teFace.setRepeatU(1);
				teFace.setRepeatV(1);
				teFace.setOffsetU(0);
				teFace.setOffsetV(0);
			}

			// Need to adjust UV for spheres as they are sort of half-prim
			if (prim.PrimData.getProfileCurve() == ProfileCurve.HalfCircle)
			{
				teFace = (TextureEntryFace)teFace.clone();
				teFace.setRepeatV(teFace.getRepeatV() * 2);
				teFace.setOffsetV(teFace.getOffsetV() +  0.5f);
			}

			// Sculpt UV vertically flipped compared to prims. Flip back
			if (prim.Sculpt != null && !prim.Sculpt.SculptTexture.equals(UUID.Zero) 
					&& prim.Sculpt.getType() != SculptType.Mesh)
			{
				teFace = (TextureEntryFace)teFace.clone();
				teFace.setRepeatV(teFace.getRepeatV() *  -1);
			}

			// Texture transform for this face
			TransformTexCoords(face.Vertices, face.Center, teFace, prim.Scale);

			// Set the UserData for this face to our FaceData struct
			face.UserData = data;
			rprim.Faces.set(j, face);
		}
	}

	private FacetedMesh GenerateSculptOrMeshPrim(final Primitive prim, final DetailLevel SculptRenderDetail, 
			final DetailLevel MeshRenderDetail) throws Exception
			{
		FacetedMesh mesh = null;

//		System.out.println(String.format("Prim %d Invert %b mirror %s", prim.LocalID, prim.Sculpt.getInvert(), prim.Sculpt.getMirror()));
		
		if (prim.Sculpt.getType() != SculptType.Mesh)
		{ // Regular sculptie
			IBitmap img = null;
			
//			System.out.println("Sculpt Texture " + prim.Sculpt.SculptTexture.toString());
			
			if ((img = LoadTexture(prim.Sculpt.SculptTexture, true))!=null)
			{
				mesh = GenerateFacetedSculptMesh(prim, img, SculptRenderDetail);
			}
			else
				return null;
		}
		else
		{ // Mesh
			final AutoResetEvent gotMesh = new AutoResetEvent(false);
			final FacetedMesh[] tmpMesh = new FacetedMesh[1];  
			client.assets.RequestMesh(prim.Sculpt.SculptTexture, new MethodDelegate<Void, MeshDownloadCallbackArgs>()
					{
				public Void execute(MeshDownloadCallbackArgs e) {
					if (!e.isSuccess() || !FacetedMesh.TryDecodeFromAsset(prim, e.getAssetMesh(), MeshRenderDetail, tmpMesh))
					{
						JLogger.warn("Failed to fetch or decode the mesh asset");
					}
					gotMesh.set();								
					return null;
				}
					});
			mesh =  tmpMesh[0];
			gotMesh.waitOne(20 * 1000);
		}
		return mesh;
			}


	public IBitmap LoadTexture(final UUID textureID, final boolean removeAlpha)
	{
		final ManualResetEvent gotImage = new ManualResetEvent(false);
		IBitmap img = null;

		//FIXME we need to handle caching images with removeAlpha true or false
//		if((img = bitmapLruCache.get(textureID)) != null)
//		{
//			//System.out.println("Found in LRU Cache " + textureID.toString());
//			return img;
//		}
		
		try
		{
			gotImage.reset();              
			LoadCachedImageResult result = client.assets.Cache.loadCompressedImageFromCache(textureID);

			if (result != null)
			{
				ByteArrayInputStream baos = new ByteArrayInputStream(result.data);
				img = LoadTGAClass.LoadTGA(baos);
				baos.close();
			}
			else
			{
				final IBitmap[] texDownloadResult = new IBitmap[1];
				client.assets.RequestImage(textureID, new MethodDelegate<Void, TextureDownloadCallbackArgs>()
						{
					public Void execute(TextureDownloadCallbackArgs e) {
						TextureRequestState state = e.getState(); 
						AssetTexture assetTexture = e.getAssetTexture();

						if (state == TextureRequestState.Finished)
						{
							try{
								ManagedImage mi = OpenJPEGFactory.getIntance().DecodeToImage(assetTexture.AssetData);

								if (removeAlpha)
								{
									if ((ImageChannels.and(mi.Channels,  ManagedImage.ImageChannels.Alpha)) != 0)
									{
										mi.ConvertChannels(ImageChannels.get(ImageChannels.and(mi.Channels, ~ManagedImage.ImageChannels.Alpha.getIndex())));
									}
								}
								byte[] tgaData = mi.ExportTGA();
								
								texDownloadResult[0] = LoadTGAClass.LoadTGA(new ByteArrayInputStream(tgaData));								
								client.assets.Cache.compressAndSaveImageToCache(textureID, tgaData, ImageChannels.and(mi.Channels, ManagedImage.ImageChannels.Alpha) != 0, false, false);
							}
							catch(Exception ex)
							{
								JLogger.error(Utils.getExceptionStackTraceAsString(ex));
								texDownloadResult[0]= null;
							}
						}
						gotImage.set();

						return null;
					}
						});
				gotImage.waitOne(30 * 1000);
				img = texDownloadResult[0];				
				
			}
			if (img != null)
			{
//				bitmapLruCache.put(textureID, img);
				return img;
			}
			return null;
		}
		catch (Exception e)
		{
			JLogger.error(Utils.getExceptionStackTraceAsString(e));
			return null;
		}
	}
}
