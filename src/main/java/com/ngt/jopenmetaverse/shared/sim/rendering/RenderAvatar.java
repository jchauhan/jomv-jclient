/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering;

import com.ngt.jopenmetaverse.shared.protocol.primitives.Primitive;
import com.ngt.jopenmetaverse.shared.types.Vector3;

public class RenderAvatar extends SceneObject{

	@Override
	public Primitive getBasePrim() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setBasePrim(Primitive value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void calPrimPosAndRot(Vector3 cameraRenderPosition) {
		// TODO Auto-generated method stub
		RenderPosition = new Vector3(0, 0, 0);
		RenderRotation = InterpolatedRotation.Identity;
	}

}
