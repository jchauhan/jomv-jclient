/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering.mesh;

import com.ngt.jopenmetaverse.shared.types.Vector2;
import com.ngt.jopenmetaverse.shared.types.Vector3;
import com.ngt.jopenmetaverse.shared.util.Utils;


	    //[StructLayout(LayoutKind.Explicit)]
	    public class Vertex
	    {
//	        [FieldOffset(0)]
	        public Vector3 Position;
//	        [FieldOffset(12)]
	        public Vector3 Normal;
//	        [FieldOffset(24)]
	        public Vector2 TexCoord;

	        public static final int Size = 32;
	        
	        public byte[] toBytes()
	        {
	        	byte[] bytes = new byte[32];
	        	Position.toBytes(bytes, 0);
	        	Normal.toBytes(bytes, 12);
	        	TexCoord.toBytes(bytes, 24);
	        	return bytes;
	        }
	        
	        public static byte[] toBytes(Vertex[] vertices)
	        {
	        	byte[] bytes = new byte[Size*vertices.length];
	        	int offset = 0;
	        	for(Vertex vertex: vertices)
	        	{
	        		Utils.arraycopy(vertex.toBytes(), 0, bytes, offset, Size);
	        		offset += Size;
	        	}
	        	return bytes;
	        }
	        
	        @Override
	        public String toString()
	        {
	            return String.format("P: %s N: %s T: %s", Position, Normal, TexCoord);
	        }
	    }