/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;

import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.BoundingVolume;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.Face;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.Vertex;
import com.ngt.jopenmetaverse.shared.util.JLogger;

   /// <summary>
    /// Contains per primitive face data
    /// </summary>
    public class FaceData 
    {
        public float[] Vertices;
        //ushort
        public short[] Indices;
        public float[] TexCoords;
        public float[] Normals;
        public int PickingID = -1;
        public int VertexVBO = -1;
        public int IndexVBO = -1;
        public TextureInfo TextureInfo = new TextureInfo();
        public BoundingVolume BoundingVolume = new BoundingVolume();
        public static int VertexSize = 32; // sizeof (vertex), 2  x vector3 + 1 x vector2 = 8 floats x 4 bytes = 32 bytes 
        public TextureAnimationInfo AnimInfo;
        public int QueryID = 0;
        public boolean VBOFailed = false;
        public FloatBuffer vertexDataBuffer;
        public ShortBuffer indexDataBuffer;

        /// <summary>
        /// Dispose VBOs if we have them in graphics card memory
        /// </summary>
        public void Dispose()
        {
            if (VertexVBO != -1) GL15.glDeleteBuffers(VertexVBO);
            if (IndexVBO != -1) GL15.glDeleteBuffers(IndexVBO);
            VertexVBO = -1;
            IndexVBO = -1;
            vertexDataBuffer.clear();
            indexDataBuffer.clear();
            Vertices = null;
            //ushort
            Indices = null;
            TexCoords = null;
            Normals = null;
        }

        /// <summary>
        /// Checks if VBOs are created, if they are, bind them, if not create new
        /// </summary>
        /// <param name="face">Which face's mesh is uploaded in this VBO</param>
        /// <returns>True, if face data was succesfully uploaded to the graphics card memory</returns>
        public boolean CheckVBO(Face face)
        {
            if (VertexVBO == -1)
            {
                Vertex[] vArray = face.Vertices.toArray(new Vertex[0]);
                byte[] vertexData = Vertex.toBytes(vArray); 
                
                VertexVBO = GL15.glGenBuffers();
                GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, VertexVBO);
                float[] fdata = new float[vertexData.length/4];
                ByteBuffer.wrap(vertexData).asFloatBuffer().get(fdata);
                
                vertexDataBuffer = ByteBuffer.allocateDirect(vertexData.length).order(ByteOrder.nativeOrder()).asFloatBuffer()
				 .put(fdata);                
                vertexDataBuffer.position(0);
                
                GL15.glBufferData(GL15.GL_ARRAY_BUFFER, vertexDataBuffer, GL15.GL_STATIC_DRAW);
               
//                Compat.GenBuffers(out VertexVBO);
//                Compat.BindBuffer(BufferTarget.ArrayBuffer, VertexVBO);
//                Compat.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(vArray.Length * VertexSize), vArray, BufferUsageHint.StaticDraw);
                
                if (checkErrors())
                {
                    VBOFailed = true;
                    GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
                    GL15.glDeleteBuffers(VertexVBO);
                    VertexVBO = -1;
                    return false;
                }
            }

            if (IndexVBO == -1)
            {
//                Integer[] iArray = face.Indices.toArray(new Integer[0]);
                short[] vertexData = face.toIndicesArray();
                
                IndexVBO  =  GL15.glGenBuffers();
                GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, IndexVBO);
                
                indexDataBuffer = ByteBuffer
                	    .allocateDirect(vertexData.length*2).order(ByteOrder.nativeOrder()).asShortBuffer()
                	    .put(vertexData);
                indexDataBuffer.position(0);
                
                GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, indexDataBuffer, GL15.GL_STATIC_DRAW);

                
//                Compat.GenBuffers(out IndexVBO);
//                Compat.BindBuffer(BufferTarget.ElementArrayBuffer, IndexVBO);
//                Compat.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(iArray.Length * sizeof(ushort)), iArray, BufferUsageHint.StaticDraw);
                
                if (checkErrors())
                {
                    VBOFailed = true;                    
                    GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
                    GL15.glDeleteBuffers(IndexVBO);
                    IndexVBO = -1;
                    return false;
                }
            }

            return true;
        }
        
        private boolean checkErrors() {
    		int error = GL11.glGetError();
    		boolean isError = false;
    		if (error != GL11.GL_NO_ERROR) {
    			isError = true;
    			String message = "WebGL Error: " + error;
    			switch(error)
    			{
    			case GL11.GL_INVALID_OPERATION:
    			{
    				message += " INVALID_OPERATION";
    				break;
    			}
    			case GL11.GL_INVALID_ENUM:
    			{
    				message += " INVALID_ENUM";
    				break;
    			}
    			case GL11.GL_INVALID_VALUE:
    			{
    				message += " INVALID_VALUE";
    				break;
    			}
    			case GL11.GL_OUT_OF_MEMORY:
    			{
    				message += " OUT_OF_MEMORY";
    				break;
    			}
    			}
    			JLogger.error(message);
    		}   
			return isError;
        }
    }

    /// <summary>
    /// Class handling texture animations
    /// </summary>
    