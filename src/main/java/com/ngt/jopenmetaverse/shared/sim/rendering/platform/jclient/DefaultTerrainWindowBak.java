/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering.platform.jclient;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.util.glu.GLU;

import com.ngt.jopenmetaverse.shared.sim.Settings;
import com.ngt.jopenmetaverse.shared.sim.imaging.IBitmap;
import com.ngt.jopenmetaverse.shared.sim.rendering.ColorVertex;
import com.ngt.jopenmetaverse.shared.sim.rendering.RHelp;
import com.ngt.jopenmetaverse.shared.sim.rendering.ShaderProgram;
import com.ngt.jopenmetaverse.shared.sim.rendering.math.FloatMatrix;
import com.ngt.jopenmetaverse.shared.sim.rendering.math.MatrixUtil;

public class DefaultTerrainWindowBak extends GLApp{
	
	int terrainVBO = -1;
	int terrainIndexVBO = -1;
	int[] textureId = new int[]{-1};
	ShaderProgram shaderProgram = null;
	
	int textureCoordAttribute = -1;
	int textureUniform = -1;
	int projectionMatrixUniform = -1;
	private int vertexPositionAttribute =-1;
	private int vertexNormalAttribute = -1;
	
	IBitmap terrainImage;
	ColorVertex[] terrainVertices;
	short[] terrainIndices;
	
	
	private FloatMatrix perspectiveMatrix;
	private FloatMatrix translationMatrix;
	private FloatMatrix rotationMatrix;
	private FloatMatrix resultingMatrix;
	private int speedX = 0;
	private int speedY = 0;
	private int angleX = 0;
	private int angleY = 0;
	private int angleZ = 0;
	private float translateZ = -2;
	
	FloatBuffer resultingMatrixBuffer;
	
    /**
     * Initialize the scene.  Called by GLApp.run().  For now the default
     * settings will be fine, so no code here.
     */
	
	public DefaultTerrainWindowBak(IBitmap terrainImage, ColorVertex[] terrainVertices, short[] terrainIndices)
	{
		this.terrainImage = terrainImage;
		this.terrainVertices = terrainVertices;
		this.terrainIndices = terrainIndices;
	}
	
	@Override
    public void initGL() {
		super.initGL();
        // Select The Modelview Matrix (controls model orientation)
        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        // Reset the coordinate system to center of screen
        GL11.glLoadIdentity();
        
        GL11.glColor3f(1f, 1f, 1f);
	}
	
	public void dispose()
	{
		shaderProgram.Stop();
	}
	
    public void setup() throws Exception
    {
    	shaderProgram = new ShaderProgram();
    	boolean shaderLoadingStatus = shaderProgram.Load(new String[] {Settings.RESOURCE_DIR + "/shaders/vertex-shader.vert"
    			, Settings.RESOURCE_DIR + "/shaders/fragment-shader.frag"}, 
    			new String[] {"vertexPosition", "texPosition", "aVertexNormal"});
    	
    	if(!shaderLoadingStatus)
    		throw new Exception("Shader could not be loaded");
    	
    	shaderProgram.Start();
    	
		perspectiveMatrix = MatrixUtil.createPerspectiveMatrix(45, 1.0f, 0.1f, 100);
		translationMatrix = MatrixUtil.createTranslationMatrix(0, 0, translateZ);
		rotationMatrix = MatrixUtil.createRotationMatrix(angleX, angleY, angleZ);
		resultingMatrix = perspectiveMatrix.multiply(translationMatrix).multiply(rotationMatrix);
    	float[] resultingMatrixData = resultingMatrix.getColumnWiseFlatData(); 
    	resultingMatrixBuffer = ByteBuffer.allocateDirect(resultingMatrixData.length*4).order(ByteOrder.nativeOrder()).asFloatBuffer();
		resultingMatrixBuffer.put(resultingMatrixData);
		resultingMatrixBuffer.position(0);
		
		
		// Bind texPosition to attribute 1
		vertexPositionAttribute = shaderProgram.getAttribLocation("vertexPosition");
    	GL20.glEnableVertexAttribArray(vertexPositionAttribute);		
    	textureCoordAttribute = shaderProgram.getAttribLocation("texPosition");
    	GL20.glEnableVertexAttribArray(textureCoordAttribute);
    	
    	vertexNormalAttribute = shaderProgram.getAttribLocation("aVertexNormal");
    	GL20.glEnableVertexAttribArray(vertexNormalAttribute);
    	
    	textureUniform = shaderProgram.getUniformLocation("tex");
		projectionMatrixUniform = shaderProgram.getUniformLocation("projectionMatrix");
    	
        textureId[0] = RHelp.GLLoadImage(terrainImage, false);
        
        GL11.glEnable(GL11.GL_TEXTURE_2D);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureId[0]);
    	
        GL11.glEnableClientState(GL11.GL_VERTEX_ARRAY);
        GL11.glEnableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
        GL11.glEnableClientState(GL11.GL_NORMAL_ARRAY);

        
        
        IntBuffer terrainVBOBuffer = ByteBuffer
        	    .allocateDirect(1*4).order(ByteOrder.nativeOrder()).asIntBuffer();
        terrainVBOBuffer.position(0);
        GL15.glGenBuffers(terrainVBOBuffer);
        terrainVBO = terrainVBOBuffer.get(0);
        
        byte[] verticesData = ColorVertex.toBytes(terrainVertices); 
        ByteBuffer dataBuffer = ByteBuffer.allocateDirect(verticesData.length).order(ByteOrder.nativeOrder());
        dataBuffer.put(verticesData);
        dataBuffer.position(0);
        
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, terrainVBO);
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, dataBuffer, GL15.GL_STATIC_DRAW);
        
        IntBuffer terrainIndexVBOBuffer = terrainVBOBuffer;
        terrainIndexVBOBuffer.position(0);
        GL15.glGenBuffers(terrainIndexVBOBuffer);
        terrainIndexVBO = terrainIndexVBOBuffer.get(0);
                
        ShortBuffer terrainIndicesDataBuffer =  ByteBuffer.allocateDirect(terrainIndices.length*4).order(ByteOrder.nativeOrder()).asShortBuffer();
        terrainIndicesDataBuffer.put(terrainIndices);
        terrainIndicesDataBuffer.position(0);        
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, terrainIndexVBO);
        GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, terrainIndicesDataBuffer, GL15.GL_STATIC_DRAW);
    	
    	
    }

    /**
     * Render one frame.  Called by GLApp.run().
     * @throws Exception 
     */
    public void draw() throws Exception {

    	System.out.println("Start Drawing..");
        // Clear screen and depth buffer
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
	                
		// Bind the texture to texture unit 0
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureId[0]);
		shaderProgram.setUniform1(textureUniform, 0);
        
		GL20.glUniformMatrix4(projectionMatrixUniform, false, resultingMatrixBuffer);

		
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, terrainVBO);
        
        GL20.glVertexAttribPointer(vertexNormalAttribute, 3, GL11.GL_FLOAT, false, ColorVertex.Size, 12L);
        checkErrors("After Setting NormalPointer");
        GL20.glEnableVertexAttribArray(vertexNormalAttribute);

        
        GL20.glVertexAttribPointer(textureCoordAttribute, 2, GL11.GL_FLOAT, false, ColorVertex.Size, 24L);
        checkErrors("After Setting TexCoordPointer");
        
        GL20.glEnableVertexAttribArray(textureCoordAttribute);
        checkErrors("After Enabling textureCoordAttribute");
        
        GL20.glVertexAttribPointer(vertexPositionAttribute, 3, GL11.GL_FLOAT, false, ColorVertex.Size, 0L);
        checkErrors("After Setting glVertexAttribPointer");
        GL20.glEnableVertexAttribArray(vertexPositionAttribute);

		
        checkErrors("After Activating Texture");

		
//        // Place the viewpoint
//        GLU.gluLookAt(
//            0f, 0f, 10f,   // eye position (10 units in front of the origin)
//            0f, 0f, 0f,    // target to look at (the origin)
//            0f, 1f, 0f);   // which way is up (Y axis)
////        // draw a triangle centered around 0,0,0

        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, terrainIndexVBO);
        checkErrors("After Binding Element Array Buffer");
        GL11.glDrawElements(GL11.GL_TRIANGLES, terrainIndices.length, GL11.GL_UNSIGNED_SHORT, 0);
        
        checkErrors("After Drawing Elements");

//        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
//        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);;
//        GL20.glDisableVertexAttribArray(textureCoordAttribute);
//        GL20.glDisableVertexAttribArray(vertexPositionAttribute);
//        GL20.glDisableVertexAttribArray(vertexNormalAttribute);

        
//        GL11.glBegin(GL11.GL_TRIANGLES);           // draw triangles
//            GL11.glVertex3f( 0.0f, 1.0f, 0.0f);         // Top
//            GL11.glVertex3f(-1.0f,-1.0f, 0.0f);         // Bottom Left
//            GL11.glVertex3f( 1.0f,-1.0f, 0.0f);         // Bottom Right
//        GL11.glEnd();                              // done
    }
    
    /**
	 * Checks the WebGL Errors and throws an exception if there is an error.
	 */
	private void checkErrors(String locaton) {
		int error = GL11.glGetError();
		if (error != GL11.GL_NO_ERROR) {
			String message = "@ " + locaton + " OpenGL Error: " + error;
			switch(error)
			{
			case GL11.GL_INVALID_OPERATION:
			{
				message += " INVALID_OPERATION";
				break;
			}
			case GL11.GL_INVALID_ENUM:
			{
				message += " INVALID_ENUM";
				break;
			}
			case GL11.GL_INVALID_VALUE:
			{
				message += " INVALID_VALUE";
				break;
			}
			case GL11.GL_OUT_OF_MEMORY:
			{
				message += " OUT_OF_MEMORY";
				break;
			}
			}			
			throw new RuntimeException(message);
		}
	}
}