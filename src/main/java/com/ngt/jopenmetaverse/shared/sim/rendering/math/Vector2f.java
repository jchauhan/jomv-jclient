/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering.math;


/**
 * Represents a vector with two elements.
 * 
 * @author Sönke Sothmann
 * @author Steffen Schäfer
 * 
 */
public class Vector2f implements Vectorf {
	private float u;
	private float v;

	/**
	 * Constructs a new instance of the Vector2f with the given coordinates to
	 * set.
	 * 
	 * @param u
	 * @param v
	 */
	public Vector2f(float u, float v) {
		super();
		this.u = u;
		this.v = v;
	}

	/**
	 * Returns the u coordinate.
	 * 
	 * @return the u coordinate
	 */
	public float getU() {
		return u;
	}

	/**
	 * Sets the u coordinate.
	 * 
	 * @param u
	 *            the u coordinate to set
	 */
	public void setU(float u) {
		this.u = u;
	}

	/**
	 * Returns the v coordinate.
	 * 
	 * @return the v coordinate
	 */
	public float getV() {
		return v;
	}

	/**
	 * Sets the v coordinate.
	 * 
	 * @param v
	 *            the v coordinate to set
	 */
	public void setV(float v) {
		this.v = v;
	}

	/* (non-Javadoc)
	 * @see com.googlecode.gwtgl.example.client.util.math.Vector#multiply(float)
	 */
	@Override
	public Vectorf multiply(float scalar) {
		return new Vector2f(this.u * scalar, this.v * scalar);
	}

	/* (non-Javadoc)
	 * @see com.googlecode.gwtgl.example.client.util.math.Vector#length()
	 */
	@Override
	public float length() {
		return (float) Math.sqrt(this.u * this.u + this.v * this.v);
	}

	/* (non-Javadoc)
	 * @see com.googlecode.gwtgl.example.client.util.math.Vector#toUnitVector()
	 */
	@Override
	public Vectorf toUnitVector() {
		float length = length();
		return new Vector2f(this.u / length, this.v / length);
	}

	/* (non-Javadoc)
	 * @see com.googlecode.gwtgl.example.client.util.math.Vector#toArray()
	 */
	@Override
	public float[] toArray() {
		return new float[] { this.u, this.v };
	}
}
