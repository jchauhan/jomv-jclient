/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering.mesh;

import java.util.ArrayList;
import java.util.List;

import com.ngt.jopenmetaverse.shared.protocol.primitives.Primitive;
import com.ngt.jopenmetaverse.shared.sim.asset.AssetMesh;
import com.ngt.jopenmetaverse.shared.structureddata.OSD;
import com.ngt.jopenmetaverse.shared.structureddata.OSDArray;
import com.ngt.jopenmetaverse.shared.structureddata.OSDMap;
import com.ngt.jopenmetaverse.shared.types.Vector2;
import com.ngt.jopenmetaverse.shared.types.Vector3;
import com.ngt.jopenmetaverse.shared.util.JLogger;
import com.ngt.jopenmetaverse.shared.util.Utils;

/// <summary>
    /// Contains all mesh faces that belong to a prim
    /// </summary>
    public class FacetedMesh extends Mesh
    {
        /// <summary>List of primitive faces</summary>
        public List<Face> Faces;

        /// <summary>
        /// Decodes mesh asset into FacetedMesh
        /// </summary>
        /// <param name="prim">Mesh primitive</param>
        /// <param name="meshAsset">Asset retrieved from the asset server</param>
        /// <param name="LOD">Level of detail</param>
        /// <param name="mesh">Resulting decoded FacetedMesh</param>
        /// <returns>True if mesh asset decoding was successful</returns>
        public static boolean TryDecodeFromAsset(Primitive prim, AssetMesh meshAsset, 
        		DetailLevel LOD, FacetedMesh[] mesh)
        {
            mesh[0] = null;

            try
            {
                if (!meshAsset.Decode())
                {
                    return false;
                }

                OSDMap MeshData = meshAsset.MeshData;

                mesh[0] = new FacetedMesh();

                mesh[0].Faces = new ArrayList<Face>();
                mesh[0].Prim = prim;
                mesh[0].Profile.Faces = new ArrayList<ProfileFace>();
                mesh[0].Profile.Positions = new ArrayList<Vector3>();
                mesh[0].Path.Points = new ArrayList<PathPoint>();

                OSD facesOSD = null;

                switch (LOD)
                {
                    default:
                    case Highest:
                        facesOSD = MeshData.get("high_lod");
                        break;

                    case High:
                        facesOSD = MeshData.get("medium_lod");
                        break;

                    case Medium:
                        facesOSD = MeshData.get("low_lod");
                        break;

                    case Low:
                        facesOSD = MeshData.get("lowest_lod");
                        break;
                }

                if (facesOSD == null || !(facesOSD instanceof OSDArray))
                {
                    return false;
                }

                OSDArray decodedMeshOsdArray = (OSDArray)facesOSD;

                for (int faceNr = 0; faceNr < decodedMeshOsdArray.count(); faceNr++)
                {
                    OSD subMeshOsd = decodedMeshOsdArray.get(faceNr);

                    // Decode each individual face
                    if (subMeshOsd instanceof OSDMap)
                    {
                        Face oface = new Face();
                        oface.ID = faceNr;
                        oface.Vertices = new ArrayList<Vertex>();
                        oface.Indices = new ArrayList<Integer>();
                        oface.TextureFace = prim.Textures.GetFace(faceNr);

                        OSDMap subMeshMap = (OSDMap)subMeshOsd;

                        Vector3 posMax;
                        Vector3 posMin;

                        // If PositionDomain is not specified, the default is from -0.5 to 0.5
                        if (subMeshMap.containsKey("PositionDomain"))
                        {
                            posMax = ((OSDMap)subMeshMap.get("PositionDomain")).get("Max").asVector3();
                            posMin = ((OSDMap)subMeshMap.get("PositionDomain")).get("Min").asVector3();
                        }
                        else
                        {
                            posMax = new Vector3(0.5f, 0.5f, 0.5f);
                            posMin = new Vector3(-0.5f, -0.5f, -0.5f);
                        }

                        // Vertex positions
                        byte[] posBytes = subMeshMap.get("Position").asBinary();

                        // Normals
                        byte[] norBytes = null;
                        if (subMeshMap.containsKey("Normal"))
                        {
                            norBytes = subMeshMap.get("Normal").asBinary();
                        }

                        // UV texture map
                        Vector2 texPosMax = Vector2.Zero;
                        Vector2 texPosMin = Vector2.Zero;
                        byte[] texBytes = null;
                        if (subMeshMap.containsKey("TexCoord0"))
                        {
                            texBytes = subMeshMap.get("TexCoord0").asBinary();
                            texPosMax = ((OSDMap)subMeshMap.get("TexCoord0Domain")).get("Max").asVector2();
                            texPosMin = ((OSDMap)subMeshMap.get("TexCoord0Domain")).get("Min").asVector2();
                        }

                        // Extract the vertex position data
                        // If present normals and texture coordinates too
                        for (int i = 0; i < posBytes.length; i += 6)
                        {
                            int uX = Utils.bytesToUInt16(posBytes, i);
                            int uY = Utils.bytesToUInt16(posBytes, i + 2);
                            int uZ = Utils.bytesToUInt16(posBytes, i + 4);

                            Vertex vx = new Vertex();

                            vx.Position = new Vector3(
                                Utils.uint16ToFloat(uX, posMin.X, posMax.X),
                                Utils.uint16ToFloat(uY, posMin.Y, posMax.Y),
                                Utils.uint16ToFloat(uZ, posMin.Z, posMax.Z));

                            if (norBytes != null && norBytes.length >= i + 4)
                            {
                                int nX = Utils.bytesToUInt16(norBytes, i);
                                int nY = Utils.bytesToUInt16(norBytes, i + 2);
                                int nZ = Utils.bytesToUInt16(norBytes, i + 4);

                                vx.Normal = new Vector3(
                                    Utils.uint16ToFloat(nX, posMin.X, posMax.X),
                                    Utils.uint16ToFloat(nY, posMin.Y, posMax.Y),
                                    Utils.uint16ToFloat(nZ, posMin.Z, posMax.Z));
                            }

                            int vertexIndexOffset = oface.Vertices.size() * 4;

                            if (texBytes != null && texBytes.length >= vertexIndexOffset + 4)
                            {
                                int tX = Utils.bytesToUInt16(texBytes, vertexIndexOffset);
                                int tY = Utils.bytesToUInt16(texBytes, vertexIndexOffset + 2);

                                vx.TexCoord = new Vector2(
                                    Utils.uint16ToFloat(tX, texPosMin.X, texPosMax.X),
                                    Utils.uint16ToFloat(tY, texPosMin.Y, texPosMax.Y));
                            }

                            oface.Vertices.add(vx);
                        }

                        byte[] triangleBytes = subMeshMap.get("TriangleList").asBinary();
                        for (int i = 0; i < triangleBytes.length; i += 6)
                        {
                            int v1 = Utils.bytesToUInt16(triangleBytes, i);
                            oface.Indices.add(v1);
                            int v2 = Utils.bytesToUInt16(triangleBytes, i + 2);
                            oface.Indices.add(v2);
                            int v3 = Utils.bytesToUInt16(triangleBytes, i + 4);
                            oface.Indices.add(v3);
                        }

                        mesh[0].Faces.add(oface);
                    }
                }

            }
            catch (Exception ex)
            {
                JLogger.warn("Failed to decode mesh asset: " + Utils.getExceptionStackTraceAsString(ex));
                return false;
            }

            return true;
        }
    }
