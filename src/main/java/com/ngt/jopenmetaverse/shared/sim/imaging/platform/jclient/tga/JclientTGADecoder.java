/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.imaging.platform.jclient.tga;

import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.util.Arrays;

import com.ngt.jopenmetaverse.shared.sim.imaging.tga.CoreTGADecoder;
import com.ngt.jopenmetaverse.shared.util.Utils;

public class JclientTGADecoder extends CoreTGADecoder{

	public static BufferedImage loadImage(InputStream source) throws Exception
	{
		DecodedImage decodedImage = decodeImage(source);
		TgaHeader header = decodedImage.getHeader();
		byte[] decoded = decodedImage.getPixels();

		BufferedImage b = null;
		//			System.out.println("\n\nStoring Pixels");

		if (header.ImageSpec.getAlphaBits() > 0 ||
				header.ImageSpec.PixelDepth == 8 ||	// Assume  8 bit images are alpha only
				header.ImageSpec.PixelDepth == 32)	// Assume 32 bit images are ARGB
		{
			b = new BufferedImage(
					header.ImageSpec.Width,
					header.ImageSpec.Height,
					BufferedImage.TYPE_INT_ARGB);

			for(int j = 0; j < header.ImageSpec.Height; j++)
				for(int i = 0; i < header.ImageSpec.Width; i++) {
					int index = ( j* header.ImageSpec.Width + i) * (4);

					int value =  ((decoded[index + 3] & 0xFF) << 0) |
							((decoded[index + 2] & 0xFF) <<  8) |
							((decoded[index + 1] & 0xFF) << 16) | ((decoded[index + 0] & 0xFF) << 24);

//					if(value != 0)
//						System.out.println(String.format("<X = %d,  Y = %d, %d %s -- %s>", i, j, index, 
//								Utils.bytesToHexDebugString(Utils.intToBytes(value), ""),
//								Utils.bytesToHexDebugString(Arrays.copyOfRange(decoded, index, index + 4), "")
//								));

					b.setRGB(i, j,value);
				} 
		}
		else
		{
			b = new BufferedImage(
					header.ImageSpec.Width,
					header.ImageSpec.Height,
					BufferedImage.TYPE_INT_RGB);
			for(int j = 0; j < header.ImageSpec.Height; j++)
				for(int i = 0; i < header.ImageSpec.Width; i++) {
					int index = ( (j)* header.ImageSpec.Width + i) * (4);

					int value =  ((decoded[index + 3] & 0xFF) << 0)|
							((decoded[index + 2] & 0xFF) <<  8)|
							((decoded[index + 1] & 0xFF) << 16) | ((0xff) << 24);


//					if(value != 0)
//					System.out.println(String.format("<X = %d,  Y = %d, %d %s -- %s>", i, j, index, 
//							Utils.bytesToHexDebugString(Utils.intToBytes(value), ""),
//							Utils.bytesToHexDebugString(Arrays.copyOfRange(decoded, index, index + 4), "")
//							));
					
					b.setRGB(i, j,value);
				}  
		}
		return b;

	}
}
