/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering.mesh.primmesher;
 public class Quat
	    {
	        /// <summary>X value</summary>
	        public float X;
	        /// <summary>Y value</summary>
	        public float Y;
	        /// <summary>Z value</summary>
	        public float Z;
	        /// <summary>W value</summary>
	        public float W;

	        public Quat(float x, float y, float z, float w)
	        {
	            X = x;
	            Y = y;
	            Z = z;
	            W = w;
	        }
	        
	        public Quat(Coord axis, float angle)
	        {
	            axis = axis.Normalize();

	            angle *= 0.5f;
	            float c = (float)Math.cos(angle);
	            float s = (float)Math.sin(angle);

	            X = axis.X * s;
	            Y = axis.Y * s;
	            Z = axis.Z * s;
	            W = c;

	            Normalize();
	        }

	        public float Length()
	        {
	            return (float)Math.sqrt(X * X + Y * Y + Z * Z + W * W);
	        }

	        public Quat Normalize()
	        {
	            final float MAG_THRESHOLD = 0.0000001f;
	            float mag = Length();

	            // Catch very small rounding errors when normalizing
	            if (mag > MAG_THRESHOLD)
	            {
	                float oomag = 1f / mag;
	                X *= oomag;
	                Y *= oomag;
	                Z *= oomag;
	                W *= oomag;
	            }
	            else
	            {
	                X = 0f;
	                Y = 0f;
	                Z = 0f;
	                W = 1f;
	            }

	            return this;
	        }

	        public static Quat multiply(Quat q1, Quat q2)
	        {
	            float x = q1.W * q2.X + q1.X * q2.W + q1.Y * q2.Z - q1.Z * q2.Y;
	            float y = q1.W * q2.Y - q1.X * q2.Z + q1.Y * q2.W + q1.Z * q2.X;
	            float z = q1.W * q2.Z + q1.X * q2.Y - q1.Y * q2.X + q1.Z * q2.W;
	            float w = q1.W * q2.W - q1.X * q2.X - q1.Y * q2.Y - q1.Z * q2.Z;
	            return new Quat(x, y, z, w);
	        }

	        @Override
	        public String toString()
	        {
	            return "< X: " + this.X + ", Y: " + this.Y + ", Z: " + this.Z + ", W: " + this.W + ">";
	        }
	    }

	    