/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering;

  public class RenderSettings
    {
        //region VBO support
        public static boolean UseVBO = true;
        public static boolean CoreVBOPresent = true;
        public static boolean ARBVBOPresent;
        //endregion VBO support

        //region Occlusion queries
        /// <summary>Should we try to optimize by not drawing objects occluded behind other objects</summary>
        public static boolean OcclusionCullingEnabled = false;
        public static boolean CoreQuerySupported = false;
        public static boolean ARBQuerySupported = false;
        //endregion Occlusion queries

        public static boolean HasMultiTexturing;
        public static boolean UseFBO;
        public static boolean HasMipmap;
        public static boolean HasShaders;
//        public static DetailLevel PrimRenderDetail = DetailLevel.High;
//        public static DetailLevel SculptRenderDetail = DetailLevel.High;
//        public static DetailLevel MeshRenderDetail = DetailLevel.Highest;
        public static boolean AllowQuickAndDirtyMeshing = true;
        public static int MeshesPerFrame = 2;
        public static int TexturesToDownloadPerFrame = 2;
        /// <summary>Should we try to make sure that large prims that are > our draw distance are in view when we are standing on them</summary>
        public static boolean HeavierDistanceChecking = true;
        /// <summary>Minimum time between rebuilding terrain mesh and texture</summary>
        public static float MinimumTimeBetweenTerrainUpdated = 15f;
        /// <summary>Are textures that don't have dimensions that are powers of two supported</summary>
        public static boolean TextureNonPowerOfTwoSupported;

        /// <summary>
        /// Render avatars
        /// </summary>
        public static boolean AvatarRenderingEnabled = true;

        /// <summary>
        /// Render prims
        /// </summary>
        public static boolean PrimitiveRenderingEnabled = true;

        /// <summary>
        /// Show avatar skeloton
        /// </summary>
        public static boolean RenderAvatarSkeleton = false;

        /// <summary>
        /// Enable shader for shiny
        /// </summary>
        public static boolean EnableShiny = false;

        //region Water
        public static boolean WaterReflections = false;
        //endregion Water
    }