/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Map.Entry;

import com.ngt.jopenmetaverse.shared.protocol.primitives.Primitive;
import com.ngt.jopenmetaverse.shared.protocol.primitives.TextureEntryFace;
import com.ngt.jopenmetaverse.shared.sim.GridClient;
import com.ngt.jopenmetaverse.shared.sim.Simulator;
import com.ngt.jopenmetaverse.shared.sim.events.EventObserver;
import com.ngt.jopenmetaverse.shared.sim.events.om.PrimEventArgs;
import com.ngt.jopenmetaverse.shared.sim.events.om.TerseObjectUpdateEventArgs;
import com.ngt.jopenmetaverse.shared.sim.imaging.IBitmap;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.BoundingVolume;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.DetailLevel;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.Face;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.InterleavedVBO;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.MeshmerizerR;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.primitive.PrimitiveMesher;
import com.ngt.jopenmetaverse.shared.sim.rendering.terrain.TerrainHelper;
import com.ngt.jopenmetaverse.shared.sim.rendering.terrain.TerrainSplat;
import com.ngt.jopenmetaverse.shared.types.Action;
import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.shared.types.EnumsPrimitive.PCode;
import com.ngt.jopenmetaverse.shared.types.Vector3;
import com.ngt.jopenmetaverse.shared.util.JLogger;
import com.ngt.jopenmetaverse.shared.util.Utils;

public class Rendering {

	GridClient client;
	PrimitiveMesher primMesher;
	TextureManagerR textureManager;
	float lastFrameTime;
	Map<Long, RenderPrimitive> renderingPrims = Collections.synchronizedMap(new HashMap<Long, RenderPrimitive>());
	Map<Long, List<RenderPrimitive>> pendingRenderingPrims = Collections.synchronizedMap(new HashMap<Long, List<RenderPrimitive>>());	
	SortedSet<SceneObject> SortedObjects= new TreeSet<SceneObject>(); 
	Camera camera;
	RenderingContext renderingContext;
	
	/*
	 * Terrain Specific Variables
	 */
    float[][] heightMap = new float[256][256];
	short[] newIndices;
	InterleavedVBO newVertexVBO;
	IBitmap terrainImage; 
	RenderTerrain renderTerrain;
	
	
	
	public Rendering(GridClient client, Camera camera, RenderingContext renderingContext)
	{
		this.client = client;
		this.camera = camera;
		this.renderingContext = renderingContext;
		lastFrameTime = 0;
		textureManager = renderingContext.getTextureManager();
		primMesher = new PrimitiveMesher(this.client);
		registerEventHandlers();
		try{
		initalize();
		}
		catch(Exception e)
		{JLogger.warn("Error while initializing..." + Utils.getExceptionStackTraceAsString(e));}
	}

	private void verify()
	{
		for(long parentId: pendingRenderingPrims.keySet())
		{
			if(renderingPrims.containsKey(parentId))
				System.out.println("Could not pass validation Parent is already added .." + parentId);
		}
	}
	
	public void render(boolean picking) throws Exception
	{
		//Load Textures into OpenGL Cache
		textureManager.glLoadTextures();
		SortCullInterpolate();
		
        if (!renderTerrain.Initialized) 
        	renderTerrain.Initialize();
        
		renderTerrain.Render(renderingContext);
		
		Vector3 myPos = camera.getPosition();
				
//		verify();
		
		System.out.println("Going to Print Primitives...");
//		System.out.println("pendingRenderingPrims size " + pendingRenderingPrims.size());
		
		//Render the Primitives
		for(RenderPrimitive renderPrimitive: SortedObjects.toArray(new RenderPrimitive[0]))
		{
//			System.out.println("Primitive Distance: " + Math.sqrt(renderPrimitive.DistanceSquared));
//            if (FindClosestDistanceSquared(myPos, renderPrimitive) > 48)
//            {
//            	System.out.println("Skipping Primitive...");
//            	continue;
//            }
			try {
				renderPrimitive.Render(RenderPass.Simple, 0, renderingContext, 0);
				renderPrimitive.Render(RenderPass.Alpha, 0, renderingContext, 0);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private void initalize() throws Exception
	{
		//initialize here	
//		textureManager.start();
		loadTerrain();
		loadAllPrimitives();
	}
	
	private void loadTerrain() throws Exception
	{
		updateTerrain();
		renderTerrain = new RenderTerrain(client, newVertexVBO, newIndices, terrainImage);
	}
	
	private void loadAllPrimitives()
	{
		client.network.getCurrentSim().ObjectsPrimitives.foreach(new Action<Entry<Long, Primitive>>()
		{
			public void execute(Entry<Long, Primitive> entry) 
			{
				try{
		        UpdatePrimBlocking(entry.getValue());
				}
				catch(Exception e)
				{JLogger.warn("Error while LoadAllPrimitives: " + Utils.getExceptionStackTraceAsString(e));}
			}
		});
	}
	
	private void registerEventHandlers()
	{
		client.objects.registerOnTerseObjectUpdate(new EventObserver<TerseObjectUpdateEventArgs>()
				{
					@Override
					public void handleEvent(Observable sender,
							TerseObjectUpdateEventArgs arg) {
						try{
							handleTerseObjectUpdate(arg);
						}
						catch(Exception e)
						{JLogger.debug("Error on TerseObjectUpdate\n" + Utils.getExceptionStackTraceAsString(e));}
					}
				});

				client.objects.registerOnObjectUpdate(new EventObserver<PrimEventArgs>()
				{
					@Override
					public void handleEvent(Observable sender, PrimEventArgs arg) {
						try{
							handleObjectUpdate(arg);
						}
						catch(Exception e)
						{JLogger.debug("Error on TerseObjectUpdate\n" + Utils.getExceptionStackTraceAsString(e));}
					} 
				});
	}
	
	
	public void handleObjectUpdate(PrimEventArgs e) throws Exception
	{
        if (! (e.getSimulator().Handle.equals(client.network.getCurrentSim().Handle)) )
        	return;
        UpdatePrimBlocking(e.getPrim());
	}

	public void handleTerseObjectUpdate(TerseObjectUpdateEventArgs e) throws Exception
	{
        if (! (e.getSimulator().Handle.equals(client.network.getCurrentSim().Handle)) ) 
        	return;
        
        if (e.getPrim().ID.equals(client.self.getAgentID()))
        {
        	//TODO need to handle
            //trackedObject = myself;
        }

        //If it is an avatar, we don't need to deal with the terse update stuff, unless it sends textures to us
        if (e.getPrim().PrimData.PCode == PCode.Avatar && e.getUpdate().Textures == null)
            return;

        UpdatePrimBlocking(e.getPrim());
	}
	
	public void UpdatePrimBlocking(Primitive prim) throws Exception
	{
        if (prim.PrimData.PCode == PCode.Avatar)
        {
        	//TODO need to handle
        	JLogger.debug("Need to handle UpdatePrimBlocking for Avatar...");
            //AddAvatarToScene(client.network.getCurrentSim().ObjectsAvatars[prim.LocalID]);
            return;
        }

        // Skip foliage
        if (prim.PrimData.PCode != PCode.Prim) 
        	return;

        if (prim.Textures == null)
        {
        	System.out.println("Primitive Texture is null " + prim.LocalID);
        	return;
        }

        RenderPrimitive rPrim = null;
        synchronized(renderingPrims)
        {
        	if (renderingPrims.containsKey(prim.LocalID))
        	{
        		rPrim = renderingPrims.get(prim.LocalID);
        		rPrim.AttachedStateKnown = false;
        	}
        	else
        	{
        		rPrim = new RenderPrimitive();
        		rPrim.Meshed = false;
        		rPrim.BoundingVolume = new BoundingVolume();
        		rPrim.BoundingVolume.FromScale(prim.Scale);
        	}
        	rPrim.setBasePrim(prim);
        	if(prim.ParentID != 0)
        	{
        		RenderPrimitive parentRPrim = renderingPrims.get(prim.ParentID);
        		if(parentRPrim != null)
        		{
        			rPrim.setParentSceneObject(parentRPrim);
                	renderingPrims.put(prim.LocalID,  rPrim);
        		}
        		else
        		{
            	    addToPendingRenderingPrims(prim.ParentID, rPrim);
        		}
        	}
        	else
        	{
        		List<RenderPrimitive> list = getRenderingPrimsAndClear(prim.LocalID);
        		for(RenderPrimitive pprim: list)
        		{
        			pprim.setParentSceneObject(rPrim);
                	renderingPrims.put(pprim.getBasePrim().LocalID,  pprim);
        		}
        		
            	renderingPrims.put(prim.LocalID,  rPrim);
        	}
        }
      
//
//		RenderPrimitive rPrim = new RenderPrimitive();
//		rPrim.setBasePrim(prim);
//		rPrim.Meshed = false;
//		rPrim.BoundingVolume = new BoundingVolume();
//		rPrim.BoundingVolume.FromScale(prim.Scale);         
//		primMesher.MeshPrim(rPrim, true, DetailLevel.High, DetailLevel.High, DetailLevel.Highest);
		//TODO Update the  rPrim
	}	
	
    void SortCullInterpolate() throws Exception
    {
        SortedObjects = new TreeSet<SceneObject>(); 
//        VisibleAvatars = new List<RenderAvatar>();
        
        synchronized(renderingPrims)
        {
            for (RenderPrimitive obj : renderingPrims.values())
            {
                obj.PositionCalculated = false;
            }

            // Calculate positions and rotations of root prims
            // Perform interpolation om objects that survive culling
            for (RenderPrimitive obj : renderingPrims.values())
            {
                if (obj.getBasePrim().ParentID != 0) continue;
                
                SortCullInterpolatePrimitiveParent(obj);
            }
        }
        
        SortCullInterpolatePrimitiveAvatars();
        
        synchronized(renderingPrims)
        {
            // Calculate positions and rotations of root prims
            // Perform interpolation om objects that survive culling
            for (RenderPrimitive obj : renderingPrims.values())
            {
                if (obj.getBasePrim().ParentID == 0) continue;
                
                SortCullInterpolatePrimitiveChild(obj);
            }
        }
    }
    
    protected void SortCullInterpolatePrimitiveAvatars()
    {
//      // Calculate avatar positions and perform interpolation tasks
//      lock (Avatars)
//      {
//          foreach (RenderAvatar obj in Avatars.Values)
//          {
//              if (!obj.Initialized) obj.Initialize();
//              if (RenderSettings.AvatarRenderingEnabled) obj.Step(lastFrameTime);
//              PrimPosAndRot(obj, out obj.RenderPosition, out obj.RenderRotation);
//              obj.DistanceSquared = Vector3.DistanceSquared(Camera.RenderPosition, obj.RenderPosition);
//              obj.PositionCalculated = true;
//
//              if (!Frustum.ObjectInFrustum(obj.RenderPosition, obj.BoundingVolume)) continue;
//
//              VisibleAvatars.Add(obj);
//              // SortedObjects.Add(obj);
//          }
//      }

    }
    
    
    protected boolean withInCloseRange(RenderPrimitive obj)
    {
    	float distanceSquared = FindClosestDistanceSquared(camera.getPosition(), obj);
//    	System.out.println("Primitive : " + obj.getBasePrim().LocalID 
//    			+ " Distance " + distanceSquared + " Camera Pos: " + camera.getPosition() + " Obj Pos: " + obj.RenderPosition);
    	return distanceSquared < 1000;
    }
    
    protected void SortCullInterpolatePrimitiveParent(RenderPrimitive obj) throws Exception
    {
        if (!obj.Initialized) obj.Initialize();
        
        obj.Step(lastFrameTime, client.network.getCurrentSim().Stats.Dilation);

        if (!obj.PositionCalculated)
        {
        	obj.calPrimPosAndRot(camera.getPosition());
        	//        PrimPosAndRot(obj, out obj.RenderPosition, out obj.RenderRotation);
        	obj.DistanceSquared = Vector3.distanceSquared(camera.getPosition(), obj.RenderPosition);
        	obj.PositionCalculated = true;
        }
        
        if(!withInCloseRange(obj))
        	return ;
        //TODO need to enable
//        if (!Frustum.ObjectInFrustum(obj.RenderPosition, obj.BoundingVolume)) continue;
//        if (LODFactor(obj.DistanceSquared, obj.BoundingVolume.ScaledR) < minLODFactor) continue;

        if (!obj.Meshed)
        {
            if (!obj.Meshing)
            	//TODO need to handle
//            		&& meshingsRequestedThisFrame < RenderSettings.MeshesPerFrame)
            {
//                meshingsRequestedThisFrame++;
                MeshPrim(obj);
            }
        }

        if (obj.Faces == null) return;

        obj.Attached = false;
//        if (obj.Occluded())
//        {
//            OccludedObjects.Add(obj);
//        }
//        else
//        {
            SortedObjects.add(obj);
//        }
    }
    
    private void MeshPrim(RenderPrimitive rprim) throws Exception
    {
    	primMesher.MeshPrim(rprim, true, DetailLevel.High, DetailLevel.High, DetailLevel.Highest);
    	
    	Primitive prim = rprim.getBasePrim();
    	for(int i =0;  i < rprim.Faces.size(); i ++)
		{
			TextureEntryFace tef = prim.Textures.GetFace(i);
			System.out.println(String.format("\t\tFace Index: %d, Texture ID: %s", i, tef.getTextureID()));
			textureManager.requestDownloadTexture(new TextureLoadItem((FaceData)rprim.Faces.get(i).UserData, 
					prim, prim.Textures.GetFace(i)));
		}
    	
    }
    
    public void SortCullInterpolatePrimitiveChild(RenderPrimitive obj) throws Exception
    {
    	SortCullInterpolatePrimitiveParent(obj);

        if (!obj.AttachedStateKnown)
        {
            obj.Attached = isAttached(obj.getBasePrim().ParentID);
            obj.AttachedStateKnown = true;
        }
    }

	
	public void updateTerrain() throws Exception
	{		
		Simulator sim = client.network.getCurrentSim();
		heightMap = TerrainHelper.createHeightTable(sim.Terrain, heightMap);
		MeshmerizerR renderer = new MeshmerizerR();
		Face terrainFace = renderer.TerrainMesh(heightMap, 0f, 255f, 0f, 255f);
		ColorVertex[] terrainVertices = TerrainHelper.genColorVertices(terrainFace);
		newIndices = terrainFace.toIndicesArray();
		byte[] data = ColorVertex.toBytes(terrainVertices);
		newVertexVBO = new InterleavedVBO(data, ColorVertex.Size, 3, 3, 
				2, 0, 12, 24);
		
		UUID[] textureIds = new UUID[] { sim.TerrainDetail0, sim.TerrainDetail1, sim.TerrainDetail2, sim.TerrainDetail3 };
		float[] startHeights = new float[] { sim.TerrainStartHeight00, sim.TerrainStartHeight01, sim.TerrainStartHeight10, sim.TerrainStartHeight11 };
		float[] heightRanges = new float[] { sim.TerrainHeightRange00, sim.TerrainHeightRange01, sim.TerrainHeightRange10, sim.TerrainHeightRange11 };
		terrainImage = TerrainSplat.Splat(client, heightMap, textureIds, startHeights, heightRanges);

		System.out.println(String.format("Image splated Width %d, Height %d", terrainImage.getWidth(), terrainImage.getHeight()));
	}	
    
    /// <summary>
    /// Gets attachment state of a prim
    /// </summary>
    /// <param name="parentLocalID">Prim's parent id</param>
    /// <returns>True, if prim is part of an attachment</returns>
    boolean isAttached(long parentLocalID)
    {
        if (parentLocalID == 0) return false;
        try
        {
            if (client.network.getCurrentSim().ObjectsAvatars.containsKey(parentLocalID))
            {
                return true;
            }
            else if (client.network.getCurrentSim().ObjectsPrimitives.containsKey(parentLocalID))
            {
                return isAttached(client.network.getCurrentSim().ObjectsPrimitives.get(parentLocalID).ParentID);
            }
        }
        catch(Exception e) {JLogger.warn("Exception while evaluating isAttached" + Utils.getExceptionStackTraceAsString(e));}
        return false;
    }
    
    /// <summary>
    /// Finds the closest distance between the given pos and an object
    /// (Assumes that the object is a box slightly)
    /// </summary>
    /// <param name="vector3"></param>
    /// <param name="p"></param>
    /// <returns></returns>
    private float FindClosestDistanceSquared(Vector3 calcPos, SceneObject p)
    {
//        if (p.BoundingVolume == null
//            || !RenderSettings.HeavierDistanceChecking
//            || p.BoundingVolume.ScaledR < 10f
//            )
            return Vector3.distanceSquared(calcPos, p.RenderPosition);

//        Vector3 posToCheckFrom = Vector3.Zero;
//        //Get the bounding boxes for this prim
//        Vector3 boundingBoxMin = p.RenderPosition + p.BoundingVolume.ScaledMin;
//        Vector3 boundingBoxMax = p.RenderPosition + p.BoundingVolume.ScaledMax;
//        posToCheckFrom.X = (calcPos.X < boundingBoxMin.X) ? boundingBoxMin.X : (calcPos.X > boundingBoxMax.X) ? boundingBoxMax.X : calcPos.X;
//        posToCheckFrom.Y = (calcPos.Y < boundingBoxMin.Y) ? boundingBoxMin.Y : (calcPos.Y > boundingBoxMax.Y) ? boundingBoxMax.Y : calcPos.Y;
//        posToCheckFrom.Z = (calcPos.Z < boundingBoxMin.Z) ? boundingBoxMin.Z : (calcPos.Z > boundingBoxMax.Z) ? boundingBoxMax.Z : calcPos.Z;
//        return Vector3.distanceSquared(calcPos, posToCheckFrom);
    }
    
    private void addToPendingRenderingPrims(long parentID, RenderPrimitive rPrim )
    {
    	synchronized(pendingRenderingPrims)
    	{
    		if(!pendingRenderingPrims.containsKey(parentID))
    		{
    			pendingRenderingPrims.put(parentID, new LinkedList<RenderPrimitive>());
    		}
    		pendingRenderingPrims.get(parentID).add(rPrim);
    	}
    }
 
    private List<RenderPrimitive> getRenderingPrimsAndClear(long parentID)
    {
    	List<RenderPrimitive> list;
    	synchronized(pendingRenderingPrims)
    	{
    		if(pendingRenderingPrims.containsKey(parentID))
    		{
    			list = pendingRenderingPrims.remove(parentID);
    		}
    		else
    			list = new LinkedList<RenderPrimitive>();
    	}
    	
    	return list;
    }
    
	
}
