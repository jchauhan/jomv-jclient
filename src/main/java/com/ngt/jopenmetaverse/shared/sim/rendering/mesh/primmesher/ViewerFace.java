/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering.mesh.primmesher;
public class ViewerFace
	    {
	        public int primFaceNumber;

	        public Coord v1;
	        public Coord v2;
	        public Coord v3;

	        public int coordIndex1;
	        public int coordIndex2;
	        public int coordIndex3;

	        public Coord n1;
	        public Coord n2;
	        public Coord n3;

	        public UVCoord uv1;
	        public UVCoord uv2;
	        public UVCoord uv3;

	        public ViewerFace(int primFaceNumber)
	        {
	            this.primFaceNumber = primFaceNumber;

	            this.v1 = new Coord();
	            this.v2 = new Coord();
	            this.v3 = new Coord();

	            this.coordIndex1 = this.coordIndex2 = this.coordIndex3 = -1; // -1 means not assigned yet

	            this.n1 = new Coord();
	            this.n2 = new Coord();
	            this.n3 = new Coord();

	            this.uv1 = new UVCoord();
	            this.uv2 = new UVCoord();
	            this.uv3 = new UVCoord();
	        }
	        
	        public ViewerFace(int primFaceNumber, Coord v1, Coord v2, Coord v3,
					int coordIndex1, int coordIndex2, int coordIndex3,
					Coord n1, Coord n2, Coord n3, UVCoord uv1, UVCoord uv2,
					UVCoord uv3) {
				super();
				this.primFaceNumber = primFaceNumber;
				this.v1 = v1;
				this.v2 = v2;
				this.v3 = v3;
				this.coordIndex1 = coordIndex1;
				this.coordIndex2 = coordIndex2;
				this.coordIndex3 = coordIndex3;
				this.n1 = n1;
				this.n2 = n2;
				this.n3 = n3;
				this.uv1 = uv1;
				this.uv2 = uv2;
				this.uv3 = uv3;
			}

	        public ViewerFace(ViewerFace face)
	        {
	        	this(face.primFaceNumber, new Coord(face.v1), new Coord(face.v2), new Coord(face.v3),
	        			face.coordIndex1, face.coordIndex2, face.coordIndex3,
	        			new Coord(face.n1), new Coord(face.n2), new Coord(face.n3), new UVCoord(face.uv1), new UVCoord(face.uv2),
	        			new UVCoord(face.uv3));
	        }     

			public void Scale(float x, float y, float z)
	        {
	            this.v1.X *= x;
	            this.v1.Y *= y;
	            this.v1.Z *= z;

	            this.v2.X *= x;
	            this.v2.Y *= y;
	            this.v2.Z *= z;

	            this.v3.X *= x;
	            this.v3.Y *= y;
	            this.v3.Z *= z;
	        }

	        public void addPos(float x, float y, float z)
	        {
	            this.v1.X += x;
	            this.v2.X += x;
	            this.v3.X += x;

	            this.v1.Y += y;
	            this.v2.Y += y;
	            this.v3.Y += y;

	            this.v1.Z += z;
	            this.v2.Z += z;
	            this.v3.Z += z;
	        }

	        public void addRot(Quat q)
	        {
	            this.v1 = Coord.multiply(this.v1, q);
	            this.v2 = Coord.multiply(this.v2, q);
	            this.v3 = Coord.multiply(this.v3, q);
	            
	            this.n1 = Coord.multiply(this.n1, q);
	            this.n2 = Coord.multiply(this.n2, q);
	            this.n3 = Coord.multiply(this.n3, q);
	            
	        }

	        public void CalcSurfaceNormal()
	        {

	            Coord edge1 = new Coord(this.v2.X - this.v1.X, this.v2.Y - this.v1.Y, this.v2.Z - this.v1.Z);
	            Coord edge2 = new Coord(this.v3.X - this.v1.X, this.v3.Y - this.v1.Y, this.v3.Z - this.v1.Z);

	            this.n1 = Coord.Cross(edge1, edge2).Normalize();
	            this.n2 = new Coord(this.n1);
	            this.n3 = new Coord(this.n1);
	        }
	    }

	    