/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering.math;



import java.util.ArrayList;
import java.util.List;


/**
 * Helper class for creation of sphere mesh Inspired by "The WebGL Cookbook"
 * - http://learningwebgl.com/cookbook/index.php/How_to_draw_a_sphere
 * 
 * @author Sönke Sothmann
 * 
 */
public class Sphere extends IndexedMesh {

	/**
	 * Create sphere geometry
	 * @param latitudeBands
	 * @param longitudeBands
	 * @param radius
	 */
	public Sphere(int latitudeBands, int longitudeBands, int radius) {
		List<Float> vertexNormalsList = new ArrayList<Float>();
		List<Float> texCoordsList = new ArrayList<Float>();
		List<Float> verticesList = new ArrayList<Float>();

		for (int latNumber = 0; latNumber <= latitudeBands; latNumber++) {
			double theta = latNumber * Math.PI / latitudeBands;
			double sinTheta = Math.sin(theta);
			double cosTheta = Math.cos(theta);

			for (int longNumber = 0; longNumber <= longitudeBands; longNumber++) {
				double phi = longNumber * 2 * Math.PI / longitudeBands;
				double sinPhi = Math.sin(phi);
				double cosPhi = Math.cos(phi);

				double x = cosPhi * sinTheta;
				double y = cosTheta;
				double z = sinPhi * sinTheta;
				double u = 1.0 - ((1.0* longNumber) / longitudeBands);
				double v = 1.0 - ((1.0* latNumber) / latitudeBands);

				vertexNormalsList.add((float) x);
				vertexNormalsList.add((float) y);
				vertexNormalsList.add((float) z);
				texCoordsList.add((float) u);
				texCoordsList.add((float) v);
				verticesList.add((float) (radius * x));
				verticesList.add((float) (radius * y));
				verticesList.add((float) (radius * z));
				
				//GWT.log("Sphere: Normal:" + x + ":" + y + ":" + z + "Tex:" + u + ":" + v + "Ver:" + radius*x + ":" + radius*y + ":" + radius*z, null);
				
			}
		}

		List<Integer> indicesList = new ArrayList<Integer>();
		for (int latNumber = 0; latNumber < latitudeBands; latNumber++) {
			for (int longNumber = 0; longNumber < longitudeBands; longNumber++) {
				int first = ((latNumber * (longitudeBands + 1)) + longNumber);
				int second = (first + longitudeBands + 1);
				indicesList.add(first);
				indicesList.add(second);
				indicesList.add((first + 1));

				indicesList.add(second);
				indicesList.add( second + 1);
				indicesList.add(first + 1);
				
				//GWT.log("Sphere: Indices:" + first + ":" + second + ":" + (first+1) + ":" + second + ":" + (second + 1) + ":" + (first + 1), null);
			}
		}
		
		verticesArray = ConversionUtils.floatListToFloatArray(verticesList);
		texCoordsArray = ConversionUtils.floatListToFloatArray(texCoordsList);
		vertexNormalsArray = ConversionUtils.floatListToFloatArray(vertexNormalsList);
		indices = ConversionUtils.integerListToIntegerArray(indicesList);
	}

	/**
	 * Create sphere geometry with latitudeBands=30, longitudeBands=30 and radius=2
	 */
	public Sphere() {
		this(30, 30, 2);
	}
}
