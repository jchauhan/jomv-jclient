/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering;

import com.ngt.jopenmetaverse.shared.util.Utils;

public  class ColorVertex
{
	//			        [FieldOffset(0)]
	public com.ngt.jopenmetaverse.shared.sim.rendering.mesh.Vertex Vertex;
	//			        [FieldOffset(32)]
	public Color4b Color;

	public ColorVertex(
			com.ngt.jopenmetaverse.shared.sim.rendering.mesh.Vertex vertex,
			Color4b color) {
		super();
		Vertex = vertex;
		Color = color;
	}

	public byte[] toBytes()
	{
		byte[] vBytes = Vertex.toBytes();
		byte[] cBytes = Color.toBytes();		
		byte[] bytes = new byte[vBytes.length + cBytes.length];
		
		Utils.arraycopy(vBytes, 0, bytes, 0, vBytes.length);
		Utils.arraycopy(cBytes, 0, bytes, vBytes.length, cBytes.length);
		return bytes;
	}
	
	public static byte[] toBytes(ColorVertex[] colorVertices)
	{
		byte[] bytes = new byte[Size*colorVertices.length];
		int i =0;
		for(ColorVertex c: colorVertices)
		{
			Utils.arraycopy(c.toBytes(), 0, bytes, i, Size);
			i += Size;
		}
		
		return bytes;
	}
	
	@Override
	public String toString()
	{
		return "< "+  Vertex.toString() + " " + Color.toString() + " >";
	}
	
	public static final int Size = 36;
}

