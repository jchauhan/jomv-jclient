/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering.terrain;

import com.ngt.jopenmetaverse.shared.sim.TerrainCompressor.TerrainPatch;
import com.ngt.jopenmetaverse.shared.sim.rendering.Color4b;
import com.ngt.jopenmetaverse.shared.sim.rendering.ColorVertex;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.Face;
import com.ngt.jopenmetaverse.shared.util.Utils;

public class TerrainHelper 
{
	/*
	 * Create Height Table from set of patches
	 * @param patches is the array of TerrainPatch @see com.ngt.jopenmetaverse.shared.sim.Simulator#Terrain
	 * @param dstHeightTable, destination dstHeightTable to fill. It should be of size float[256][256]. if it is null, a new dstHeightTable is created and returned
	 * 
	 * @return dstHeightTable 
	 */
	public static float[][] createHeightTable(TerrainPatch[] patches,  float[][] dstHeightTable)
	{
		if(dstHeightTable == null)
		{
			dstHeightTable = new float[256][256];
		}
		
		 int step = 1;
         for (int x = 0; x < 256; x += step)
         {
             for (int y = 0; y < 256; y += step)
             {
                 float z = 0;
                 int patchNr = ((int)(x / 16)) * 16 + (int)(y / 16);
                 if (patches[patchNr] != null
                     && patches[patchNr].Data != null)
                 {
                     float[] data = patches[patchNr].Data;
                     z = data[(int)x % 16 * 16 + (int)y % 16];
                 }
                 dstHeightTable[x][y] = z;
             }
         }
         return dstHeightTable;
	}	
	
	
	public static ColorVertex[] genColorVertices(Face terrainFace)
	{
		ColorVertex[] terrainVertices = new ColorVertex[terrainFace.Vertices.size()];
         for (int i = 0; i < terrainFace.Vertices.size(); i++)
         {
             byte[] part = Utils.intToBytesLit(i);
             terrainVertices[i] = new ColorVertex
             (
                 terrainFace.Vertices.get(i),
                 new Color4b
                 (
                     part[0],
                     part[1],
                     part[2],
                     (byte)253 // terrain picking
                 )
             );
         }
         return terrainVertices;
	}
}
