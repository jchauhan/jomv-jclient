/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering.mesh;


  public  class RenderingLoader
	    {
	  //TODO need to handle following
//	        public static List<String> ListRenderers(String path)
//	        {
//	            List<String> plugins = new ArrayList<String>();
//	            String[] files = Directory.GetFiles(path, "OpenMetaverse.Rendering.*.dll");
//
//	            foreach (String f in files)
//	            {
//	                try
//	                {
//	                    Assembly a = Assembly.LoadFrom(f);
//	                    System.Type[] types = a.GetTypes();
//	                    foreach (System.Type type in types)
//	                    {
//	                        if (type.GetInterface("IRendering") != null)
//	                        {
//	                            if (type.GetCustomAttributes(typeof(RendererNameAttribute), false).Length == 1)
//	                            {
//	                                plugins.Add(f);
//	                            }
//	                            else
//	                            {
//	                                Logger.Log("Rendering plugin does not support the [RendererName] attribute: " + f,
//	                                    Helpers.LogLevel.Warning);
//	                            }
//
//	                            break;
//	                        }
//	                    }
//	                }
//	                catch (Exception e)
//	                {
//	                    Logger.Log(String.Format("Unrecognized rendering plugin {0}: {1}", f, e.Message),
//	                        Helpers.LogLevel.Warning, e);
//	                }
//	            }
//
//	            return plugins;
//	        }
//
//	        public static IRendering LoadRenderer(String filename)
//	        {
//	            try
//	            {
//	                Assembly a = Assembly.LoadFrom(filename);
//	                System.Type[] types = a.GetTypes();
//	                foreach (System.Type type in types)
//	                {
//	                    if (type.GetInterface("IRendering") != null)
//	                    {
//	                        if (type.GetCustomAttributes(typeof(RendererNameAttribute), false).Length == 1)
//	                        {
//	                            return (IRendering)Activator.CreateInstance(type);
//	                        }
//	                        else
//	                        {
//	                            throw new RenderingException(
//	                                "Rendering plugin does not support the [RendererName] attribute");
//	                        }
//	                    }
//	                }
//
//	                throw new RenderingException(
//	                    "Rendering plugin does not support the IRendering interface");
//	            }
//	            catch (Exception e)
//	            {
//	                throw new RenderingException("Failed loading rendering plugin: " + e.Message, e);
//	            }
//	        }
	    }
