/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering;

import java.nio.FloatBuffer;

import com.ngt.jopenmetaverse.shared.sim.rendering.math.FloatMatrix;

public class RenderingContext {
	protected TextureManagerR textureManager;
	
	private ShaderProgram shaderProgram;
	
	private int projectionMatrixUniform;
	private int textureUniform;	
	private int uAmbientColorUniform;
	private int uLightingDirectionUniform;
	private int uDirectionalColorUniform;
	private int uUseLightingUniform;
	private int normalMatrixUniform;
	
	private int vertexPositionAttribute;
	private int textureCoordAttribute;
	private int vertexNormalAttribute;
	
	FloatBuffer resultingMatrixBuffer;
	FloatMatrix resultingMatrix;
	private FloatMatrix perspectiveMatrix;
	private FloatMatrix translationMatrix;
	private FloatMatrix rotationMatrix;
	
	
	public RenderingContext(TextureManagerR textureManager,
			ShaderProgram shaderProgram, int projectionMatrixUniform,
			int textureUniform, int uAmbientColorUniform,
			int uLightingDirectionUniform, int uDirectionalColorUniform,
			int uUseLightingUniform, int normalMatrixUniform,
			int vertexPositionAttribute, int textureCoordAttribute,
			int vertexNormalAttribute) {
		super();
		this.textureManager = textureManager;
		this.shaderProgram = shaderProgram;
		this.projectionMatrixUniform = projectionMatrixUniform;
		this.textureUniform = textureUniform;
		this.uAmbientColorUniform = uAmbientColorUniform;
		this.uLightingDirectionUniform = uLightingDirectionUniform;
		this.uDirectionalColorUniform = uDirectionalColorUniform;
		this.uUseLightingUniform = uUseLightingUniform;
		this.normalMatrixUniform = normalMatrixUniform;
		this.vertexPositionAttribute = vertexPositionAttribute;
		this.textureCoordAttribute = textureCoordAttribute;
		this.vertexNormalAttribute = vertexNormalAttribute;
	}

	public TextureManagerR getTextureManager() {
		return textureManager;
	}

	public void setTextureManager(TextureManagerR textureManager) {
		this.textureManager = textureManager;
	}

	public ShaderProgram getShaderProgram() {
		return shaderProgram;
	}

	public void setShaderProgram(ShaderProgram shaderProgram) {
		this.shaderProgram = shaderProgram;
	}

	public int getProjectionMatrixUniform() {
		return projectionMatrixUniform;
	}

	public void setProjectionMatrixUniform(int projectionMatrixUniform) {
		this.projectionMatrixUniform = projectionMatrixUniform;
	}

	public int getTextureUniform() {
		return textureUniform;
	}

	public void setTextureUniform(int textureUniform) {
		this.textureUniform = textureUniform;
	}

	public int getuAmbientColorUniform() {
		return uAmbientColorUniform;
	}

	public void setuAmbientColorUniform(int uAmbientColorUniform) {
		this.uAmbientColorUniform = uAmbientColorUniform;
	}

	public int getuLightingDirectionUniform() {
		return uLightingDirectionUniform;
	}

	public void setuLightingDirectionUniform(int uLightingDirectionUniform) {
		this.uLightingDirectionUniform = uLightingDirectionUniform;
	}

	public int getuDirectionalColorUniform() {
		return uDirectionalColorUniform;
	}

	public void setuDirectionalColorUniform(int uDirectionalColorUniform) {
		this.uDirectionalColorUniform = uDirectionalColorUniform;
	}

	public int getuUseLightingUniform() {
		return uUseLightingUniform;
	}

	public void setuUseLightingUniform(int uUseLightingUniform) {
		this.uUseLightingUniform = uUseLightingUniform;
	}

	public int getNormalMatrixUniform() {
		return normalMatrixUniform;
	}

	public void setNormalMatrixUniform(int normalMatrixUniform) {
		this.normalMatrixUniform = normalMatrixUniform;
	}

	public int getVertexPositionAttribute() {
		return vertexPositionAttribute;
	}

	public void setVertexPositionAttribute(int vertexPositionAttribute) {
		this.vertexPositionAttribute = vertexPositionAttribute;
	}

	public int getTextureCoordAttribute() {
		return textureCoordAttribute;
	}

	public void setTextureCoordAttribute(int textureCoordAttribute) {
		this.textureCoordAttribute = textureCoordAttribute;
	}

	public int getVertexNormalAttribute() {
		return vertexNormalAttribute;
	}

	public void setVertexNormalAttribute(int vertexNormalAttribute) {
		this.vertexNormalAttribute = vertexNormalAttribute;
	}

	public FloatBuffer getResultingMatrixBuffer() {
		return resultingMatrixBuffer;
	}

	public void setResultingMatrixBuffer(FloatBuffer resultingMatrixBuffer) {
		this.resultingMatrixBuffer = resultingMatrixBuffer;
	}
	
	public FloatMatrix getPerspectiveMatrix() {
		return perspectiveMatrix;
	}

	public void setPerspectiveMatrix(FloatMatrix perspectiveMatrix) {
		this.perspectiveMatrix = perspectiveMatrix;
	}

	public FloatMatrix getTranslationMatrix() {
		return translationMatrix;
	}

	public void setTranslationMatrix(FloatMatrix translationMatrix) {
		this.translationMatrix = translationMatrix;
	}

	public FloatMatrix getRotationMatrix() {
		return rotationMatrix;
	}

	public void setRotationMatrix(FloatMatrix rotationMatrix) {
		this.rotationMatrix = rotationMatrix;
	}

	public FloatMatrix getResultingMatrix() {
		return resultingMatrix;
	}

	public void setResultingMatrix(FloatMatrix resultingMatrix) {
		this.resultingMatrix = resultingMatrix;
	}
}
