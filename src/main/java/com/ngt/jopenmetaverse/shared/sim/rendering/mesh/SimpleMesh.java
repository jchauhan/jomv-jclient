/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering.mesh;

import java.util.ArrayList;
import java.util.List;

import com.ngt.jopenmetaverse.shared.types.Vector3;
   public class SimpleMesh extends Mesh
	    {
	        public List<Vertex> Vertices;
	        //ushort
	        public List<Integer> Indices;

	        public SimpleMesh()
	        {
	        }

	        public SimpleMesh(SimpleMesh mesh)
	        {
	            this.Indices = new ArrayList<Integer>(mesh.Indices);
	            this.Path.Open = mesh.Path.Open;
	            this.Path.Points = new ArrayList<PathPoint>(mesh.Path.Points);
	            this.Prim = mesh.Prim;
	            this.Profile.Concave = mesh.Profile.Concave;
	            this.Profile.Faces = new ArrayList<ProfileFace>(mesh.Profile.Faces);
	            this.Profile.MaxX = mesh.Profile.MaxX;
	            this.Profile.MinX = mesh.Profile.MinX;
	            this.Profile.Open = mesh.Profile.Open;
	            this.Profile.Positions = new ArrayList<Vector3>(mesh.Profile.Positions);
	            this.Profile.TotalOutsidePoints = mesh.Profile.TotalOutsidePoints;
	            this.Vertices = new ArrayList<Vertex>(mesh.Vertices);
	        }
	    }
