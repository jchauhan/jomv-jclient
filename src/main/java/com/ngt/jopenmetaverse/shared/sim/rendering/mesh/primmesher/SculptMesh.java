/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering.mesh.primmesher;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ngt.jopenmetaverse.shared.sim.imaging.IBitmap;
import com.ngt.jopenmetaverse.shared.util.FileUtils;
import com.ngt.jopenmetaverse.shared.util.JLogger;
import com.ngt.jopenmetaverse.shared.util.Utils;

 public class SculptMesh
    {
        public List<Coord> coords;
        public List<Face> faces;

        public List<ViewerFace> viewerFaces;
        public List<Coord> normals;
        public List<UVCoord> uvs;

        public static enum SculptType 
        { 
        	sphere (1), 
        			torus (2), 
        			plane (3), 
        			cylinder (4);
        	private int index;
        	SculptType(int index)
        	{
        		this.index = index;
        	}     

        	public int getIndex()
        	{
        		return index;
        	}
        	
    		private static final Map<Integer,SculptType> lookup  = new HashMap<Integer,SculptType>();

    		static {
    			for(SculptType s : EnumSet.allOf(SculptType.class))
    				lookup.put(s.getIndex(), s);
    		}

    		public static SculptType get(Integer index)
    		{
    			return lookup.get(index);
    		}
        
        };

//#if SYSTEM_DRAWING
//
//        public SculptMesh SculptMeshFromFile(String fileName, SculptType sculptType, int lod, boolean viewerMode)
//        {
//            Bitmap bitmap = (Bitmap)Bitmap.FromFile(fileName);
//            SculptMesh sculptMesh = new SculptMesh(bitmap, sculptType, lod, viewerMode);
//            bitmap.Dispose();
//            return sculptMesh;
//        }
//
//
//        public SculptMesh(String fileName, int sculptType, int lod, int viewerMode, int mirror, int invert)
//        {
//            Bitmap bitmap = (Bitmap)Bitmap.FromFile(fileName);
//            _SculptMesh(bitmap, (SculptType)sculptType, lod, viewerMode != 0, mirror != 0, invert != 0);
//            bitmap.Dispose();
//        }
//#endif

        /// <summary>
        /// ** Experimental ** May disappear from future versions ** not recommeneded for use in applications
        /// Construct a sculpt mesh from a 2D array of floats
        /// </summary>
        /// <param name="zMap"></param>
        /// <param name="xBegin"></param>
        /// <param name="xEnd"></param>
        /// <param name="yBegin"></param>
        /// <param name="yEnd"></param>
        /// <param name="viewerMode"></param>
        public SculptMesh(float[][] zMap, float xBegin, float xEnd, float yBegin, float yEnd, boolean viewerMode)
        {
            float xStep, yStep;
            float uStep, vStep;

            int numYElements = zMap.length;
            int numXElements = zMap[0].length;

            try
            {
                xStep = (xEnd - xBegin) / (float)(numXElements - 1);
                yStep = (yEnd - yBegin) / (float)(numYElements - 1);

                uStep = 1.0f / (numXElements - 1);
                vStep = 1.0f / (numYElements - 1);
            }
            catch (ArithmeticException e)
            {
            	JLogger.debug(Utils.getExceptionStackTraceAsString(e));
                return;
            }

            coords = new ArrayList<Coord>();
            faces = new ArrayList<Face>();
            normals = new ArrayList<Coord>();
            uvs = new ArrayList<UVCoord>();

            viewerFaces = new ArrayList<ViewerFace>();

            int p1, p2, p3, p4;

            int x, y;
            int xStart = 0, yStart = 0;

            for (y = yStart; y < numYElements; y++)
            {
                int rowOffset = y * numXElements;

                for (x = xStart; x < numXElements; x++)
                {
                    /*
                    *   p1-----p2
                    *   | \ f2 |
                    *   |   \  |
                    *   | f1  \|
                    *   p3-----p4
                    */

                    p4 = rowOffset + x;
                    p3 = p4 - 1;

                    p2 = p4 - numXElements;
                    p1 = p3 - numXElements;

                    Coord c = new Coord(xBegin + x * xStep, yBegin + y * yStep, zMap[y][x]);
                    this.coords.add(c);
                    if (viewerMode)
                    {
                        this.normals.add(new Coord());
                        this.uvs.add(new UVCoord(uStep * x, 1.0f - vStep * y));
                    }

                    if (y > 0 && x > 0)
                    {
                        Face f1, f2;

                        if (viewerMode)
                        {
                            f1 = new Face(p1, p4, p3, p1, p4, p3);
                            f1.uv1 = p1;
                            f1.uv2 = p4;
                            f1.uv3 = p3;

                            f2 = new Face(p1, p2, p4, p1, p2, p4);
                            f2.uv1 = p1;
                            f2.uv2 = p2;
                            f2.uv3 = p4;
                        }
                        else
                        {
                            f1 = new Face(p1, p4, p3);
                            f2 = new Face(p1, p2, p4);
                        }

                        this.faces.add(f1);
                        this.faces.add(f2);
                    }
                }
            }

            if (viewerMode)
                calcVertexNormals(SculptType.plane, numXElements, numYElements);
        }

        public SculptMesh(IBitmap sculptBitmap, SculptType sculptType, int lod, boolean viewerMode) throws Exception
        {
            _SculptMesh(sculptBitmap, sculptType, lod, viewerMode, false, false);
        }

        public SculptMesh(IBitmap sculptBitmap, SculptType sculptType, int lod, boolean viewerMode, boolean mirror, boolean invert) throws Exception
        {
            _SculptMesh(sculptBitmap, sculptType, lod, viewerMode, mirror, invert);
        }
        public SculptMesh(List<List<Coord>> rows, SculptType sculptType, boolean viewerMode, boolean mirror, boolean invert)
        {
            _SculptMesh(rows, sculptType, viewerMode, mirror, invert);
        }


//#if SYSTEM_DRAWING
//        /// <summary>
//        /// converts a bitmap to a list of lists of coords, while scaling the image.
//        /// the scaling is done in floating point so as to allow for reduced vertex position
//        /// quantization as the position will be averaged between pixel values. this routine will
//        /// likely fail if the bitmap width and height are not powers of 2.
//        /// </summary>
//        /// <param name="bitmap"></param>
//        /// <param name="scale"></param>
//        /// <param name="mirror"></param>
//        /// <returns></returns>
//        private List<List<Coord>> bitmap2Coords(Bitmap bitmap, int scale, boolean mirror)
//        {
//            int numRows = bitmap.Height / scale;
//            int numCols = bitmap.Width / scale;
//            List<List<Coord>> rows = new ArrayList<List<Coord>>(numRows);
//
//            float pixScale = 1.0f / (scale * scale);
//            pixScale /= 255;
//
//            int imageX, imageY = 0;
//
//            int rowNdx, colNdx;
//
//            for (rowNdx = 0; rowNdx < numRows; rowNdx++)
//            {
//                List<Coord> row = new ArrayList<Coord>(numCols);
//                for (colNdx = 0; colNdx < numCols; colNdx++)
//                {
//                    imageX = colNdx * scale;
//                    int imageYStart = rowNdx * scale;
//                    int imageYEnd = imageYStart + scale;
//                    int imageXEnd = imageX + scale;
//                    float rSum = 0.0f;
//                    float gSum = 0.0f;
//                    float bSum = 0.0f;
//                    for (; imageX < imageXEnd; imageX++)
//                    {
//                        for (imageY = imageYStart; imageY < imageYEnd; imageY++)
//                        {
//                            Color c = bitmap.GetPixel(imageX, imageY);
//                            if (c.A != 255)
//                            {
//                                bitmap.SetPixel(imageX, imageY, Color.FromArgb(255, c.R, c.G, c.B));
//                                c = bitmap.GetPixel(imageX, imageY);
//                            }
//                            rSum += c.R;
//                            gSum += c.G;
//                            bSum += c.B;
//                        }
//                    }
//                    if (mirror)
//                        row.add(new Coord(-(rSum * pixScale - 0.5f), gSum * pixScale - 0.5f, bSum * pixScale - 0.5f));
//                    else
//                        row.add(new Coord(rSum * pixScale - 0.5f, gSum * pixScale - 0.5f, bSum * pixScale - 0.5f));
//
//                }
//                rows.add(row);
//            }
//            return rows;
//        }
//
//        private List<List<Coord>> bitmap2CoordsSampled(Bitmap bitmap, int scale, boolean mirror)
//        {
//            int numRows = bitmap.Height / scale;
//            int numCols = bitmap.Width / scale;
//            List<List<Coord>> rows = new ArrayList<List<Coord>>(numRows);
//
//            float pixScale = 1.0f / 256.0f;
//
//            int imageX, imageY = 0;
//
//            int rowNdx, colNdx;
//
//            for (rowNdx = 0; rowNdx <= numRows; rowNdx++)
//            {
//                List<Coord> row = new ArrayList<Coord>(numCols);
//                imageY = rowNdx * scale;
//                if (rowNdx == numRows) imageY--;
//                for (colNdx = 0; colNdx <= numCols; colNdx++)
//                {
//                    imageX = colNdx * scale;
//                    if (colNdx == numCols) imageX--;
//
//                    Color c = bitmap.GetPixel(imageX, imageY);
//                    if (c.A != 255)
//                    {
//                        bitmap.SetPixel(imageX, imageY, Color.FromArgb(255, c.R, c.G, c.B));
//                        c = bitmap.GetPixel(imageX, imageY);
//                    }
//
//                    if (mirror)
//                        row.add(new Coord(-(c.R * pixScale - 0.5f), c.G * pixScale - 0.5f, c.B * pixScale - 0.5f));
//                    else
//                        row.add(new Coord(c.R * pixScale - 0.5f, c.G * pixScale - 0.5f, c.B * pixScale - 0.5f));
//
//                }
//                rows.add(row);
//            }
//            return rows;
//        }
//
//
        void _SculptMesh(IBitmap sculptBitmap, SculptType sculptType, int lod, boolean viewerMode, boolean mirror, boolean invert) throws Exception
        {
            _SculptMesh(new SculptMap(sculptBitmap, lod).ToRows(mirror), sculptType, viewerMode, mirror, invert);
        }
//#endif

        void _SculptMesh(List<List<Coord>> rows, SculptType sculptType, boolean viewerMode, boolean mirror, boolean invert)
        {
            coords = new ArrayList<Coord>();
            faces = new ArrayList<Face>();
            normals = new ArrayList<Coord>();
            uvs = new ArrayList<UVCoord>();

            sculptType = SculptType.get(sculptType.getIndex() & 0x07);

            if (mirror)
                invert = !invert;

            viewerFaces = new ArrayList<ViewerFace>();

            int width = rows.get(0).size();

            int p1, p2, p3, p4;

            int imageX, imageY;

            if (sculptType != SculptType.plane)
            {
                if (rows.size() % 2 == 0)
                {
                    for (int rowNdx = 0; rowNdx < rows.size(); rowNdx++)
                        rows.get(rowNdx).add(new Coord(rows.get(rowNdx).get(0)));
                }
                else
                {
                    int lastIndex = rows.get(0).size() - 1;

                    for (int i = 0; i < rows.size(); i++)
                        rows.get(i).set(0, new Coord(rows.get(i).get(lastIndex)));
                }
            }

            Coord topPole = new Coord(rows.get(0).get(width / 2));
            Coord bottomPole = new Coord(rows.get(rows.size() - 1).get(width / 2));

            if (sculptType == SculptType.sphere)
            {
                if (rows.size() % 2 == 0)
                {
                    int count = rows.get(0).size();
                    List<Coord> topPoleRow = new ArrayList<Coord>(count);
                    List<Coord> bottomPoleRow = new ArrayList<Coord>(count);

                    for (int i = 0; i < count; i++)
                    {
                        topPoleRow.add(new Coord(topPole));
                        bottomPoleRow.add(new Coord(bottomPole));
                    }
                    rows.add(0, topPoleRow);
                    rows.add(bottomPoleRow);
                }
                else
                {
                    int count = rows.get(0).size();

                    List<Coord> topPoleRow = rows.get(0);
                    List<Coord> bottomPoleRow = rows.get(rows.size() - 1);

                    for (int i = 0; i < count; i++)
                    {
                        topPoleRow.set(i, new Coord(topPole));
                        bottomPoleRow.set(i, new Coord(bottomPole));
                    }
                }
            }

            if (sculptType == SculptType.torus)
                rows.add(rows.get(0));

            int coordsDown = rows.size();
            int coordsAcross = rows.get(0).size();
            int lastColumn = coordsAcross - 1;

            float widthUnit = 1.0f / (coordsAcross - 1);
            float heightUnit = 1.0f / (coordsDown - 1);

            for (imageY = 0; imageY < coordsDown; imageY++)
            {
                int rowOffset = imageY * coordsAcross;

                for (imageX = 0; imageX < coordsAcross; imageX++)
                {
                    /*
                    *   p1-----p2
                    *   | \ f2 |
                    *   |   \  |
                    *   | f1  \|
                    *   p3-----p4
                    */

                    p4 = rowOffset + imageX;
                    p3 = p4 - 1;

                    p2 = p4 - coordsAcross;
                    p1 = p3 - coordsAcross;

                    this.coords.add(rows.get(imageY).get(imageX));
                    if (viewerMode)
                    {
                        this.normals.add(new Coord());
                        this.uvs.add(new UVCoord(widthUnit * imageX, heightUnit * imageY));
                    }

                    if (imageY > 0 && imageX > 0)
                    {
                        Face f1, f2;

                        if (viewerMode)
                        {
                            if (invert)
                            {
                                f1 = new Face(p1, p4, p3, p1, p4, p3);
                                f1.uv1 = p1;
                                f1.uv2 = p4;
                                f1.uv3 = p3;

                                f2 = new Face(p1, p2, p4, p1, p2, p4);
                                f2.uv1 = p1;
                                f2.uv2 = p2;
                                f2.uv3 = p4;
                            }
                            else
                            {
                                f1 = new Face(p1, p3, p4, p1, p3, p4);
                                f1.uv1 = p1;
                                f1.uv2 = p3;
                                f1.uv3 = p4;

                                f2 = new Face(p1, p4, p2, p1, p4, p2);
                                f2.uv1 = p1;
                                f2.uv2 = p4;
                                f2.uv3 = p2;
                            }
                        }
                        else
                        {
                            if (invert)
                            {
                                f1 = new Face(p1, p4, p3);
                                f2 = new Face(p1, p2, p4);
                            }
                            else
                            {
                                f1 = new Face(p1, p3, p4);
                                f2 = new Face(p1, p4, p2);
                            }
                        }

                        this.faces.add(f1);
                        this.faces.add(f2);
                    }
                }
            }

            if (viewerMode)
                calcVertexNormals(sculptType, coordsAcross, coordsDown);
        }

        /// <summary>
        /// Duplicates a SculptMesh object. All object properties are copied by value, including lists.
        /// </summary>
        /// <returns></returns>
//        public SculptMesh Copy()
//        {
//            return new SculptMesh(this);
//        }

//        public SculptMesh(SculptMesh sm)
//        {
//        	//TODO do we need to do deep copy
//            coords = new ArrayList<Coord>(sm.coords);
//            faces = new ArrayList<Face>(sm.faces);
//            viewerFaces = new ArrayList<ViewerFace>(sm.viewerFaces);
//            normals = new ArrayList<Coord>(sm.normals);
//            uvs = new ArrayList<UVCoord>(sm.uvs);
//        }

        private void calcVertexNormals(SculptType sculptType, int xSize, int ySize)
        {  // compute vertex normals by summing all the surface normals of all the triangles sharing
            // each vertex and then normalizing
            int numFaces = this.faces.size();
            for (int i = 0; i < numFaces; i++)
            {
                Face face = this.faces.get(i);
                Coord surfaceNormal = face.SurfaceNormal(this.coords);
                this.normals.set(face.n1, Coord.add(this.normals.get(face.n1), surfaceNormal));
                this.normals.set(face.n2, Coord.add(this.normals.get(face.n2), surfaceNormal));
                this.normals.set(face.n3, Coord.add(this.normals.get(face.n3), surfaceNormal));
//
//                this.normals[face.n2] = this.normals[face.n2] + surfaceNormal;
//                this.normals[face.n3] = this.normals[face.n3] + surfaceNormal;
            }

            int numNormals = this.normals.size();
            for (int i = 0; i < numNormals; i++)
                this.normals.set(i, new Coord(this.normals.get(i)).Normalize());

            if (sculptType != SculptType.plane)
            { // blend the vertex normals at the cylinder seam
                for (int y = 0; y < ySize; y++)
                {
                    int rowOffset = y * xSize;
                    Coord c = Coord.add(this.normals.get(rowOffset) , this.normals.get(rowOffset + xSize - 1)).Normalize();
                    this.normals.set(rowOffset, c);
                    this.normals.set(rowOffset + xSize - 1, c); 
                }
            }

            for (Face face : this.faces)
            {
                ViewerFace vf = new ViewerFace(0);
                vf.v1 = new Coord(this.coords.get(face.v1));
                vf.v2 = new Coord(this.coords.get(face.v2));
                vf.v3 = new Coord(this.coords.get(face.v3));

                vf.coordIndex1 = face.v1;
                vf.coordIndex2 = face.v2;
                vf.coordIndex3 = face.v3;

                vf.n1 = new Coord(this.normals.get(face.n1));
                vf.n2 = new Coord(this.normals.get(face.n2));
                vf.n3 = new Coord(this.normals.get(face.n3));

                vf.uv1 = new UVCoord(this.uvs.get(face.uv1));
                vf.uv2 = new UVCoord(this.uvs.get(face.uv2));
                vf.uv3 = new UVCoord(this.uvs.get(face.uv3));

                this.viewerFaces.add(vf);
            }
        }

        /// <summary>
        ///.adds a value to each XYZ vertex coordinate in the mesh
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public void addPos(float x, float y, float z)
        {
            int i;
            int numVerts = this.coords.size();
            Coord vert;

            for (i = 0; i < numVerts; i++)
            {
                vert = this.coords.get(i);
                vert.X += x;
                vert.Y += y;
                vert.Z += z;
                this.coords.set(i,  vert);
            }

            if (this.viewerFaces != null)
            {
                int numViewerFaces = this.viewerFaces.size();

                for (i = 0; i < numViewerFaces; i++)
                {
                    ViewerFace v = this.viewerFaces.get(i);
                    v.addPos(x, y, z);
                    this.viewerFaces.set(i,  v);
                }
            }
        }

        /// <summary>
        /// Rotates the mesh
        /// </summary>
        /// <param name="q"></param>
        public void addRot(Quat q)
        {
            int i;
            int numVerts = this.coords.size();

            for (i = 0; i < numVerts; i++)
//                this.coords[i] *= q;
            	this.coords.set(i, Coord.multiply(this.coords.get(i), q));


            int numNormals = this.normals.size();
            for (i = 0; i < numNormals; i++)
//                this.normals[i] *= q;
            	this.normals.set(i, Coord.multiply(this.normals.get(i), q));

            
            if (this.viewerFaces != null)
            {
                int numViewerFaces = this.viewerFaces.size();

                for (i = 0; i < numViewerFaces; i++)
                {
                    ViewerFace v = this.viewerFaces.get(i);
                    
                    v.v1 = Coord.multiply(v.v1, q);
                    v.v2 = Coord.multiply(v.v2, q);
                    v.v3 = Coord.multiply(v.v3, q);

//                    v.v1 *= q;
//                    v.v2 *= q;
//                    v.v3 *= q;

//                    v.n1 *= q;
//                    v.n2 *= q;
//                    v.n3 *= q;

                    v.n1 = Coord.multiply(v.n1, q);
                    v.n2 = Coord.multiply(v.n2, q);
                    v.n3 = Coord.multiply(v.n3, q);
                    
                    this.viewerFaces.set(i, v);
                }
            }
        }

        public void Scale(float x, float y, float z)
        {
            int i;
            int numVerts = this.coords.size();

            Coord m = new Coord(x, y, z);
            for (i = 0; i < numVerts; i++)
                this.coords.set(i, Coord.multiply(this.coords.get(i), m));

            if (this.viewerFaces != null)
            {
                int numViewerFaces = this.viewerFaces.size();
                for (i = 0; i < numViewerFaces; i++)
                {
                    ViewerFace v = this.viewerFaces.get(i);
                    
                    v.v1 = Coord.multiply(v.v1, m);
                    v.v2 = Coord.multiply(v.v2, m);
                    v.v3 = Coord.multiply(v.v3, m);
//                    v.v1 *= m;
//                    v.v2 *= m;
//                    v.v3 *= m;
                    
                    this.viewerFaces.set(i, v);
                }
            }
        }

        public void DumpRaw(String path, String name, String title) throws IOException
        {
            if (path == null)
                return;
//            String fileName = name + "_" + title + ".raw";
//            String completePath = FileUtils.combineFilePath(path, fileName);
//            StreamWriter sw = new StreamWriter(completePath);
//            StringWriter sw = new StringWriter(new FileOutputStream());
            String fileName = name + "_" + title + ".raw";
            String completePath = FileUtils.combineFilePath(path, fileName);
            BufferedWriter sw = new BufferedWriter(new FileWriter(completePath));
            
            for (int i = 0; i < this.faces.size(); i++)
            {
//                String s = this.coords[this.faces[i].v1].toString();
//                s += " " + this.coords[this.faces[i].v2].toString();
//                s += " " + this.coords[this.faces[i].v3].toString();
                
                String s = this.coords.get(this.faces.get(i).v1).toString();
                s += " " + this.coords.get(this.faces.get(i).v2).toString();
                s += " " + this.coords.get(this.faces.get(i).v3).toString();

                sw.write(s);
                sw.newLine();
            }
            sw.close();
        }
    }