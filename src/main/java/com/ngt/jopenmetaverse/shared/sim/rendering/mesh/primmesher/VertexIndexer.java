/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering.mesh.primmesher;

import java.util.ArrayList;
import java.util.List;

public class VertexIndexer
    {
        public List<List<ViewerVertex>> viewerVertices;
        public List<List<ViewerPolygon>> viewerPolygons;
        public int numPrimFaces;
        private int[][] viewerVertIndices;

        public VertexIndexer()
        {
        }


        public VertexIndexer(PrimMesh primMesh)
        {
            int maxPrimFaceNumber = 0;

            for (ViewerFace vf : primMesh.viewerFaces)
                if (maxPrimFaceNumber < vf.primFaceNumber)
                    maxPrimFaceNumber = vf.primFaceNumber;

            this.numPrimFaces = maxPrimFaceNumber + 1;

            int[] numViewerVerts = new int[numPrimFaces];
            int[] numVertsPerPrimFace = new int[numPrimFaces];

            for (int i = 0; i < numPrimFaces; i++)
            {
                numViewerVerts[i] = 0;
                numVertsPerPrimFace[i] = 0;
            }

            for (ViewerFace vf : primMesh.viewerFaces)
                numVertsPerPrimFace[vf.primFaceNumber] += 3;

            this.viewerVertices = new ArrayList<List<ViewerVertex>>(numPrimFaces);
            this.viewerPolygons = new ArrayList<List<ViewerPolygon>>(numPrimFaces);
            this.viewerVertIndices = new int[numPrimFaces][];

            // create index lists
            for (int primFaceNumber = 0; primFaceNumber < numPrimFaces; primFaceNumber++)
            {
                //set all indices to -1 to indicate an invalid index
                int[] vertIndices = new int[primMesh.coords.size()];
                for (int i = 0; i < primMesh.coords.size(); i++)
                    vertIndices[i] = -1;
                viewerVertIndices[primFaceNumber] = vertIndices;
                
                viewerVertices.add(new ArrayList<ViewerVertex>(numVertsPerPrimFace[primFaceNumber]));
                viewerPolygons.add(new ArrayList<ViewerPolygon>());
            }

            // populate the index lists
            for (ViewerFace vf :primMesh.viewerFaces)
            {
                int v1, v2, v3;
                
                int[] vertIndices = viewerVertIndices[vf.primFaceNumber];
                List<ViewerVertex> viewerVerts = viewerVertices.get(vf.primFaceNumber);

                // add the vertices
                if (vertIndices[vf.coordIndex1] < 0)
                {
                    viewerVerts.add(new ViewerVertex(vf.v1, vf.n1, vf.uv1));
                    v1 = viewerVerts.size() - 1;
                    vertIndices[vf.coordIndex1] = v1;
                }
                else v1 = vertIndices[vf.coordIndex1];

                if (vertIndices[vf.coordIndex2] < 0)
                {
                    viewerVerts.add(new ViewerVertex(vf.v2, vf.n2, vf.uv2));
                    v2 = viewerVerts.size() - 1;
                    vertIndices[vf.coordIndex2] = v2;
                }
                else v2 = vertIndices[vf.coordIndex2];

                if (vertIndices[vf.coordIndex3] < 0)
                {
                    viewerVerts.add(new ViewerVertex(vf.v3, vf.n3, vf.uv3));
                    v3 = viewerVerts.size() - 1;
                    vertIndices[vf.coordIndex3] = v3;
                }
                else v3 = vertIndices[vf.coordIndex3];

                viewerPolygons.get(vf.primFaceNumber).add(new ViewerPolygon(v1, v2, v3));
            }

        }
    }