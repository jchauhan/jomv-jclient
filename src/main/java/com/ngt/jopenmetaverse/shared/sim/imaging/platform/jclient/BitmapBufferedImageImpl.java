/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.imaging.platform.jclient;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import javax.imageio.ImageIO;
import com.ngt.jopenmetaverse.shared.exception.NotSupportedException;
import com.ngt.jopenmetaverse.shared.sim.imaging.AbstractIBitmapImpl;
import com.ngt.jopenmetaverse.shared.sim.imaging.IBitmap;
import com.ngt.jopenmetaverse.shared.sim.imaging.PixelFormat;

public class BitmapBufferedImageImpl  extends AbstractIBitmapImpl
{
	BufferedImage img;
	static Map<PixelFormat, Integer> PixelFormatMap = new HashMap<PixelFormat, Integer>();
	static Map<Integer, PixelFormat> PixelFormatMapReverse = new HashMap<Integer, PixelFormat>();
	static 
	{
		PixelFormatMap.put(PixelFormat.Format32bppArgb, BufferedImage.TYPE_INT_ARGB);
		PixelFormatMap.put(PixelFormat.Format24bppRgb, BufferedImage.TYPE_3BYTE_BGR);
		PixelFormatMap.put(PixelFormat.Format32bppRgb, BufferedImage.TYPE_INT_RGB);

		PixelFormatMap.put(PixelFormat.Format16bppGrayScale, BufferedImage.TYPE_USHORT_GRAY);
		PixelFormatMap.put(PixelFormat.Format8bppGrayScale, BufferedImage.TYPE_BYTE_GRAY);
		PixelFormatMap.put(PixelFormat.Custom, BufferedImage.TYPE_CUSTOM);

		
		//Add here more mapping
		
		for(Entry<PixelFormat, Integer> e: PixelFormatMap.entrySet())
		{
			PixelFormatMapReverse.put(e.getValue(), e.getKey());
		}
	}
	
	public BitmapBufferedImageImpl(int width, int height, int imageType) {
		img = new BufferedImage(width, height, imageType);
	}

	public BitmapBufferedImageImpl(int width, int height, PixelFormat format) {
		img = new BufferedImage(width, height, PixelFormatMap.get(format));
	}
		
	public BitmapBufferedImageImpl(BufferedImage img) {
		this.img = img;
	}
	
	public BitmapBufferedImageImpl(int w, int h, int[] pixels) {
		img = new BufferedImage( w, h, BufferedImage.TYPE_INT_ARGB );
		final int[] a = ( (DataBufferInt) img.getRaster().getDataBuffer() ).getData();
		System.arraycopy(pixels, 0, a, 0, pixels.length);
		
//		int k =0;
//		for(int j =0; j< h; j++)
//		{
//			for(int i =0; i< w; i++)
//			{
//				k = j*w + i;
//				if(pixels[k] != img.getRGB(i,j))
//				{
//					System.out.println(String.format("Mistach while coping pixels <%d %d> <%d, %d>", i, j, pixels[k], img.getRGB(i,j)));
//				}
//				else
//					System.out.println(String.format("Pixels @ <%d %d> <%d, %d>", i, j, pixels[k], img.getRGB(i,j)));
//			}
//		}
		
//		
//		
//        Image piximg =  Toolkit.getDefaultToolkit().createImage(new MemoryImageSource(w, h, pixels, 0, w));
//		this.img = (BufferedImage)piximg;
	}
	
	public boolean hasPixelFormat(PixelFormat pixelFormat) throws NotSupportedException{
		if(!PixelFormatMap.containsKey(pixelFormat))
			return false;
		
		return img.getType() == PixelFormatMap.get(pixelFormat); 
	}
	
	
	 public int getRGB(int x, int y)
	{
		return img.getRGB(x, y);
	}

	 public void setRGB(int x, int y, int rgb)
	{
		img.setRGB(x, y, rgb);
	}
	 
	public String getPixelFormatAsString() {
		if(PixelFormatMapReverse.containsKey(img.getType()))
			return PixelFormatMapReverse.get(img.getType()).toString();
		else
			return String.format("Unknown Format with int value (%d)", img.getType());  
			
	}

	public PixelFormat getPixelFormat() {
		return PixelFormatMapReverse.get(img.getType());
	}

	public int getHeight() {
		return img.getHeight();
	}

	public int getWidth() {
		return img.getWidth();
	}
	
	public BufferedImage getImage()
	{
		return img;
	}

	public void setImage(BufferedImage img)
	{
		this.img = img;
	}

	public IBitmap createIBitmap(int w, int h, int[] pixels) {
		return new BitmapBufferedImageImpl(h, w, pixels);
	}

	public IBitmap cloneAndTile(int tiles)
	{
		BufferedImage resizedImage = new BufferedImage(this.getWidth()*tiles, this.getHeight()*tiles, 
			img.getType());
		Graphics2D g = resizedImage.createGraphics();
		  for (int x = 0; x < tiles; x++)
          {
              for (int y = 0; y < tiles; y++)
              {
                  g.drawImage(img, x * 256, y * 256, x * 256 + 256, y * 256 + 256, null);
              }
          }
		  g.dispose();
		  return new BitmapBufferedImageImpl(resizedImage);
	}
	
	public IBitmap cloneRotateAndFlip(double radians, boolean flipx, boolean flipy)
	{
		AffineTransform tx = AffineTransform.getScaleInstance(flipx ? -1: 1, flipy ? -1: 1);
		tx.translate(flipx? -1* this.getWidth() : 0, flipy? -1*this.getHeight() : 0);
		tx.rotate(radians, this.getWidth()/2, this.getHeight()/2);
				
		BufferedImage resizedImage = new BufferedImage(this.getWidth(), this.getHeight(), 
				img.getType());
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(img, tx, null);
		g.dispose();
		return new BitmapBufferedImageImpl(resizedImage);
	}
	
	public IBitmap cloneAndResize(int w, int h) {
		BufferedImage resizedImage = new BufferedImage(w, h, img.getType());
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(img, 0, 0, w, h, null);
		g.dispose();
		g.setComposite(AlphaComposite.Src);
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.setRenderingHint(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
		return new BitmapBufferedImageImpl(resizedImage);
	}
	
	public IBitmap createImageWithSolidColor(int width, int height, int r, int g, int b, int a)
	{
		// create red image
		BufferedImage newImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D gx = (Graphics2D) newImage.getGraphics();
		gx.setColor(new Color(r, g, b, a));
		gx.fillRect(0, 0, width, height);

		// paint original with composite
//		gx.setComposite(AlphaComposite.DstIn);
		gx.drawRect(0, 0, width, height);
//		gx.drawImage(image, 0, 0, width, height, 0, 0, width, height, null);
		return new BitmapBufferedImageImpl(newImage);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.ngt.jopenmetaverse.shared.sim.imaging.IBitmap#resize(int, int)
	 */
	public void resize(int w, int h) {		
		BitmapBufferedImageImpl bimg = (BitmapBufferedImageImpl)cloneAndResize(w, h);
		img.flush();
		img = bimg.getImage();
	}
	
	public void tile(int tiles) {
		BitmapBufferedImageImpl bimg = (BitmapBufferedImageImpl)cloneAndTile(tiles);
		img.flush();
		img = bimg.getImage();
	}

	public void rotateAndFlip(double radians, boolean flipx, boolean flipy) {
		BitmapBufferedImageImpl bimg = (BitmapBufferedImageImpl)cloneRotateAndFlip(radians, flipx, flipy);
		img.flush();
		img = bimg.getImage();
	}
	
	public void dumpToFile(String filePath) throws Exception
	{
		FileOutputStream fos = new FileOutputStream(new File(filePath));
		ImageIO.write(img, "jpeg", fos);
		fos.flush();
		fos.close();
	}
	
	public void dispose() {
		img.flush();
	}	
}
