/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import com.ngt.jopenmetaverse.shared.util.JLogger;

public class ShaderProgram
    {
        public int CurrentProgram = 0;
        public int ID = -1;
        Shader[] shaders;

        public boolean Load(String[] shaderNames, String[] attribs)
        {
            shaders = new Shader[shaderNames.length];
            for (int i = 0; i < shaderNames.length; i++)
            {
                Shader s = new Shader();
                if (!s.Load(shaderNames[i]))
                    return false;
                shaders[i] = s;
            }

            ID = GL20.glCreateProgram();
            for (int i = 0; i < shaders.length; i++)
            {
            	GL20.glAttachShader(ID, shaders[i].ID);
            }
            
            for(int i =0; i < attribs.length; i++)
            {
            	GL20.glBindAttribLocation(ID, i, attribs[i]);
            }
            
            GL20.glLinkProgram(ID);
            int res;
            res = GL20.glGetProgram(ID, GL20.GL_LINK_STATUS);
            if (res != GL11.GL_TRUE)
            {
                JLogger.debug("Linking shader program failed!");
                return false;
            }

            JLogger.debug(String.format("Linking shader program consitsting of %d shaders successful", shaders.length));
            return true;
        }

        public void Start()
        {
            if (ID != -1)
            {
                GL20.glUseProgram(ID);
                CurrentProgram = ID;
            }
        }

        public  void Stop()
        {            
            if (CurrentProgram != 0)
            {
                GL20.glUseProgram(0);
            }
        }

        public int getAttribLocation(String name)
        {
        	return GL20.glGetAttribLocation(ID, name);
        }
        
        public int getUniformLocation(String var)
        {
            return GL20.glGetUniformLocation(ID, var);
        }

        public void setUniform1(String var, int value)
        {
            int varID = getUniformLocation(var);
            if (varID == -1)
            {
                return;
            }
            GL20.glUniform1i(varID, value);
        }

        public void setUniform1(int varID, int value)
        {
            GL20.glUniform1i(varID, value);
        }
        
        
        public void setUniform1(String var, float value)
        {
            int varID = getUniformLocation(var);
            if (varID == -1)
            {
                return;
            }
            GL20.glUniform1f(varID, value);
        }

        public void dispose()
        {
            if (ID != -1)
            {
                if (CurrentProgram == ID)
                {
                    GL20.glUseProgram(0);
                }
                GL20.glDeleteProgram(ID);
                ID = -1;
            }

            if (shaders != null)
            {
                for (int i = 0; i < shaders.length; i++)
                {
                    shaders[i].Dispose();
                }
            }
            shaders = null;
        }
    }