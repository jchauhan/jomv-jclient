/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.shared.sim.rendering.mesh;

import java.util.List;

import com.ngt.jopenmetaverse.shared.protocol.primitives.TextureEntryFace;
import com.ngt.jopenmetaverse.shared.types.Vector3;
import com.ngt.jopenmetaverse.shared.util.Utils;

public class Face
{
	// Only used for Inner/Outer faces
	public int BeginS;
	public int BeginT;
	public int NumS;
	public int NumT;

	public int ID;
	public Vector3 Center;
	public Vector3 MinExtent;
	public Vector3 MaxExtent;
	public List<Vertex> Vertices;

	//ushort
	public List<Integer> Indices;
	public List<Integer> Edge;

	public FaceMask Mask;
	public TextureEntryFace TextureFace;
	public Object UserData;

	@Override
	public String toString()
	{
		return Mask.toString();
	}

	public short[] toIndicesArray()
	{
		short[] indices = new short[Indices.size()];
		int i = 0;
		for(Integer v: Indices)
		{
			indices[i] = (short)(v.intValue());
			i++;
		}
		return indices;
	}
	
	public byte[] indicesToBytes()
	{
		byte[] bytes = new byte[Indices.size()*2];
		int i =0;
		for(Integer index: Indices)
		{
			Utils.uint16ToBytes(index, bytes, i);
			i+= 2;
		}
		return bytes;
	}
	
}